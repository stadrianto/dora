<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Absensi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url','cookie'));
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function antiinjection($data)
	{
		$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter_sql;
	}	
		
	public function doLogout(){


	 $array_items = array('UserId'=>'', 'NIP' => '','Nama' => '', 'Username'=>'', 'Email'=>'' ,'Role' => ''  );
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url(). "home");

	}
	
	public function cekLogin(){
		
		$username=$this->antiinjection($this->input->post('username'));
		$captcha=trim(strtolower($this->input->post('captcha')));
		$psw=$this->antiinjection($this->input->post('password'));
		$flag = $this->input->post('flag');
		
		if($captcha == "" && $flag > 3)
		{
			$data["status"] = "gagal";
			$data["msg"] = "Captcha Code harus diisi";		
		}
		else
		{
			session_start();

			if($flag > 3)
			{
				if($captcha != $_SESSION['captcha'] )
				{
					$data["status"] = "gagal";
					$data["msg"] = "Code Captcha tidak cocok";	
				}
				else
				{
 
				$result = $this->all_model->query_data("SELECT * FROM  msuser WHERE username = '$username' and password='".md5($psw)."'", true);	
				if($result){
					$newdata = array(
								'UserId'		=> $result["IdUser"],
								'NIP'		=> $result["NIP"],
								'Nama'		=> $result["Nama"],
								'Username'		=> $result["Username"],
								'Email'		=> $result["Email"],
								'Role'		=> $result["IdRole"],
						   );
						 
					$this->session->set_userdata($newdata);
					$data["status"] = "sukses";
					$data["msg"] = $newdata;
				}
				else{
					$data["status"] = "gagal";
					$data["msg"] = "Username dan Password tidak cocok";
				}
				}
			}
			else
			{
	
			$result = $this->all_model->query_data("SELECT * FROM  msuser WHERE username = '$username' and password='".md5($psw)."'", true);	
				if($result){
					$newdata = array(
								'UserId'		=> $result["IdUser"],
								'NIP'		=> $result["NIP"],
								'Nama'		=> $result["Nama"],
								'Username'		=> $result["Username"],
								'Email'		=> $result["Email"],
								'Role'		=> $result["IdRole"],
						   );
						 
					$this->session->set_userdata($newdata);
					$data["status"] = "sukses";
					$data["msg"] = $newdata;		
				}
				else{
					$data["status"] = "gagal";
					$data["msg"] = "Username dan Password tidak cocok";
				}
			}
		}
			
				
		echo json_encode($data);
		exit();
	}

	
	public function profile(){
	
		$data['title']="Profile";
		$data['page_title']="CIPS";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('profile_view',$data);
		$this->load->view('home_footer');
	}
	
	public function insertAbsenMasuk(){
		
		$data = array(
		'NIP'  => $this->input->post('NIP'),
		'AbsenMasuk' => date("Y-m-d H:i:s"),		
		'Tanggal' 	=> date("Y-m-d"),
			);
		$query = $this->all_model->insert_data("absensi", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function insertAbsenKeluar(){
	
		$data = array(
		'AbsenKeluar' => date("Y-m-d H:i:s"),
			);
	
		$where = array(	
					'NIP'=> $this->input->post('NIP'),
					'Tanggal' => date("Y-m-d")
				
		);	
		$query = $this->all_model->update_data("absensi", $data ,$where);
		
		echo json_encode($query);
		exit();
	
	
	}
	
	public function cekNIP(){
	
		$NIP = $this->input->post('NIP');

		$result = $this->all_model->query_data("SELECT * FROM mskaryawan WHERE NIP='".$NIP."'", true);
		if(!$result)
			echo "false";
		else{
			echo "true";
		}
		exit();
	}
	
	public function cekAbsenMasuk(){
	
		$NIP = $this->input->post('NIP');

		$result = $this->all_model->query_data("SELECT * FROM absensi WHERE NIP='".$NIP ."' and Tanggal='".date("Y-m-d")."'", true);
		if(!$result)
			echo "false";
		else{
			echo "true";
		}
		exit();
	}
	
	public function cekAbsenKeluar(){
	
		$NIP = $this->input->post('NIP');

		$result = $this->all_model->query_data("SELECT * FROM absensi WHERE NIP='".$NIP ."' and Tanggal='".date("Y-m-d")."' and AbsenKeluar  like '0000-00-00 00:00:00'", true);
		if(!$result)
			echo "false";
		else{
			echo "true";
		}
		exit();
	}
	
	public function index(){
		$data['page_title']="CIPS";
		$data['title']="Absensi CIPS";
		$data['include']=$this->load->view('script','',true);
		$name = $this->session->userdata('Name');
		$this->load->view('home_header',$data);
		$this->load->view('absensi_view',$data);
		$this->load->view('home_footer');
		 
		return $data;					
	}
	private function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}	
}


