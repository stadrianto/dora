<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($request = true){
		$search = array();
		$where = array();
		$join = array(
			//array('table'=>'msjabatan b','field' => 'a.idjabatan = b.idjabatan','method'=>'Left')
			);
		$result2 = $this->all_model->get_data("CustomerId, CustomerName, PhoneNumber, EmailAddress, Gender", "mscustomer",$join, $where, $search, false,true);
		return $result2;	
		exit();
	}

	public function ajax_finder(){
			$msg = array();
			if ( $_POST ){
				$key = $this->input->post("key");
				if ( empty($key) ){
					$msg['type'] = 'failed';
					$msg['msg'] = "Key Empty.";
				}
				else{
					$data = $this->all_model->view("mscustomer", "CustomerId, CustomerName, PhoneNumber, EmailAddress, Gender", array("CustomerId"=>$key));
					if ( $data == '0' ){
						$msg['type'] = "failed";
						$msg['msg'] = "Data tidak ditemukan.";
					}
					else{
						//foreach($data as $a) xss_filter($a);
						$msg['type'] = "done";
						$msg['msg'] = $data;
					}
				}
			}
			else{
				$msg['type'] = 'failed';
				$msg['msg'] = "Parameter tidak ditemukan.";
			}
			//die (var_dump($msg));
			echo json_encode($msg);
		}

	public function getNewId(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdJenisKendaraan,3) as IdJenisKendaraan FROM msjeniskendaraan ORDER BY IdJenisKendaraan DESC LIMIT 1", true);
		$result["IdJenisKendaraan"] = $result["IdJenisKendaraan"]+1;
		if($result["IdJenisKendaraan"] < 10)
			$result["IdJenisKendaraan"] = "00".$result["IdJenisKendaraan"];
		else if($result["IdJenisKendaraan"]< 100)
			$result["IdJenisKendaraan"] = "0".$result["IdJenisKendaraan"];
		else
			$result["IdJenisKendaraan"] = $result["IdJenisKendaraan"];
		return $result["IdJenisKendaraan"];
	}

	public function insert(){
		
		$data = array( //$this->input->post('Id')
		'NamaClient' => $this->input->post('Nama'),	
		'JenisMuatan' => $this->input->post('JenisMuatan')
			);
		$query = $this->all_model->insert_data("msclient", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function edit(){
		$data = array(
			'NamaClient' => $this->input->post('Nama'),	
			'JenisMuatan' => $this->input->post('JenisMuatan')
			);
		$where = array(	
					'IdClient' => $this->input->post('Id'),
				
		);
		$query = $this->all_model->update_data("msclient", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function delete(){
		$Id = $this->input->post('Id');
		$where = array('IdClient'=>$Id);
		$query = $this->all_model->delete_data("msclient", $where);
		echo json_encode($query);
		exit();
	}

	
	public function getAccess()
	{	
		$page=$this->all_model->query_data("select pageid from mspage where pagename = '".$this->router->fetch_class()."'",true);
		//die($page["pageid"]);
		$result = $this->all_model->query_data("select flag from trprivillege where idrole=".$this->session->userdata('Role')." and PageName=".$page["pageid"]." and flag!=0",true);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "login","refresh");
		if($this->session->userdata('Role')!="1")
		{
			$access=$this->getAccess();
			if(!is_array($access))
			{
				redirect(base_url(). "home","refresh");
			}
		}
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Master Customer";
		$data['page']="customer_view";
		$data['nama']=$this->session->userdata('Name');
		$data['data'] = $this->getData(false);
		//$data2['data4'] = json_encode($this->getSatuan());
		//$data2['data3'] = json_encode($this->getKategoriBesar());
		//$data['include']=$this->load->view('script','',true);
		//die (var_dump($data));
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}