<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class kelompokakun extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function getKelompokNeraca()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdNeraca,NamaKelompok", "kelompokneraca a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
		
	public function getData($perPage=5, $segmen=0,  $request = true){
			$current = $this->input->post('page');
		
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
			);
		$join = array(
			array('table'=>'kelompokneraca b','field' => 'a.idneraca = b.idneraca','method'=>'Left'),
		);
		$where = array();	
		$result = $this->all_model->get_data("IdKelompokAkun as IdKelompokAkun, NamaKelompokAkun as NamaKelompokAkun, NamaKelompok as KelompokNeraca","kelompokakun a",$join, $where, $search, false, $perPage, $segmen, false,"IdKelompokAkun","ASC");
		$result2 = $this->all_model->get_data("IdKelompokAkun as IdKelompokAkun, NamaKelompokAkun as NamaKelompokAkun, NamaKelompok as KelompokNeraca","kelompokakun a", $join, $where, $search, false);
	
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				
				foreach($result as $key => $value){	
				}
				
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertKelompokAkun(){
		$data = array(
		'IdKelompokAkun'  => $this->input->post('IdKelompokAkun'),
		'NamaKelompokAkun' => $this->input->post('NamaKelompokAkun'),
		'IdNeraca' => $this->input->post('KelompokNeraca'),
		'ActiveYN' => 'Y',
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("kelompokakun", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function editKelompokAkun(){
		$data = array(
		'NamaKelompokAkun' => $this->input->post('NamaKelompokAkun'),
		'IdNeraca' => $this->input->post('KelompokNeraca'),
		'ActiveYN' => 'Y',
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")			
			);
		$where = array(	
					'IdKelompokAkun'=> $this->input->post('IdKelompokAkun'),
				
		);	
		$query = $this->all_model->update_data("kelompokakun", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function deleteKelompokAkun()
	{
		$IdKelompokAKun = $this->input->post('IdKelompokAkun');
		$where = array('IdKelompokAkun'=>$IdKelompokAKun);
		$query = $this->all_model->delete_data("kelompokakun", $where);
		echo json_encode($query);
		exit();
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Kelompok Akun";
		$data['data3'] = json_encode($this->getKelompokNeraca());
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('kelompokakun_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function addKelompokAkun(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['data3'] = json_encode($this->getKelompokNeraca());
		$data['page_title']="CIPS - Tambah Kelompok Akun";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('addkelompokakun_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

