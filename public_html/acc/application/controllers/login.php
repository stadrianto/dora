<?php defined('BASEPATH') OR exit('No direct script access allowed');
 	class Login extends CI_Controller {
		
		public function __construct(){
			parent::__construct();
			$this->load->helper(array('url','cookie'));
			$this->load->library(array('session'));
			$this->load->model('all_model');
			date_default_timezone_set('Asia/Jakarta');
		}
		function index()
		{
			$this->load->view('login_view');					
		}
		public function doLogout(){

		 $array_items = array('UserId'=>'', 'NIP' => '','Nama' => '', 'Username'=>'', 'Email'=>'' ,'Role' => ''  );
			$this->session->unset_userdata($array_items);
			$this->session->sess_destroy();
			redirect(base_url(). "login");

		}
		/*public function antiinjection($data)
		{
			$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
			return $filter_sql;
		}*/
		
		function cekLogin(){
		
		$username=$this->input->post('username');
		$psw=$this->input->post('password');
		
		
		$result = $this->all_model->query_data("SELECT * FROM  msuser WHERE username = '$username' and password='".md5($psw)."'", true);	
		$var = $this->all_model->query_data("SELECT NoRekening FROM  variabel", true);	
		if($result){
			$newdata = array(
						'UserId'	=> $result["IdUser"],
						'NIP'		=> $result["NIP"],
						'Nama'		=> $result["Nama"],
						'Username'	=> $result["Username"],
						'Email'		=> $result["Email"],
						'Role'		=> $result["IdRole"],
						'NoRekPerusahaan' => $var["NoRekening"],
				   );
						 
					$this->session->set_userdata($newdata);
					$data['type'] = 'done';
					$data['msg'] = '<br />Successfully login';		
				}
				else{
					  $data['type'] = 'failed';
					  $data['msg'] = '<br />Invalid username or password';
				}
			
				
		echo json_encode($data);
		exit();
		}
	}
