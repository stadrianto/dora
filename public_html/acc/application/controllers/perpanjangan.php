<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Perpanjangan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getInvoiceCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(NoInvoice,4) as NoInvoice FROM pembayaran ORDER BY NoInvoice DESC", true);
		$result["NoInvoice"] = $result["NoInvoice"]+1;
		if($result["NoInvoice"] < 10)
			$result["NoInvoice"] = "BY0000".$result["NoInvoice"];
		else if($result["NoInvoice"]< 100)
			$result["NoInvoice"] = "BY000".$result["NoInvoice"];
		else if($result["NoInvoice"]< 1000)
			$result["NoInvoice"] = "BY00".$result["NoInvoice"];
		else if($result["NoInvoice"]< 10000)
			$result["NoInvoice"] = "BY0".$result["NoInvoice"];	
		else
			$result["NoInvoice"] = "BY".$result["NoInvoice"];
		return $result["NoInvoice"];
	}
	
	public function getData($perPage=10, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.NoOrder' => $this->input->post('search')
			);
			$where = array(
			
			);
		}else{
			$search = array(
					'a.NoOrder' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
			array('table'=>'trorder b','field' => 'a.NoOrder = b.NoOrder','method'=>'Left'),
			array('table'=>'mscustomer c','field' => 'b.IdCustomer = c.IdCustomer','method'=>'Left'),
		);
		
		
		$result = $this->all_model->get_data("NoPerpanjangan,a.NoOrder as NoOrder,b.Project as NamaProject,b.IdCustomer as IdCustomer,c.Nama as Nama,b.Nominal,BiayaPerpanjang,MasaPerpanjang", "projectextend a",$join, $where, $search, false, $perPage, $segmen, false,"NoPerpanjangan","DESC");
		
		$result2 = $this->all_model->get_data("NoPerpanjangan,a.NoOrder as NoOrder,b.Project as NamaProject,b.IdCustomer as IdCustomer,c.Nama as Nama,b.Nominal,BiayaPerpanjang,MasaPerpanjang", "projectextend a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['BiayaPerpanjang1'] = $this->all_model->rp($result[$key]['BiayaPerpanjang']);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['BiayaPerpanjang1'] = $this->all_model->rp($result[$key]['BiayaPerpanjang']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertPerpanjangan(){
		
		$data = array(
		'NoOrder' => $this->input->post('NoOrder'),	
		'MasaPerpanjang' => $this->input->post('MasaPerpanjang'),	
		'BiayaPerpanjang' => $this->input->post('BiayaPerpanjang'),	
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")		
			);
		$query = $this->all_model->insert_data("projectextend", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	
	public function updatePerpanjangan(){
	
		$data = array(
		'MasaPerpanjang' => $this->input->post('MasaPerpanjang'),	
		'BiayaPerpanjang' => $this->input->post('BiayaPerpanjang'),	
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")		
			);
		$where = array(	
				'NoPerpanjangan' => $this->input->post('NoPerpanjangan'),	
				
		);	
		$query = $this->all_model->update_data("projectextend", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	

	
	
	public function deletePerpanjangan()
	{
		$NoInvoice = $this->input->post('NoPerpanjangan');
		$where = array('NoPerpanjangan'=>$NoInvoice);
		$query = $this->all_model->delete_data("projectextend", $where);
		echo json_encode($query);
		exit();
	}
	
	
	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Perpanjangan Project";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('projectextend_view',$data2);
		$this->load->view('home_footer');
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


