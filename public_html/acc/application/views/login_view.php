<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login Page</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?php echo base_url(); ?>media/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>media/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>media/css/form-elements.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>media/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        

    </head>

    <body style="background-color:black">

        <!-- Top content -->
        <div class="top-content">        	
            <div class="inner-bg">
                <div class="container">                    
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login</h3>
                            		<p>Enter your username and password to log on:</p>
                        		</div>                        		
                            </div>
                            <div class="form-bottom">
							<p class="login-box-msg" style="margin-bottom:10px;">Sign in to start your session</p>
			                    <form id="zlog" role="form" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="username" placeholder="Username..." class="form-username form-control" id="username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control" id="password">
			                        </div>
			                        <button onclick="" type="submit" class="btn">Sign In !!</button>
			                    </form>
		                    </div>
                        </div>
                    </div>                    
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
		<script src="<?php echo base_url();?>media/js/jquery.js"></script>		
        <script src="<?php echo base_url(); ?>media/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>media/js/jquery.backstretch.min.js"></script>
		<script src="<?php echo base_url(); ?>media/js/jQuery-2.1.4.min.js"></script>
		<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
        <!--script src="<?php echo base_url(); ?>media/js/scripts.js"> </script-->
<script>
	$(function(){		
		$("#zlog").ajaxForm({
			url : "<?php echo base_url()?>login/cekLogin",
			type : "post",
			dataType : "json",
			beforeSubmit : function(){
				$(".login-box-msg").html ( "<br />Checkin username and password..." ).addClass('bg-danger').removeClass('bg-red');
				$(".btn").prop('disabled', true);
			},
			success : function(data){
				if ( data.type == "failed" ){					
					$(".login-box-msg").html( data.msg ).addClass('bg-red');											
					setTimeout(function(){
						$(".login-box-msg").html ( "<br />Sign in to start your session").removeClass('bg-red bg-danger');
						$(".btn").prop('disabled', false);
					}, 2000);
				}
				else if ( data.type == "done" ){
					$(".login-box-msg").html( data.msg ).removeClass('bg-danger bg-red').addClass('bg-success');
					setTimeout(function(){ window.location.replace("<?php echo base_url()?>"); }, 2000);
					//alert(data.msg);
				}
			}
		}); 
		
		/* function cek()
		{			
			//alert($("#username").val());
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>login/cekLogin",
				data: "username="+$("#username").val()+"&password="+$("#password").val(),
				success: function(msg){
					var hasil = JSON.parse(msg);
					if(hasil.type=="success"){
						//$("div#panel").slideUp("slow");
						alert(hasil.msg)
						$(".login-box-msg").html("Selamat Datang , "+hasil.msg);
						$(".login-box-msg").dialog( "open" );
						
						setTimeout(function(){
						location.reload();
						}, 1500);
					}
					else{
						alert(hasil.msg)						
						$(".login-box-msg").html(hasil.msg);
						$(".login-box-msg").dialog( "open" );
						
					}
				}
			}); 
		}*/
		
	});
</script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>