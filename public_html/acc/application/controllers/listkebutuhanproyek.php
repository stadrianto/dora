<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ListKebutuhanProyek extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function getItemList($perPage=5, $segmen=0, $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.ActiveYN'	=>"Y",
		
		);
			
		
		$result = $this->all_model->get_data("IdKebutuhanProyek as IdKebutuhanProyek,Nama as NamaItem", "mskebutuhanproyek a",$join, $where, $search, false, $perPage, $segmen, false,"IdKebutuhanProyek","ASC");
		$result2 = $this->all_model->get_data("IdKebutuhanProyek as IdKebutuhanProyek,Nama as NamaItem","mskebutuhanproyek a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getItemList($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
			
					$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
			
				}		
		
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getItemList($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){

					$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);

				}
			}
			echo json_encode($result);
			exit();
		}
	}
	

	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 7; 
		$config['segmen'] = 0;
		$data['title']="List Kebutuhan Proyek";
		$data['page_title']="List Kebutuhan Proyek";
		$data2['data'] = json_encode($this->getItemList($config['per_page'], $config['segmen'],false ));
		$data2['include']=$this->load->view('script','',true);
	
		$this->load->view('listkebutuhanproyek_view',$data2);
		
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


