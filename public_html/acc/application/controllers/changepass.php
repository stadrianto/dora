<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Changepass extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
		
	}
	
	
	public function antiinjection($data)
	{
		$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter_sql;
	}
	
	public function cekPassword(){
		$pass = md5($this->antiinjection($this->input->post('Password')));
		$userid = $this->session->userdata('UserId');
		$result = $this->all_model->query_data("SELECT Password FROM msuser WHERE IdUser = '".$userid ."'", true);
		if($result)
		{
			if($result['Password'] == $pass)
			echo "false";
			else echo "true";
		}
		else{
			echo "true";
		}
		exit();
	}
	
	public function changePassword(){
			$data = array(
				'Password'	=> md5($this->antiinjection($this->input->post('pass')))
			);
		$where = array("IdUser" => $this->session->userdata('UserId'));
		$query = $this->all_model->update_data("msuser", $data, $where );
		echo "true";
		exit();
	}
	public function index(){
		$data['title']="CIPS";
		$data['page_title']="CIPS";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('changepass_view');
		$this->load->view('home_footer');
	}
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


