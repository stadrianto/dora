		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Sales<small></small></h1>                    
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Menu List </h3>
                        </div>
                        <div id="menudiv" class="panel-body">
                        	<div class="col-lg-4" align="left">
								<input placeholder="Customer Id" id="txtcust" name="txtcust" class="form-control">	
								<!--button onclick="sendPrint()" class="btn btn-primary" type="button">Print</button-->
							</div>	
                          	<div class="col-lg-8"  align="right">
								<button onclick="saveOrder()" class="btn btn-primary" type="button">Save Order</button>
								<!--button onclick="sendPrint()" class="btn btn-primary" type="button">Print</button-->
							</div>	
                        </div>
                    </div>
                </div>
                
            </div>         		 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> New Menu </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div style="display:none" class="form-group">
								<label>MenuId</label>
								<input type="hidden" name="hfid" id="hfid" value="">
							</div>
							<div class="form-group">
								<label>Menu Name</label>
								<input id="txtname" name="txtname" class="form-control">			
							</div>
							<div class="form-group">
								<label>Price</label>
								<input id="txtprice" name="txtprice" class="form-control">			
							</div>						
							<div class="form-group">
								<label>Category</label>
								<div id="kategoriarea"></div>
							</div>
							<div>
								<table id="itemArea">
																	
								</table>
								<button id="btnRow" type="button" class="btn btn-default">Add Item</button>
							</div>
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>	
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes03" id="tes03" class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Menu </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-12" style="float:none;">
							<h4>Are you sure want to delete this data?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Menu Id</th>
										<th>Menu Name</th>
										<th>Menu Price</th>
										<th>Menu Category</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div id="deleteid"></div></td>
										<td><div id="deletename"></div></td>
										<td><div id="deleteprice"></div></td>
										<td><div id="deletecategory"></div></td>
									</tr>							
								</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="button" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
		var flag=0
		var orderArray=[];
		$(document).ready(function () {
			var menulist =  JSON.parse('<?php echo $menu; ?>');
			var text='';
			//<div data-toggle="collapse" data-target="#demo">Collapsible</div>
			//<div id="demo" class="collapse">
			//Lorem ipsum dolor text....
			//</div> 
			for(i=0;i<menulist.length;i++)
			{
				if(i==0)
				{
					text='<div data-toggle="collapse" data-target="#'+menulist[i].CategoryName+'"><h3 style="border-bottom:solid white 1px">'+menulist[i].CategoryName+'</h3></div>';
					text+='<div id="'+menulist[i].CategoryName+'" class="collapse">';
					text+='<span>'+menulist[i].MenuName+'&nbsp;<input style="color:black" readonly value="'+menulist[i].Price+'" type="text" id="pricemenu'+i+'"/><input value="'+menulist[i].MenuId+'" type="hidden" id="idmenu'+i+'"/><input onChange="addRemoveItem('+i+')" id="qty'+i+'" style="color:black" type="number"></span>';
				}
				else
				{
					if(menulist[i].CategoryName==menulist[i-1].CategoryName)
					{
						text+='<span>'+menulist[i].MenuName+'&nbsp;<input type="number"></span>';
					}
					else
					{
						text+='</div>';
						text+='<div data-toggle="collapse" data-target="#'+menulist[i].CategoryName+'"><h3 style="border-bottom:solid white 1px">'+menulist[i].CategoryName+'</h3></div>';
						text+='<div id="'+menulist[i].CategoryName+'" class="collapse">';
						text+='<span>'+menulist[i].MenuName+'&nbsp;<input style="color:black" readonly value="'+menulist[i].Price+'" type="text" id="pricemenu'+i+'"/><input value="'+menulist[i].MenuId+'" type="hidden" id="idmenu'+i+'"/><input id="qty'+i+'" onChange="addRemoveItem('+i+')" style="color:black" type="number"></span>';
					}
				}
			}
			$('#menudiv').append(text);
			//alert(text);
		});
		function sendPrint()
		{
			$.ajax({				
					url : "<?php echo base_url()?>Sales/cetak",
					type : "post",
					data : {action : "print"},	
					dataType : "json",			
					success : function(msg){
						console.log(msg);
						var hasil = JSON.parse(msg);
						if ( hasil == true || hasil == "true"){		
							location.reload();
						}
					},
					error: function(ts) { alert(ts.responseText) }		
					});		
		}
		function saveOrder()
		{
			var r= confirm("Are you sure?");
			if(r==true)
			{
				if(orderArray.length>0)
				{
					var tanggal= new Date();
					var dd = tanggal.getDate();
					var mm = tanggal.getMonth()+1; //January is 0!
					var yyyy = tanggal.getFullYear();

					if(dd<10) {
					    dd = '0'+dd;
					} 

					if(mm<10) {
					    mm = '0'+mm;
					} 		

					var id="TR"+(tanggal.getFullYear()+"")+""+(('0'+(tanggal.getMonth()+1)).slice(-2))+""+dd;

					var params = { arry : orderArray };
					var jsonString = JSON.stringify(params);

					console.log(jsonString);
					//alert(id);
					var custid= $('#txtcust').val();
					if(custid=="")
					{
						custid="-";
					}
					
					$.ajax({				
					url : "<?php echo base_url()?>Sales/insert",
					type : "post",
					data : {SalesId : id, CustomerId: custid, detail:jsonString},	
					dataType : "json",			
					success : function(msg){
						console.log(msg);
						if(Array.isArray(msg))
						{
							alert("failed : " + msg);
						}
						else
						{
							if(msg.type=="failed")
							{
								alert(msg.error);
							}
							else
							{
								var hasil = JSON.parse(msg);
								if ( hasil == true || hasil == "true"){		
									location.reload();
								}
								else {								
									
									alert("failed : " + hasil);
									
								}
							}
						}
					},
					error: function(ts) { alert(ts.responseText) }		
					});		

				}
				else
				{
					alert("No Order");
				}
			}
		}
		function addRemoveItem(i)
		{
			var flagfind=0;
			if($('#qty'+i).val()>=0)
			{
				if($('#qty'+i).val()!=0)
				{
					if(orderArray.length>0)
					{
						flagfind=0;
						for(var o = 0; o < orderArray.length; o++) {
						   if(orderArray[o].id === $('#idmenu'+i).val()) {
						      orderArray[o].qty=$('#qty'+i).val();
						      flagfind=1;
						   }
						}
						if (flagfind==0)
						{
							orderArray.push({"id":$('#idmenu'+i).val(),"qty":$('#qty'+i).val(),"price":$('#pricemenu'+i).val()});
						}
					}
					else
					{
						orderArray.push({"id":$('#idmenu'+i).val(),"qty":$('#qty'+i).val(),"price":$('#pricemenu'+i).val()});
					}
				}
				else
				{
					for(var o = 0; o < orderArray.length; o++) {
						   if(orderArray[o].id === $('#idmenu'+i).val()) {
						       orderArray.splice(o,1);
						   }
						}
				}
			}
			//console.log(orderArray.length);
		}
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
             
            
        });
		
		
    </script>
</body>
</html>