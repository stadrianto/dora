<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Calendar extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function getDataOrder1($noorder)
	{
		$result = $this->all_model->query_data("SELECT TanggalSelesaiReal,HargaNego,HargaPPN,TanggalSelesai,NoFakturPajak,Status FROM trorder a where  NoOrder='".$noorder."' ", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 7; 
		$config['segmen'] = 0;
		$dataorder = $this->getDataOrder1($this->session->userdata('NoOrder'));
		$data['title']="List Barang";
		$data['page_title']="List Barang";
		$data2['data3'] = json_encode($dataorder);	
		$data2['include']=$this->load->view('script','',true);
		$this->load->view('calendar_view',$data2);
		
	}
	

}


