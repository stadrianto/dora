		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Master Supplier <small>Data Supplier</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New Supplier</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Supplier List </h3>
                        </div>
                        <div class="panel-body">
                            <div id="grid"></div>				
                        </div>
                    </div>
                </div>
            </div>			 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Supplier </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div class="form-group">
								<label>Supplier Id</label>
								<input readonly="readonly" id="suppid" name="suppid" class="form-control">					
							</div>
							<div class="form-group">
								<label>Supplier Name</label>
								<input id="suppname" name="suppname" class="form-control">					
							</div>
							<div class="form-group">
								<label>Supplier Address</label>
								<textarea id="suppaddress" name="suppaddress" class="form-control" rows="3"></textarea>					
							</div>
							<div class="form-group">
								<label>Supplier Phone</label>
								<input id="suppphone" name="suppphone" class="form-control">					
							</div>
							<div class="form-group">
								<label>Keterangan</label>
								<input id="suppdefinition" name="suppdefinition" class="form-control">					
							</div>
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>
							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Supplier</h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-6" style="float:none;">
							<h4>Yakin Hapus Data Ini?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Id Supplier</th>
									<th>Nama Supplier</th>
									<th>Alamat Supplier</th>
									<th>Telepon Supplier</th>
									<th>Keterangan</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="deleteid"></div></td>
									<td><div id="deletenama"></div></td>
									<td><div id="deletealamat"></div></td>
									<td><div id="deletephone"></div></td>
									<td><div id="deletedefinition"></div></td>
								</tr>							
							</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="submit" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	$(document).ready(function () {
		/* $("#test1").on("click", "#btnSave", function(){
			
			
		}); */	
	});
	
	function addNew(){
		var defin="";
		if($('#suppname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Supplier Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#suppaddress').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Alamat Supplier harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#suppphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Telepon Supplier harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		if($('#suppdefinition').val()== "" )
		{
			defin="-";		
		}
		else
		{
			defin=$('#suppdefinition').val();
		}
			
		var data= "Nama="+$('#suppname').val()+"&Alamat="+$('#suppaddress').val()+"&Telepon="+$('#suppphone').val()+"&Keterangan="+defin;

			$.ajax({				
			url : "<?php echo base_url()?>supplier/insertSupplier",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
			});
	}
	function updateData(){
		var defin="";
		if($('#suppname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Supplier Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#suppaddress').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Alamat Supplier harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#suppphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Telepon Supplier harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		if($('#suppdefinition').val()== "" )
		{
			defin="-";		
		}
		else
		{
			defin=$('#suppdefinition').val();
		}
		var data= "IdSupp="+$('#suppid').val()+"&Nama="+$('#suppname').val()+"&Alamat="+$('#suppaddress').val()+"&Telepon="+$('#suppphone').val()+"&Keterangan="+defin;
			$.ajax({				
			url : "<?php echo base_url()?>supplier/updateDataSupplier",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	function deleteData(id){		
			var data= "IdSupp="+id;
			$.ajax({				
			url : "<?php echo base_url()?>supplier/deleteSupplier",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#suppname').val('');
		$('#suppaddress').val('');
		$('#suppphone').val('');
		$('#suppdefinition').val('');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");			
	}
	
	function setUpdate(id,nama,alamat,telp,keterangan){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#dialog').modal('show');
		$('#suppid').val(id);
		$('#suppname').val(nama);
		$('#suppaddress').val(alamat);
		$('#suppphone').val(telp);
		$('#suppdefinition').val(keterangan);
		$('#btnSave').attr("onclick","updateData()");			
	}	
	function setDelete(id,nama,alamat,telp,keterangan){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(id);
		$('#deletenama').html(nama);
		$('#deletealamat').html(alamat);
		$('#deletephone').html(telp);
		$('#deletedefinition').html(keterangan);
		$('#deleteBtn').attr("onclick","deleteData('"+id+"')");			
	}
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>,
                traffic = [
                {
                    Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
                },
                {
                    Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
                },
                {
                    Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
                },
                {
                    Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
                },
                {
                    Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
                }];


            /* $("#shieldui-chart1").shieldChart({
                theme: "dark",

                primaryHeader: {
                    text: "Visitors"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
            });

            $("#shieldui-chart2").shieldChart({
                theme: "dark",
                primaryHeader: {
                    text: "Traffic Per week"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "pie",
                    collectionAlias: "traffic",
                    data: visits
                }]
            }); */
			/* $("#shieldui-grid1").shieldGrid({
                dataSource: {
                    data: traffic
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: true,
                columns: [
                { field: "Source", width: "170px", title: "Source" },
                { field: "Amount", title: "Amount" },                
                { field: "Percent", title: "Percent", format: "{0} %" },
                { field: "Target", title: "Target" },
                ]
            }); */
            $("#grid").shieldGrid({
                dataSource: {
                    data: hasil
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: true,
				selection: 
				{
					type: "cell",
					multiple: false,
					toggle: false
				},
				events: {
                    selectionChanged: onSelectedChanged,
					doubleClick: onDouble
				},				
                columns: [
                { field: "IdSupplier", width: "170px", title: "IdSupplier" },
                { field: "Nama", title: "Nama" },                
                { field: "Alamat", title: "Alamat"},
                { field: "Telepon", title: "Telepon" },
				{ field: "Keterangan", title: "Keterangan" },
				{
					width: "12%",
					title: " ",
					buttons: [						
						{
							cls: "button button-primary",
							caption: " Edit",
							commandName: "details", // build in - edit, delete
							click: function(rowIndex) {
								
								var id=$("#grid").swidget().dataItem(rowIndex).IdSupplier;
								var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
								var alamat=$("#grid").swidget().dataItem(rowIndex).Alamat;
								var telepon=$("#grid").swidget().dataItem(rowIndex).Telepon;
								var keterangan=$("#grid").swidget().dataItem(rowIndex).Keterangan;
								//alert(id);
								setUpdate(id,nama,alamat,telepon,keterangan);
							}
						},
						{
							cls: "mybuttonCssClass",
							caption: " Delete",
							commandName: "details", // build in - edit, delete
							click: function(rowIndex) {
								//alert(rowIndex);
								var id=$("#grid").swidget().dataItem(rowIndex).IdSupplier;
								var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
								var alamat=$("#grid").swidget().dataItem(rowIndex).Alamat;
								var telepon=$("#grid").swidget().dataItem(rowIndex).Telepon;
								var keterangan=$("#grid").swidget().dataItem(rowIndex).Keterangan;
								setDelete(id,nama,alamat,telepon,keterangan);
							}
						}
					]
				}
                ]
            });            
        });
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
