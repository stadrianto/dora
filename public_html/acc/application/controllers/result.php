<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Result extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
		
	}
	
	public function checkBankAnswer($IdSoal,$NoUrut,$Jawaban)
	{
		$result = $this->all_model->query_data("SELECT mdsoal_urutjawaban FROM to_md_banksoal WHERE MDSoal_ID = '".$IdSoal ."' and MDSoal_NoUrut='".$NoUrut."'", true);
		if($result)
		{
			if($Jawaban == $result['mdsoal_urutjawaban']) return true;
			else return false;
		}
		else return false;
	}
	
	public function checkAnswer($IdAnswer,$IdSoal,$nilaipersoal)
	{
		$nilai = 0;
		
		$search = array();
		$join = array();
		$where = array('mhanswer_id' => $IdAnswer);		
		$result = $this->all_model->get_data("MDSoal_NoUrut as NoUrut,MDAnswer_NoUrutJawaban as Jawaban", "to_md_answer a",$join, $where, $search);
		
		if(!$result){
			$nilai= "0";
		}	
		else
		{
			foreach($result as $key => $value){
			
				if($this->checkBankAnswer($IdSoal,$result[$key]['NoUrut'],$result[$key]['Jawaban']))
				{
					$nilai+=$nilaipersoal;
				}
			}
		}
		return $nilai;
	
	}
	
	public function getResult2($perPage=5, $segmen=0, $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.mhanswer_activeyn'	=>"Y",
				'a.mhanswer_userid'	=> $this->session->userdata('UserId'),
		);
			
			
		$result = $this->all_model->get_data("mhanswer_id as IdAnswer,mhsoal_id as IdSoal", "to_mh_answer a",$join, $where, $search, false, $perPage, $segmen, false,"mhanswer_id","ASC");
		$result2 = $this->all_model->get_data("mhanswer_id as IdAnswer,mhsoal_id as IdSoal", "to_mh_answer a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getResult2($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					$result[$key]['Result'] = $this->checkAnswer($result[$key]['IdAnswer'],$result[$key]['IdSoal']);
				}
		
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getResult2($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					$result[$key]['Result'] = $this->checkAnswer($result[$key]['IdAnswer'],$result[$key]['IdSoal']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function getKeyActivities($IdKeyAct)
	{
	$result = $this->all_model->query_data("select mkeyh_nama,mkeyh_nilaipersoal,mkeyh_preview from to_mh_keyactivities where mkeyh_id='".$IdKeyAct."'", true);
		if($result)
		{
			return $result;
		}
		return "";
	}	
	
	public function requestReschedule(){
		$idschedule = $this->input->post('idschedule');
	
		$data = array(
			'Reschedule' 	 => 'W',
		);
		$where = array(	
			'ScheduleID'			=> $idschedule,					
		);
		
		$query = $this->all_model->update_data("schedule", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	
	public function cancelReschedule(){
		$idschedule = $this->input->post('idschedule');
	
		$data = array(
			'Reschedule' 	 => 'N',
		);
		$where = array(	
			'ScheduleID'			=> $idschedule,					
		);
		
		$query = $this->all_model->update_data("schedule", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	
	public function reschedule(){
		$idschedule = $this->input->post('idschedule');
		
		$data = array(
			'ActiveYN' 	 => 'N',
		);
		$where = array(	
			'ScheduleID'			=> $idschedule,					
		);
		
		$query = $this->all_model->update_data("schedule", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	
	public function getResult($perPage=5, $segmen=0, $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
		
			);
		$join = array(

		);
		
		
		$where = array(
				'a.activeyn'=>"Y",
				'a.userid'	=> $this->session->userdata('UserId'),
				'a.Result > '=>"-1",
				//'a.mhanswer_id > '	=> "0",
				//'Waktu < '	=> date("Y-m-d H:i:s", strtotime('-1440 minutes', time())),
		);
			
			
		$result = $this->all_model->get_data("scheduleid as IdSchedule,mhanswer_id as IdAnswer,mhsoal_id as IdSoal,Waktu as WaktuUjian,mkeyh_id as IdKeyAct,result as Result,reschedule as Reschedule,StatusLulus as StatusLulus", "schedule a",$join, $where, $search, false, $perPage, $segmen, false,"scheduleid","DESC");
		$result2 = $this->all_model->get_data("scheduleid as IdSchedule,mhanswer_id as IdAnswer,mhsoal_id as IdSoal,Waktu as WaktuUjian,mkeyh_id as IdKeyAct,result as Result,reschedule as Reschedule,StatusLulus as StatusLulus", "schedule a",$join, $where, $search, false, $perPage, $segmen, false,"scheduleid","DESC");
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getResult($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					
					$activity = $this->getKeyActivities($result[$key]['IdKeyAct']);
					if($result[$key]['StatusLulus'] == 'N') $result[$key]['StatusLulus'] = "Tidak";
					else $result[$key]['StatusLulus'] = "Ya";		
					$result[$key]['NamaMateri'] = $activity['mkeyh_nama'];
					$result[$key]['Preview'] = $activity['mkeyh_preview'];
					$dt = strtotime($result[$key]['WaktuUjian']);
					$result[$key]['Hari'] = date("D",$dt);
					$result[$key]['Tanggal'] = date("d-m-Y",$dt);
					$result[$key]['Pukul'] = date("H:i",$dt);
				}
		
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getResult($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					$activity = $this->getKeyActivities($result[$key]['IdKeyAct']);
					if($result[$key]['StatusLulus'] == 'N') $result[$key]['StatusLulus'] = "Tidak";
					else $result[$key]['StatusLulus'] = "Ya";		
					$result[$key]['NamaMateri'] = $activity['mkeyh_nama'];
					$result[$key]['Preview'] = $activity['mkeyh_preview'];
					$dt = strtotime($result[$key]['WaktuUjian']);
					$result[$key]['Hari'] = date("D",$dt);
					$result[$key]['Tanggal'] = date("d-m-Y",$dt);
					$result[$key]['Tanggal'] = date("d-m-Y",$dt);
					$result[$key]['Pukul'] = date("H:i",$dt);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="Result";
		$data['page_title']="Website Training Pharos";
		$data2['data'] = json_encode($this->getResult($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('result_view',$data2);
		$this->load->view('home_footer');
	}
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


