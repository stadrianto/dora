
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payroll extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getPayrollCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdPayroll,4) as IdPayroll FROM payroll ORDER BY IdPayroll DESC", true);
		$result["IdPayroll"] = $result["IdPayroll"]+1;
		if($result["IdPayroll"] < 10)
			$result["IdPayroll"] = "PR000".$result["IdPayroll"];
		else if($result["IdPayroll"]< 100)
			$result["IdPayroll"] = "PR00".$result["IdPayroll"];
		else if($result["IdPayroll"]< 1000)
			$result["IdPayroll"] = "PR0".$result["IdPayroll"];
		else
			$result["IdPayroll"] = "PR".$result["IdPayroll"];
		return $result["IdPayroll"];
	}
	
	public function checkPayroll($bulan,$tahun){
	
		$result = $this->all_model->query_data("SELECT * FROM payroll WHERE Bulan=".$bulan." and Tahun=".$tahun."", true);
		
		if($result)
		{
			return $result["IdPayroll"];
		}
		else return "0";
		

	}
	
	public function getDataKaryawan($NIP)
	{
		$result = $this->all_model->query_data("SELECT * FROM mskaryawan where NIP='".$NIP."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}
	
	public function checkNIP(){

		$idpayroll = $this->input->post('IdPayroll');
		$nip = $this->input->post('NIP');
		$result = $this->all_model->query_data("SELECT NIP FROM detailpayroll WHERE IdPayroll='".$idpayroll."' and NIP='".$nip."'", true);
		
		if($result)
		{
			echo json_encode("true");
		}
		else echo json_encode("false");
		
		
		exit();
	}
	
	
	public function generatePayroll()
	{
	
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		
		$idpayroll =  $this->checkPayroll($bulan,$tahun);
		
		
		if($idpayroll == "0")
		{
		
			$idpayroll = $this->getPayrollCode();
			
			$data1 = array(
			'IdPayroll' => $idpayroll,		
			'Tahun' => $tahun,
			'Bulan' => $bulan,
			'Tanggal' => date("Y-m-d"),
			'ActiveYN' => 'Y',
			'UpdateId' => $this->session->userdata("UserId"),
			'UpdateTime' => date("Y-m-d H:i:s")	
				);
			$query = $this->all_model->insert_data("payroll", $data1 );
				
			$this->insertDataPayroll($idpayroll);
		
		}
		
	
		$data['data'] = $this->getDataPayroll($config['per_page'], $config['segmen'],false,$idpayroll);	 
		$data["status"] = "sukses";
		$data["msg"] = $this->getDataPayroll($config['per_page'], $config['segmen'],false,$idpayroll);
		echo json_encode($data);
		
		exit();

	}

	
	public function addDetailPayroll(){
	
		$idpayroll = $this->input->post('IdPayroll');
		$nip = $this->input->post('NIP');
		$potongan = $this->input->post('Potongan');
		$result = $this->getDataKaryawan($nip);
		
		$payroll = $this->perhitunganPPH($result['GajiPokok'],$potongan,$result['StatusKerja'],$result['StatusMarital'],$result['JumlahAnak'],$result['NPWP']);
		
		$data = array(
		'IdPayroll' => $idpayroll,	
		'NIP' => $nip,
		'GajiPokok' => $payroll['GajiPokok'],
		'PTKP' => $payroll['PTKP'],
		'PPH21' => $payroll['PPH21'],
		'GajiBersih' => $payroll['GajiBersih'],
		'Potongan' => 0,
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")	
			);
			
		$query = $this->all_model->insert_data("detailpayroll", $data );
		
		echo json_encode($query);
		exit();
	}	
	
		//var data="NIP="+$("#nip").val()+"&Potongan="+$("#potongan").val()+"&IdPayroll="+$("#idnyapayroll").val()+"&IdDetailPayroll="+$("#iddetailpayroll").val();
		
	public function editDetailPayroll(){
	
		$idpayroll = $this->input->post('IdPayroll');
		$iddetailpayroll = $this->input->post('IdDetailPayroll');
		$nip = $this->input->post('NIP');
		$potongan = $this->input->post('Potongan');
		$result = $this->getDataKaryawan($nip);
		
		$payroll = $this->perhitunganPPH($result['GajiPokok'],$potongan,$result['StatusKerja'],$result['StatusMarital'],$result['JumlahAnak'],$result['NPWP']);
		
		$data = array(
		'GajiPokok' => $payroll['GajiPokok'],
		'PTKP' => $payroll['PTKP'],
		'PPH21' => $payroll['PPH21'],
		'GajiBersih' => $payroll['GajiBersih'],
		'Potongan' => $potongan,
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")	
			);
			
	
		$where = array(	
					'IdDetailPayroll'=> $iddetailpayroll,
				
		);	
		$query = $this->all_model->update_data("detailpayroll", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
		
	public function deleteDetailPayroll()
	{
		$IdDetailPayroll = $this->input->post('IdDetailPayroll');
		$where = array('IdDetailPayroll'=>$IdDetailPayroll);
		$query = $this->all_model->delete_data("detailpayroll", $where);
		echo json_encode($query);
		exit();
	}

	public function perhitunganPPH($gajipokok,$potongan,$statuskerja,$statusmarital,$jumlahanak,$npwp)
	{
		
		$ptkp = 0;
		$pph21 = 0;
		$totalgajiptkp = 0; // $totalgaji - $ptkp
		if($statuskerja == 'Tetap')
		{
			$biayajabatan = 5*$gajipokok/100;
			if($biayajabatan > 500000) $biayajabatan = 500000;
			$totalgaji = $gajipokok  - $biayajabatan - $potongan;
		}
		else
		{
			$totalgaji =  $gajipokok - $potongan;
		}		
		
		if($statusmarital == 'Lajang')
		{
			$ptkp = 2025000;
		}
		else
		{
			$ptkp = 2193750;
			if($jumlahanak < 4) 
			{
				$ptkp+= 168750*$jumlahanak;
			}
			else
				$ptkp+= 168750*3;
		}
		
		
		$totalptkp = $ptkp*12;
		$totalgaji = $totalgaji*12;
		if($totalptkp > $totalgaji) 
		{
			$totalgajiptkp = 0;
		}
		else
		{
			$totalgajiptkp = $totalgaji - $totalptkp;
		}
		
		$payroll['PTKP'] = $ptkp;
		
		if($totalgajiptkp <= 50000000)
		{
			$pph21 = ($totalgajiptkp)*5 /100;
		}
		
		if($totalgajiptkp > 50000000 && $totalgajiptkp <= 250000000)
		{
			$pph21 = 50000000*5/100;
			$pph21 += ($totalgajiptkp-50000000)*15/100;
		}
		
		if($totalgajiptkp > 250000000 && $totalgajiptkp <= 500000000)
		{
			$pph21 = 50000000*5/100;
			$pph21 += 200000000*15/100;
			$pph21 += ($totalgajiptkp-250000000)*25/100;
		}
		
		if($totalgajiptkp > 500000000 )
		{
			$pph21 = 50000000*5/100;
			$pph21 += 200000000*15/100;
			$pph21 += 250000000*25/100;
			$pph21 += ($totalgajiptkp-500000000)*30/100;
		}
		
		if($npwp == '' || $npwp == null)
		{
			$pph21 = $pph21 * 1.2;
		}
		
		$payroll['PPH21'] = $pph21/12;
		$payroll['GajiBersih'] = ($totalgaji - $pph21)/12;
		$payroll['GajiPokok'] = $gajipokok;	
		return $payroll;
	}
		
	public function insertDataPayroll($idpayroll){

		$search = array(
			);
		$join = array(
		);
		$where = array(
				'a.ActiveYN'	=>"Y",
		);
			
		$result = $this->all_model->get_data("NIP as NIP,Nama as Nama,GajiPokok as GajiPokok,StatusKerja as StatusKerja,JumlahAnak as JumlahAnak,StatusMarital as StatusMarital,NPWP as NPWP", "mskaryawan a",$join, $where, $search, false);
		
		if($result){
				
		foreach($result as $key => $value){
		
			$payroll = $this->perhitunganPPH($result[$key]['GajiPokok'],0,$result[$key]['StatusKerja'],$result[$key]['StatusMarital'],$result[$key]['JumlahAnak'],$result[$key]['NPWP']);
			
			$data = array(
			'IdPayroll' => $idpayroll,	
			'NIP' => $result[$key]['NIP'],
			'GajiPokok' => $payroll['GajiPokok'],
			'PTKP' => $payroll['PTKP'],
			'PPH21' => $payroll['PPH21'],
			'GajiBersih' => $payroll['GajiBersih'],
			'Potongan' => 0,
			'UpdateId' => $this->session->userdata("UserId"),
			'UpdateTime' => date("Y-m-d H:i:s")	
				);
		
			$query = $this->all_model->insert_data("detailpayroll", $data );
			
		}
				
		}	
	}
	
	public function getDataPayroll($perPage=10, $segmen=0, $request = true,$idpayroll){
		$current = $this->input->post('page');
		
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
			);
			
		$join = array(
			array('table'=>'mskaryawan b','field' => 'a.NIP = b.NIP','method'=>'Left')			
			
		);
		$where = array(
			'a.IdPayroll'=>$idpayroll,	
		);
			

		$result = $this->all_model->get_data("IdDetailPayroll as IdDetailPayroll,IdPayroll as IdPayroll,a.NIP as NIP,Nama as Nama,a.GajiPokok as GajiPokok,a.GajiBersih as GajiBersih,Potongan as Potongan,PTKP as PTKP,PPH21 as PPH21","detailpayroll a",$join, $where, $search, false, $perPage, $segmen, false,"IdDetailPayroll","ASC");
		$result2 = $this->all_model->get_data("IdDetailPayroll as IdDetailPayroll,IdPayroll as IdPayroll,a.NIP as NIP,Nama as Nama,a.GajiPokok as GajiPokok,a.GajiBersih as GajiBersih,Potongan as Potongan,PTKP as PTKP,PPH21 as PPH21", "detailpayroll a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataPayroll($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				
				foreach($result as $key => $value){
				
					$result[$key]['PTKP'] = $this->all_model->rp($result[$key]['PTKP']);
					$result[$key]['PPH21'] = $this->all_model->rp($result[$key]['PPH21']);
					$result[$key]['GajiBersih'] = $this->all_model->rp($result[$key]['GajiBersih']);
					$result[$key]['GajiPokok2'] = $this->all_model->rp($result[$key]['GajiPokok']);
					$result[$key]['Potongan2'] = $this->all_model->rp($result[$key]['Potongan']);
				}
				
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataPayroll($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					$result[$key]['PTKP'] = $this->all_model->rp($result[$key]['PTKP']);
					$result[$key]['PPH21'] = $this->all_model->rp($result[$key]['PPH21']);
					$result[$key]['GajiBersih'] = $this->all_model->rp($result[$key]['GajiBersih']);
					$result[$key]['GajiPokok2'] = $this->all_model->rp($result[$key]['GajiPokok']);
					$result[$key]['Potongan2'] = $this->all_model->rp($result[$key]['Potongan']);	
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Payroll";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('payroll_view');
		$this->load->view('home_footer');
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


