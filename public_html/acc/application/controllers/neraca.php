<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class neraca extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataJurnal($noakun,$tanggal2)
	{
		$result = $this->all_model->query_data("SELECT Debit as Debit,Kredit as Kredit FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where noakun='".$noakun."' and tanggaltransaksi<'".$tanggal2."'", false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataPrinter($id)
	{
		$result = $this->all_model->query_data("SELECT Nama as Nama from msuser where iduser=".$id, false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataTrial()
	{
		$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NamaAkun as NamaAkun,SaldoAwal as SaldoAwal,IdNeraca as IdNeraca FROM tabelakun a join kelompokakun b on a.idkelompokakun=b.idkelompokakun where left(a.noakun,1)<3 order by a.noakun asc", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	
	public function printBS()
	{
		//$noproject = $this->session->userdata('NoProject');
		//$dataproject = $this->getDataTrial();
		$tanggal2=$this->input->post('dob1');
		$qtanggal2=substr($tanggal2,6,4).'-'.substr($tanggal2,0,2).'-'.substr($tanggal2,3,2);		
		$result = $this->getDataTrial();
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		//$this->fpdf->Image('media/img/logo.png',35,10,30,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('Arial','B',15);
		$this->fpdf->Cell(190,6,'Dahob.com',0,0,'C');
		$this->fpdf->Ln(5);
		$this->fpdf->Cell(190,6,'BalanceSheet',0,0,'C');
		$this->fpdf->SetFont('Arial','BU',12);
		$this->fpdf->Ln(5);
		$this->fpdf->Cell(190,6,'Per Tanggal '.$tanggal2,0,0,'C');
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
		$this->fpdf->Ln(15);

		$y_axis_initial = 50;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(50);
		//Header tabel halaman 1
		/*
		$this->fpdf->CELL(25,6,'NoAkun',1,0,'C',1);
		$this->fpdf->Cell(60,6,'NamaAkun',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Debit',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Kredit',1,0,'C',1);
		*/
		//$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);

		$this->fpdf->Ln();
		$max=45;//max baris perhalaman
		$awala=$awalb=$awalc=$awald=$awale=$awalf=0;
		$i=0;
		$no4=0;
		$no5=0;
		$no6=0;
		$no7=0;
		$no8=0;
		$hpp=0;
		$totala=0;
		$totalb=0;
		$totalc=0;
		$totald=0;
		$totale=0;
		$totalf=0;
		$gantipage=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		//$grandtotal = 0;
		if($result != "0")
		{
			foreach($result as $key => $value)
			{
				$i++;
				if ($i >= $max){//jika $i=25 maka buat header baru seperti di atas
					$this->fpdf->AddPage();
					$this->fpdf->SetY(10);
					$this->fpdf->SetX(30);
					$this->fpdf->SetY(10);
					$this->fpdf->SetX(55);
					$y_axis = $y_axis + $row_height;
					$i=0;
					$gantipage=70;
					$no4=$no5=$no6=$no7=0;
					$this->fpdf->Ln();
				}
				
				$saldo=floatval($result[$key]["SaldoAwal"]);
				$result2 = $this->getDataJurnal($result[$key]["NoAkun"]."",$tanggal2."");
				if($result[$key]["IdNeraca"]=="1")
				{	
					//$saldo=0;
					if($result2!="0")
					{
						for($z=0;$z<count($result2);$z++){
							$saldo+=$result2[$z]["Debit"];
							$saldo-=$result2[$z]["Kredit"];
						}
					}
				}	
				else if($result[$key]["IdNeraca"]=="2")
				{
					//$saldo=0;
					if($result2!="0")
					{
						for($z=0;$z<count($result2);$z++){
							$saldo-=$result2[$z]["Debit"];
							$saldo+=$result2[$z]["Kredit"];				
						}
					}
				}
				//$i++;
				//$no++;
				if(substr($result[$key]["NoAkun"],0,3)=="111")
				{
					if($awala==0)
					{					
						$this->fpdf->SetFont('Arial','BU',10);
						$this->fpdf->SetY(40-$gantipage+($no4*5));
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Aktiva',0,0,'L',0);
						$this->fpdf->SetY(45-$gantipage+($no4*5));
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Harta Lancar',0,0,'L',0);
						$awala=1;
					}
					$this->fpdf->SetFont('Arial','',10);
					$this->fpdf->SetY(50-$gantipage+($no4*5));
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(60,6,$result[$key]["NamaAkun"],0,0,'L',0);
					$this->fpdf->SetX(120);
					$this->fpdf->Cell(60,6,$this->all_model->rp($saldo),0,0,'R',0);
					//$this->fpdf->Cell(25,6,45+($no4*5),0,0,'R',0);
					//$this->fpdf->Ln(0);
					$no4++;
					$totala+=$saldo;
				}
				else if(substr($result[$key]["NoAkun"],0,3)=="112")
				{		
					if($awalb==0)
					{				
						$this->fpdf->SetFont('Arial','BU',10);				
						$this->fpdf->SetY(50-$gantipage+($no4*5));
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Harta Tetap',0,0,'L',0);
						$awalb=1;
					}
					$this->fpdf->SetFont('Arial','',10);
					$this->fpdf->SetY(55-$gantipage+($no4*5)+($no5*5));
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(60,6,$result[$key]["NamaAkun"],0,0,'L',0);
					$this->fpdf->SetX(120);
					$this->fpdf->Cell(60,6,$this->all_model->rp($saldo),0,0,'R',0);
					//$this->fpdf->Cell(25,6,45+($no4*5)+($no5*5),0,0,'R',0);
					//$this->fpdf->Ln(0);			
					$no5++;
					$hpp+=$saldo;
				}
				else if(substr($result[$key]["NoAkun"],0,2)=="21")
				{
					if($awalb==0)
					{
						//LabaKotor totala
						$this->fpdf->SetFont('Arial','B',10);
						$this->fpdf->SetY(45-$gantipage+($no4*5)+($no5*5));
						$this->fpdf->SetX(150);
						$this->fpdf->Cell(60,6,'_______________-',0,0,'L',0);				
						$this->fpdf->SetY(50-$gantipage+($no4*5)+($no5*5));
						$i++;
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Laba Kotor',0,0,'L',0);
						$this->fpdf->SetX(120);
						$this->fpdf->Cell(60,6,$this->all_model->rp($totala-$hpp),0,0,'R',0);
						//headerbeban				
						$this->fpdf->SetY(60-$gantipage+($no4*5)+($no5*5));
						$i++;
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Beban',0,0,'L',0);
						$i++;
						$awalb=1;
					}
					$this->fpdf->SetFont('Arial','',10);
					//$this->fpdf->SetY(60);
					$this->fpdf->SetY(65-$gantipage+($no4*5)+($no5*5)+($no6*5));
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(30,6,$result[$key]["NoAkun"].' - '.$result[$key]["NamaAkun"],0,0,'L',0);
					$this->fpdf->SetX(90);
					$this->fpdf->Cell(60,6,$this->all_model->rp($saldo),0,0,'R',0);
					//$this->fpdf->Cell(25,6,65+($no4*5)+($no6*5),0,0,'R',0);
					$no6++;
					$totalb+=$saldo;
					//$this->fpdf->SetY(70);
				}		
				else if(substr($result[$key]["NoAkun"],0,1)=="22")
				{
					if($awalc==0)
					{
						//totalbeban totalb
						$this->fpdf->SetFont('Arial','B',10);
						$this->fpdf->SetY(65-$gantipage+($no4*5)+($no5*5)+($no6*5));						
						$this->fpdf->SetX(120);
						$this->fpdf->Cell(60,6,'_______________+',0,0,'L',0);						
						$i++;
						if ($i >= $max){//jika $i=25 maka buat header baru seperti di atas
							$this->fpdf->AddPage();
							$y_axis = $y_axis + $row_height;
							$i=0;
							$gantipage=70;
							$no4=$no5=$no6=$no7=0;
							//$this->fpdf->Ln();
						}
						$this->fpdf->SetY(70-$gantipage+($no4*5)+($no5*5)+($no6*5));
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Total Beban',0,0,'L',0);
						$this->fpdf->SetX(120);				
						$this->fpdf->Cell(60,6,$this->all_model->rp($totalb),0,0,'R',0);
						//laba usaha totalc
						$totalc=$totala-$hpp-$totalb;						
						$i++;
						if ($i >= $max){//jika $i=25 maka buat header baru seperti di atas
							$this->fpdf->AddPage();
							$y_axis = $y_axis + $row_height;
							$i=0;
							$gantipage=70;
							$no4=$no5=$no6=$no7=0;
							//$this->fpdf->Ln();
						}
						$this->fpdf->SetY(75-$gantipage+($no4*5)+($no5*5)+($no6*5));
						$this->fpdf->SetX(150);
						$this->fpdf->Cell(60,6,'_______________-',0,0,'L',0);	
						$i++;
						if ($i >= $max){//jika $i=25 maka buat header baru seperti di atas
							$this->fpdf->AddPage();
							$y_axis = $y_axis + $row_height;
							$i=0;
							$gantipage=70;
							$no4=$no5=$no6=$no7=0;
							//$this->fpdf->Ln();
						}
						$this->fpdf->SetY(80-$gantipage+($no4*5)+($no5*5)+($no6*5));						
						$this->fpdf->SetX(20);
						if($totala-$hpp>=$totalb)				
							$this->fpdf->Cell(60,6,'Laba Usaha',0,0,'L',0);				
						else
							$this->fpdf->Cell(60,6,'Rugi Usaha',0,0,'L',0);
						$this->fpdf->SetX(120);				
						$this->fpdf->Cell(60,6,$this->all_model->rp($totalc),0,0,'R',0);
						//header penghasilan diluar usaha						
						$i++;
						if ($i >= $max){//jika $i=25 maka buat header baru seperti di atas
							$this->fpdf->AddPage();
							$y_axis = $y_axis + $row_height;
							$i=0;
							$gantipage=70;
							$no4=$no5=$no6=$no7=0;
							//$this->fpdf->Ln();
						}
						$this->fpdf->SetY(90-$gantipage+($no4*5)+($no5*5)+($no6*5));
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Penghasilan di Luar Usaha',0,0,'L',0);
						$awalc=1;
						$i++;
						if ($i >= $max){//jika $i=25 maka buat header baru seperti di atas
							$this->fpdf->AddPage();
							$y_axis = $y_axis + $row_height;
							$i=0;
							$gantipage=70;
							$no4=$no5=$no6=$no7=0;
							//$this->fpdf->Ln();
						}
					}
					//$this->fpdf->SetY(60);
					$this->fpdf->SetFont('Arial','',10);
					$this->fpdf->SetY(95-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5));
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(60,6,$result[$key]["NamaAkun"],0,0,'L',0);
					$this->fpdf->SetX(90);
					$this->fpdf->Cell(60,6,$this->all_model->rp($saldo),0,0,'R',0);
					//$this->fpdf->Cell(25,6,95+($no4*5)+($no6*5),0,0,'R',0);
					$no7++;
					$totald+=$saldo;
					//$this->fpdf->SetY(70);
				}
				else if(substr($result[$key]["NoAkun"],0,1)=="3")
				{
					if($awald==0)
					{
						$this->fpdf->SetFont('Arial','B',10);
						//total penghasilan diluar usaha totald
						$this->fpdf->SetY(95-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5));						
						$this->fpdf->SetX(120);
						$this->fpdf->Cell(60,6,'_______________+',0,0,'L',0);	
						$this->fpdf->SetY(100-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5));
						$i++;
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Total Penghasilan di Luar Usaha',0,0,'L',0);
						$this->fpdf->SetX(120);				
						$this->fpdf->Cell(60,6,$this->all_model->rp($totald),0,0,'R',0);
						$this->fpdf->SetY(105-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5));
						$i++;
						$this->fpdf->SetX(150);
						$this->fpdf->Cell(60,6,'_______________+',0,0,'L',0);	
						//header beban diluar usaha
						$this->fpdf->SetY(115-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5));
						$i++;
						$this->fpdf->SetX(20);
						$this->fpdf->Cell(60,6,'Beban di Luar Usaha',0,0,'L',0);
						$awald=1;
						$i++;
					}
					//$this->fpdf->SetY(60);
					$this->fpdf->SetFont('Arial','',10);
					$this->fpdf->SetY(120-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
					$i++;
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(60,6,$result[$key]["NamaAkun"],0,0,'L',0);
					$this->fpdf->SetX(90);
					$this->fpdf->Cell(60,6,$this->all_model->rp($saldo),0,0,'R',0);
					//$this->fpdf->Cell(25,6,105+($no4*5)+($no6*5),0,0,'R',0);
					$no8++;
					$totale+=$saldo;
					//$this->fpdf->SetY(70);
				}				
			}
				
								
				/* $this->fpdf->SetFont('Arial','B',10);
				//total beban diluar usaha totale
				$this->fpdf->SetY(120-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
				$i++;
				$this->fpdf->SetX(120);
				$this->fpdf->Cell(60,6,'_______________+',0,0,'L',0);	
				$this->fpdf->SetY(125-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
				$i++;
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,'Total Beban di Luar Usaha',0,0,'L',0);
				$this->fpdf->SetX(120);				
				$this->fpdf->Cell(60,6,$this->all_model->rp($totale),0,0,'R',0);
				$this->fpdf->SetY(130-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
				$i++;
				$this->fpdf->SetX(150);
				$this->fpdf->Cell(60,6,'_______________-',0,0,'L',0);
				//Laba Bersih Sbelum Pajak
				$totalf=($totalc+$totald)-$totale;
				$this->fpdf->SetY(135-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
				$i++;
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,'Laba Bersih Sebelum Pajak',0,0,'L',0);
				$this->fpdf->SetX(120);				
				$this->fpdf->Cell(60,6,$this->all_model->rp($totalf),0,0,'R',0);
				//Pajak
				$pajak=0;
				if($totala<=4800000000)
				{
					$pajak=($totalf*0.25)*0.5;
				}
				else if($totala>4800000000 && $totala<=50000000000)
				{
					$pajak=((((4800000000/$totala)* $totalf)*0.25) *0.5) + (($totalf-((4800000000/$totala)* $totalf))*0.25);
				}
				else
				{
					$pajak=($totalf*0.25);
				}
				$this->fpdf->SetY(140-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
				$i++;
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,'Pajak',0,0,'L',0);
				$this->fpdf->SetX(120);				
				$this->fpdf->Cell(60,6,$this->all_model->rp($pajak),0,0,'R',0);
				$this->fpdf->SetY(145-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
				$i++;
				$this->fpdf->SetX(150);
				$this->fpdf->Cell(60,6,'_______________-',0,0,'L',0);
				$this->fpdf->SetY(150-$gantipage+($no4*5)+($no5*5)+($no6*5)+($no7*5)+($no8*5));
				$i++;
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,'Laba Bersih Sesudah Pajak',0,0,'L',0);
				$this->fpdf->SetX(120);				
				$this->fpdf->Cell(60,6,$this->all_model->rp($totalf-$pajak),0,0,'R',0); */
		}
		//$this->fpdf->SetX(30);
		//$this->fpdf->Cell(25,6,'',1,0,'C',0);
		//$this->fpdf->Cell(60,6,'Total',1,0,'C',0);
		//$this->fpdf->Cell(25,6,$totala,1,0,'R',0);
		//$this->fpdf->Cell(25,6,$this->all_model->rp($totala),1,0,'R',0);
		//$this->fpdf->Cell(25,6,$this->all_model->rp($totalb),1,0,'R',0);
		//buat footer
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$printer=$this->getDataPrinter($this->session->userdata("UserId"));
		$this->fpdf->SetX(0);			
		$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'C');
		$this->fpdf->SetX(40);		
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Output('Laporan Laba Rugi'.date("F Y").'.pdf', 'I');
		
	}
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "login","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="Balance Sheet";
		$data['page']="balancesheet_view";
		$data['nama']=$this->session->userdata('Nama');
		//$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		//$data['include']=$this->load->view('script','',true);
		$this->load->view('main',$data);
	}	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

