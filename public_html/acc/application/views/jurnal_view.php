		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
		<style>
			#grid th{
			text-align:center;				
			}
		</style>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Journal <small>Jurnal</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New Jurnal</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Jurnal List </h3>
                        </div>
                        <div class="panel-body">
                            <div id="grid"></div>				
                        </div>
                    </div>
                </div>
            </div>			 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Account </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">							
							<div class="form-group">
								<label>No Jurnal</label>					
								<input size="9" id="NoJurnal" name="NoJurnal" value="" class="form-control">		
							</div>							
							<div class="form-group">
								<label>TanggalTransaksi</label>
								<input type="date" id="TanggalTransaksi" name="TanggalTransaksi" class="form-control">				
							</div>							
							<div class="form-group">
								<label>Deskripsi</label>
								<textarea id="Descr" name="Descr" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<table id="coba1">
									<tr>
										<td align="center">Akun</td><td align="center">Debit</td><td align="center">Kredit</td>
									</tr>
									<tr>
										<td><div id="akunarea"></div></td>
										<td><input class="form-control" type="text" name="debit0" id="debit0"/></td>
										<td><input class="form-control" type="text" name="kredit0" id="kredit0"/></td>
									</tr>									
								</table>
							</div>
							<div><button id="btnRow" type="button" class="btn btn-default">AddRow</button></div>				
							</div>							
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogkonf" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div name="tes02" id="tes02" class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Detail Jurnal </h3>
					</div>					
					<div class="panel-body">
						<form>
							<table class="table table-bordered table-striped">
							<tbody>
								<tr>
									<td>NoJurnal:</td>
									<td><input type="hidden" id="NoJurnal2" name="NoJurnal2"/><input class="form-control" type="text" id="konfnojurnal" readonly="readonly" name="konfnojurnal"/></td>
									
								</tr>
								<tr>
									<td>Tanggal:</td>
									<td><input class="form-control" readonly="readonly" type="text" id="konftanggal" name="konftanggal"/></td>			
								</tr>
								<tr>
									<td>Deskripsi:</td>
									<td><textarea class="form-control" readonly="readonly" id="konfdeskripsi" name="konfdeskripsi"></textarea></td>			
								</tr>
								<tr>
									<td>Status:</td>
									<td><input class="form-control" readonly="readonly" type="text" id="konfstatus" name="konfstatus"/></td>			
								</tr>
								<tr>
									<td id="detailjurnal" colspan=2></td>			
								</tr>						
							</tbody>							
							</table>							
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	var flag=0;
	function getId()
	{
		$('#NoAkun').val($('#IdKelompokGL').val().substring(0,1));
		$('#NoAkun1').val($('#IdKelompokGL').val().substring(0,4));
	}
	
	$(document).ready(function () {
		var iCnt = 1;
		flag=1;		
		var hasil3 = JSON.parse('<?php echo $akun; ?>');
		var text = '';
		
		if(hasil3 == "No Data"){
			$('#akunarea').html("No Active Departemen");
		}else{
			text+= '<select class="form-control" onchange="" name="NamaAkun0" id="NamaAkun0">';
			for(i=0;i<hasil3.length;i++){
				
				text+= '<option value="'+hasil3[i].NoAkun+'">'+hasil3[i].NoAkun+'-'+hasil3[i].NamaAkun+'</option>'	
			}
			text+= '</select>';
			$('#akunarea').html(text);
		
		$('#btnRow').click(function() {
            if (iCnt <= 10) {
                // ADD TEXTBOX.
				var test='<tr><td><select class="form-control" name="NamaAkun'+iCnt+'" id="NamaAkun'+iCnt+'">';
				for(i=0;i<hasil3.length;i++){
					test+='<option value="'+hasil3[i].NoAkun+'">'+hasil3[i].NoAkun+'-'+hasil3[i].NamaAkun+'</option>'}				
				test+='</select></td><td><input class="form-control" type=text name="debit" id=debit' + iCnt + ' ' +'/></td><td><input class="form-control" type=text name="kredit" id=kredit' + iCnt + ' ' +'/></td></tr>';
				
				$('#coba1').append(test);
				/*
                $(container).append('<input type=text name="debit" id=debit' + iCnt + ' ' +
                            ' /><input type=text name="kredit" id=kredit' + iCnt + ' ' +
                            '" />');
				*/
				
				/*
                if (iCnt == 2) {        // SHOW SUBMIT BUTTON IF ATLEAST "1" ELEMENT HAS BEEN CREATED.

                    var divSubmit = $(document.createElement('div'));
                    $(divSubmit).append('<input type=button class="bt" onclick="" id="btSubmit" value="Submit" />');

                }*/

                //$('#coba1').after(container);   // ADD BOTH THE DIV ELEMENTS TO THE "main" CONTAINER.
				iCnt = iCnt + 1;
				flag=flag+1;
            }
            else {      // AFTER REACHING THE SPECIFIED LIMIT, DISABLE THE "ADD" BUTTON. (20 IS THE LIMIT WE HAVE SET)
                $(container).append('<label>Reached the limit</label>'); 
                $('#btnRow').attr('class', 'bt-disable'); 
                $('#btnRow').attr('disabled', 'disabled');
            }
        });
		
		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
			for(x=iCnt;x>0;x--)
			{
				$('#NamaAkun'+x).remove()
				$('#debit'+x).remove()
				$('#kredit'+x).remove()
			}
			iCnt = 1;
		});
		$('#tes02').click(function(e) {
			$('#dialogkonf').modal('hide');
		});
		
		}	
	});
	
	function addNew(){
		var flagdebit=0;
		var flagkredit=0;
		var sumkredit=0;
		var sumdebit=0;
		var flagakun=0;
		for(i=0;i<flag;i++)
		{ 
			for(j=flag;j>i;j--)
			{
				if($('#NamaAkun'+i).val()==$('#NamaAkun'+j).val())
				{
					flagakun=1;
					break;
				}
			}			
			flagdebit=0;
			if($('#debit'+i).val()==""&&$('#kredit'+i).val()=="")
			{
				flagdebit=2;
				break;
			}
			else if($('#debit'+i).val()!=""&&$('#kredit'+i).val()!="")
			{
				flagdebit=2;
				break;
			}
			if($('#debit'+i).val()!="")
			{
				if(isNaN($('#debit'+i).val())==true)
				{
					flagdebit=1;
					break;
				}
				else
				{
					sumdebit+=parseInt($('#debit'+i).val());
				}
			}			
			
			if($('#kredit'+i).val()!="")
			{
				if(isNaN($('#kredit'+i).val())==true)
				{
					flagkredit=1;
					break;
				}
				else
				{
					sumkredit+=parseInt($('#kredit'+i).val());
					
				}
			}
			
		}
		if($('#NoJurnal').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>NoJurnal harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#TanggalTransaksi').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Tanggal Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flag<2){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Jurnal minimal 2 akun</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if(flagdebit==2){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Debit atau kredit harus disi salah 1</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagdebit==1||flagkredit==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Debit dan kredit harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(sumkredit!=sumdebit){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Total debit dan kredit harus sama</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagakun==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Akun yang digunakan tidak bole sama</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		var tanggaltransaksi=$('#TanggalTransaksi').val();
		var des=$('#Descr').val();
		if(des==""||des==null)
			des="-";
		/* if(tanggaltransaksi != "" || tanggaltransaksi != null)
		tanggaltransaksi= tanggaltransaksi.substring(7,4) +"-"+tanggaltransaksi.substring(0,2) +"-"+tanggaltransaksi.substring(3,5); */	
		var data = "NoJurnal="+$('#NoJurnal').val()+"&TanggalTransaksi="+tanggaltransaksi+"&JenisJurnal=1&Deskripsi="+des;
		//alert(data);
		
		$.ajax({				
			url : "<?php echo base_url()?>jurnal/insertJurnal",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					alert("sukses");
					setTimeout(function(){
			
						location.reload();
						}, 1500);
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
		});
		
		var data2="";
		for(k=0;k<flag;k++)
		{
		//alert(flag);
			data2 = "NoJurnal="+$('#NoJurnal').val()+"&NoAkun="+$('#NamaAkun'+k).val()+"&Debit="+$('#debit'+k).val()+"&Kredit="+$('#kredit'+k).val();
			//alert(data2);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>jurnal/insertDetailJurnal",
				data: data2,
				success: function(msg){
					var hasil = JSON.parse(msg);
					if(hasil == true || hasil == "true"){
						
					}else{
						alert(hasil.msg);
					}
				}
			});
		}
	}
	
	
	function updateData(){
		if($('#NamaAkun').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Akun Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if($('#SaldoAwal').val()== ""){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Saldo Awal harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(isNaN($('#SaldoAwal').val())==true ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Saldo Awal harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		var test1=$('#NoAkun').val()+$('#NoAkun1').val()+$('#NoAkun2').val();
		var data = "NoAkun="+test1+"&NamaAkun="+$('#NamaAkun').val()+"&SaldoAwal="+$('#SaldoAwal').val()+"&IdKelompokGL="+$('#NoAkun1').val()+"&IdKelompokAkun="+$('#NoAkun').val();
			$.ajax({				
			url : "<?php echo base_url()?>akun/editAkun",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					alert("sukses");
					setTimeout(function(){
			
						location.reload();
						}, 1500);
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
			
	}
	function deleteData(id){		
			var data= "NoAkun="+id;
			$.ajax({				
			url : "<?php echo base_url()?>akun/deleteAkun",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#Descr').val('');
		$('#TanggalTransaksi').val('');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");
		flag=1;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>jurnal/getJurnalCode",
			success: function(msg){			
				var hasil = JSON.parse(msg);
				var text='';
				//alert(hasil[0].NoJurnal);
				$("#NoJurnal").val(hasil.nojurnal);			
			}
		});
	}

	function setUpdate(NoJurnal){
		$('#dialogkonf').modal('show');  
		$('#NoJurnal2').val(NoJurnal+'');		
		var NoJurnal= NoJurnal;		
		var data = "NoJurnal="+$('#NoJurnal2').val();
		//alert(data);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>jurnal/getHeaderJurnal",
			data: data,
			success: function(msg){			
				var hasil = JSON.parse(msg);
				var text='';
				//alert(hasil[0].NoJurnal);
				$("#konfnojurnal").val(hasil[0].NoJurnal);
				$("#konftanggal").val(hasil[0].TanggalTransaksi);
				$("#konfdeskripsi").val(hasil[0].Deskripsi);
				$("#konfstatus").val(hasil[0].Status);
				text+= '<table class="table table-bordered table-striped">';
				text+= '<tr><td>No Akun</td><td>Nama Akun</td><td>Debit</td><td>Kredit</td></tr>';
				for(h=0;h<hasil.length;h++)
				{
					text+= '<tr><td>'+hasil[h].NoAkun+'</td><td>'+hasil[h].NamaAkun+'</td><td>'+hasil[h].Debit+'</td><td>'+hasil[h].Kredit+'</td></tr>';					
					
				}
				text+='</table>';
				
				text+='<div align="center"><button id="deleteBtn" type="submit" class="btn btn-default">Delete</button></div>';				
				$('#detailjurnal').html(text);
				
				/*if(hasil == true || hasil == "true"){
					$("#konfnojurnal").val(hasil[0].NoJurnal);
					setTimeout(function(){location.reload();}, 1500);
				}else{
					$("#error-dialog").html(hasil[0].NoJurnal);
					$("#error-dialog").dialog( "open" );
				}*/
			}
		});
		
		//$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(id,nama,kelakun,kelgl,sawal){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(id);
		$('#deletenama').html(nama);
		$('#deletekelakun').html(kelakun);
		$('#deletekelgl').html(kelgl);
		$('#deletesawal').html(sawal);
		$('#deleteBtn').attr("onclick","deleteData('"+id+"')");			
	}
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>;
				

            if(hasil.length>0)
			{
				$("#grid").shieldGrid({
					dataSource: {
						data: hasil
					},
					sorting: {
						multiple: true
					},
					rowHover: false,
					paging: true,
					selection: 
					{
						type: "cell",
						multiple: false,
						toggle: false
					},
					events: {
						selectionChanged: onSelectedChanged,
						doubleClick: onDouble
					},				
					columns: [
					{ field: "NoJurnal", title: "NoJurnal",attributes: {style: "text-align: center;"}},
					{ field: "Nama", title: "Nama",attributes: {style: "text-align: center;"}},
					{ field: "TanggalTransaksi", title: "TanggalTransaksi",attributes: {style: "text-align: center;"} },       
					{ field: "NamaAkun", title: "NamaAkun",attributes: {style: "text-align: center;"} },
					{ field: "Debit", title: "Debit" , attributes: {style: "text-align: right;"}},
					{ field: "Kredit", title: "Kredit", attributes: {style: "text-align: right;"}},
					{
						width: "60px",
						title: " ",
						buttons: [						
							{
								cls: "button button-primary",
								caption: " Detail",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).NoJurnal;
									/* var nama=$("#grid").swidget().dataItem(rowIndex).NamaAkun;
									var kelakun=$("#grid").swidget().dataItem(rowIndex).NamaKelompokAkun;
									var kelgl=$("#grid").swidget().dataItem(rowIndex).KelompokGL;
									var sawal=$("#grid").swidget().dataItem(rowIndex).SaldoAwal; */
									setUpdate(id); 
								}
							}
						]
					}
					]
				}); 
			}			
        });
		
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
