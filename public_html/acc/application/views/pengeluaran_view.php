		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
		<style>
			#grid th{
			text-align:center;				
			}
		</style>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Expenditure <small>Pengeluaran</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New Expenditure</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Expenditure List </h3>
                        </div>
                        <div class="panel-body">
                            <div id="grid"></div>				
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Laporan Pengeluaran </h3>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<form method='post' action='<?php echo base_url(); ?>pengeluaran/printPengeluaran' id='fm1' name='fm1'>
								Dari Tanggal:<input type='date' id='dob1' name='dob1' class="form-control"/>
								Sampai Tanggal:<input type='date' id='dob2' name='dob2' class="form-control"/>
								<input class='btn btn-primary' type='submit' value=' Buat Laporan Pengeluaran ' style='margin-right:15px;margin-top:15px;' /><br/>
								</form>
							</div>				
						</div>
					</div>
				</div>
			</div>			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Expenditure </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">							
							<div class="form-group">
								<label>Id Pengeluaran</label>					
								<input readonly size="9" id="IdPengeluaran" name="IdPengeluaran" value="" class="form-control">		
							</div>							
							<div class="form-group">
								<label>Tanggal Pengeluaran</label>
								<input type="date" id="TanggalTransaksi" name="TanggalTransaksi" class="form-control">				
							</div>														
							<div class="form-group">
								<label>Keterangan</label>
								<textarea id="Keterangan" name="Keterangan" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<table id="coba1">
									<tr>
										<td align="center">Expense</td>
										<td align="center">Nominal</td>
									</tr>
									<tr>
										<td><div id="akunarea"></td>
										<td><input class="form-control" type="text" name="Nominal0" id="Nominal0"/></td>							
									</tr>									
								</table>
							</div>
							<div><button id="btnRow" type="button" class="btn btn-default">AddRow</button></div>				
							</div>							
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>							
						</form>			
					</div>
				</div>				
			</div>
		</div>		
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div name="tes02" id="tes02" class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Detail Expenditure </h3>
					</div>					
					<div class="panel-body">
						<form>
							<table class="table table-bordered table-striped">
							<tbody>
								<tr>
									<td>Id Pengeluaran:</td>
									<td><input class="form-control" type="text" id="delId" readonly="readonly" name="delId"/></td>
									
								</tr>
								<tr>
									<td>Tanggal Pengeluaran:</td>
									<td><input class="form-control" readonly="readonly" type="text" id="delTanggal" name="delTanggal"/></td>			
								</tr>								
								<tr>
									<td>Keterangan:</td>
									<td><textarea class="form-control" readonly="readonly" id="delKeterangan" name="delKeterangan"></textarea></td>			
								</tr>
								<tr>
									<td id="detailTrans" colspan=2></td>		
								</tr>						
							</tbody>							
							</table>	<div align="center"><input type="button" id="deleteBtn" class="btn btn-default" value="Delete"/></div>						
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	var flag=0;

	
	$(document).ready(function () {
		var iCnt = 1;
		flag=1;		
		
		var hasil3 =  <?=$expense?>;
		var text = '';
		
		 if(hasil3 == "No Data"){
			$('#akunarea').html("No Active Departemen");
		}else{
			text+= '<select class="form-control" onchange="" name="NamaExpense0" id="NamaExpense0">';
			for(i=0;i<hasil3.length;i++){
				
				text+= '<option value="'+hasil3[i].IdExpense+'">'+hasil3[i].IdExpense+'-'+hasil3[i].Nama+'</option>'	
			}
			text+= '</select>';
			$('#akunarea').html(text);
		
		$('#btnRow').click(function() {
            if (iCnt <= 10) {
                
				var test='<tr><td><select class="form-control" name="NamaExpense'+iCnt+'" id="NamaExpense'+iCnt+'">';
				for(i=0;i<hasil3.length;i++){
					test+='<option value="'+hasil3[i].IdExpense+'">'+hasil3[i].IdExpense+'-'+hasil3[i].Nama+'</option>'}				
				test+='</select></td><td><input class="form-control" type=text name="Nominal" id=Nominal' + iCnt + ' ' +'/></td></tr>';
				
				$('#coba1').append(test);
				
				iCnt = iCnt + 1;
				flag=flag+1;
            }
            else {  
                $(container).append('<label>Reached the limit</label>'); 
                $('#btnRow').attr('class', 'bt-disable'); 
                $('#btnRow').attr('disabled', 'disabled');
            }
        });
		
		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
			for(x=iCnt;x>0;x--)
			{
				$('#NamaExpense'+x).remove()
				$('#Nominal'+x).remove()
			}
			iCnt = 1;
		});
		$('#tes02').click(function(e) {
			$('#dialogDelete').modal('hide');
		});
		
		$('#dialog').on('hidden.bs.modal', function () {			
			for(x=iCnt;x>0;x--)
			{
				$('#NamaBarang'+x).remove()
				$('#Qty'+x).remove()
				$('#Harga'+x).remove()
			}
			iCnt = 1;
			flag=1;
		})
		
		}
	});
	
	function addNew(){
		var flagdebit=0;
		var flagkredit=0;
		var sumkredit=0;
		var sumdebit=0;
		var flagakun=0;
		for(i=0;i<flag;i++)
		{ 
			//alert(i);
			for(j=flag;j>i;j--)
			{
				if($('#NamaExpense'+i).val()==$('#NamaExpense'+j).val())
				{
					flagakun=1;
					break;
				}
			}			
			flagdebit=0;
			if($('#Nominal'+i).val()=="")
			{
				flagdebit=2;
				break;
			}
			if(isNaN($('#Nominal'+i).val())==true)
			{
				flagdebit=1;
				break;
			}
			else{
				
				sumdebit+=parseInt($('#Nominal'+i).val());
				
			}

		}		
		if($('#TanggalTransaksi').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Tanggal Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if(flagdebit==2){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nominal harus disi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagdebit==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nominal harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagakun==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Beban yang dibayar tidak boleh sama</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		var tanggaltransaksi=$('#TanggalTransaksi').val();
		var des=$('#Keterangan').val();
		if(des==""||des==null)
			des="-";
		/* if(tanggaltransaksi != "" || tanggaltransaksi != null)
		tanggaltransaksi= tanggaltransaksi.substring(7,4) +"-"+tanggaltransaksi.substring(0,2) +"-"+tanggaltransaksi.substring(3,5); */	
		var data = "TanggalTransaksi="+tanggaltransaksi+"&Keterangan="+des+"&TotalPengeluaran="+sumdebit;
		//alert(data);
		
		$.ajax({				
			url : "<?php echo base_url()?>pengeluaran/insertPengeluaran",
			type : "post",
			data : data,	
			dataType : "json",
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					//alert("sukses");
					
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
		});
		
		var data2="";
		for(k=0;k<flag;k++)
		{
		//alert(flag);
			data2 = "IdPengeluaran=-&IdExpense="+$('#NamaExpense'+k).val()+"&Nominal="+$('#Nominal'+k).val();
			//lert(data2);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>pengeluaran/insertDetailPengeluaran",
				data: data2,
				success: function(msg){
					var hasil = JSON.parse(msg);
					if(hasil == true || hasil == "true"){
						
					}else{
						alert(hasil.msg);
					}
				}
			});
		}
		location.reload();
	}
	
	
	function updateData(){
		var flagdebit=0;
		var flagkredit=0;
		var sumkredit=0;
		var sumdebit=0;
		var flagakun=0;
		for(i=0;i<flag;i++)
		{ 
			//alert(i);
			for(j=flag;j>i;j--)
			{
				if($('#NamaExpense'+i).val()==$('#NamaExpense'+j).val())
				{
					flagakun=1;
					break;
				}
			}			
			flagdebit=0;
			if($('#Nominal'+i).val()=="")
			{
				flagdebit=2;
				break;
			}
			if(isNaN($('#Nominal'+i).val())==true)
			{
				flagdebit=1;
				break;
			}
			else{
				
				sumdebit+=parseInt($('#Nominal'+i).val());
				
			}

		}		
		if($('#TanggalTransaksi').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Tanggal Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if(flagdebit==2){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nominal harus disi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagdebit==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nominal harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagakun==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Beban yang dibayar tidak boleh sama</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		var tanggaltransaksi=$('#TanggalTransaksi').val();
		var des=$('#Keterangan').val();
		if(des==""||des==null)
			des="-";
		/* if(tanggaltransaksi != "" || tanggaltransaksi != null)
		tanggaltransaksi= tanggaltransaksi.substring(7,4) +"-"+tanggaltransaksi.substring(0,2) +"-"+tanggaltransaksi.substring(3,5); */	
		var data = "IdPengeluaran="+$('#IdPengeluaran').val()+"&TanggalTransaksi="+tanggaltransaksi+"&Keterangan="+des+"&TotalPengeluaran="+sumdebit;
		
			$.ajax({				
			url : "<?php echo base_url()?>pengeluaran/editPengeluaran",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					alert("sukses");
					
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
		
			$.ajax({				
			url : "<?php echo base_url()?>pengeluaran/deleteDetailPengeluaran",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					//alert("sukses");
					
				}
				else {
					
					setTimeout(function(){
						alert("gagal delete Detail");
					}, 2000);
				}
			}	
			});
		var data2="";
		//alert(flag);
		for(k=0;k<flag;k++)
		{
		
			data2 = "IdPengeluaran="+$('#IdPengeluaran').val()+"&IdExpense="+$('#NamaExpense'+k).val()+"&Nominal="+$('#Nominal'+k).val();
			//alert(data2);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>pengeluaran/insertDetailPengeluaran",
				data: data2,
				success: function(msg){
					var hasil = JSON.parse(msg);
					if(hasil == true || hasil == "true"){
						
					}else{
						alert(hasil.msg);
					}
				}
			});
		}
		location.reload();
	}
	
	
	
	function deleteData(id){		
			var data= "IdPengeluaran="+id;
			//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>pengeluaran/deletePengeluaran",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
			
			$.ajax({				
			url : "<?php echo base_url()?>pengeluaran/deleteDetailPengeluaran",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					setTimeout(function(){
			
						location.reload();
						}, 1500);
				}
				else {
					
					
				}
			}	
			});
	}
	
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		//$("#NamaSupplier").val('');
		$("#Keterangan").val('');
		$("#TanggalTransaksi").val('');
		$("#NamaBarang0").val('');
		$("#Qty0").val('');
		$("#Harga0").val('');
		$('#dialog').modal('show'); 
		$('#btnSave').html("Save");
		$('#btnSave').attr("onclick","addNew()");
		flag=1;
		iCnt=1;		
	}

	function setUpdate(IdPengeluaran){
		$('#dialog').modal('show');  
		//$('#IdPembelian').val(IdPembelian);			
		var data = "IdPengeluaran="+IdPengeluaran;
		//alert(data);
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>pengeluaran/getPengeluaran",
			data: data,
			success: function(msg){			
				var hasil = JSON.parse(msg);
				var text='';
				//alert(hasil[0].NoJurnal);
				$("#IdPengeluaran").val(hasil[0].IdPengeluaran);
				$("#Keterangan").val(hasil[0].Keterangan);
				$("#TanggalTransaksi").val(hasil[0].TanggalPengeluaran);
				$("#NamaExpense0").val(hasil[0].IdExpense);
				$("#Nominal0").val(hasil[0].Nominal);
				for(z=1;z<hasil.length;z++)
				{
					var hasil3 =  <?=$expense?>;
					var test='<tr><td><select class="form-control" name="NamaExpense'+z+'" id="NamaExpense'+z+'">';
					for(i=0;i<hasil3.length;i++){
						test+='<option value="'+hasil3[i].IdExpense+'">'+hasil3[i].IdExpense+'-'+hasil3[i].Nama+'</option>'}				
					test+='</select></td><td><input class="form-control" type=text name="Nominal" id=Nominal' + z + ' ' +'/></td></tr>';
					
					$('#coba1').append(test);
					$("#NamaExpense"+z).val(hasil[z].IdExpense);
					$("#Nominal"+z).val(hasil[z].Nominal);
				}
				iCnt = hasil.length;
				flag = hasil.length;				
			}
		});
		
		$('#btnSave').attr("onclick","updateData()");
		//$('#btnSave').html("Delete");			
	}
	function setDelete(IdPengeluaran){
		$('#dialogDelete').modal('show');  	
		var data = "IdPengeluaran="+IdPengeluaran;
		$('#deleteBtn').attr("onclick","deleteData('"+IdPengeluaran+"')");	
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>pengeluaran/getPengeluaran",
			data: data,
			success: function(msg){			
				var hasil = JSON.parse(msg);
				var text='';	
				$("#delId").val(hasil[0].IdPengeluaran);
				$("#delKeterangan").val(hasil[0].Keterangan);
				$("#delTanggal").val(hasil[0].TanggalPengeluaran);				
				text+= '<table class="table table-bordered table-striped">';
				text+= '<tr><td>Id Expense</td><td>Nama Expense</td><td>Nominal</td></tr>';
				for(h=0;h<hasil.length;h++)
				{
					text+= '<tr><td>'+hasil[h].IdExpense+'</td><td>'+hasil[h].NamaExpense+'</td><td>'+hasil[h].Nominal+'</td></tr>';
				}
				text+='</table>';
				text+='<div>Total Pengeluaran='+hasil[0].TotalPengeluaran+'</div>';				
				$('#detailTrans').html(text);			
			}
		});
	}
		
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>;
				

            if(hasil.length>0)
			{
				$("#grid").shieldGrid({
					dataSource: {
						data: hasil
					},
					sorting: {
						multiple: true
					},
					rowHover: false,
					paging: true,
					selection: 
					{
						type: "cell",
						multiple: false,
						toggle: false
					},
					events: {
						selectionChanged: onSelectedChanged,
						doubleClick: onDouble
					},				
					columns: [
					{ field: "IdPengeluaran", title: "IdPengeluaran",attributes: {style: "text-align: center;"}},
					{ field: "TanggalPengeluaran", title: "TanggalPengeluaran",attributes: {style: "text-align: center;"}},
					{ field: "Keterangan", title: "Keterangan" , attributes: {style: "text-align: center;"}},
					{ field: "TotalPengeluaran", title: "TotalPengeluaran",attributes: {style: "text-align: right;"}},					
					{
						width: "13%",
						title: " ",
						buttons: [						
							{
								cls: "button button-primary",
								caption: " Detail",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									var id=$("#grid").swidget().dataItem(rowIndex).IdPengeluaran;			
									setUpdate(id); 
								}
							},
							{
								cls: "button button-primary",
								caption: " Delete",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									var id=$("#grid").swidget().dataItem(rowIndex).IdPengeluaran;			
									setDelete(id); 
								}
							}
						]
					}
					]
				});       
			}			
        });
		
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
