<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url','cookie'));
		$this->load->library(array('session'));
		//$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function antiinjection($data)
	{
		$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter_sql;
	}	
		
	public function doLogout(){


	 $array_items = array('UserId'=>'', 'NIP' => '','Nama' => '', 'Username'=>'', 'Email'=>'' ,'Role' => ''  );
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url(). "home");

	}
	
	public function cekLogin(){
		
		$username=$this->antiinjection($this->input->post('username'));
		$captcha=trim(strtolower($this->input->post('captcha')));
		$psw=$this->antiinjection($this->input->post('password'));
		$flag = $this->input->post('flag');
		
		if($captcha == "" && $flag > 3)
		{
			$data["status"] = "gagal";
			$data["msg"] = "Captcha Code harus diisi";		
		}
		else
		{
			session_start();

			if($flag > 3)
			{
				if($captcha != $_SESSION['captcha'] )
				{
					$data["status"] = "gagal";
					$data["msg"] = "Code Captcha tidak cocok";	
				}
				else
				{
 
				$result = $this->all_model->query_data("SELECT * FROM  msuser WHERE username = '$username' and password='".md5($psw)."'", true);	
				$var = $this->all_model->query_data("SELECT NoRekening FROM  variabel", true);	
				if($result){
					$newdata = array(
								'UserId'		=> $result["IdUser"],
								'NIP'		=> $result["NIP"],
								'Nama'		=> $result["Nama"],
								'Username'		=> $result["Username"],
								'Email'		=> $result["Email"],
								'Role'		=> $result["IdRole"],
								'NoRekPerusahaan' => $var["NoRekening"],
						   );
						 
					$this->session->set_userdata($newdata);
					$data["status"] = "sukses";
					$data["msg"] = $newdata;
				}
				else{
					$data["status"] = "gagal";
					$data["msg"] = "Username dan Password tidak cocok";
				}
				}
			}
			else
			{
	
			$result = $this->all_model->query_data("SELECT * FROM  msuser WHERE username = '$username' and password='".md5($psw)."'", true);	
				$var = $this->all_model->query_data("SELECT NoRekening FROM  variabel", true);	
				if($result){
					$newdata = array(
								'UserId'		=> $result["IdUser"],
								'NIP'		=> $result["NIP"],
								'Nama'		=> $result["Nama"],
								'Username'		=> $result["Username"],
								'Email'		=> $result["Email"],
								'Role'		=> $result["IdRole"],
								'NoRekPerusahaan' => $var["NoRekening"],
						   );
						 
					$this->session->set_userdata($newdata);
					$data["status"] = "sukses";
					$data["msg"] = $newdata;		
				}
				else{
					$data["status"] = "gagal";
					$data["msg"] = "Username dan Password tidak cocok";
				}
			}
		}
			
				
		echo json_encode($data);
		exit();
	}

	
	public function profile(){
	
		$data['title']="Profile";
		$data['page_title']="CIPS";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('profile_view',$data);
		$this->load->view('home_footer');
	}
	
	public function updateProfile()
	{
		$NoRekPerusahaan = $this->antiinjection($this->input->post('NoRekPerusahaan'));
	
		$data = array(
			'NoRekening' => $NoRekPerusahaan,
			
		);
		$where = array(	
							
		);
		
		$query = $this->all_model->update_data("variabel", $data , $where);
		$var = $this->all_model->query_data("SELECT NoRekening FROM  variabel", true);
		$newdata = array(
					'NoRekPerusahaan' => $var["NoRekening"],
			   );	 
		$this->session->set_userdata($newdata);
		echo json_encode($query);
		exit();
	
	}
	
	public function index(){
		$data['page_title']="CIPS";
		$data['title']="Home";
		$data['page']="home";
		$data['nama']=$this->session->userdata('Nama');
		$this->load->view('main',$data);
		
		return $data;					
	}
	private function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}	
}


