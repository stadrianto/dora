<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manageacara extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		$this->load->model('all_model2');
		date_default_timezone_set('Asia/Jakarta');
	}

	
	
	public function getDataStation()
	{
		$join = array();
		$where = array(
			'a.MS_ActiveYN'	=>"Y"
		);	
		$result = $this->all_model->get_data("MS_KodeStation as KodeStation,MS_NamaStation as NamaStation", "m_station a",$join, $where, array(),false);
		if(!$result){
			$result= "No Data";
		}
		echo json_encode($result);
		exit();
	}
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.MHA_NamaAcara' => $this->input->post('search')
			);
			$where = array(
				'a.MHA_KodeStation' => $this->input->post('menu'),
			);
		}else{
			$search = array(
					'a.MHA_NamaAcara' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
				array('table'=>'m_station b','field' => 'a.MHA_KodeStation = b.MS_KodeStation','method'=>'Left'),array('table'=>'m_genre c','field' => 'a.MHA_GenID = c.Gen_ID','method'=>'Left'),
			);
			
		$result = $this->all_model->get_data("MHA_KodeAcara as KodeAcara,MHA_NamaAcara as NamaAcara,Gen_Name as Genre,Gen_ID as Gen_ID,MHA_KodeStation as KodeStation, MHA_UpdateID as UpdateID, MS_NamaStation as NamaStation,MHA_BaseRate30s as BaseRate30s", "mh_acara a",$join, $where, $search, false, $perPage, $segmen, false,"MHA_KodeAcara","ASC");
		$result2 = $this->all_model->get_data("MHA_KodeAcara as KodeAcara,Gen_ID as Gen_ID,Gen_Name as Genre,MHA_NamaAcara as NamaAcara,MHA_KodeStation as KodeStation, MHA_UpdateID as UpdateID, MS_NamaStation as NamaStation,MHA_BaseRate30s as BaseRate30s", "mh_acara a",$join, $where, $search, false);
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function addAcara(){

		$namaacara = $this->input->post('namaacara');
		$station = $this->input->post('station');
		$genre = $this->input->post('genre');
		$baserate = $this->input->post('baserate');
		
		$data = array(
				'MHA_NamaAcara' => $namaacara,
				'MHA_ActiveYN' => 'Y',
				'MHA_GenID' => $genre,
				'MHA_KodeStation' => $station,
				'MHA_BaseRate30s' => $baserate,
				'MHA_UpdateID' => $this->session->userdata("UserId"),
			
			);
		$query = $this->all_model->insert_data("mh_acara", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function editAcara(){

		$namaacara = $this->input->post('namaacara');
		$station = $this->input->post('station');
		$genre = $this->input->post('genre');
		$kodeacara = $this->input->post('kodeacara');
		$baserate = $this->input->post('baserate');
		
		$data = array(
				'MHA_NamaAcara' => $namaacara,
				'MHA_KodeStation' => $station,
				'MHA_GenID' => $genre,
				'MHA_BaseRate30s' => $baserate,
				'MHA_UpdateID' => $this->session->userdata("UserId"),
			
			);
		$where = array(	
					'MHA_KodeAcara'			=> $kodeacara,
				
		);	
		$query = $this->all_model->update_data("mh_acara", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function getStation()
	{	
		$search = array();
		$join = array();
		$where = array('a.MS_ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("MS_KodeStation as KodeStation,MS_NamaStation as NamaStation", "m_station a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getGenre()
	{	
		$search = array();
		$join = array();
		$where = array('a.Gen_ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("Gen_ID as Gen_ID,Gen_Name as Genre", "m_genre a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
		
	public function getNameStation($station)
	{
		$result = $this->all_model->query_data("SELECT MS_NamaStation FROM m_station where MS_KodeStation='".$station."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result["MS_NamaStation"];
	
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="Materi";
		$data['page_title']="Media Planning Pharos";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data2['data3'] = json_encode($this->getStation());
		$data2['data4'] = json_encode($this->getGenre());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('acara_view',$data2);
		$this->load->view('home_footer');
	}
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


