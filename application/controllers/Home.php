<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url','cookie'));
		$this->load->library(array('session'));
		//$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function index(){
		if($this->session->userdata("Username")=="")
		redirect(base_url(). "login","refresh");
		
		$data['page_title']="Dorayaki";
		$data['title']="Home";
		$data['page']="home";
		$data['nama']=$this->session->userdata('Username');
		$this->load->view('main',$data);
		
		return $data;					
	}
	private function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}	
}


