<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KategoriBesar extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	

	public function editKategoriBesar(){
		$IdKategoriBesar = $this->input->post('IdKategoriBesar');
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'IdKategoriBesar' => $IdKategoriBesar,
				'Keterangan' => $this->input->post('Keterangan'),
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array("IdKategoriBesar" => $this->input->post('IdKategoriBesar2'));
		$query = $this->all_model->update_data("kategoribesar", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	

	public function addKategoriBesar(){
		
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'IdKategoriBesar' => $this->input->post('IdKategoriBesar'),
				'Keterangan' => $this->input->post('Keterangan'),
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		
		$query = $this->all_model->insert_data("kategoribesar", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function deleteKategoriBesar()
	{
		$IdKategoriBesar = $this->input->post('IdKategoriBesar');
		$where = array('IdKategoriBesar' =>$IdKategoriBesar );
		$query = $this->all_model->delete_data("kategoribesar", $where);
		echo json_encode($query);
		exit();
	}
	
	public function generateKategoriBesar()
	{ 
		$data["status"] = "sukses";
		$data["msg"] = $this->getAllKategoriBesar(100,0,false);
		echo json_encode($data);
		exit();
	}
	
	public function checkKategoriBesar()
	{ 
		$IdKategoriBesar = $this->input->post('IdKategoriBesar');
		$result = $this->all_model->query_data("SELECT * FROM kategoribesar WHERE IdKategoriBesar = '".$IdKategoriBesar."'", true);	
			if(!$result)
				echo "false";
			else{
				echo "true";
			}
		exit();
	}

	public function getAllKategoriBesar($perPage=100, $segmen=0, $request = true){
		$current = $this->input->post('page');
		
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
			);
		$join = array(
		);
		$where = array(
		);

		$result = $this->all_model->get_data("IdKategoriBesar as IdKategoriBesar,Keterangan as Keterangan, UpdateID as UpdateID,ActiveYN as ActiveYN,UpdateTime as UpdateTime", "kategoribesar a",$join, $where, $search, false, $perPage, $segmen, false,"IdKategoriBesar","ASC");
		$result2 = $this->all_model->get_data("IdKategoriBesar as IdKategoriBesar,Keterangan as Keterangan, UpdateID as UpdateID,ActiveYN as ActiveYN,UpdateTime as UpdateTime", "kategoribesar a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllKategoriBesar($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				
				foreach($result as $key => $value){	
				}
				
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllKategoriBesar($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				}
			}
			echo json_encode($result);
			exit();
		}
	}

	
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		
		$data['title']="CIPS - Master KategoriBesar";
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['page_title']="CIPS - Master KategoriBesar";
		$data2['data'] = json_encode($this->getAllKategoriBesar($config['per_page'], $config['segmen'],false));
		//$data['data'] = json_encode($this->getNews());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('kategoribesar_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


