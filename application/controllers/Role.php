<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Role extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($request = true){
		$search = array();
		$where = array();
		$join = array(
			//array('table'=>'msjabatan b','field' => 'a.idjabatan = b.idjabatan','method'=>'Left')
			);
		$result2 = $this->all_model->get_data("RoleId, RoleName", "msrole",$join, $where, $search, false,true);
		return $result2;	
		exit();
	}

	public function ajax_finder(){
			$msg = array();
			if ( $_POST ){
				$key = $this->input->post("key");
				if ( empty($key) ){
					$msg['type'] = 'failed';
					$msg['msg'] = "Key Empty.";
				}
				else{
					$data = $this->all_model->view("msrole", "RoleId, RoleName", array("RoleId"=>$key));
					if ( $data == '0' ){
						$msg['type'] = "failed";
						$msg['msg'] = "Data tidak ditemukan.";
					}
					else{
						//foreach($data as $a) xss_filter($a);
						$msg['type'] = "done";
						$msg['msg'] = $data;
					}
				}
			}
			else{
				$msg['type'] = 'failed';
				$msg['msg'] = "Parameter tidak ditemukan.";
			}
			//die (var_dump($msg));
			echo json_encode($msg);
		}

	public function insert(){
		
		$data = array( //$this->input->post('Id')
		'RoleName' => $this->input->post('Nama')
			);
		$query = $this->all_model->insert_data("msrole", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function edit(){
		$data = array(
			'RoleName' => $this->input->post('Nama')
			);
		$where = array(	
					'RoleId' => $this->input->post('Id'),
				
		);
		$query = $this->all_model->update_data("msrole", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function delete(){
		$Id = $this->input->post('Id');
		$where = array('RoleId'=>$Id);
		$query = $this->all_model->delete_data("msrole", $where);
		echo json_encode($query);
		exit();
	}

	
	
	public function index(){
		if($this->session->userdata("Username")=="")
		redirect(base_url(). "login","refresh");
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Master Role";
		$data['page']="role_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['data'] = $this->getData(false);
		//$data2['data4'] = json_encode($this->getSatuan());
		//$data2['data3'] = json_encode($this->getKategoriBesar());
		//$data['include']=$this->load->view('script','',true);
		//die (var_dump($data));
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}