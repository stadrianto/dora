		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Customer<small> Data Customer</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Customer List </h3>
                        </div>
                        <div class="panel-body">
                            <table id="example1" class="table table-bordered table-striped">
								<thead>
								  <tr>
									<th>Customer Id</th>
									<th>Customer Name</th>
									<th>Phone</th>
									<th>Email</th>
									<th>Action</th>
								  </tr>
								</thead>
								<tbody>
									<?php
										//die (var_dump($data));
										if ( isset($data) and $data != '0' ){
											$no = 1;
											foreach($data as $row){
									?>
									<tr>
										<td><?php echo $row->CustomerId?></td>
										<td><?php echo $row->CustomerName?></td>
										<td><?php echo $row->PhoneNumber?></td>
										<td><?php echo $row->EmailAddress?></td>							
										<td>
											<div class="btn-group" style="width:100px">
											  <button type="button" class="btn btn-primary btn-edit" data-key="<?php echo $row->CustomerId?>">Action</button>
											  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											  </button>
											  <ul class="dropdown-menu" role="menu" style="margin-left:-70px;position:relative">
												<li><a href="javascript:void(0);" class="btn-edit" data-key="<?php echo $row->CustomerId?>"><i class="fa fa-edit"></i> Edit</a></li>
												<li><a href="javascript:void(0);" class="btn-rem" data-key="<?php echo $row->CustomerId?>"><i class="fa fa-times"></i> Remove</a></li>
											  </ul>
											</div>
										</td>
									</tr>
									<?php $no++;}}?>
								</tbody>
							</table>		
                        </div>
                    </div>
                </div>
            </div>         		 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> New Customer </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div style="display:none" class="form-group">
								<label>CustomerId</label>
								<input id="txtid" name="txtid" disabled="disabled" value="" class="form-control">
							</div>							
							<div class="form-group">
								<label>Customer Name</label>
								<input id="txtname" name="txtname" class="form-control">			
							</div>							
							<div class="form-group">
								<label>Phone Number</label>
								<input id="txtphone" name="txtphone" class="form-control" >
							</div>
							<div class="form-group">
								<label>Email Address</label>
								<input id="txtemail" name="txtemail" class="form-control" >
							</div>
							<div class="form-group">
								<label>Gender</label>
								<select id="ddlgender" name="ddlgender" class="form-control">
									<option value="0">Select</option>
									<option value="M">Male</option>
									<option value="F">Female</option>
								</select>
							</div>
							<div class="form-group">
								<label>Birth Date</label>
								<div class="input-group"  data-provide="datepicker">
								    <input id="txttgl" name="txttgl" type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>				
							</div>													
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>	
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes03" id="tes03" class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Client </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-6" style="float:none;">
							<h4>Yakin Hapus Data Ini?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Id</th>
										<th>Name</th>
										<th>Phone Number</th>
										<th>Email Address</th>
										<th>Gender</th>
										<th>Birthdate</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div id="deleteid"></div></td>
										<td><div id="deletename"></div></td>
										<td><div id="deletephone"></div></td>
										<td><div id="deleteemail"></div></td>
										<td><div id="deletegender"></div></td>
										<td><div id="deletedate"></div></td>
									</tr>							
								</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="button" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	$(document).ready(function () {
		$("#example1").DataTable({
			scrollX : true,
			scrollCollapse : true
		});

		$("#example1").on("click", ".btn-edit", function(){
			var data = {"key" : $(this).attr("data-key")};
			$.ajax({
				url : "<?php echo base_url()?>/Customer/ajax_finder",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){					
					if ( data.type === "done" ){
						console.log(data.msg[0].Gender);
						setUpdate(data.msg[0].CustomerId, data.msg[0].CustomerName, data.msg[0].PhoneNumber, data.msg[0].EmailAddress, data.msg[0].Gender,data.msg[0].Birthdate);
					}
					else{
						alert(data.msg);
					}
				},
			   	error: function(ts) { alert(ts.responseText) }
			});
		});
		$("#example1").on("click", ".btn-rem", function(){
			var data = {"key" : $(this).attr("data-key")};
			$.ajax({
				url : "<?php echo base_url()?>/Customer/ajax_finder",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){					
					if ( data.type === "done" ){
						setDelete(data.msg[0].CustomerId, data.msg[0].CustomerName, data.msg[0].PhoneNumber, data.msg[0].EmailAddress, data.msg[0].Gender,data.msg[0].Birthdate);													
					}
					else{
						alert(data.msg);
					}
				},
			   	error: function(ts) { alert(ts.responseText) }
			});
		});

		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd', autoclose: true });

		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
		});
		
		$('#tes03').click(function(e) {
			$('#dialogDelete').modal('hide');
		});


	});

	function addNew(){
		if($('#txtname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Customer Name must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Phone must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtemail').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Email must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#ddlgender').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Gender must be chosen</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txttgl').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Birth Date must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		
		var tanggal= new Date();
		var dd = tanggal.getDate();
		var mm = tanggal.getMonth()+1; //January is 0!
		var yyyy = tanggal.getFullYear();

		if(dd<10) {
		    dd = '0'+dd;
		} 

		if(mm<10) {
		    mm = '0'+mm;
		} 
		//tanggal =yyyy +'-' +mm+'-'+dd;

		var id=(tanggal.getFullYear()+"")+""+(('0'+(tanggal.getMonth()+1)).slice(-2));

		var data= "Name="+$('#txtname').val()+"&Phone="+$('#txtphone').val()+"&Email="+$('#txtemail').val()+"&Gender="+$('#ddlgender').val()+"&Birthdate="+$('#txttgl').val()+"&Id="+id;
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>Customer/insert",
			type : "post",
			data : data,	
			dataType : "json",			
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("failed");
					}, 2000);
				}
			}	
			});			
	}
	
	function updateData(){
		if($('#txtname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Customer Name must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Phone must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtemail').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Email must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#ddlgender').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Gender must be chosen</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txttgl').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Birth Date must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}


		var data= "Name="+$('#txtname').val()+"&Phone="+$('#txtphone').val()+"&Email="+$('#txtemail').val()+"&Gender="+$('#ddlgender').val()+"&Birthdate="+$('#txttgl').val()+"&Id="+$('#txtid').val();
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>Customer/edit",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			},
			error: function(ts) { alert(ts.responseText) }	
			});
			
	}
	function deleteData(id){		
		var data="Id="+id;
		//alert(data);
		$.ajax({				
		url : "<?php echo base_url()?>Customer/delete",
		type : "post",
		data : data,	
		dataType : "json",				
		success : function(msg){
			var hasil = JSON.parse(msg);
			if ( hasil == true || hasil == "true"){					
				
				location.reload();
			}
			else {	
				
				setTimeout(function(){
					alert(hasil);
				}, 2000);
			}
		}
		});
	}

	function numberWithCommas(x) {
		var parts = x.toString().replace(/,/g,"")
	    parts = parts.toString().split(".");
	    //alert(parts[0])
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#txtid').val('');
		$('#txtname').val('');
		$('#txtphone').val('');
		$('#txtemail').val('');
		$('#txttgl').val('');
		$('#ddlgender').val('0');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");
	}
	function setUpdate(id,nama,phone,email,gender,birthdate){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#txtid').val(id);
		$('#txtname').val(nama);
		$('#txtphone').val(phone);
		$('#txtemail').val(email);
		$('#ddlgender').val(gender);
		$('.datepicker').datepicker('update', birthdate);
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(id,nama,phone,email,gender,date){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(id);
		$('#deletename').html(nama);
		$('#deleteemail').html(email);
		$('#deletephone').html(phone);
		$('#deletegender').html(gender);
		$('#deletedate').html(date);
		$('#deleteBtn').attr("onclick","deleteData('"+id+"')");
	}
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
             
            
        });
		
		
    </script>
</body>
</html>