<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pembayaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getInvoiceCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(NoInvoice,4) as NoInvoice FROM pembayaran ORDER BY NoInvoice DESC", true);
		$result["NoInvoice"] = $result["NoInvoice"]+1;
		if($result["NoInvoice"] < 10)
			$result["NoInvoice"] = "BY0000".$result["NoInvoice"];
		else if($result["NoInvoice"]< 100)
			$result["NoInvoice"] = "BY000".$result["NoInvoice"];
		else if($result["NoInvoice"]< 1000)
			$result["NoInvoice"] = "BY00".$result["NoInvoice"];
		else if($result["NoInvoice"]< 10000)
			$result["NoInvoice"] = "BY0".$result["NoInvoice"];	
		else
			$result["NoInvoice"] = "BY".$result["NoInvoice"];
		return $result["NoInvoice"];
	}

	public function getDataDetailPembayaranPrint($noinvoice)
	{
		$totalpembelian = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.NoInvoice' => $noinvoice
		);
		
		$join = array(
			//array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),		
		);	
	
		$result = $this->all_model->get_data("IdDetailPembayaran,NoInvoice,Jumlah,Tanggal", "detailpembayaran a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				//$totalpembelian += ($result[$key]['Harga']*$result[$key]['Jumlah']);
			}
			return $result;
			
		}
		else
			return "No Data";
		
	}

	
	public function printPembayaran($NoInvoice)
	{
	
		
		$this->load->library('fpdf17/fpdf');
	
		$result2 = $this->all_model->query_data("SELECT NoInvoice,a.NoOrder,a.Nominal,JenisPembayaran,TotalPembayaran,TanggalJatuhTempo,c.Nama as Nama,b.IdCustomer,b.Project FROM pembayaran a join trorder b on a.NoOrder = b.NoOrder join mscustomer c on b.IdCustomer = c.IdCustomer WHERE a.NoInvoice='".$NoInvoice."'", true);
		$result = $this->getDataDetailPembayaranPrint($NoInvoice);
$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Laporan Pembayaran Project',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',12);
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->Cell(190,6,'No Invoice : '.$NoInvoice.'',0,0,'C');
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Customer : '.$result2['Nama'].'',0,0,'C');
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Project : '.$result2['Project'].'',0,0,'C');			
		$this->fpdf->Ln(15);
		
		$y_axis_initial = 86;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(35);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'No Invoice',1,0,'C',1);
		$this->fpdf->Cell(50,6,'Tanggal Pembayaran',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Jumlah',1,0,'C',1);

		

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(35);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'No Invoice',1,0,'C',1);
		$this->fpdf->Cell(50,6,'Tanggal Pembayaran',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Jumlah',1,0,'C',1);

		

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

	
		$i++;
		$no++;
		$this->fpdf->SetX(35);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$NoInvoice,1,0,'C',0);
		$this->fpdf->Cell(50,6,$result[$key]["Tanggal"],1,0,'C',0);
		$this->fpdf->Cell(45,6,$result[$key]["Jumlah"],1,0,'C',0);
	
		

		$this->fpdf->Ln();

		}
		}

		//buat footer
		$sisapembayaran = $result2['Nominal']-$result2['TotalPembayaran'];
		if($sisapembayaran <= 0) $sisapembayaran = 0;
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(303,6,"Sisa Pembayaran : ".$this->all_model->rp($sisapembayaran)."",0,0,'C');
		$this->fpdf->Ln();
		
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();		
		$this->fpdf->Output('Surat Jalan'.date("F Y").'.pdf', 'I');	
	
	}	
	
	
	public function getData($perPage=10, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				//'a.Nama' => $this->input->post('search')
			);
			$where = array(
			
			);
		}else{
			$search = array(
					//'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
			array('table'=>'trorder b','field' => 'a.NoOrder = b.NoOrder','method'=>'Left'),
			array('table'=>'mscustomer c','field' => 'b.IdCustomer = c.IdCustomer','method'=>'Left'),
			array('table'=>'mskaryawan d','field' => 'a.PICFollowUp = d.NIP','method'=>'Left')
		);
		
		
		$result = $this->all_model->get_data("NoInvoice,a.NoOrder as NoOrder,b.IdCustomer as IdCustomer,c.Nama as Nama,a.Nominal,JenisPembayaran,TanggalJatuhTempo,TanggalReminder1,TanggalReminder2,TanggalPembayaran,StatusPembayaran,Keterangan,PICFollowUp,d.Nama as NamaFollowUp,HargaNego,HargaPPN,UangMuka,TotalPembayaran", "pembayaran a",$join, $where, $search, false, $perPage, $segmen, false,"NoInvoice","DESC");
		
		$result2 = $this->all_model->get_data("NoInvoice,a.NoOrder as NoOrder,b.IdCustomer as IdCustomer,c.Nama as Nama,a.Nominal,JenisPembayaran,TanggalJatuhTempo,TanggalReminder1,TanggalReminder2,TanggalPembayaran,StatusPembayaran,Keterangan,PICFollowUp,d.Nama as NamaFollowUp,HargaNego,HargaPPN,UangMuka,TotalPembayaran", "pembayaran a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				$sisa = $result[$key]['HargaNego']+$result[$key]['HargaPPN']-$result[$key]['UangMuka']-$result[$key]['TotalPembayaran'];
				if($sisa <= 0 ) $sisa = 0;
				$result[$key]['SisaPembayaran'] = $this->all_model->rp($sisa);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				$sisa = $result[$key]['HargaNego']+$result[$key]['HargaPPN']-$result[$key]['UangMuka']-$result[$key]['TotalPembayaran'];
				if($sisa <= 0 ) $sisa = 0;
				$result[$key]['SisaPembayaran'] = $this->all_model->rp($sisa);			
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertPembayaran(){
		
		$data = array(
		'NoInvoice' => $this->getInvoiceCode(),		
		'NoOrder' => $this->input->post('NoOrder'),	
		'Nominal' => $this->input->post('Nominal'),	
		'JenisPembayaran' => $this->input->post('JenisPembayaran'),
		'TanggalJatuhTempo' => $this->input->post('TanggalJatuhTempo'),
		'TanggalReminder1' => $this->input->post('TanggalReminder1'),
		'TanggalReminder2' => $this->input->post('TanggalReminder2'),
		'StatusPembayaran' => 'Belum Bayar',
		'Keterangan' => $this->input->post('Keterangan'),
		'PICFollowUp' => $this->input->post('PICFollowUp'),
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")		
			);
		$query = $this->all_model->insert_data("pembayaran", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	
	public function updatePembayaran(){
	
		$data = array(
		'TanggalPembayaran' => $this->input->post('TanggalPembayaran'),
		'StatusPembayaran' => $this->input->post('StatusPembayaran'),
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")		
			);
		$where = array(	
				'NoInvoice' => $this->input->post('NoInvoice'),	
				
		);	
		$query = $this->all_model->update_data("pembayaran", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	

	
	
	public function deletepembayaran()
	{
		$NoInvoice = $this->input->post('NoInvoice');
		$where = array('NoInvoice'=>$NoInvoice);
		$query = $this->all_model->delete_data("pembayaran", $where);
		echo json_encode($query);
		exit();
	}
	
	public function insertDetailPembayaran(){
		
		$data = array(
		'NoInvoice'  => $this->input->post('NoInvoice'),
		'Jumlah' => $this->input->post('Jumlah'),	
		'Tanggal' => $this->input->post('Tanggal'),		
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("detailpembayaran", $data );
		$this->calculatePembayaran($this->input->post('NoInvoice'));
		echo json_encode($query);
		exit();
	}

	public function getDetailOrderPembayaran($IdDetailPembayaran)
	{
		$result = $this->all_model->query_data("
		SELECT * FROM detailpembayaran a join pembayaran b on a.NoInvoice=b.NoInvoice join trorder c on b.NoOrder = c.NoOrder join mscustomer d on c.IdCustomer= d.IdCustomer where a.IdDetailPembayaran='".$IdDetailPembayaran."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function printKwitansi($IdDetailPembayaran)
	{
		
		
		
		$this->load->library('fpdf17/fpdf');
		$result2 = $this->getDetailOrderPembayaran($IdDetailPembayaran);
		
	
	
		require "classConversi.php";
		$convert = new classConversi();
		
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		
		$arraybln=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'); 
		$bln=$arraybln[date('n')-1]; 
		$thn=date('Y');
		$tgl=date('d'); 
	


	
		$this->fpdf->setMargins(10,6,10); 
		$this->fpdf->AddPage(); 
		$this->fpdf->SetFont('Arial','B',13); 
		$this->fpdf->Cell(0,5,"PT Citra Inti Prima Sejati",0,1,'L'); 
		$this->fpdf->SetFont('Arial','',11); 
		$this->fpdf->MultiCell(0,5,"Jln. Pemuteran Raya No.58 d/h 147 Kebun Pisang, Kav. POLRI Jelambar - Jakarta Barat"); 
		$this->fpdf->SetLineWidth(0.8);
		//$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		//$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Line(10,28,199,28);
		$this->fpdf->Ln(18); 
		$this->fpdf->SetFont('Arial','B',13);
		$this->fpdf->Cell(60,5,'',0,0,''); 
		$this->fpdf->Cell(0,5,"Tanda Bukti Pembayaran",0,1,'L'); 
		$this->fpdf->SetLineWidth(0.4); 
		$this->fpdf->Rect(60,30,80,13);/*ubah ukuran Kotak Judul -> Rect(sumbu x, sumbu y, lebar kotak,tinggi kotak)*/ 
		$this->fpdf->SetFont('Arial','',11);
		$this->fpdf->Ln(10); 
		$this->fpdf->Cell(45,5,'Telah Terima Dari :',0,0,'L'); 
		$this->fpdf->SetFont('Arial','',12);
		$this->fpdf->Cell(70,7,$result2["Nama"],0,1,'J'); 
		$this->fpdf->Line(50,56,150,56);
		$this->fpdf->Rect(50,61,115,10); 
		$this->fpdf->Rect(50,74,115,10);
		$this->fpdf->Ln(6); 
		$this->fpdf->Cell(40,20,'Uang Sejumlah :',0,0,'L'); 
		$grandtotal = 0;

		$grandtotal = $result2['Jumlah'];
		$this->fpdf->MultiCell(113,11,$convert->conversiAngka($grandtotal)." Rupiah",'J'); 
		if(strlen($convert->conversiAngka($grandtotal))>40)
		$lnBreak=8;
		else
		$lnBreak=16;
		$this->fpdf->Ln($lnBreak); 
		$this->fpdf->Cell(45,5,'Untuk Pembayaran :',0,0,'L');
		$this->fpdf->Cell(60,7,$result2["Project"],0,1,'J'); 
		$this->fpdf->Line(50,97,150,97); 
		$this->fpdf->Ln(18); 
		$this->fpdf->Cell(116,5,'',0,0,'');
		$this->fpdf->SetFont('Arial','U',12); 
		$this->fpdf->Cell(0,5,'Jakarta, '.$tgl.' '.$bln.' '.$thn,0,1,'L'); 
		//$this->fpdf->Ln(10); 
		$this->fpdf->SetFont('Arial','',14); 
		$this->fpdf->Cell(116,5,'Rp. '.number_format($grandtotal,0,",",".").',-',0,1,'L'); 
		$this->fpdf->Rect(8,116,50,10); 
		$this->fpdf->SetFont('Arial','',12); 
		$this->fpdf->Ln(16); 
		$this->fpdf->Cell(130-strlen("So Wee Ming"),5,'',0,0,'L'); 
		$this->fpdf->SetFont('Arial','BU',14);
		$this->fpdf->Cell(30,5,'( So Wee Ming )',0,1,'L'); 
		$this->fpdf->Output();
	
	
	
	}
	
	public function editDetailPembayaran(){
		$data = array(
		'NoInvoice'  => $this->input->post('NoInvoice'),
		'Jumlah' => $this->input->post('Jumlah'),	
		'Tanggal' => $this->input->post('Tanggal'),		
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdDetailPembayaran'=> $this->input->post('IdDetailPembayaran'),
				
		);	
		$query = $this->all_model->update_data("detailpembayaran", $data ,$where);
		$this->calculatePembayaran($this->input->post('NoInvoice'));
		echo json_encode($query);
		exit();
	}	
	
	public function deleteDetailPembayaran()
	{
		$IdDetailPembayaran = $this->input->post('IdDetailPembayaran');
		$where = array('IdDetailPembayaran'=>$IdDetailPembayaran);
		$query = $this->all_model->delete_data("detailpembayaran", $where);
		$this->calculatePembayaran($this->input->post('NoInvoice'));
		echo json_encode($query);
		exit();
	}
	
	public function calculatePembayaran($NoInvoice){
	
		$totalpembayaran = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.NoInvoice' => $NoInvoice
		);
		
		$join = array(
				
		);	
	
		$result = $this->all_model->get_data("IdDetailPembayaran as IdDetailPembayaran,NoInvoice as NoInvoice,a.Jumlah as Jumlah", "detailpembayaran a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				$totalpembayaran += $result[$key]['Jumlah'];
			}
		}
		
		$data = array(	
		'TotalPembayaran' => $totalpembayaran,	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			
		$where = array(	
					'NoInvoice'=> $NoInvoice,
				
		);	
		$query = $this->all_model->update_data("pembayaran", $data ,$where);
		
		
	}	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Pembayaran";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('pembayaran_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function addPembayaran(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Tambah Pembayaran";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('addpembayaran_view');
		$this->load->view('home_footer');
	}

	public function getDataDetailPembayaran($perPage=100, $segmen=0,  $request = true,$NoInvoice){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				//'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.NoInvoice' => $NoInvoice
			);
		}else{
			$search = array(
				//'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.NoInvoice' => $NoInvoice
			);
		}
		$join = array(
			//array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),		
		);			
						
		$result = $this->all_model->get_data("IdDetailPembayaran as IdDetailPembayaran,NoInvoice as NoInvoice,Tanggal,a.Jumlah as Jumlah", "detailpembayaran a",$join, $where, $search, false, $perPage, $segmen, false,"IdDetailPembayaran","ASC");
		
		$result2 = $this->all_model->get_data("IdDetailPembayaran as IdDetailPembayaran,NoInvoice as NoInvoice,Tanggal,a.Jumlah as Jumlah", "detailpembayaran a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailPembayaran(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Jumlah1'] = $this->all_model->rp($result[$key]['Jumlah']);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailPembayaran(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Jumlah1'] = $this->all_model->rp($result[$key]['Jumlah']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}

	public function getDetailPembayaran($NoInvoice)
	{
		$result = $this->all_model->query_data("SELECT * FROM pembayaran a join trorder b on a.NoOrder=b.NoOrder join mscustomer c on b.IdCustomer=c.IdCustomer where a.NoInvoice='".$NoInvoice."'", true);
		$SisaPembayaran = $result['HargaNego']+$result['HargaPPN']-$result['UangMuka']-$result['TotalPembayaran'];
		if($SisaPembayaran < 0) $SisaPembayaran = 0;
		$result['SisaPembayaran'] =  $this->all_model->rp($SisaPembayaran);
		$result['SisaPembayaran1'] =  $SisaPembayaran;
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function detailPembayaran($NoInvoice){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Pembayaran";
		$data2['data'] = json_encode($this->getDataDetailPembayaran($config['per_page'], $config['segmen'],false,$NoInvoice ));
		$data2['data2'] = json_encode($this->getDetailPembayaran($NoInvoice));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailpembayaran_view',$data2);
		$this->load->view('home_footer');
	}	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


