<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class generalledger extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function getNamaAkun()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("NoAkun as NoAkun,NamaAkun as NamaAkun", "tabelAkun a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function getDataJurnal($noakun,$tanggal1,$tanggal2)
	{
		$result = $this->all_model->query_data("SELECT a.NoJurnal as NoJurnal,Deskripsi as Deskripsi,TanggalTransaksi as TanggalTransaksi,Debit as Debit,Kredit as Kredit FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where status='Approved' and idjenisjurnal='1' and noakun='".$noakun."' and tanggaltransaksi between '".$tanggal1."' and '".$tanggal2."'", false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getJurnalAwal($noakun,$tanggal1,$tanggal2)
	{
		$result = $this->all_model->query_data("SELECT a.NoJurnal as NoJurnal,TanggalTransaksi as TanggalTransaksi,Debit as Debit,Kredit as Kredit FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where status='Approved' and noakun='".$noakun."' and tanggaltransaksi < '".$tanggal1."'", false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataHeader($noakun)
	{
		$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NamaAkun as NamaAkun,SaldoAwal as SaldoAwal,IdNeraca as IdNeraca FROM tabelakun a join kelompokakun b on a.idkelompokakun=b.idkelompokakun where a.noakun like '%".$noakun."%'", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataPrinter($id)
	{
		$result = $this->all_model->query_data("SELECT Nama as Nama from msuser where iduser=".$id, false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function printGeneralLedger()
	{
		//$noproject = $this->session->userdata('NoProject');
		//$dataproject = $this->getDataTrial();
		$noakunz=$this->input->post('NamaAkun');
		$tanggal1=$this->input->post('dob');
		$tanggal2=$this->input->post('dob1');
		$qtanggal1=substr($tanggal1,6,4).'-'.substr($tanggal1,0,2).'-'.substr($tanggal1,3,2);
		$qtanggal2=substr($tanggal2,6,4).'-'.substr($tanggal2,0,2).'-'.substr($tanggal2,3,2);	
		$result2 = $this->getDataHeader($noakunz);			
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('L','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		
		//$this->fpdf->Image('images/logo.png',10,0,50,0,'','http://www.cips.or.id/'); 
		
		
		for($z=0;$z<count($result2);$z++)
		{	
			$this->fpdf->AddPage();
			$y_axis_initial = 40;
			$saldo=floatval($result2[$z]["SaldoAwal"]);
			$result3 = $this->getJurnalAwal($result2[$z]["NoAkun"]."",$qtanggal1,$qtanggal2);
			if($result3 != "0")
			{
				for($x=0;$x<count($result3);$x++)
				{
					if($result2[$z]["IdNeraca"]=="1")
					{
						$saldo+=floatval($result3[$x]["Debit"]);
						$saldo-=floatval($result3[$x]["Kredit"]);
					}
					else
					{	
						$saldo-=floatval($result3[$x]["Debit"]);
						$saldo+=floatval($result3[$x]["Kredit"]);
					}
				}
			}
			//echo "<script type='text/javascript'>alert(".count($result2)."'');</script>";
			//$this->fpdf->Ln(10);
			$this->fpdf->SetFont('Arial','B',15);
			$this->fpdf->Image('images/logo.png',35,8,30,0,'','http://www.cips.or.id/'); 
			$this->fpdf->SetX(150);
			$this->fpdf->Cell(190,6,'General Ledger',0,0,'C');
			$this->fpdf->Ln(10);
			$this->fpdf->Cell(270,6,'PT. Citra Inti Prima Sejati',0,0,'C');
			$this->fpdf->Ln(5);
			$this->fpdf->Cell(270,6,''.$result2[$z]["NamaAkun"],0,0,'C');
			$result = $this->getDataJurnal($result2[$z]["NoAkun"]."",$qtanggal1,$qtanggal2);	
			$this->fpdf->Ln(5);
			$this->fpdf->SetFont('Arial','',12);
			//$this->fpdf->Cell(270,6,'Saldo Awal : '.$saldo,0,0,'C');	
			$this->fpdf->Ln(15);	
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->SetY($y_axis_initial);			
			$this->fpdf->SetX(20);
			//Header tabel halaman 1
			$this->fpdf->CELL(25,6,'NoJurnal',1,0,'C',1);
			$this->fpdf->Cell(40,6,'TanggalTransaksi',1,0,'C',1);
			$this->fpdf->Cell(110,6,'Deskripsi',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Debit',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Kredit',1,0,'C',1);
			$this->fpdf->Cell(35,6,'Saldo',1,0,'C',1);
			//$y_axis_initial += (6*count($result))+40;
			$this->fpdf->Ln();
			$max=25;//max baris perhalaman
			$i=0;
			$no=0;
			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			
			
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(25,6,'Saldo Awal',1,0,'C',0);
					$this->fpdf->Cell(40,6,$qtanggal1,1,0,'C',0);
					$this->fpdf->Cell(110,6,'',1,0,'C',0);
					$this->fpdf->Cell(25,6,'',1,0,'R',0);
					$this->fpdf->Cell(25,6,'',1,0,'R',0);
					$this->fpdf->Cell(35,6,$this->all_model->rp($saldo),1,0,'R',0);
					$this->fpdf->Ln();
					
			
			//$grandtotal = 0;
			if($result != "0")
			{
				foreach($result as $key => $value)
				{
					$i++;
					//$total += $row['Total'];

					if ($i==20){               //jika $i=25 maka buat header baru seperti di atas
					$this->fpdf->AddPage();
					$this->fpdf->SetY(10);
					$this->fpdf->SetX(20);
					$this->fpdf->CELL(25,6,'NoJurnal',1,0,'C',1);
					$this->fpdf->Cell(40,6,'TanggalTransaksi',1,0,'C',1);
					$this->fpdf->Cell(110,6,'Deskripsi',1,0,'C',1);
					$this->fpdf->Cell(25,6,'Debit',1,0,'C',1);
					$this->fpdf->Cell(25,6,'Kredit',1,0,'C',1);
					$this->fpdf->Cell(35,6,'Saldo',1,0,'C',1);

					$this->fpdf->SetY(10);
					$this->fpdf->SetX(55);
					$y_axis = $y_axis + $row_height;
					
					//$i=0;
					$this->fpdf->Ln();
					}

					//$grandtotal+=$row['Total'];
					
					if($result2[$z]["IdNeraca"]=="1")
					{
						$saldo+=floatval($result[$key]["Debit"]);
						$saldo-=floatval($result[$key]["Kredit"]);
					}
					else
					{	
						$saldo-=floatval($result[$key]["Debit"]);
						$saldo+=floatval($result[$key]["Kredit"]);
					}				
					$i++;
					$no++;
					$this->fpdf->SetX(20);
					//$this->fpdf->Cell(10,6,$no,1,0,'C',0);
					$this->fpdf->Cell(25,6,$result[$key]["NoJurnal"],1,0,'C',0);
					$this->fpdf->Cell(40,6,$result[$key]["TanggalTransaksi"],1,0,'C',0);
					$this->fpdf->Cell(110,6,$result[$key]["Deskripsi"],1,0,'C',0);
					$this->fpdf->Cell(25,6,$this->all_model->rp($result[$key]["Debit"]),1,0,'R',0);
					$this->fpdf->Cell(25,6,$this->all_model->rp($result[$key]["Kredit"]),1,0,'R',0);
					$this->fpdf->Cell(35,6,$this->all_model->rp($saldo),1,0,'R',0);
					//$this->fpdf->Cell(35,6,$this->all_model->rp($result[$key]["Insentif"]),1,0,'C',0);
					$this->fpdf->Ln();
				}
			}

			//buat footer
			$now = date("d F Y H:i:s");
			$this->fpdf->Ln();
			$this->fpdf->Ln();
			$this->fpdf->SetFont('Arial','B',10);
			$printer=$this->getDataPrinter($this->session->userdata("UserId"));
			$this->fpdf->SetX(0);			
			$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'C');
			$this->fpdf->SetX(40);			
			$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
			$this->fpdf->Ln();
			
		}
		$this->fpdf->Output('General Ledger'.date("F Y").'.pdf', 'I');
	}
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['data4'] = json_encode($this->getNamaAKun());
		$data['page_title']="CIPS - General Ledger";
		//$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('generalledger_view');
		$this->load->view('home_footer');
	}		
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

