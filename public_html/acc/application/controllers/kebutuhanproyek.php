<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KebutuhanProyek extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function getKebutuhanProyekCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdKebutuhanProyek,4) as IdKebutuhanProyek FROM mskebutuhanproyek ORDER BY IdKebutuhanProyek DESC", true);
		$result["IdKebutuhanProyek"] = $result["IdKebutuhanProyek"]+1;
		if($result["IdKebutuhanProyek"] < 10)
			$result["IdKebutuhanProyek"] = "KP000".$result["IdKebutuhanProyek"];
		else if($result["IdKebutuhanProyek"]< 100)
			$result["IdKebutuhanProyek"] = "KP00".$result["IdKebutuhanProyek"];
		else if($result["IdKebutuhanProyek"]< 1000)
			$result["IdKebutuhanProyek"] = "KP0".$result["IdKebutuhanProyek"];
		else
			$result["IdKebutuhanProyek"] = "KP".$result["IdKebutuhanProyek"];
		return $result["IdKebutuhanProyek"];
	}
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
			
			);
		}else{
			$search = array(
				'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array();			
						
		$result = $this->all_model->get_data("IdKebutuhanProyek as IdKebutuhanProyek,Nama as Nama,ActiveYN as ActiveYN", "mskebutuhanproyek a",$join, $where, $search, false, $perPage, $segmen, false,"IdKebutuhanProyek","ASC");
		
		$result2 = $this->all_model->get_data("IdKebutuhanProyek as IdKebutuhanProyek,Nama as Nama,ActiveYN as ActiveYN", "mskebutuhanproyek a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
			
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertKebutuhanProyek(){
		
		$data = array(
		'IdKebutuhanProyek' => $this->getKebutuhanProyekCode(),
		'Nama' => $this->input->post('Nama'),		
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("mskebutuhanproyek", $data );
		
		echo json_encode($query);
		exit();
	}	
	

	public function editKebutuhanProyek(){
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'ActiveYN' => $this->input->post('activeyn'),
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdKebutuhanProyek'			=> $this->input->post('idKebutuhanProyek'),
				
		);	
		$query = $this->all_model->update_data("mskebutuhanproyek", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	
	public function deleteKebutuhanProyek()
	{
		$IdKebutuhanProyek = $this->input->post('IdKebutuhanProyek');
		$where = array('IdKebutuhanProyek'=>$IdKebutuhanProyek);
		$query = $this->all_model->delete_data("mskebutuhanproyek", $where);
		echo json_encode($query);
		exit();
	}
	
		
	public function getDetailKebutuhanProyek($IdKebutuhanProyek)
	{
		$result = $this->all_model->query_data("SELECT * FROM mskebutuhanproyek where IdKebutuhanProyek='".$IdKebutuhanProyek."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Master Kebutuhan Proyek";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('kebutuhanproyek_view',$data2);
		$this->load->view('home_footer');
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


