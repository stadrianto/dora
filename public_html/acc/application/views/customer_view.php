		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Master Customer <small>Data Pelanggan</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New Customer</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Customer List </h3>
                        </div>
                        <div class="panel-body">
                            <div id="grid"></div>				
                        </div>
                    </div>
                </div>
            </div>			 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Customer </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div class="form-group">
								<label>ID Customer</label>
								<input id="custid" name="custid" disabled="true" class="form-control">							
							</div>
							<div class="form-group">
								<label>Customer Name</label>
								<input id="custname" name="custname" class="form-control">					
							</div>
							<div class="form-group">
								<label>Customer Address</label>
								<textarea id="custaddress" name="custaddress" class="form-control" rows="3"></textarea>					
							</div>
							<div class="form-group">
								<label>Customer Phone</label>
								<input id="custphone" name="custphone" class="form-control">					
							</div>
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>
							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Customer </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-6" style="float:none;">
							<h4>Yakin Hapus Data Ini?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Id Customer</th>
									<th>Nama Customer</th>
									<th>Alamat Customer</th>
									<th>Telepon Customer</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="deleteid"></div></td>
									<td><div id="deletenama"></div></td>
									<td><div id="deletealamat"></div></td>
									<td><div id="deletephone"></div></td>
								</tr>							
							</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="submit" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	$(document).ready(function () {
		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
		});
	});
	
	function addNew(){
		if($('#custname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Customer Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#custaddress').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Alamat Customer harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#custphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Telepon Customer harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
	
		var data= "Nama="+$('#custname').val()+"&Alamat="+$('#custaddress').val()+"&Telepon="+$('#custphone').val();

			$.ajax({				
			url : "<?php echo base_url()?>customer/insertCustomer",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
			});
	}
	function updateData(){
		if($('#custname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Customer Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#custaddress').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Alamat Customer harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#custphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Telepon Customer harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		var data= "IdCust="+$('#custid').val()+"&Nama="+$('#custname').val()+"&Alamat="+$('#custaddress').val()+"&Telepon="+$('#custphone').val();
			$.ajax({				
			url : "<?php echo base_url()?>customer/updateDataCustomer",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	function deleteData(idcust){		
			var data= "IdCust="+idcust;
			$.ajax({				
			url : "<?php echo base_url()?>customer/deleteCustomer",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#custname').val('');
		$('#custaddress').val('');
		$('#custphone').val('');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");			
	}
	
	/* function setUpdate(idcust,namacust,alamatcust,telpcust){
		$('#dialog').modal('show');
		$('#custid').val(idcust);
		$('#custname').val(namacust);
		$('#custaddress').val(alamatcust);
		$('#custphone').val(telpcust);
		$('#btnSave').attr("onclick","updateData()");			
	} */
	function setUpdate(idcust,namacust,alamatcust,telpcust){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#dialog').modal('show');
		$('#custid').val(idcust);
		$('#custname').val(namacust);
		$('#custaddress').val(alamatcust);
		$('#custphone').val(telpcust);
		$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(idcust,namacust,alamatcust,telpcust){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(idcust);
		$('#deletenama').html(namacust);
		$('#deletealamat').html(alamatcust);
		$('#deletephone').html(telpcust);
		$('#deleteBtn').attr("onclick","deleteData('"+idcust+"')");			
	}
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>,
                traffic = [
                {
                    Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
                },
                {
                    Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
                },
                {
                    Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
                },
                {
                    Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
                },
                {
                    Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
                }];


            if(hasil.length>0)
			{
				$("#grid").shieldGrid({
					dataSource: {
						data: hasil
					},
					sorting: {
						multiple: true
					},
					rowHover: false,
					paging: true,
					selection: 
					{
						type: "cell",
						multiple: false,
						toggle: false
					},
					events: {
						selectionChanged: onSelectedChanged,
						doubleClick: onDouble
					},				
					columns: [
					{ field: "IdCustomer", width: "170px", title: "IdCustomer" },
					{ field: "Nama", title: "Nama" },                
					{ field: "Alamat", title: "Alamat"},
					{ field: "Telepon", title: "Telepon" },
					{
						width: "12%",
						title: " ",
						buttons: [						
							{
								cls: "button button-primary",
								caption: " Edit",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).IdCustomer;
									var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
									var alamat=$("#grid").swidget().dataItem(rowIndex).Alamat;
									var telepon=$("#grid").swidget().dataItem(rowIndex).Telepon;
									setUpdate(id,nama,alamat,telepon);
								}
							},
							{
								cls: "mybuttonCssClass",
								caption: " Delete",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).IdCustomer;
									var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
									var alamat=$("#grid").swidget().dataItem(rowIndex).Alamat;
									var telepon=$("#grid").swidget().dataItem(rowIndex).Telepon;
									setDelete(id,nama,alamat,telepon);
								}
							}
						]
					}
					]
				}); 
			}			
        });
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
