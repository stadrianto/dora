<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getCustomerCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdCustomer,4) as IdCustomer FROM mscustomer ORDER BY IdCustomer DESC", true);
		$result["IdCustomer"] = $result["IdCustomer"]+1;
		if($result["IdCustomer"] < 10)
			$result["IdCustomer"] = "C000".$result["IdCustomer"];
		else if($result["IdCustomer"]< 100)
			$result["IdCustomer"] = "C00".$result["IdCustomer"];
		else if($result["IdCustomer"]< 1000)
			$result["IdCustomer"] = "C0".$result["IdCustomer"];
		else
			$result["IdCustomer"] = "C".$result["IdCustomer"];
		return $result["IdCustomer"];
	}
	public function getDataNew($request = true){
		$search = array(
				
			);
		$where = array(
		
		);
		$join = array();
		$result2 = $this->all_model->get_data("IdCustomer as IdCustomer,Nama as Nama,Alamat as Alamat,Telepon as Telepon,Fax as Fax,NPWP as NPWP", "mscustomer a",$join, $where, $search, false);
		/* foreach($result2 as $key => $val){
				
		} */
		return $result2;
		echo json_encode($result2);		
		exit();
	}
	public function getData($perPage=5, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
			
			);
		}else{
			$search = array(
					'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array();			
						
		$result = $this->all_model->get_data("IdCustomer as IdCustomer,Nama as Nama,Alamat as Alamat,Telepon as Telepon,Fax as Fax,NPWP as NPWP", "mscustomer a",$join, $where, $search, false, $perPage, $segmen, false,"IdCustomer","ASC");
		
		$result2 = $this->all_model->get_data("IdCustomer as IdCustomer,Nama as Nama,Alamat as Alamat,Telepon as Telepon,Fax as Fax,NPWP as NPWP", "mscustomer a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
			
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
public function getDataContactPerson($perPage=100, $segmen=0,  $request = true,$idcustomer){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdCustomer' => $idcustomer
			);
		}else{
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdCustomer' => $idcustomer
			);
		}
		$join = array();			
						
		$result = $this->all_model->get_data("IdContactPerson as IdContactPerson,IdCustomer as IdCustomer,Nama as Nama,Jabatan as Jabatan,Telepon as Telepon,Handphone as Handphone,ActiveYN as ActiveYN", "contactperson a",$join, $where, $search, false, $perPage, $segmen, false,"IdContactPerson","ASC");
		
		$result2 = $this->all_model->get_data("IdContactPerson as IdContactPerson,IdCustomer as IdCustomer,Nama as Nama,Jabatan as Jabatan,Telepon as Telepon,Handphone as Handphone,ActiveYN as ActiveYN", "contactperson a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataContactPerson(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataContactPerson(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
			
				}
			}
			echo json_encode($result);
			exit();
		}
	}


	
	public function insertCustomer(){		
		$data = array(
		'IdCustomer'  => $this->getCustomerCode(),
		'Nama' => $this->input->post('Nama'),		
		'Alamat' => $this->input->post('Alamat'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'ActiveYN' => 'Y',
		//'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("mscustomer", $data );
		
		echo json_encode($query);
		
		exit();
	}	
	
	public function insertContactPerson(){
		
		$data = array(
		'IdCustomer'  => $this->input->post('IdCustomer'),
		'Nama' => $this->input->post('Nama'),		
		'Jabatan' => $this->input->post('Jabatan'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'Handphone' => $this->input->post('Handphone'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("contactperson", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function editCustomer(){
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'Alamat' => $this->input->post('Alamat'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'Handphone' => $this->input->post('Handphone'),
		'NPWP' => $this->input->post('NPWP'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdCustomer'			=> $this->input->post('IdCustomer'),
				
		);	
		$query = $this->all_model->update_data("mscustomer", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function updateDataCustomer(){
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'Alamat' => $this->input->post('Alamat'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("Id"),		
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdCustomer'			=> $this->input->post('IdCust'),
				
		);	
		$query = $this->all_model->update_data("mscustomer", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	

	
	public function editContactPerson(){
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'Jabatan' => $this->input->post('Jabatan'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'Handphone' => $this->input->post('Handphone'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")	
			);
		$where = array(	
					'IdContactPerson'=> $this->input->post('IdContactPerson'),
				
		);	
		$query = $this->all_model->update_data("contactperson", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function deleteCustomer()
	{
		$IdUser = $this->input->post('IdCust');
		$where = array('IdCustomer'=>$IdUser);
		$query = $this->all_model->delete_data("mscustomer", $where);
		echo json_encode($query);
		exit();
	}
	
	public function deleteContactPerson()
	{
		$IdContactPerson = $this->input->post('IdContactPerson');
		$where = array('IdContactPerson'=>$IdContactPerson);
		$query = $this->all_model->delete_data("contactperson", $where);
		echo json_encode($query);
		exit();
	}
		
	public function getDetailCustomer($idcustomer)
	{
		$result = $this->all_model->query_data("SELECT * FROM mscustomer where IdCustomer='".$idcustomer."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "login","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="Master Customer";
		$data['page']="customer_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['data'] = json_encode($this->getDataNew(false));
		//$data['include']=$this->load->view('script','',true);
		$this->load->view('main',$data);
	}
	
	public function detailCustomer($idcustomer){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Master Customer";
		$data2['data'] = json_encode($this->getDataContactPerson($config['per_page'], $config['segmen'],false,$idcustomer ));
		$data2['data2'] = json_encode($this->getDetailCustomer($idcustomer));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailcustomer_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


