<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchasereport extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getPrintProject($Periode,$Periode2)
	{
		$result = $this->all_model->query_data("SELECT a.PurchaseId, PurchaseDate, EmployeeId, Sum(Qty*Price) as Total from trpurchase a join trpurchasedetail b on a.PurchaseId = b.PurchaseId WHERE PurchaseDate >= '".$Periode."' AND PurchaseDate <= '".$Periode2."' group by a.PurchaseId, PurchaseDate, EmployeeId", false);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function printLaporan(){
	
		$Periode = $this->input->post('dob1');
		$Periode2 = $this->input->post('dob2');
		
			$result = $this->getPrintProject($Periode,$Periode2);
			
			$this->load->library('fpdf17/fpdf');
			
			//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
			$this->fpdf->FPDF('P','mm','A4');
			$this->fpdf->Open();
			$this->fpdf->SetAutoPageBreak(false);
			$this->fpdf->AddPage('P');
			$this->fpdf->Ln(30);
			$this->fpdf->SetX(90);
			$this->fpdf->SetFont('Arial','BU',15);
			
			$this->fpdf->Cell(30,6,'Purchase Report',0,0,'C');
			$this->fpdf->Ln();
			$this->fpdf->SetX(90);
			$this->fpdf->Cell(40,6,'Between '.$Periode.' and '.$Periode2.'',0,0,'C');
			$this->fpdf->Ln(10);
			$this->fpdf->SetFont('Arial','BU',12);
			//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
			$this->fpdf->Ln(10);

			$y_axis_initial = 60;
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->SetY($y_axis_initial);
			$this->fpdf->SetX(20);
			//Header tabel halaman 1
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(30,6,'Purchase No',1,0,'C',1);
			$this->fpdf->Cell(50,6,'Purchase Date',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Employee',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Total',1,0,'C',1);
			$this->fpdf->Ln();
			$max=20;//max baris perhalaman
			$i=0;
			$no=0;
			$total=0;
			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			$grandtotal = 0;
			if($result != "No Data")
			{
			foreach($result as $key => $value){
			//$total += $row['Total'];

			if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
			$this->fpdf->AddPage();
			$this->fpdf->SetY(10);
			$this->fpdf->SetX(20);
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(30,6,'Purchase No',1,0,'C',1);
			$this->fpdf->Cell(50,6,'Purchase Date',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Employee',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Total',1,0,'C',1);	
			$this->fpdf->SetY(10);
			$this->fpdf->SetX(55);
			$y_axis = $y_axis + $row_height;
			$i=0;
			$this->fpdf->Ln();

			}

				$grandtotal+=($result[$key]["Total"]);
				$i++;
				$no++;
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(10,6,$no,1,0,'C',0);
				$this->fpdf->Cell(30,6,$result[$key]["PurchaseId"],1,0,'C',0);
				$this->fpdf->Cell(50,6,date("d-m-Y",strtotime($result[$key]["PurchaseDate"])),1,0,'C',0);
				$this->fpdf->Cell(45,6,$result[$key]["EmployeeId"],1,0,'C',0);				
				$this->fpdf->Cell(45,6,$this->all_model->rp($result[$key]["Total"]),1,0,'R',0);
				$this->fpdf->Ln();

			}
			}
			
				//buat footer
				
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(10);

		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(50,6,"Total           : ".$this->all_model->rp($grandtotal)."",0,0,'L');
		$this->fpdf->SetX(20);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Name"),0,0,'');
		$this->fpdf->SetX(120);			
		$this->fpdf->Cell(50,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		
		$this->fpdf->Output('Laporan Sales'.date("F Y").'.pdf', 'I');
		
	}
	
	
	public function getAccess()
	{	
		$page=$this->all_model->query_data("select pageid from mspage where pagename = '".$this->router->fetch_class()."'",true);
		//die($page["pageid"]);
		$result = $this->all_model->query_data("select flag from trprivillege where roleid=".$this->session->userdata('Role')." and PageName=".$page["pageid"]." and flag!=0",true);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function index(){
		if($this->session->userdata("Username")=="")
		redirect(base_url(). "login","refresh");
		/*if($this->session->userdata('Role')!="1")
		{
			$access=$this->getAccess();
			if(!is_array($access))
			{
				redirect(base_url(). "home","refresh");
			}
		}*/
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Purchase Report";
		$data['page']="purchasereport_view";
		$data['nama']=$this->session->userdata('Name');
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}