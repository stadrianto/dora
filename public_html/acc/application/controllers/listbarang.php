<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ListBarang extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function getListBarang($perPage=7, $segmen=0, $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
		
			);
		$join = array(
			array('table'=>'kategoribesar b','field' => 'a.IdKategoriBesar = b.IdKategoriBesar','method'=>'Left'),
			array('table'=>'kategori c','field' => 'a.IdKategori = c.IdKategori','method'=>'Left'),
		);
		$where = array(
				'a.ActiveYN'	=>"Y",
		
		);
		$result = $this->all_model->get_data("IdBarang as IdBarang,Nama as NamaItem,Jumlah as Harga,Keterangan as Keterangan,Jenis as Jenis,Quantity as Jumlah", "barang a",$join, $where, $search, false, $perPage, $segmen, false,"IdBarang","ASC");
		$result2 = $this->all_model->get_data("IdBarang as IdBarang,Nama as NamaItem,Jumlah as Harga,Keterangan as Keterangan,Jenis as Jenis,Quantity as Jumlah","barang a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getListBarang($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					if($result[$key]['Jumlah'] != 0)
					$result[$key]['Harga'] = $result[$key]['Harga']/$result[$key]['Jumlah'];
					$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					$result[$key]["Harga"] = $this->all_model->rp($result[$key]["Harga"]);
				}		
		
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getListBarang($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
									if($result[$key]['Jumlah'] != 0)
					$result[$key]['Harga'] = $result[$key]['Harga']/$result[$key]['Jumlah'];
					$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					$result[$key]["Harga"] = $this->all_model->rp($result[$key]["Harga"]);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 7; 
		$config['segmen'] = 0;
		$data['title']="List Barang";
		$data['page_title']="List Barang";
		$data2['data'] = json_encode($this->getListBarang($config['per_page'], $config['segmen'],false ));
		$data2['include']=$this->load->view('script','',true);
		$this->load->view('listbarang_view',$data2);
		
	}
	

}


