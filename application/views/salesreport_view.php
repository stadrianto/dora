<div id="page-wrapper">
	<script type="text/javascript">
		/* $(document).ready(function(){
		$(".homenav").html( data.msg ).addClass('selected')			
		});		 */	
	</script>
    <div class="row">
	    <div class="col-lg-12">
	        <h1>Sales Report <small>Sales Report</small></h1>                    
	    </div>
    </div>	           
	<div class="row">
		<div class="col-lg-12">
			
		</div>
	</div>
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Sales Report </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
						<form method='post' target="_blank" action='<?php echo base_url(); ?>Salesreport/printLaporan' id='fm1' name='fm1'>
						<div class="form-group">
								<label>From Date</label>
								<div class="input-group"  data-provide="datepicker">
								    <input id="dob1" name="dob1" type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>				
						</div>
						<div class="form-group">
								<label>End Date</label>
								<div class="input-group"  data-provide="datepicker">
								    <input id="dob2" name="dob2" type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>				
						</div>						
						<input class='btn btn-primary' type='submit' value=' Buat Laporan ' style='margin-right:15px;margin-top:15px;' /><br/>
						</form>
					</div>				
                </div>
            </div>
        </div>
    </div>			
</div>

<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	$(document).ready(function () {
		

		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd', autoclose: true });

		
	});

	

	function numberWithCommas(x) {
		var parts = x.toString().replace(/,/g,"")
	    parts = parts.toString().split(".");
	    //alert(parts[0])
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}
	
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
             
            $("#example1").DataTable({
			scrollX : true,
			scrollCollapse : true
		});
        });
		
		
    </script>
</body>
</html>
