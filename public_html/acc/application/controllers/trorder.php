<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Trorder extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getOrderCode($idcustomer){
		$result = $this->all_model->query_data("SELECT RIGHT(NoOrder,3) as NoOrder FROM trorder where IdCustomer='".$idcustomer."' ORDER BY NoOrder DESC", true);
		$result["NoOrder"] = $result["NoOrder"]+1;
		if($result["NoOrder"] < 10)
			$result["NoOrder"] = "00".$result["NoOrder"];
		else if($result["NoOrder"]< 100)
			$result["NoOrder"] = "0".$result["NoOrder"];
		else
			$result["NoOrder"] = "".$result["NoOrder"];
		return $idcustomer."-".$result["NoOrder"];
	}
	
	public function getProjectCode(){
		$noorder = $this->session->userdata('NoOrder');
		$result = $this->all_model->query_data("SELECT RIGHT(NoProject,2) as NoProject FROM project where NoOrder='".$noorder."' ORDER BY NoProject DESC", true);
		$result["NoProject"] = $result["NoProject"]+1;
		if($result["NoProject"] < 10)
			$result["NoProject"] = "0".$result["NoProject"];
		else
			$result["NoProject"] = "".$result["NoProject"];
		return $noorder."-".$result["NoProject"];
	}
	
	public function getData($perPage=10, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'b.Nama' => $this->input->post('search')
			);
			$where = array(
			//	'a.Status' => 'Pending'
			);
		}else{
			$search = array(
				'b.Nama' => $this->input->post('search')
				);
			$where = array(
				//'a.Status' => 'Pending'
			);
		}
		$join = array(
			array('table'=>'mscustomer b','field' => 'a.IdCustomer = b.IdCustomer','method'=>'Left'),
			array('table'=>'mskaryawan c','field' => 'a.PIC = c.NIP','method'=>'Left'),
		);			
						
		$result = $this->all_model->get_data("NoPenawaran as NoPenawaran,Tanggal as Tanggal,TanggalSelesai as TanggalSelesai,a.IdCustomer as IdCustomer,Project as Project,Status as Status,Keterangan as Keterangan,PIC as PIC,Nominal as Nominal,b.Nama as Nama,c.Nama as Nama1", "penawaran a",$join, $where, $search, false, $perPage, $segmen, false,"NoPenawaran","ASC");
		
		$result2 = $this->all_model->get_data("NoPenawaran as NoPenawaran,Tanggal as Tanggal,TanggalSelesai as TanggalSelesai,a.IdCustomer as IdCustomer,Project as Project,Status as Status,Keterangan as Keterangan,PIC as PIC,Nominal as Nominal,b.Nama as Nama,c.Nama as Nama1", "penawaran a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Nominal1'] = $this->all_model->rp($result[$key]['Nominal']);
					$result[$key]["Project"] = str_replace("\"", "@@@", $result[$key]["Project"]);
					$result[$key]["Project"] = str_replace("'", "~~~", $result[$key]["Project"]);
					$result[$key]["Project"] = str_replace("\n", " ", $result[$key]["Project"]);
					$result[$key]["Project"] = str_replace("\r", " ", $result[$key]["Project"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Nominal1'] = $this->all_model->rp($result[$key]['Nominal']);
					$result[$key]["Project"] = str_replace("\"", "@@@", $result[$key]["Project"]);
					$result[$key]["Project"] = str_replace("'", "~~~", $result[$key]["Project"]);
					$result[$key]["Project"] = str_replace("\n", " ", $result[$key]["Project"]);
					$result[$key]["Project"] = str_replace("\r", " ", $result[$key]["Project"]);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	

	
	public function addNewProject(){
		
		$data = array(
		'NoProject'  => $this->getProjectCode(),
		'NoOrder' => $this->input->post('NoOrder'),		
		'NamaProject' => $this->input->post('NamaProject'),	
		'BiayaProject' => $this->input->post('BiayaProject'),
		'PIC' => $this->input->post('PIC'),
		'TanggalProject' => date("Y-m-d"),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("project", $data );
		
		if($query){
			$data3["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data3["msg"] = $calculate;	
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
		
	public function getCalculate()
	{
		$total = 0;

		$join = array(
			
		);
		$where = array(
				'a.NoOrder' => $this->session->userdata("NoOrder"),
		);	
		$search = array(
	
		);
		$result = $this->all_model->get_data("BiayaProject as Biaya","project a",$join, $where, $search);
		if($result)
		{
			foreach($result as $key => $value){
				
				$total += $result[$key]['Biaya'];
	
			}
	
	
			
			$newdata = array(
				'TotalBiayaProject'		=> $this->all_model->rp($total),
				'TotalBiayaProject1'		=> $total,
			);
			return $newdata;
		}
		else
		{
			$newdata = array(
				'TotalBiayaProject'		=> $this->all_model->rp(0),
				'TotalBiayaProject1'		=> 0,
			);
			return $newdata;
		}
	}
	
	
	public function deleteItemProject()
	{
		$NoProject = $this->input->post('NoProject');
		$where = array('NoProject'=>$NoProject);
		$query = $this->all_model->delete_data("project", $where);
		
		if($query){
			$data3["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data3["msg"] = $calculate;	
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}
	
	public function getDataPenawaran($nopenawaran)
	{
		$result = $this->all_model->query_data("SELECT a.IdCustomer as IdCustomer,Project,Tanggal as Tanggal,TanggalSelesai as TanggalSelesai,Nominal,Nama,Alamat,Telepon,Project,Status,Keterangan,PIC,Nominal FROM penawaran a join mscustomer b where a.IdCustomer=b.IdCustomer and NoPenawaran='".$nopenawaran."' ", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}
	
	public function getDataOrder1($noorder)
	{
		$result = $this->all_model->query_data("SELECT TanggalSelesaiReal,HargaNego,HargaPPN,TanggalSelesai,NoFakturPajak,Status FROM trorder a where  NoOrder='".$noorder."' ", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}
	
	public function getDetailSupplier($idSupplier)
	{
		$result = $this->all_model->query_data("SELECT * FROM mssupplier where IdSupplier='".$idSupplier."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
		public function getNeed($noproject)
	{
		$search = array(
		
			);
		$join = array(
				array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("NoProject as NoProject,a.IdBarang as IdBarang,b.Quantity as Quantity,b.Jumlah as Harga,a.Jumlah as Jumlah,b.Nama as Nama","projectneeds a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){/*
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				
				
				$total+= $result[$key]['BiayaProject'];
				$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
	*/
				$result[$key]['Harga'] = $result[$key]['Harga'] / $result[$key]['Quantity'];
			}	
			return $result;
		}
		else
		{
			return "No Data";
		}
	
	}
	
	public function getKbProyek($noproject)
	{
		$search = array(
		
			);
		$join = array(
				array('table'=>'mskebutuhanproyek b','field' => 'a.IdKebutuhanProyek = b.IdKebutuhanProyek','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("NoProject as NoProject,a.IdKebutuhanProyek as IdKebutuhanProyek,a.Jumlah as Jumlah,b.Nama as Nama","kebutuhanproyek a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){
	
			}	
			return $result;
		}
		else
		{
			return "No Data";
		}
	
	}
	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Order";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('order_view',$data2);
		$this->load->view('home_footer');
	}
	
	
	public function getItemProject()
	{
		$noorder = $this->session->userdata("NoOrder");
		$result = $this->getProject($noorder);
		if($result != "No Data")
		echo json_encode($result);
		else echo json_encode(array());
		
	}
	
	public function getProject($noorder)
	{
		$total = 0;
		$floater = 0;
		$package = 0;
		$search = array(
		
			);
		$join = array(
			array('table'=>'mskaryawan b','field' => 'a.PIC = b.NIP','method'=>'Left'),
		);
		$where = array(
				'a.NoOrder' => $noorder
		);
			
		$result = $this->all_model->get_data("NoProject as NoProject,NoOrder as NoOrder,NamaProject as NamaProject,TanggalProject as TanggalProject,BiayaProject as BiayaProject,b.Nama as PIC","project a",$join, $where, $search);

		$array_items = array('TotalBiayaProject'=>'');
		$this->session->unset_userdata($array_items);
		
		
		if($result)
		{
			foreach($result as $key => $value){/*
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				$result[$key]['TotalSpot'] = $result[$key]['Qty'];
				if($result[$key]['Bonus'] == "N")
				$total += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				else
				$floater += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				*/
				$total+= $result[$key]['BiayaProject'];
				$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
	
			}
			
			$total = $this->all_model->rp($total);

			$newdata = array(
				'TotalBiayaProject'		=> $total,
			);
	 
			$this->session->set_userdata($newdata);
			
			return $result;
		}
		else
		{

			$total = $this->all_model->rp($total);

			$newdata = array(
				'TotalBiayaProject'		=> $total,
			);
	 
			$this->session->set_userdata($newdata);
			
			return "No Data";
		}
	
	}
	
	public function updateData(){
		
		$data = array(		
		'HargaNego' => $this->input->post('HargaNegosiasi'),	
		'TanggalSelesaiReal' => $this->input->post('TanggalSelesaiReal'),	
		'TanggalSelesai' => $this->input->post('TanggalSelesai'),
		'HargaPPN' => $this->input->post('HargaPPN'),
		'NoFakturPajak' => $this->input->post('NoFakturPajak')
			);
			

		$where = array('NoOrder'=>$this->session->userdata("NoOrder"));
		$query = $this->all_model->update_data("trorder", $data,$where);	
		
		
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}
	
	public function finishProject(){
		
		$data = array(		
		'HargaNego' => $this->input->post('HargaNegosiasi'),	
		'TanggalSelesaiReal' => $this->input->post('TanggalSelesaiReal'),	
		'TanggalSelesai' => $this->input->post('TanggalSelesai'),
		'HargaPPN' => $this->input->post('HargaPPN'),
		'NoFakturPajak' => $this->input->post('NoFakturPajak'),
		'Status' => "Finish Project",
			);
			

		$where = array('NoOrder'=>$this->session->userdata("NoOrder"));
		$query = $this->all_model->update_data("trorder", $data,$where);	
		
		
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}
	
	
	public function checkPenawaran($nopenawaran)
	{
		$result = $this->all_model->query_data("SELECT * FROM  trorder WHERE NoPenawaran = '$nopenawaran'", true);	
		if($result) return false;
		else return true;
	}
	
	public function getNoOrder($nopenawaran)
	{
		$result = $this->all_model->query_data("SELECT NoOrder FROM  trorder WHERE NoPenawaran = '$nopenawaran'", true);	
		if($result) return $result['NoOrder'];
		else return "No Data";	
	}
	
	public function detailOrder($nopenawaran){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
	
		$datapenawaran = $this->getDataPenawaran($nopenawaran);
		if($this->checkPenawaran($nopenawaran))
		{	
				$data = array(
					'NoOrder' => $this->getOrderCode($datapenawaran['IdCustomer']),
					'NoPenawaran'	=> $nopenawaran,
					'IdCustomer' 	=> $datapenawaran['IdCustomer'],
					'Project' 	=> $datapenawaran['Project'],
					'Nominal' => $datapenawaran['Nominal'],	
					'TanggalOrder' => date("Y-m-d"),
					'TanggalSelesai' => $datapenawaran['TanggalSelesai'],
					'Status' => "On Progress",
				);
		
				$query = $this->all_model->insert_data("trorder", $data );
				
			$data2 = array(
				'Status' => 'Order',
				'UpdateID' => $this->session->userdata("UserId"),
			
			);
			$where2 = array(	
					'NoPenawaran'			=> $nopenawaran,		
			);	
			$this->all_model->update_data("penawaran", $data2 ,$where2);
		}
		
		$NoOrder = $this->getNoOrder($nopenawaran);
		$newdata = array(
			'NoOrder'		=> $NoOrder,
		);
		$this->session->set_userdata($newdata);
		$datatotalproject = $this->getCalculate();
		$dataorder = $this->getDataOrder1($NoOrder);
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Order";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data2['data2'] = json_encode($datapenawaran);
		$data2['data3'] = json_encode($dataorder);
		$data2['data4'] = json_encode($datatotalproject);
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailorder_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function getDataOrder($noorder)
	{
		$totalpembelian = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.NoOrder' => $noorder
		);
		
		$join = array(
			//array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),		
		);	
	
		$result = $this->all_model->get_data("NoProject as NoProject,NoOrder as NoOrder,NamaProject as NamaProject,TanggalProject as TanggalProject,BiayaProject as BiayaProject", "project a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				//$totalpembelian += ($result[$key]['Harga']*$result[$key]['Jumlah']);
			}
			return $result;
			
		}
		else
			return "No Data";
		
	}

	public function getDataOrderSuratJalan($nosuratjalan)
	{
		$totalpembelian = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.NoSuratJalan' => $nosuratjalan
		);
		
		$join = array(
			//array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),		
		);	
	
		$result = $this->all_model->get_data("IdDetailSuratJalan,NoSuratJalan,NamaBarang,Jumlah", "detailsuratjalan a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				//$totalpembelian += ($result[$key]['Harga']*$result[$key]['Jumlah']);
			}
			return $result;
			
		}
		else
			return "No Data";
		
	}
	
	public function getDetailOrder($noorder)
	{
		$result = $this->all_model->query_data("SELECT * FROM trorder a join mscustomer b on a.IdCustomer=b.IdCustomer where a.NoOrder='".$noorder."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function checkSuratJalan($noorder){

		$result = $this->all_model->query_data("SELECT NoSuratJalan,a.NoOrder,TanggalSuratJalan,AlamatSuratJalan,c.Nama,c.Alamat,c.Telepon FROM suratjalan a join trorder b on a.NoOrder = b.NoOrder join mscustomer c on b.IdCustomer = c.IdCustomer WHERE a.NoOrder='".$noorder."'", true);
		
		if($result)
		{
			return $result;
		}
		else
		{
				$data = array(
					'NoOrder' => $noorder,
					'NoSuratJalan' => 'SJ-'.$noorder,
					'UpdateId' => $this->session->userdata("UserId"),
					'UpdateTime' => date("Y-m-d H:i:s")
				);
		
				$query = $this->all_model->insert_data("suratjalan", $data );	
				$result = $this->all_model->query_data("SELECT NoSuratJalan,a.NoOrder,TanggalSuratJalan,AlamatSuratJalan,c.Nama,c.Alamat,c.Telepon FROM suratjalan a join trorder b on a.NoOrder = b.NoOrder join mscustomer c on b.IdCustomer = c.IdCustomer WHERE a.NoOrder='".$noorder."'", true);
				return $result;
		
		}		
	}
	
	public function updateSuratJalan(){
		
		$data = array(		
		'AlamatSuratJalan' => $this->input->post('AlamatSuratJalan'),	
		'TanggalSuratJalan' => $this->input->post('TanggalSuratJalan'),	
	
			);
			

		$where = array('NoSuratJalan'=>$this->input->post("NoSuratJalan"));
		$query = $this->all_model->update_data("suratjalan", $data,$where);	
		
			
		echo json_encode($query);
		exit();
	}
	
	public function getDataSuratJalan($perPage=100, $segmen=0,  $request = true,$nosuratjalan){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				
			);
			$where = array(
				'a.NoSuratJalan' => $nosuratjalan
			);
		}else{
			$search = array(
				
			);
			$where = array(
				'a.NoSuratJalan' => $nosuratjalan
			);
		}
		$join = array();			
						
		$result = $this->all_model->get_data("IdDetailSuratJalan,NamaBarang,Jumlah,NoSuratJalan", "detailsuratjalan a",$join, $where, $search, false, $perPage, $segmen, false,"IdDetailSuratJalan","ASC");
		
		$result2 = $this->all_model->get_data("IdDetailSuratJalan,NamaBarang,Jumlah,NoSuratJalan", "detailsuratjalan a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataSuratJalan(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataSuratJalan(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
			
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertDetailSuratJalan(){
		
		$data = array(
		'NoSuratJalan'  => $this->input->post('NoSuratJalan'),
		'NamaBarang' => $this->input->post('NamaBarang'),		
		'Jumlah' => $this->input->post('Jumlah'),	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("detailsuratjalan", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function deleteDetailSuratJalan()
	{
		$IdDetailSuratJalan = $this->input->post('IdDetailSuratJalan');
		$where = array('IdDetailSuratJalan'=>$IdDetailSuratJalan);
		$query = $this->all_model->delete_data("detailsuratjalan", $where);
		echo json_encode($query);
		exit();
	}
		
	public function suratJalan()
	{
		$noorder = $this->session->userdata("NoOrder");
		$result = $this->checkSuratJalan($noorder);
		$data['title']="CIPS";
		$data['page_title']="CIPS - Surat Jalan";
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data2['data'] = json_encode($result);
		$data2['datasuratjalan'] = json_encode($this->getDataSuratJalan($config['per_page'], $config['segmen'],false,$result['NoSuratJalan'] ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('suratjalan_view',$data2);
		$this->load->view('home_footer');
	
	}
	
	public function printSuratJalan()
	{
	
		$noorder = $this->session->userdata('NoOrder');
		
		
		
		$this->load->library('fpdf17/fpdf');
		$result2 = $this->all_model->query_data("SELECT NoSuratJalan,a.NoOrder,TanggalSuratJalan,AlamatSuratJalan,c.Nama,c.Alamat,c.Telepon FROM suratjalan a join trorder b on a.NoOrder = b.NoOrder join mscustomer c on b.IdCustomer = c.IdCustomer WHERE a.NoOrder='".$noorder."'", true);
		$result = $this->getDataOrderSuratJalan($result2['NoSuratJalan']);
$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Surat Jalan',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','BU',12);
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->Cell(190,6,'No Surat Jalan : SJ-'.$noorder.'',0,0,'C');
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Tanggal Surat Jalan : '.$result2['TanggalSuratJalan'].'',0,0,'C');		
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(190,6,'Pengirim',0,0,'');
		$this->fpdf->SetFont('Arial','',12);		
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Perusahaan : PT Citra Inti Prima Sejati',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Alamat : Jln. Pemuteran Raya No.58 d/h 147 Kebun Pisang, Kav. POLRI Jelambar - Jakarta Barat',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Line(10, 97, 200, 97, $style);
		$this->fpdf->Ln(6);
		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(190,6,'Penerima',0,0,'');
		$this->fpdf->SetFont('Arial','',12);	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Customer : '.$result2['Nama'].'',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Alamat Surat Jalan: '.$result2['AlamatSuratJalan'].'',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Line(10, 120, 200, 120, $style);
		$this->fpdf->Ln(6);
		$y_axis_initial = 126;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(35);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'No Order',1,0,'C',1);
		$this->fpdf->Cell(50,6,'Nama Barang',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Jumlah',1,0,'C',1);

		

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(35);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'No Order',1,0,'C',1);
		$this->fpdf->Cell(50,6,'Nama Barang',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Jumlah',1,0,'C',1);

		

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

	
		$i++;
		$no++;
		$this->fpdf->SetX(35);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result2["NoOrder"],1,0,'C',0);
		$this->fpdf->Cell(50,6,$result[$key]["NamaBarang"],1,0,'C',0);
		$this->fpdf->Cell(45,6,$result[$key]["Jumlah"],1,0,'C',0);
	
		

		$this->fpdf->Ln();

		}
		}

		//buat footer

		$this->fpdf->Ln(35);		
		$this->fpdf->Cell(30,6,"Jakarta , ".date("d F Y"),0,0,'C');
		$this->fpdf->Ln(15);	
		$this->fpdf->Cell(40,6,"Pengirim,",0,0,'C');	
		$this->fpdf->Cell(260,6,"Penerima,",0,0,'C');
		$this->fpdf->Ln(35);
		$this->fpdf->Cell(40,6, "(                                            )",0,0,'C');	
		$this->fpdf->Cell(260,6, "(                                            )",0,0,'C');	
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();		
		$this->fpdf->Output('Surat Jalan '.date("F Y").'.pdf', 'I');	
	
	}
	
	
	public function printOrder()
	{
	
		$noorder = $this->session->userdata('NoOrder');
		$result = $this->getDataOrder($noorder);
		$this->load->library('fpdf17/fpdf');
		$result2 = $this->getDetailOrder($noorder);
$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		//$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		//$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Faktur Pajak',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','BU',12);
		
		$style3 = array('width' => 1, 'cap' => 'round', 'join' => 'round', 'dash' => '2,10', 'color' => array(255, 0, 0));
		$style4 = array('L' => 0,
                'T' => array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => '20,10', 'phase' => 10, 'color' => array(100, 100, 255)),
                'R' => array('width' => 0.50, 'cap' => 'round', 'join' => 'miter', 'dash' => 0, 'color' => array(50, 50, 127)),
                'B' => array('width' => 0.75, 'cap' => 'square', 'join' => 'miter', 'dash' => '30,10,5,10'));
		
		//$this->fpdf->Rect(100, 10, 40, 20, 'DF', $style4, array(220, 220, 200));
		$this->fpdf->Rect(8, 30, 193, 240, 'D', array('all' => $style3));
	
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->Cell(190,6,'No Faktur Pajak : '.$result2['NoFakturPajak'].'',0,0,'C');		
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(190,6,'Perusahaan Kena Pajak',0,0,'');
		$this->fpdf->SetFont('Arial','',12);		
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Perusahaan : PT Citra Inti Prima Sejati',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Alamat : Jln. Pemuteran Raya No.58 d/h 147 Kebun Pisang, Kav. POLRI Jelambar - Jakarta Barat',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'NPWP : '.$result2['NPWP'].'',0,0,'',0);			
		$this->fpdf->Ln(6);
		$this->fpdf->Line(10, 95, 200, 95, $style);
		$this->fpdf->Ln(6);
		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(190,6,'Penerima Jasa Kena Pajak',0,0,'');
		$this->fpdf->SetFont('Arial','',12);	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Customer : '.$result2['Nama'].'',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Alamat : '.$result2['Alamat'].'',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'NPWP : '.$result2['NPWP'].'',0,0,'',0);			
		$this->fpdf->Ln(6);
		$this->fpdf->Line(10, 128, 200, 128, $style);
		$this->fpdf->Ln(6);
		$y_axis_initial = 135;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(15);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(120,6,'Nama Barang / Jasa Kena Pajak',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Harga Jual',1,0,'C',1);
		

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(120,6,'Nama Barang / Jasa Kena Pajak',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Harga Jual',1,0,'C',1);
	
		

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		$grandtotal+=($result[$key]["BiayaProject"]);
		$i++;
		$no++;
		$this->fpdf->SetX(15);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(120,6,$result[$key]["NamaProject"],1,0,'C',0);
		$this->fpdf->Cell(45,6,$this->all_model->rp($result[$key]["BiayaProject"]),1,0,'C',0);
		

		$this->fpdf->Ln();

		}
		}
		
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Jumlah Harga Jual',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($grandtotal),1,0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Dikurangi Potongan Harga ',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp("0"),1,0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Dikurangi Uang Muka Diterima',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($result2['UangMuka']),1,0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Dasar Pengenaan Pajak',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp(($grandtotal-$result2['UangMuka'])),1,0,'C',0);
		$this->fpdf->Ln();	
		$this->fpdf->SetX(15);		
		$this->fpdf->CELL(130,6,'PPN 10% ',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($result2['HargaPPN']),1,0,'C',0);	
		$this->fpdf->Ln();	
		$this->fpdf->SetX(15);		
		$this->fpdf->CELL(130,6,'Total yang harus dibayar ',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($grandtotal-$result2['UangMuka']+$result2['HargaPPN']),1,0,'C',0);
		
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(10,6,"Pajak Penjualan atas barang mewah",0,0,'L');
		$this->fpdf->Ln();

	
		$this->fpdf->CELL(30,6,'Tarif',1,0,'C',1);
		$this->fpdf->Cell(30,6,'DPP',1,0,'C',1);
		$this->fpdf->Cell(30,6,'PPn BM',1,0,'C',1);		
		$this->fpdf->Ln();
		$this->fpdf->Cell(30,6,"........ %",1,0,'C',0);
		$this->fpdf->Cell(30,6,"Rp. ............",1,0,'C',0);
		$this->fpdf->Cell(30,6,"Rp. ............",1,0,'C',0);		
		$this->fpdf->Ln();
		$this->fpdf->Cell(30,6,"........ %",1,0,'C',0);
		$this->fpdf->Cell(30,6,"Rp. ............",1,0,'C',0);
		$this->fpdf->Cell(30,6,"Rp. ............",1,0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->Cell(30,6,"........ %",1,0,'C',0);
		$this->fpdf->Cell(30,6,"Rp. ............",1,0,'C',0);
		$this->fpdf->Cell(30,6,"Rp. ............",1,0,'C',0);		
		$this->fpdf->SetY(190);
		$this->fpdf->Ln();	
		$this->fpdf->Cell(300,6,"Jakarta , ".date("d F Y"),0,0,'C');	
		$this->fpdf->Ln(35);
		$this->fpdf->Cell(300,6, "(                                            )",0,0,'C');	
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();		
		$this->fpdf->Output('Faktur Pajak '.date("F Y").'.pdf', 'I');	
	
	}

public function printInvoice()
	{
	
		$noorder = $this->session->userdata('NoOrder');
		$result = $this->getDataOrder($noorder);
		$this->load->library('fpdf17/fpdf');
		$result2 = $this->getDetailOrder($noorder);
$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		//$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		//$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Faktur Pajak',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','BU',12);
		
		$style3 = array('width' => 1, 'cap' => 'round', 'join' => 'round', 'dash' => '2,10', 'color' => array(255, 0, 0));
		$style4 = array('L' => 0,
                'T' => array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => '20,10', 'phase' => 10, 'color' => array(100, 100, 255)),
                'R' => array('width' => 0.50, 'cap' => 'round', 'join' => 'miter', 'dash' => 0, 'color' => array(50, 50, 127)),
                'B' => array('width' => 0.75, 'cap' => 'square', 'join' => 'miter', 'dash' => '30,10,5,10'));
		
		//$this->fpdf->Rect(100, 10, 40, 20, 'DF', $style4, array(220, 220, 200));
		$this->fpdf->Rect(8, 30, 193, 240, 'D', array('all' => $style3));
	
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->Cell(190,6,'No Order: '.$noorder.'',0,0,'C');		
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(190,6,'Perusahaan Jasa',0,0,'');
		$this->fpdf->SetFont('Arial','',12);		
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Perusahaan : PT Citra Inti Prima Sejati',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Alamat : Jln. Pemuteran Raya No.58 d/h 147 Kebun Pisang, Kav. POLRI Jelambar - Jakarta Barat',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'No Rekening Bank Danamon : '.$this->session->userdata('NoRekPerusahaan'),0,0,'',0);			
		$this->fpdf->Ln(6);
		$this->fpdf->Line(10, 95, 200, 95, $style);
		$this->fpdf->Ln(6);
		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(190,6,'Penerima Jasa ',0,0,'');
		$this->fpdf->SetFont('Arial','',12);	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Nama Customer : '.$result2['Nama'].'',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'Alamat : '.$result2['Alamat'].'',0,0,'');	
		$this->fpdf->Ln(6);
		$this->fpdf->Cell(190,6,'No Telepon : '.$result2['Telepon'].'',0,0,'',0);			
		$this->fpdf->Ln(6);
		$this->fpdf->Line(10, 128, 200, 128, $style);
		$this->fpdf->Ln(6);
		$y_axis_initial = 135;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(15);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(120,6,'Nama Barang / Jasa Kena Pajak',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Harga Jual',1,0,'C',1);
		

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(120,6,'Nama Barang / Jasa Kena Pajak',1,0,'C',1);
		$this->fpdf->Cell(45,6,'Harga Jual',1,0,'C',1);
	
		

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		$grandtotal+=($result[$key]["BiayaProject"]);
		$i++;
		$no++;
		$this->fpdf->SetX(15);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(120,6,$result[$key]["NamaProject"],1,0,'C',0);
		$this->fpdf->Cell(45,6,$this->all_model->rp($result[$key]["BiayaProject"]),1,0,'C',0);
		

		$this->fpdf->Ln();

		}
		}
		
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Jumlah Harga Jual',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($grandtotal),1,0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Dikurangi Potongan Harga ',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp("0"),1,0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Dikurangi Uang Muka Diterima',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($result2['UangMuka']),1,0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->SetX(15);
		$this->fpdf->CELL(130,6,'Dasar Pengenaan Pajak',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp(($grandtotal-$result2['UangMuka'])),1,0,'C',0);
		$this->fpdf->Ln();	
		$this->fpdf->SetX(15);		
		$this->fpdf->CELL(130,6,'PPN 10% ',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($result2['HargaPPN']),1,0,'C',0);	
		$this->fpdf->Ln();	
		$this->fpdf->SetX(15);		
		$this->fpdf->CELL(130,6,'Total yang harus dibayar ',1,0,'L',0);
		$this->fpdf->Cell(45,6,"".$this->all_model->rp($grandtotal-$result2['UangMuka']+$result2['HargaPPN']),1,0,'C',0);
		


	

		$this->fpdf->Cell(300,6,"Jakarta , ".date("d F Y"),0,0,'C');	
		$this->fpdf->Ln(35);
		$this->fpdf->Cell(300,6, "(                                            )",0,0,'C');	
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();		
		$this->fpdf->Output('Faktur Komersial '.date("F Y").'.pdf', 'I');	
	
	}
	
	public function printKwitansi()
	{
		
		$noorder = $this->session->userdata('NoOrder');
		$result = $this->getDataOrder($noorder);
		$this->load->library('fpdf17/fpdf');
		$result2 = $this->getDetailOrder($noorder);
		
	
	
		require "classConversi.php";
		$convert = new classConversi();
		
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		
		$arraybln=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'); 
		$bln=$arraybln[date('n')-1]; 
		$thn=date('Y');
		$tgl=date('d'); 
	


	
		$this->fpdf->setMargins(10,6,10); 
		$this->fpdf->AddPage(); 
		$this->fpdf->SetFont('Arial','B',13); 
		$this->fpdf->Cell(0,5,"PT Citra Inti Prima Sejati",0,1,'L'); 
		$this->fpdf->SetFont('Arial','',11); 
		$this->fpdf->MultiCell(0,5,"Jln. Pemuteran Raya No.58 d/h 147 Kebun Pisang, Kav. POLRI Jelambar - Jakarta Barat"); 
		$this->fpdf->MultiCell(0,5,"No Kwitansi : KW".$noorder); 
		$this->fpdf->SetLineWidth(0.8);
		//$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		//$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Line(10,28,199,28);
		$this->fpdf->Ln(13); 
		$this->fpdf->SetFont('Arial','B',13);
		$this->fpdf->Cell(60,5,'',0,0,''); 
		$this->fpdf->Cell(0,5,"Tanda Bukti Pembayaran",0,1,'L'); 
		$this->fpdf->SetLineWidth(0.4); 
		$this->fpdf->Rect(60,30,80,13);/*ubah ukuran Kotak Judul -> Rect(sumbu x, sumbu y, lebar kotak,tinggi kotak)*/ 
		$this->fpdf->SetFont('Arial','',11);
		$this->fpdf->Ln(10); 
		$this->fpdf->Cell(45,5,'Telah Terima Dari :',0,0,'L'); 
		$this->fpdf->SetFont('Arial','',12);
		$this->fpdf->Cell(70,7,$result2["Nama"],0,1,'J'); 
		$this->fpdf->Line(50,56,150,56);
		$this->fpdf->Rect(50,61,115,10); 
		$this->fpdf->Rect(50,74,115,10);
		$this->fpdf->Ln(6); 
		$this->fpdf->Cell(40,20,'Uang Sejumlah :',0,0,'L'); 
		$grandtotal = 0;
		if($result != "No Data")
		{
			foreach($result as $key => $value){

					//$grandtotal+=($result[$key]["BiayaProject"]);
			}	
		}
		$grandtotal = $result2['HargaNego'] + $result2['HargaPPN'] - $result2['UangMuka'];
		$this->fpdf->MultiCell(113,11,$convert->conversiAngka($grandtotal)." Rupiah",'J'); 
		if(strlen($convert->conversiAngka($grandtotal))>40)
		$lnBreak=8;
		else
		$lnBreak=16;
		$this->fpdf->Ln($lnBreak); 
		$this->fpdf->Cell(45,5,'Untuk Pembayaran :',0,0,'L');
		$this->fpdf->Cell(60,7,$result2["Project"],0,1,'J'); 
		$this->fpdf->Line(50,97,150,97); 
		$this->fpdf->Ln(18); 
		$this->fpdf->Cell(116,5,'',0,0,'');
		$this->fpdf->SetFont('Arial','U',12); 
		$this->fpdf->Cell(0,5,'Jakarta, '.$tgl.' '.$bln.' '.$thn,0,1,'L'); 
		//$this->fpdf->Ln(10); 
		$this->fpdf->SetFont('Arial','',14); 
		$this->fpdf->Cell(116,5,'Rp. '.number_format($grandtotal,0,",",".").',-',0,1,'L'); 
		$this->fpdf->Rect(8,116,50,10); 
		$this->fpdf->SetFont('Arial','',12); 
		$this->fpdf->Ln(16); 
		$this->fpdf->Cell(130-strlen("So Wee Ming"),5,'',0,0,'L'); 
		$this->fpdf->SetFont('Arial','BU',14);
		$this->fpdf->Cell(30,5,'( So Wee Ming )',0,1,'L'); 
		$this->fpdf->Output();
	
	
	
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


