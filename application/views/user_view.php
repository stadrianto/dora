		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Employee<small> Employee Data</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Employee List </h3>
                        </div>
                        <div class="panel-body">
                            <table id="example1" class="table table-bordered table-striped">
								<thead>
								  <tr>
									<th>Username</th>
									<th>Employee Id</th>
									<th>Employee Name</th>
									<th>Role</th>
									<th>Branch</th>
									<th>Action</th>
								  </tr>
								</thead>
								<tbody>
									<?php
										//die (var_dump($data));
										if ( isset($data) and $data != '0' ){
											$no = 1;
											foreach($data as $row){
									?>
									<tr>
										<td><?php echo $row->Username?></td>
										<td><?php echo $row->EmployeeId?></td>
										<td><?php echo $row->EmployeeName?></td>
										<td><?php echo $row->RoleName?></td>
										<td><?php echo $row->BranchName?></td>							
										<td>
											<div class="btn-group" style="width:100px">
											  <button type="button" class="btn btn-primary btn-edit" data-key="<?php echo $row->EmployeeId?>">Action</button>
											  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											  </button>
											  <ul class="dropdown-menu" role="menu" style="margin-left:-70px;position:relative">
												<li><a href="javascript:void(0);" class="btn-edit" data-key="<?php echo $row->EmployeeId?>"><i class="fa fa-edit"></i> Edit</a></li>
												<li><a href="javascript:void(0);" class="btn-rem" data-key="<?php echo $row->EmployeeId?>"><i class="fa fa-times"></i> Remove</a></li>
											  </ul>
											</div>
										</td>
									</tr>
									<?php $no++;}}?>
								</tbody>
							</table>		
                        </div>
                    </div>
                </div>
            </div>         		 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> New Employee </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div class="form-group">
								<label>Username</label>
								<input id="txtusername" name="txtusername" value="" class="form-control">
								<input type="hidden" name="hfid" id="hfid" value="">
							</div>
							<div class="form-group">
								<label>Employee Name</label>
								<input id="txtnama" name="txtnama" class="form-control">			
							</div>
							<div class="form-group">
								<label>Phone Number</label>
								<input id="txtphone" name="txtphone" class="form-control">			
							</div>
							<div class="form-group">
								<label>Address</label>
								<textarea id="txtaddress" name="txtaddress" class="form-control" ></textarea>	
							</div>
							<div class="form-group">
								<label>Entry Date</label>
								<div class="input-group"  data-provide="datepicker">
								    <input id="txttgl" name="txttgl" type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>				
							</div>						
							<div class="form-group">
								<label>Role</label>
								<div id="kategoriarea"></div>
							</div>
							<div class="form-group">
								<label>Branch</label>
								<div id="brancharea"></div>
							</div>								
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>	
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes03" id="tes03" class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Employee </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-12" style="float:none;">
							<h4>Are you sure want to delete this data?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Employee Id</th>
										<th>Name</th>
										<th>Role</th>
										<th>Branch</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div id="deleteid"></div></td>
										<td><div id="deletenama"></div></td>
										<td><div id="deleterole"></div></td>
										<td><div id="deletebranch"></div></td>
									</tr>							
								</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="button" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	$(document).ready(function () {
		$("#example1").DataTable({
			scrollX : true,
			scrollCollapse : true
		});

		$("#example1").on("click", ".btn-edit", function(){
			var data = {"key" : $(this).attr("data-key")};
			$.ajax({
				url : "<?php echo base_url()?>/User/ajax_finder",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){					
					if ( data.type === "done" ){
						setUpdate(data.msg[0].EmployeeId, data.msg[0].EmployeeName, data.msg[0].PhoneNumber, data.msg[0].Address, data.msg[0].Username, data.msg[0].RoleId, data.msg[0].BranchId, data.msg[0].EntryDate);
					}
					else{
						alert(data.msg);
					}
				},
			   	error: function(ts) { alert(ts.responseText) }
			});
		});
		$("#example1").on("click", ".btn-rem", function(){
			var data = {"key" : $(this).attr("data-key")};
			$.ajax({
				url : "<?php echo base_url()?>/User/ajax_finder",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){					
					if ( data.type === "done" ){
						setDelete(data.msg[0].EmployeeId, data.msg[0].EmployeeName, data.msg[0].RoleName, data.msg[0].BranchName);													
					}
					else{
						alert(data.msg);
					}
				},
			   	error: function(ts) { alert(ts.responseText) }
			});
		});

		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd', autoclose: true });

		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
		});
		
		$('#tes03').click(function(e) {
			$('#dialogDelete').modal('hide');
		});

		var hasil3 = JSON.parse('<?php echo $kategori; ?>');
		var text = '';

		if(hasil3 == "No Data"){
			$('#kategoriarea').html("No Active Departemen");
		}else{
			text+= '<select class="form-control" onchange="" name="kategori" id="kategori">';
			text+='<option value="0">Select</option>';
			for(i=0;i<hasil3.length;i++){
				
				text+= '<option value="'+hasil3[i].RoleId+'">'+hasil3[i].RoleName+'</option>'	
			}
			text+= '</select>';
			$('#kategoriarea').html(text);
		
		}
		var hasil4 = JSON.parse('<?php echo $branch; ?>');
		var textbranch = '';

		if(hasil4 == "No Data"){
			$('#brancharea').html("No Active Departemen");
		}else{
			textbranch+= '<select class="form-control" onchange="" name="branch" id="branch">';
			textbranch+='<option value="0">Select</option>';
			for(i=0;i<hasil4.length;i++){
				
				textbranch+= '<option value="'+hasil4[i].BranchId+'">'+hasil4[i].BranchName+'</option>'	
			}
			textbranch+= '</select>';
			$('#brancharea').html(textbranch);
		
		}
	});

	function addNew(){
		if($('#txtusername').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Username must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtnama').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Employee name must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Phone number must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtaddress').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Address must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txttgl').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Entry Date must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#kategori').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Role harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#branch').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Role harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		
		var tanggal= new Date($('#txttgl').val());
		var id=(tanggal.getFullYear()+"")+""+(('0'+(tanggal.getMonth()+1)).slice(-2));

		var data= "Username="+$('#txtusername').val()+"&Name="+$('#txtnama').val()+"&Phone="+$('#txtphone').val()+"&Address="+$('#txtaddress').val()+"&Branch="+$('#branch').val()+"&Role="+$('#kategori').val()+"&Date="+$('#txttgl').val()+"&EmployeeId="+id;
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>User/insert",
			type : "post",
			data : data,	
			dataType : "json",			
			success : function(msg){
				if(msg.type=="failed")
				{
					alert(msg.error);
				}
				else
				{
					var hasil = JSON.parse(msg);
					if ( hasil == true || hasil == "true"){		
						location.reload();
					}
					else {
						
						setTimeout(function(){
							alert("failed");
						}, 2000);
					}
				}
			},
			error: function(ts) { alert(ts.responseText) }		
			});			
	}
	
	function updateData(){
		if($('#txtusername').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Username must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtnama').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Employee name must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtphone').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Phone number must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txtaddress').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Address must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#txttgl').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Entry Date must be filled</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#kategori').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Role harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#branch').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Role harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}


		var data= "Username="+$('#txtusername').val()+"&Name="+$('#txtnama').val()+"&Phone="+$('#txtphone').val()+"&Address="+$('#txtaddress').val()+"&Branch="+$('#branch').val()+"&Role="+$('#kategori').val()+"&Date="+$('#txttgl').val()+"&EmployeeId="+$('#hfid').val();
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>User/edit",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			},
			error: function(ts) { alert(ts.responseText) }	
			});
			
	}
	function deleteData(id){		
		var data="Id="+id;
		//alert(data);
		$.ajax({				
		url : "<?php echo base_url()?>User/delete",
		type : "post",
		data : data,	
		dataType : "json",				
		success : function(msg){
			var hasil = JSON.parse(msg);
			if ( hasil == true || hasil == "true"){					
				
				location.reload();
			}
			else {	
				
				setTimeout(function(){
					alert(hasil);
				}, 2000);
			}
		}
		});
	}

	function numberWithCommas(x) {
		var parts = x.toString().replace(/,/g,"")
	    parts = parts.toString().split(".");
	    //alert(parts[0])
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#hfid').val('');
		$('#txtnama').val('');
		$('#txtphone').val('');
		$('#txtaddress').val('');
		$('#txtusername').val('');
		$('#kategori').val('0');
		$('#branch').val('0');
		$('#txttgl').val('');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");
	}
	function setUpdate(Id, Name, PhoneNumber, Address, Username, RoleId, BranchId, EntryDate){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#hfid').val(Id);
		$('#txtnama').val(Name);
		$('#txtphone').val(PhoneNumber);
		$('#txtaddress').val(Address);
		$('#txtusername').val(Username);
		$('#kategori').val(RoleId);
		$('#branch').val(BranchId);
		$('.datepicker').datepicker('update', EntryDate);
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(Id, Name, RoleName, BranchName){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(Id);
		$('#deletenama').html(Name);
		$('#deleterole').html(RoleName);
		$('#deletebranch').html(BranchName);
		$('#deleteBtn').attr("onclick","deleteData('"+Id+"')");
	}
	/* function setUpdate(idcust,namacust,alamatcust,telpcust){
		$('#dialog').modal('show');
		$('#custid').val(idcust);
		$('#custname').val(namacust);
		$('#custaddress').val(alamatcust);
		$('#custphone').val(telpcust);
		$('#btnSave').attr("onclick","updateData()");			
	} */
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
             
            
        });
		
		
    </script>
</body>
</html>