<script type="text/javascript">
        function setChPass(){
        $("#errorplacechpass").html("");
        $("#errorplacechpass").removeClass("alert alert-dismissable alert-warning");
        $('#txtold').val('');
        $('#txtnew').val('');
        $('#txtnew2').val('');
        $('#chpass').modal('show'); 
        $('#btnSavePass').attr("onclick","chPass()");           
    }
    </script>
    <div id="chpass" class="modal"> 
        <div id="page-wrapper">
            <div align="center" class="row">
                <div align="left" class="col-lg-6" style="float:none;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 name="tes00" id="tes00" class="panel-title"><i class="fa fa-bar-chart-o"></i> Ubah Password </h3>
                        </div>
                        <div id="panchpass" class="panel-body">
                            <div id="errorplacechpass" class="">
                                
                            </div>
                            <form method="post" id="fm0" name="fm0">
                                <div class="form-group">
                                    <label>ID Karyawan</label>
                                    <input id="txtusr" name="txtusr" disabled="disabled" value="<?=$this->session->userdata('UserId')?>" class="form-control">
                                </div>                          
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" id="txtold" name="txtold" class="form-control">            
                                </div>                          
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input id="txtnew" name="txtnew" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Confirm New Password</label>
                                    <input id="txtnew2" name="txtnew2" class="form-control" >        
                                </div>
                                
                                <div align="center" class="form-group">
                                    <button id="btnSavePass" type="button" class="btn btn-default">Submit</button>
                                </div>  
                            </form>         
                        </div>
                    </div>              
                </div>
            </div>
        </div>
    </div>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">    
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Ichiban Dorayaki</a>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul id="active" class="nav navbar-nav side-nav">
            <li class="selected"><a href="<?php echo base_url()?>home"><i class="fa fa-bullseye"></i> Home</a></li>
            <li class="dropdown">
                <a class="dropdown-toogle" data-toggle="dropdown" role="button" href="#">
                    <i class="fa fa-desktop"></i> Master
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="<?php echo base_url()?>Branch">Branch</a></li>
                    <li><a href="<?php echo base_url()?>Role">Role</a></li>
                    <li><a href="<?php echo base_url()?>Employee">Employee</a></li>
                    <li><a href="<?php echo base_url()?>Customer">Customer</a></li>
                    <li><a href="<?php echo base_url()?>Item">Item</a></li>
                    <li><a href="<?php echo base_url()?>Category">Category</a></li>
                    <li><a href="<?php echo base_url()?>Menu">Menu</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toogle" data-toggle="dropdown" role="button" href="#">
                    <i class="fa fa-desktop"></i> Transaction
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="<?php echo base_url()?>Sales">Sales</a></li>
                    <li><a href="<?php echo base_url()?>Purchase">Purchase</a></li>  
                    <li><a href="<?php echo base_url()?>Salesreport">Sales Report</a></li>  
                    <li><a href="<?php echo base_url()?>Purchasereport">Purchase Report</a></li>             
                </ul>
            </li>  
        </ul>
		<ul class="nav navbar-nav navbar-right navbar-user">                    
            <li class="dropdown user-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?=$nama?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <!--li><a onclick="setChPass();" href="#"><i class="fa fa-gear"></i> Change Password</a></li-->
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url()?>login/doLogout"><i class="fa fa-power-off"></i> Log Out</a></li>

                </ul>
            </li>                    
        </ul>
    </div>
</nav>