<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class jurnal extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function getJenisJurnal()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdJenisJurnal as IdJenisJurnal,NamaJurnal as NamaJurnal", "jenisjurnal a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getHeaderJurnal()
	{	
		$nojurnal=$this->input->post('NoJurnal');

		$result = $this->all_model->query_data("select a.NoJurnal as NoJurnal,IdTransaksi,TanggalTransaksi,Deskripsi,Status,b.NoAkun,NamaAkun,Debit,Kredit from jurnal a join detailjurnal b on a.nojurnal=b.nojurnal join tabelakun c on b.noakun=c.noakun where a.activeyn='Y' and a.nojurnal='".$nojurnal."' order by debit desc", false);
		if(!$result){
			$result= "0";
		}
		echo json_encode($result);
		exit();
	}
	
	public function getNamaAkun(){	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("NoAkun as NoAkun,NamaAkun as NamaAkun", "tabelAkun a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getNamaAkun2(){	
		$result = $this->all_model->query_data("select NoAkun, NamaAkun from tabelakun a where a.activeyn='Y' order by NoAkun asc", false);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getJurnalCode(){
		$result = $this->all_model->query_data("SELECT right(nojurnal,3) as nojurnal FROM jurnal ORDER BY nojurnal DESC", true);
		$result["nojurnal"] = $result["nojurnal"]+1;
		if($result["nojurnal"] < 10)
			$result["nojurnal"] = substr(date('Y'),2,2).date('md')."00".$result["nojurnal"];
		else if($result["nojurnal"]< 100)
			$result["nojurnal"] = substr(date('Y'),2,2).date('md')."0".$result["nojurnal"];
		else
			$result["nojurnal"] = substr(date('Y'),2,2).date('md').$result["nojurnal"];
		//return $result["nojurnal"];
		echo json_encode($result);
		exit();
	}
	
	public function getDataNew($request = true){
		$search = array(
				
			);
		$where = array(
			'a.ActiveYN'=> 'Y'
		);
		$join = array(
			array('table'=>'detailjurnal b','field' => 'a.nojurnal = b.nojurnal','method'=>'Left'),
			array('table'=>'tabelakun c','field' => 'b.noakun = c.noakun','method'=>'Left'),
			array('table'=>'msuser e','field' => 'a.NIP = e.iduser','method'=>'Left'),
		);
		$result2 = $this->all_model->get_data("a.NoJurnal as NoJurnal, Nama as Nama,TanggalTransaksi as TanggalTransaksi, NamaAkun as NamaAkun, Debit as Debit, Kredit as Kredit", "jurnal a",$join, $where, $search, false);

		return $result2;
		echo json_encode($result2);		
		exit();
	}
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;

			$search = array(
					'a.NoJurnal' => $this->input->post('search')
				);
			$where = array(
			);
		$join = array(
			array('table'=>'detailjurnal b','field' => 'a.nojurnal = b.nojurnal','method'=>'Left'),
			array('table'=>'tabelakun c','field' => 'b.noakun = c.noakun','method'=>'Left'),
			array('table'=>'jenisjurnal d','field' => 'a.idjenisjurnal = d.idjenisjurnal','method'=>'Left'),
			array('table'=>'msuser e','field' => 'a.NIP = e.iduser','method'=>'Left'),
			array('table'=>'transaksi f','field' => 'a.idtransaksi = f.idtransaksi','method'=>'Left'),
		);
			
		$result = $this->all_model->get_data("a.NoJurnal as NoJurnal, Nama as Nama,TanggalTransaksi as TanggalTransaksi, NamaAkun as NamaAkun, Debit as Debit, Kredit as Kredit, NoTransaksi as NoTransaksi","jurnal a",$join, $where, $search, false, $perPage, $segmen, false,"nojurnal","ASC");
		$result2 = $this->all_model->get_data("a.NoJurnal as NoJurnal, Nama as Nama,TanggalTransaksi as TanggalTransaksi, NamaAkun as NamaAkun, Debit as Debit, Kredit as Kredit, NoTransaksi as NoTransaksi","jurnal a", $join, $where, $search, false);
	

		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
	
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function getData2($perPage=5, $segmen=0,  $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;

			$search = array(
					'a.TanggalTransaksi' => $this->input->post('search')
				);
			$where = array(
			);
		$join = array(
			array('table'=>'detailjurnal b','field' => 'a.nojurnal = b.nojurnal','method'=>'Left'),
			array('table'=>'tabelakun c','field' => 'b.noakun = c.noakun','method'=>'Left'),
			array('table'=>'jenisjurnal d','field' => 'a.idjenisjurnal = d.idjenisjurnal','method'=>'Left'),
		);
			
		$result = $this->all_model->get_data("a.NoJurnal as NoJurnal, TanggalTransaksi as TanggalTransaksi, NamaAkun as NamaAkun, Debit as Debit, Kredit as Kredit, NamaJurnal as NamaJurnal","jurnal a",$join, $where, $search, false, $perPage, $segmen, false,"nojurnal","ASC");
		$result2 = $this->all_model->get_data("a.NoJurnal as NoJurnal, TanggalTransaksi as TanggalTransaksi, NamaAkun as NamaAkun, Debit as Debit, Kredit as Kredit, NamaJurnal as NamaJurnal","jurnal a", $join, $where, $search, false);
	

		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
	
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function insertJurnal(){
		
		$data = array(
		'NoJurnal'  => $this->input->post('NoJurnal'),
		'TanggalTransaksi' => $this->input->post('TanggalTransaksi'),
		//'NoTransaksi' => $this->input->post('NoTransaksi'),
		'Deskripsi' => $this->input->post('Deskripsi'),
		'Status' => 'Approved',
		'IdJenisJurnal' => $this->input->post('JenisJurnal'),
		'TanggalInput' => date("Y-m-d H:i:s"),
		'Validator' => '',
		'NIP' => $this->session->userdata("UserId"),
		'ActiveYN' => 'Y',
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("jurnal", $data );
		
		echo json_encode($query);
		exit();
	}
	
	public function insertDetailJurnal(){
		
		$data = array(
		'NoJurnal'  => $this->input->post('NoJurnal'),
		'NoAkun' => $this->input->post('NoAkun'),
		'Debit' => $this->input->post('Debit'),
		'Kredit' => $this->input->post('Kredit'),		
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("detailjurnal", $data );
		
		echo json_encode($query);
		exit();
	}
	
	public function approveJurnal(){
		$data = array(
		'Status' => 'Approved',
		'Validator' => $this->session->userdata("UserId"),		
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")			
			);
		$where = array(	
					'NoJurnal'			=> $this->input->post('NoJurnal'),
				
		);	
		$query = $this->all_model->update_data("jurnal", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function tolakJurnal(){
		$data = array(
		'Status' => 'Denied',
		'Validator' => $this->session->userdata("UserId"),		
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")			
			);
		$where = array(	
					'NoJurnal'			=> $this->input->post('NoJurnal'),
				
		);	
		$query = $this->all_model->update_data("jurnal", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function index(){
		/* if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh"); */
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="Jurnal";
		$data['nama']=$this->session->userdata('Nama');
		//$data['data3'] = json_encode($this->getJenisJurnal());
		$data['akun'] = json_encode($this->getNamaAKun2());
		//$data['NoJurnal'] = json_encode($this->getJurnalCode());
		//$data['rolez']=$this->session->userdata("Role");
		//echo $this->session->userdata("Role");
		//die();
		$data['page']="jurnal_view";
		$data['data'] = json_encode($this->getDataNew(false));
		//$data['include']=$this->load->view('script','',true);
		$this->load->view('main',$data);
	}
	
	public function addAkun(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['data3'] = json_encode($this->getKelompokAkun());
		$data['page_title']="CIPS - Tambah Akun";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('addakun_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

