		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Profit and Loss <small>Laporan Laba Rugi</small></h1>                    
                </div>
            </div>	           
			<div class="row">
				<div class="col-lg-12">
					
				</div>
			</div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Laporan Laba Rugi </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
					<form method='post' action='<?php echo base_url(); ?>profitnloss/printPnL' id='fm1' name='fm1'>
					Dari Tanggal:<input type='date' id='dob1' name='dob1' class="form-control"/>
					Sampai Tanggal:<input type='date' id='dob1' name='dob2' class="form-control"/>
					<input class='btn btn-primary' type='submit' value=' Buat Laporan Laba Rugi ' style='margin-right:15px;margin-top:15px;' /><br/>
					</form>
					</div>				
                        </div>
                    </div>
                </div>
            </div>			 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Expense </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div class="form-group">
								<label>ID Expense</label>
								<input id="expenseid" name="expenseid" disabled="true" value="" class="form-control">						
							</div>							
							<div class="form-group">
								<label>Nama Expense</label>
								<input id="expensename" name="expensename" class="form-control">				
							</div>
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>
							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Expense </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-6" style="float:none;">
							<h4>Yakin Hapus Data Ini?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Id Expense</th>
									<th>Nama Expense</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="deleteid"></div></td>
									<td><div id="deletenama"></div></td>
								</tr>							
							</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="submit" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	$(document).ready(function () {
		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
			
		});
	});
	
	function addNew(){
		if($('#expensename').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Expense Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
	
	
		var data= "Nama="+$('#expensename').val();;
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>expense/insertExpense",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					
					alert("sukses");
					setTimeout(function(){
						
						location.reload();
						}, 1500);
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
			});			
	}
	
	
	
	function updateData(){
		if($('#expensename').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Expense Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		
		var data= "IdExpense="+$('#expenseid').val()+"&Nama="+$('#expensename').val();
			$.ajax({				
			url : "<?php echo base_url()?>expense/editExpense",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					alert("sukses");
					setTimeout(function(){
						
						location.reload();
						}, 1500);
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
			
	}
	function deleteData(id){		
			var data= "IdExpense="+id;
			$.ajax({				
			url : "<?php echo base_url()?>expense/deleteExpense",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){			
				alert("sukses");				
					setTimeout(function(){
						
						location.reload();
						}, 1500);
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}

	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#barangname').val('');
		$('#harga').val('');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");			
	}
	
	function setUpdate(id,nama){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#expenseid').val(id);
		$('#expensename').val(nama);
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(id,nama){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(id);
		$('#deletenama').html(nama);
		$('#deleteBtn').attr("onclick","deleteData('"+id+"')");			
	}
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>;		         
        
            
            $("#grid").shieldGrid({
                dataSource: {
                    data: hasil
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: true,
				selection: 
				{
					type: "cell",
					multiple: false,
					toggle: false
				},
				events: {
                    selectionChanged: onSelectedChanged,
					doubleClick: onDouble
				},				
                columns: [
                { field: "IdExpense", width: "170px", title: "IdExpense" },
				{ field: "Nama", title: "Nama Expense"},
				{
					width: "12%",
					title: " ",
					buttons: [						
						{
							cls: "button button-primary",
							caption: " Edit",
							commandName: "details", // build in - edit, delete
							click: function(rowIndex) {
								//alert(rowIndex);
								var id=$("#grid").swidget().dataItem(rowIndex).IdExpense;
								var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
								setUpdate(id,nama);
							}
						},
						{
							cls: "mybuttonCssClass",
							caption: " Delete",
							commandName: "details", // build in - edit, delete
							click: function(rowIndex) {
								//alert(rowIndex);
								var id=$("#grid").swidget().dataItem(rowIndex).IdExpense;
								var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
								setDelete(id,nama);
							}
						}
					]
				}
                ]
            });            
        });
		
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
