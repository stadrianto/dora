<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class rekonsiliasi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function getAkunBank()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y','a.idkelompokGL'=>'112');		
		$result = $this->all_model->get_data("NoAkun as NoAkun,NamaAkun as NamaAkun", "tabelakun a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function getDataJurnal($noakun,$query1)
	{
		//echo "SELECT a.NoJurnal as NoJurnal,TanggalTransaksi as TanggalTransaksi,Debit as Debit,Kredit as Kredit,Deskripsi as Deskripsi FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where noakun='".$noakun."' and nojurnal in ".$query1;
		$result = $this->all_model->query_data("SELECT a.NoJurnal as NoJurnal,TanggalTransaksi as TanggalTransaksi,Debit as Debit,Kredit as Kredit,Deskripsi as Deskripsi FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where noakun='".$noakun."' and a.nojurnal not in ".$query1, false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}	
	public function getDataHeader($akun)
	{
		$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NamaAkun as NamaAkun,SaldoAwal as SaldoAwal,IdNeraca as IdNeraca FROM tabelakun a join kelompokakun b on a.idkelompokakun=b.idkelompokakun where noakun='".$akun."'", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getJurnalAwal($noakun,$query1)
	{
		$result = $this->all_model->query_data("SELECT a.NoJurnal as NoJurnal,TanggalTransaksi as TanggalTransaksi,Debit as Debit,Kredit as Kredit FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where noakun='".$noakun."' and tanggaltransaksi < '".$tanggal1."'", false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function printRekonsiliasi($paramet)
	{
		//$noproject = $this->session->userdata('NoProject');
		//$dataproject = $this->getDataTrial();
		$saldo=0;
		if(strpos($paramet,'W') !== false)
		{
			$data=explode("W",$paramet);
			$query1="('".$data[2]."'";
			for($i=3;$i<count($data);$i++)
			{
				$query1.=",'".$data[$i]."'";
			}
			$query1.=")";
		}
		else
		{
			$data[0]=$paramet;
			$query1="('')";
		}
		//echo "SELECT a.NoJurnal as NoJurnal,TanggalTransaksi as TanggalTransaksi,Debit as Debit,Kredit as Kredit,Deskripsi as Deskripsi FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where noakun='".$data[0]."' and nojurnal in ".$query1;
		//die();
		$akunbank=$this->input->post('AkunBank');
		$tanggal1=$this->input->post('dob');
		$tanggal2=$this->input->post('dob1');
		$qtanggal1=substr($tanggal1,6,4).'-'.substr($tanggal1,0,2).'-'.substr($tanggal1,3,2);
		$qtanggal2=substr($tanggal2,6,4).'-'.substr($tanggal2,0,2).'-'.substr($tanggal2,3,2);
		$result2 = $this->getDataHeader($data[0]);			
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);		
		$this->fpdf->Image('images/logo.png',10,0,50,0,'','http://www.cips.or.id/'); 
		
		
		for($z=0;$z<count($result2);$z++)
		{	
			$this->fpdf->AddPage();
			$y_axis_initial = 60;
			//$saldo=floatval($result2[$z]["SaldoAwal"]);			
			
			/*
			$result3 = $this->getJurnalAwal($result2[$z]["NoAkun"]."",$query1);
			if($result3 != "0")
			{
				for($x=0;$x<count($result3);$x++)
				{
					if($result2[$z]["IdNeraca"]=="1")
					{
						$saldo+=floatval($result3[$x]["Debit"]);
						$saldo-=floatval($result3[$x]["Kredit"]);
					}
					else
					{	
						$saldo-=floatval($result3[$x]["Debit"]);
						$saldo+=floatval($result3[$x]["Kredit"]);
					}
				}
			}*/
			$this->fpdf->SetY(20);
			$this->fpdf->SetFont('Arial','B',15);
			$this->fpdf->Image('images/logo.png',35,15,30,0,'','http://www.cips.or.id/'); 
			$this->fpdf->Cell(190,6,'PT. Citra Inti Prima Sejati',0,0,'C');
			$this->fpdf->Ln(5);
			$this->fpdf->SetFont('Arial','B',15);
			$this->fpdf->Cell(190,6,'Rekonsiliasi Bank',0,0,'C');
			$result = $this->getDataJurnal($result2[$z]["NoAkun"]."",$query1);
			$this->fpdf->SetFont('Arial','B',15);
			$this->fpdf->Ln(5);
			//$this->fpdf->Cell(190,6,$tanggal1.' - '.$tanggal2,0,0,'C');	.$result2[$z]["NamaAkun"]	
			$this->fpdf->Cell(190,6,$result2[$z]["NoAkun"].' - '.$result2[$z]["NamaAkun"],0,0,'C');					
			$this->fpdf->Ln(12);
			$this->fpdf->SetFont('Arial','',12);
			//$this->fpdf->Cell(190,6,'Saldo Awal : '.$saldo,0,0,'C');
			//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
			//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
			$this->fpdf->Ln(10);	
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->Cell(60,6,'Unreconciled Item',0,0,'L');
			$this->fpdf->Cell(130,6,'Saldo Buku: '.$data[1],0,0,'R');
			$this->fpdf->SetY($y_axis_initial);			
			$this->fpdf->SetX(5);
			//Header tabel halaman 1
			$this->fpdf->CELL(20,6,'NoJurnal',1,0,'C',1);
			$this->fpdf->Cell(30,6,'TanggalTransaksi',1,0,'C',1);
			$this->fpdf->Cell(30,6,'Debit',1,0,'C',1);
			$this->fpdf->Cell(30,6,'Kredit',1,0,'C',1);
			$this->fpdf->Cell(35,6,'Saldo',1,0,'C',1);
			$this->fpdf->Cell(55,6,'Deskripsi',1,0,'C',1);
			//$y_axis_initial += (6*count($result))+40;
			$this->fpdf->Ln();
			$max=25;//max baris perhalaman
			$i=0;
			$no=0;
			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			
			if($result != "0")
			{
				foreach($result as $key => $value)
				{
					$i++;
					//$total += $row['Total'];

					if ($y_axis_initial>250){               //jika $i=25 maka buat header baru seperti di atas
					$this->fpdf->AddPage();
					$this->fpdf->SetY(10);
					$this->fpdf->SetX(5);
					$this->fpdf->CELL(25,6,'NoJurnal',1,0,'C',1);
					$this->fpdf->Cell(30,6,'TanggalTransaksi',1,0,'C',1);
					$this->fpdf->Cell(30,6,'Debit',1,0,'C',1);
					$this->fpdf->Cell(30,6,'Kredit',1,0,'C',1);
					$this->fpdf->Cell(35,6,'Saldo',1,0,'C',1);
					$this->fpdf->Cell(55,6,'Deskripsi',1,0,'C',1);
					$this->fpdf->SetY(10);
					$this->fpdf->SetX(55);
					$y_axis = $y_axis + $row_height;
					
					//$i=0;
					$this->fpdf->Ln();
					}

					//$grandtotal+=$row['Total'];
					
					if($result2[$z]["IdNeraca"]=="1")
					{
						$saldo+=floatval($result[$key]["Debit"]);
						$saldo-=floatval($result[$key]["Kredit"]);
					}
					else
					{	
						$saldo-=floatval($result[$key]["Debit"]);
						$saldo+=floatval($result[$key]["Kredit"]);
					}				
					$i++;
					$no++;
					$this->fpdf->SetX(5);
					//$this->fpdf->Cell(10,6,$no,1,0,'C',0);
					$this->fpdf->Cell(20,6,$result[$key]["NoJurnal"],1,0,'C',0);
					$this->fpdf->Cell(30,6,$result[$key]["TanggalTransaksi"],1,0,'C',0);
					$this->fpdf->Cell(30,6,$this->all_model->rp($result[$key]["Debit"]),1,0,'R',0);
					$this->fpdf->Cell(30,6,$this->all_model->rp($result[$key]["Kredit"]),1,0,'R',0);
					$this->fpdf->Cell(35,6,$this->all_model->rp($saldo),1,0,'R',0);
					$this->fpdf->Cell(55,6,$result[$key]["Deskripsi"],1,0,'L',0);
					$this->fpdf->Ln();
				}
			}

			//buat footer
			$now = date("d F Y H:i:s");
			$this->fpdf->Ln();
			$this->fpdf->SetX(0);			
			$this->fpdf->Cell(191,6,"Saldo: ".($data[1]+$saldo),0,0,'R');
			$this->fpdf->Ln();
			$this->fpdf->SetFont('Arial','B',10);
			$printer=$this->getDataPrinter($this->session->userdata("UserId"));
			$this->fpdf->SetX(0);			
			$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'C');
			$this->fpdf->SetX(40);
			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
			$this->fpdf->Ln();			
		}
		$this->fpdf->Output('General Ledger'.date("F Y").'.pdf', 'I');
	}
	public function getDataPrinter($id)
	{
		$result = $this->all_model->query_data("SELECT Nama as Nama from msuser where iduser=".$id, false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['data3'] = json_encode($this->getAkunBank());
		$data['page_title']="CIPS - Rekonsiliasi";
		//$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('rekonsiliasi_view');
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

