		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Chart Of Account <small>Data Akun</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New Akun</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Akun List </h3>
                        </div>
                        <div class="panel-body">
                            <div id="grid"></div>				
                        </div>
                    </div>
                </div>
            </div>			 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Account </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div class="form-group">
								<label>KelompokGL</label>
								<div id="akunarea"></div>			
							</div>
							<div class="form-group">
								<label>No Akun</label>
								<input readonly="readonly" type="text" name="NoAkun" id="NoAkun" size="2" value="" />-<input readonly="readonly" type="text" name="NoAkun1" id="NoAkun1" size="9" value="" />
								<input size="9" id="NoAkun2" name="NoAkun2" value="" class="form-control">						
							</div>							
							<div class="form-group">
								<label>Nama Akun</label>
								<input id="NamaAkun" name="NamaAkun" class="form-control">				
							</div>							
							<div class="form-group">
								<label>Saldo Awal</label>
								<input id="SaldoAwal" name="SaldoAwal" class="form-control" >
							</div>							
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Akun </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-6" style="float:none;">
							<h4>Yakin Hapus Data Ini?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>No Akun</th>
									<th>Nama Akun</th>
									<th>Kelompok Akun</th>
									<th>Kelompok GL</th>
									<th>Saldo Awal</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="deleteid"></div></td>
									<td><div id="deletenama"></div></td>
									<td><div id="deletekelakun"></div></td>
									<td><div id="deletekelgl"></div></td>
									<td><div id="deletesawal"></div></td>
								</tr>							
							</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="submit" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	function getId()
	{
		$('#NoAkun').val($('#IdKelompokGL').val().substring(0,1));
		$('#NoAkun1').val($('#IdKelompokGL').val().substring(0,4));
	}
	
	$(document).ready(function () {
		var hasil3 = JSON.parse('<?php echo $kategori; ?>');
		var text = '';
		if(hasil3 == "No Data"){
			$('#akunarea').html("No Active Departemen");
		}else{
			text+= '<select class="form-control" onchange="getId()" name="IdKelompokGL" id="IdKelompokGL">';
			text+='<option value="0">Select</option>';
			for(i=0;i<hasil3.length;i++){
				
				text+= '<option value="'+hasil3[i].IdKelompokGL+'">'+hasil3[i].IdKelompokGL+'-'+hasil3[i].NamaGL+'</option>'	
			}
			text+= '</select>';
			$('#akunarea').html(text);
		
		}
		/* $("#test1").on("click", "#btnSave", function(){			
		}); */
		
		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
		});
	});
	
	function addNew(){
		if($('#IdKelompokGL').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Kategori Barang harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(($('#NoAkun').val()+$('#NoAkun1').val()+$('#NoAkun2').val()).length<7 ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>NoAkun Minimal 7 digit</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#NamaAkun').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Akun Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if($('#SaldoAwal').val()== ""){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Saldo Awal harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(isNaN($('#SaldoAwal').val())==true ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Saldo Awal harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		var test1=$('#NoAkun').val()+$('#NoAkun1').val()+$('#NoAkun2').val();
		var data = "NoAkun="+test1+"&NamaAkun="+$('#NamaAkun').val()+"&SaldoAwal="+$('#SaldoAwal').val()+"&IdKelompokGL="+$('#NoAkun1').val()+"&IdKelompokAkun="+$('#NoAkun').val();
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>akun/insertAkun",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					alert("sukses");
					setTimeout(function(){
			
						location.reload();
						}, 1500);
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
			});			
	}
	
	
	function updateData(){
		if($('#NamaAkun').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Akun Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if($('#SaldoAwal').val()== ""){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Saldo Awal harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(isNaN($('#SaldoAwal').val())==true ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Saldo Awal harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		var test1=$('#NoAkun').val()+$('#NoAkun1').val()+$('#NoAkun2').val();
		var data = "NoAkun="+test1+"&NamaAkun="+$('#NamaAkun').val()+"&SaldoAwal="+$('#SaldoAwal').val()+"&IdKelompokGL="+$('#NoAkun1').val()+"&IdKelompokAkun="+$('#NoAkun').val();
			$.ajax({				
			url : "<?php echo base_url()?>akun/editAkun",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					alert("sukses");
					setTimeout(function(){
			
						location.reload();
						}, 1500);
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
			
	}
	function deleteData(id){		
			var data= "NoAkun="+id;
			$.ajax({				
			url : "<?php echo base_url()?>akun/deleteAkun",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#NoAkun2').removeAttr("readonly");
		$('#IdKelompokGL').removeAttr("disabled");
		$('#NoAkun').val('');
		$('#NoAkun1').val('');
		$('#NoAkun2').val('');
		$('#NamaAkun').val('');
		$('#SaldoAwal').val('');
		$('#IdKelompokGL').val('0');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");			
	}

	function setUpdate(NoAkun,NamaAkun,SaldoAwal){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#NoAkun').val(NoAkun.substring(0,1)+'');
		$('#NoAkun1').val(NoAkun.substring(1,4)+'');
		$('#NoAkun2').val(NoAkun.substring(4,7)+'');
		$('#NoAkun2').attr("readonly",true);
		$('#IdKelompokGL').attr("disabled",true);
		$('#NamaAkun').val(NamaAkun+'');
		$('#SaldoAwal').val(SaldoAwal+'');
		$('#IdKelompokGL').val(NoAkun.substring(1,4)+'');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(id,nama,kelakun,kelgl,sawal){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(id);
		$('#deletenama').html(nama);
		$('#deletekelakun').html(kelakun);
		$('#deletekelgl').html(kelgl);
		$('#deletesawal').html(sawal);
		$('#deleteBtn').attr("onclick","deleteData('"+id+"')");			
	}
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>,
				kategori = <?=$kategori?>;

            if(hasil.length>0)
			{
				$("#grid").shieldGrid({
					dataSource: {
						data: hasil
					},
					sorting: {
						multiple: true
					},
					rowHover: false,
					paging: true,
					selection: 
					{
						type: "cell",
						multiple: false,
						toggle: false
					},
					events: {
						selectionChanged: onSelectedChanged,
						doubleClick: onDouble
					},				
					columns: [
					{ field: "NoAkun", width: "170px", title: "NoAkun"},
					{ field: "NamaAkun", title: "NamaAkun"},
					{ field: "NamaKelompokAkun", title: "NamaKelompokAkun" },       
					{ field: "KelompokGL", title: "KelompokGL" },
					{ field: "SaldoAwal", title: "SaldoAwal" },
					{
						width: "12%",
						title: " ",
						buttons: [						
							{
								cls: "button button-primary",
								caption: " Edit",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).NoAkun;
									var nama=$("#grid").swidget().dataItem(rowIndex).NamaAkun;
									var kelakun=$("#grid").swidget().dataItem(rowIndex).NamaKelompokAkun;
									var kelgl=$("#grid").swidget().dataItem(rowIndex).KelompokGL;
									var sawal=$("#grid").swidget().dataItem(rowIndex).SaldoAwal;
									setUpdate(id,nama,sawal); 
								}
							},
							{
								cls: "mybuttonCssClass",
								caption: " Delete",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).NoAkun;
									var nama=$("#grid").swidget().dataItem(rowIndex).NamaAkun;
									var kelakun=$("#grid").swidget().dataItem(rowIndex).NamaKelompokAkun;
									var kelgl=$("#grid").swidget().dataItem(rowIndex).KelompokGL;
									var sawal=$("#grid").swidget().dataItem(rowIndex).SaldoAwal;
									setDelete(id,nama,kelakun,kelgl,sawal);
								}
							}
						]
					}
					]
				});  
			}			
        });
		
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
