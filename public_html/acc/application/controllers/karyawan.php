<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Karyawan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	
	public function getNewNIP(){
		$result = $this->all_model->query_data("SELECT RIGHT(NIP,3) as NIP FROM mskaryawan ORDER BY NIP DESC", true);
		$result["NIP"] = $result["NIP"]+1;
		if($result["NIP"] < 10)
			$result["NIP"] = "K00".$result["NIP"];
		else if($result["NIP"]< 100)
			$result["NIP"] = "K0".$result["NIP"];
		else
			$result["NIP"] = "K".$result["NIP"];
		return $result["NIP"];
	}
	
	public function getDataDepartemen()
	{
		$join = array();
		$where = array(
			'a.ActiveYN'	=>"Y"
		);	
		$result = $this->all_model->get_data("IdDepartemen as IdDepartemen,Nama as NamaDepartemen", "msdepartemen a",$join, $where, array(),false);
		if(!$result){
			$result= "No Data";
		}
		echo json_encode($result);
		exit();
	}
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdDepartemen' => $this->input->post('menu'),
			);
		}else{
			$search = array(
					'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
				array('table'=>'msdepartemen b','field' => 'a.IdDepartemen = b.IdDepartemen','method'=>'Left'),
				array('table'=>'msjabatan c','field' => 'a.IdJabatan = c.IdJabatan','method'=>'Left'),			
			);			
						
		$result = $this->all_model->get_data("NIP as NIP,a.Nama as Nama,NamaPanggilan as NamaPanggilan,JenisKelamin as JenisKelamin,StatusMarital as StatusMarital, AnakKe as AnakKe, JumlahSaudara as JumlahSaudara,JumlahAnak as JumlahAnak
		,StatusMarital as StatusMarital, AnakKe as AnakKe, JumlahSaudara as JumlahSaudara,JumlahAnak as JumlahAnak,GolonganDarah as GolonganDarah, TempatLahir as TempatLahir, TanggalLahir as TanggalLahir,Agama as Agama,StatusPendidikan as StatusPendidikan, Alamat as Alamat, Telepon as Telepon,NoHandphone as NoHandphone,TanggalMasuk as TanggalMasuk, TanggalKeluar as TanggalKeluar, StatusKerja as StatusKerja,LamaKontrak as LamaKontrak, BankKaryawan as BankKaryawan,NoRekening as NoRekening,a.IdJabatan as IdJabatan, a.IdDepartemen as IdDepartemen,b.Nama as NamaDepartemen,c.Nama as Jabatan, KTP as KTP,NPWP as NPWP, BPJS as BPJS,GajiPokok as GajiPokok", "mskaryawan a",$join, $where, $search, false, $perPage, $segmen, false,"NIP","ASC");
		
		$result2 = $this->all_model->get_data("NIP as NIP,a.Nama as Nama,NamaPanggilan as NamaPanggilan,JenisKelamin as JenisKelamin,StatusMarital as StatusMarital, AnakKe as AnakKe, JumlahSaudara as JumlahSaudara,JumlahAnak as JumlahAnak
		,StatusMarital as StatusMarital, AnakKe as AnakKe, JumlahSaudara as JumlahSaudara,JumlahAnak as JumlahAnak,GolonganDarah as GolonganDarah, TempatLahir as TempatLahir, TanggalLahir as TanggalLahir,Agama as Agama,StatusPendidikan as StatusPendidikan, Alamat as Alamat, Telepon as Telepon,NoHandphone as NoHandphone,TanggalMasuk as TanggalMasuk, TanggalKeluar as TanggalKeluar, StatusKerja as StatusKerja,LamaKontrak as LamaKontrak, BankKaryawan as BankKaryawan,NoRekening as NoRekening,a.IdJabatan as IdJabatan, a.IdDepartemen as IdDepartemen,b.Nama as NamaDepartemen,c.Nama as Jabatan, KTP as KTP,NPWP as NPWP, BPJS as BPJS,GajiPokok as GajiPokok", "mskaryawan a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					//$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					//$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertKaryawan(){
		
		$data = array(
		'NIP'  => $this->input->post('NIP'),
		'Nama' => $this->input->post('Nama'),		
		'NamaPanggilan' => $this->input->post('NamaPanggilan'),	
		'JenisKelamin' 	=> $this->input->post('JenisKelamin'),
		'StatusMarital' => $this->input->post('StatusMarital'),	
		'AnakKe' => $this->input->post('AnakKe'),		
		'JumlahSaudara' => $this->input->post('JumlahSaudara'),	
		'JumlahAnak' => $this->input->post('JumlahAnak'),	
		'GolonganDarah' => $this->input->post('GolonganDarah'),	
		'TempatLahir' => $this->input->post('TempatLahir'),	
		'TanggalLahir' 	=> $this->input->post('TanggalLahir'),
		'Agama' => $this->input->post('Agama'),		
		'StatusPendidikan' => $this->input->post('StatusPendidikan'),
		'Alamat' => $this->input->post('Alamat'), 		
		'Telepon' => $this->input->post('Telepon'),	
		'NoHandphone' => $this->input->post('NoHandphone'),	
		'TanggalMasuk' => $this->input->post('TanggalMasuk'),	
		'TanggalKeluar' => $this->input->post('TanggalKeluar'),	
		'StatusKerja' => $this->input->post('StatusKerja'),	
		'LamaKontrak' => $this->input->post('LamaKontrak'),	
		'BankKaryawan' => $this->input->post('BankKaryawan'),	
		'NoRekening' => $this->input->post('NoRekening'),	
		'IdJabatan' => $this->input->post('IdJabatan'), 	
		'IdDepartemen' => $this->input->post('IdDepartemen'),	
		'GajiPokok' => $this->input->post('GajiPokok'),	
		'KTP' => $this->input->post('KTP'),		
		'NPWP' => $this->input->post('NPWP'),		
		'BPJS' => $this->input->post('BPJS'),		
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("mskaryawan", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function editKaryawan(){
		$data = array(
			'Nama' => $this->input->post('Nama'),		
			'NamaPanggilan' => $this->input->post('NamaPanggilan'),	
			'JenisKelamin' 	=> $this->input->post('JenisKelamin'),
			'StatusMarital' => $this->input->post('StatusMarital'),	
			'AnakKe' => $this->input->post('AnakKe'),		
			'JumlahSaudara' => $this->input->post('JumlahSaudara'),	
			'JumlahAnak' => $this->input->post('JumlahAnak'),	
			'GolonganDarah' => $this->input->post('GolonganDarah'),	
			'TempatLahir' => $this->input->post('TempatLahir'),	
			'TanggalLahir' 	=> $this->input->post('TanggalLahir'),
			'Agama' => $this->input->post('Agama'),		
			'StatusPendidikan' => $this->input->post('StatusPendidikan'),
			'Alamat' => $this->input->post('Alamat'), 		
			'Telepon' => $this->input->post('Telepon'),	
			'NoHandphone' => $this->input->post('NoHandphone'),	
			'TanggalMasuk' => $this->input->post('TanggalMasuk'),	
			'TanggalKeluar' => $this->input->post('TanggalKeluar'),	
			'StatusKerja' => $this->input->post('StatusKerja'),	
			'LamaKontrak' => $this->input->post('LamaKontrak'),	
			'BankKaryawan' => $this->input->post('BankKaryawan'),	
			'NoRekening' => $this->input->post('NoRekening'),	
			'IdJabatan' => $this->input->post('IdJabatan'),
			'GajiPokok' => $this->input->post('GajiPokok'),	
			'IdDepartemen' => $this->input->post('IdDepartemen'),	
			'KTP' => $this->input->post('KTP'),		
			'NPWP' => $this->input->post('NPWP'),		
			'BPJS' => $this->input->post('BPJS'),		
			'ActiveYN' => 'Y',
			'UpdateId' => $this->session->userdata("UserId"),
			'UpdateTime' => date("Y-m-d H:i:s")		
			);
		$where = array(	
					'NIP'			=> $this->input->post('NIP'),
				
		);	
		$query = $this->all_model->update_data("mskaryawan", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function getDepartemen()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdDepartemen as IdDepartemen,Nama as NamaDepartemen", "msdepartemen a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getJabatan()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdJabatan as IdJabatan,Nama as Nama,Pekerjaan as Pekerjaan", "msjabatan a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
		
	public function getNameDepartemen($IdDepartemen)
	{
		$result = $this->all_model->query_data("SELECT Nama FROM msdepartemen where IdDepartemen='".$IdDepartemen."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result["Nama"];
	
	}
	
	public function getDataKaryawan($NIP)
	{
		$result = $this->all_model->query_data("SELECT * FROM mskaryawan where NIP='".$NIP."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Master Karyawan";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data2['data3'] = json_encode($this->getDepartemen());
		$data2['data4'] = json_encode($this->getJabatan());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('karyawan_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function addKaryawan(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Tambah Karyawan";
		$data2['data3'] = json_encode($this->getDepartemen());
		$data2['data4'] = json_encode($this->getJabatan());
		$data2['data5'] = json_encode($this->getNewNIP());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('addkaryawan_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function detailKaryawan($NIP){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Karyawan";
		$data2['data3'] = json_encode($this->getDepartemen());
		$data2['data4'] = json_encode($this->getJabatan());
		$data2['data5'] = json_encode($this->getDataKaryawan($NIP));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailkaryawan_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


