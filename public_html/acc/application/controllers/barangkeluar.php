<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class BarangKeluar extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function addBarangKeluar(){
		
		$data = array(
				'Keterangan' => $this->input->post('Keterangan'),
				'UpdateID' => $this->session->userdata("UserId"),
				'TanggalKeluar' => $this->input->post('TanggalKeluar'),
				'Tag' => $this->input->post('Tag'),
			);
		$where = array();
		$query = $this->all_model->insert_data("barangkeluar", $data , $where);
		
		echo json_encode($query);
		exit();
	}

	public function insertDetailBarangKeluar(){
		
		$data = array(
		'IdBarangKeluar'  => $this->input->post('IdBarangKeluar'),
		'IdBarang'  => $this->input->post('IdBarang'),
		'Nama' => $this->input->post('Nama'),		
		'Quantity' => $this->input->post('Quantity'),	
		'Keterangan' => $this->input->post('Keterangan'),
		'Jumlah' => $this->input->post('Jumlah'),
		'IdSupplier'  => $this->input->post('IdSupplier'),
		'Status' => 'Y',
			);
		$query = $this->all_model->insert_data("detailbarangkeluar", $data);
		
		echo json_encode($query);
		exit();
	}	
	
	public function editBarangKeluar(){
		$IdBarangKeluar = $this->input->post('IdBarangKeluar');
		
		$data = array(
				'Keterangan' => $this->input->post('Keterangan'),
				'UpdateID' => $this->session->userdata("UserId"),
				'TanggalKeluar' => $this->input->post('TanggalKeluar')
			);
		$where = array("IdBarangKeluar" => $IdBarangKeluar);
		$query = $this->all_model->update_data("barangkeluar", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	
	public function editDetailBarangKeluar(){
		$data = array(
		'IdBarang' => $this->input->post('IdBarang'),	
		'Nama' => $this->input->post('Nama'),		
		'Quantity' => $this->input->post('Quantity'),	
		'Jumlah' => $this->input->post('Jumlah'),
		'Keterangan' 	=> $this->input->post('Keterangan'),
		'IdSupplier'  => $this->input->post('IdSupplier'),
			);
		$where = array(	
					'IdBarang'=> $this->input->post('IdBarang2'),
					'IdBarangKeluar'=> $this->input->post('IdBarangKeluar'),
				
		);	
		$query = $this->all_model->update_data("detailbarangkeluar", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function finalizeDetailBarangKeluar(){
		$data = array(	
		'Quantity' => $this->input->post('Quantity'),
		'Jumlah' => $this->input->post('Jumlah'),
			);
		$where = array(	
					'IdBarang'=> $this->input->post('IdBarang'),
				
		);	
		$query = $this->all_model->update_data("barang", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function deleteBarangKeluar()
	{
		$IdBarangKeluar = $this->input->post('IdBarangKeluar');
		$where = array('IdBarangKeluar'=>$IdBarangKeluar);
		$query = $this->all_model->delete_data("barangkeluar", $where);
		echo json_encode($query);
		exit();
	}
	
	public function deleteDetailBarangKeluar()
	{
		$IdBarang = $this->input->post('IdBarang');
		$IdBarangKeluar = $this->input->post('IdBarangKeluar');
		$where = array('IdBarang'=>$IdBarang,'IdBarangKeluar'=>$IdBarangKeluar );
		$query = $this->all_model->delete_data("detailbarangkeluar", $where);
		echo json_encode($query);
		exit();
	}
	
	public function generateBarangKeluar()
	{ 
		$data["status"] = "sukses";
		$data["msg"] = $this->getAllBarangKeluar(100,0,false);
		echo json_encode($data);
		exit();
	}

	public function getAllBarangKeluar($perPage=100, $segmen=0, $request = true){
		$current = $this->input->post('page');
		
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
			);
		$join = array(
		);
		$where = array(
		);

		$result = $this->all_model->get_data("IdBarangKeluar as IdBarangKeluar,  Keterangan as Keterangan,a.UpdateID as UpdateID, TanggalKeluar as TanggalKeluar, Tag as Tag", "barangkeluar a",$join, $where, $search, false, $perPage, $segmen, false,"IdBarangKeluar","ASC");
		$result2 = $this->all_model->get_data("IdBarangKeluar as IdBarangKeluar,  Keterangan as Keterangan,a.UpdateID as UpdateID,TanggalKeluar as TanggalKeluar, Tag as Tag", "barangkeluar a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllBarangKeluar($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				
				foreach($result as $key => $value){	
				}
				
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllBarangKeluar($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function getDataDetailBarang($perPage=100, $segmen=0,  $request = true,$IdBarangKeluar){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdBarangKeluar' => $IdBarangKeluar
			);
		}else{
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdBarangKeluar' => $IdBarangKeluar
			);
		}
		$join = array(
			array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),
			array('table'=>'mssupplier c','field' => 'a.IdSupplier = c.IdSupplier','method'=>'Left'),
		);			
						
		$result = $this->all_model->get_data("IdDetailBarangKeluar as IdDetailBarangKeluar,IdBarangKeluar as IdBarangKeluar,a.IdBarang as IdBarang,a.Nama as Nama,a.Quantity as Quantity,(Select Satuan from satuan where IdSatuan = b.IdSatuan) as Satuan,Keterangan as Keterangan, Status as Status, (b.Jumlah/b.Quantity) AS Rata, a.Jumlah as Jumlah, c.Nama as NamaSupplier", "detailbarangkeluar a",$join, $where, $search, false, $perPage, $segmen, false,"IdBarangKeluar","ASC");
		
		$result2 = $this->all_model->get_data("IdDetailBarangKeluar as IdDetailBarangKeluar,IdBarangKeluar as IdBarangKeluar,a.IdBarang as IdBarang,a.Nama as Nama,a.Quantity as Quantity,(Select Satuan from satuan where IdSatuan = b.IdSatuan) as Satuan,Keterangan as Keterangan, status as Status,  (b.Jumlah/b.Quantity) AS Rata, a.Jumlah as Jumlah, c.Nama as NamaSupplier", "detailbarangkeluar a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailBarangKeluar(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailBarangKeluar(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
			
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function getBarang()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdBarang as IdBarang,Nama as Nama", "barang a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getSatuan()
	{	
		$search = array();
		$join = array(
				
		);
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("a.IdSatuan as IdSatuan,Satuan as Satuan", "satuan a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getSupplier()
	{	
		$search = array();
		$join = array(
				
		);
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("a.IdSupplier as IdSupplier,Nama as NamaSupplier", "mssupplier a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getBarangBar()
	{	
		$IdBarang = $this->input->post('IdBarang');
		$result = $this->all_model->query_data("SELECT IdBarang,Nama,Quantity,Jumlah FROM barang where IdBarang='".$IdBarang."'", true);
		
		if(!$result){
			$result= "0";
		}	
		echo json_encode($result);
		exit();
	}
	
	public function getDataBarangKeluar($IdBarangKeluar)
	{
		$result = $this->all_model->query_data("SELECT * FROM barangkeluar where IdBarangKeluar='".$IdBarangKeluar."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}
	
	public function getNamaBarang()
	{
		$IdBarang = $this->input->post('IdBarang');
		$result = $this->all_model->query_data("SELECT Nama FROM barang where IdBarang='".$IdBarang."'", true);
		
		if(!$result){
			$result= "0";
		}	
		echo json_encode($result);
		exit();
	}
	
	public function getQuantityBarang()
	{
		$IdBarang = $this->input->post('IdBarang');
		$result = $this->all_model->query_data("SELECT Quantity,Jumlah FROM barang where IdBarang = '".$IdBarang."'", true);
		
		if(!$result){
			$result= "0";
		}	
		echo json_encode($result);
		exit();
	}
	
	public function newStat()
	{
		$data = array(
		'Status' => 'N',		
			);
		$where = array(	
					'IdBarang'=> $this->input->post('IdBarang'),
					'IdBarangKeluar'=> $this->input->post('IdBarangKeluar'),
				
		);	
		$query = $this->all_model->update_data("detailbarangkeluar", $data ,$where);
		
		echo json_encode($query);
		exit();
		
	}
	
	public function insertHistory()
	{
		$data = array(
		'IdBarang' => $this->input->post('IdBarang'),
		'IdDetailBarangKeluar' => $this->input->post('IdDetailBarangKeluar'),	
		'SaldoQty' => $this->input->post('Quantity'),	
		'SaldoJumlah' => $this->input->post('Jumlah'),	
		'Tanggal' => $this->input->post('Tanggal'),	
		'Tag' => $this->input->post('Tag'),	
			);
		$where = array(	
				
		);	
		$query = $this->all_model->insert_data("historybar", $data ,$where);
		
		echo json_encode($query);
		exit();
		
	}
	
	public function getPrintBarangKeluar($Periode,$Periode2)
	{
		$result = $this->all_model->query_data("SELECT * FROM detailbarangkeluar a JOIN barangkeluar b ON a.IdBarangKeluar = b.IdBarangKeluar WHERE a.Status = 'N' AND b.TanggalKeluar >= '".$Periode."' AND b.TanggalKeluar <= '".$Periode2."' ORDER BY a.IdBarangKeluar", false);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function printBarangKeluar()
	{
		$Periode = $this->input->post('dob2');
		$Periode2 = $this->input->post('dob');
		$Periode=substr($Periode,6,10)."-".substr($Periode,0,2)."-".substr($Periode,3,2);
		$Periode2= substr($Periode2,6,10)."-".substr($Periode2,0,2)."-".substr($Periode2,3,2);
		$result = $this->getPrintBarangKeluar($Periode,$Periode2);
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(20);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Laporan Barang Keluar',0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(190,6,'Untuk Periode '.$Periode.' Hingga '.$Periode2.'',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','BU',12);
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->Ln(15);

		$y_axis_initial = 60;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(30,6,'IdBarangKeluar',1,0,'C',1);
		$this->fpdf->Cell(25,6,'IdBarang',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Nama',1,0,'C',1);
		$this->fpdf->Cell(15,6,'Quantity',1,0,'C',1);
		$this->fpdf->Cell(40,6,'Keterangan',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		//$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(30);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(30,6,'IdBarangKeluar',1,0,'C',1);
		$this->fpdf->Cell(25,6,'IdBarang',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Nama',1,0,'C',1);
		$this->fpdf->Cell(15,6,'Quantity',1,0,'C',1);
		$this->fpdf->Cell(40,6,'Keterangan',1,0,'C',1);

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		//$grandtotal+=$row['Total'];
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(30,6,$result[$key]["IdBarangKeluar"],1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["IdBarang"],1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["Nama"],1,0,'C',0);
		$this->fpdf->Cell(15,6,$result[$key]["Quantity"],1,0,'C',0);
		$this->fpdf->Cell(40,6,$result[$key]["Keterangan"],1,0,'C',0);

		$this->fpdf->Ln();

		}
		}

		//buat footer
		$now = date("d F Y");
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		//$this->fpdf->Cell(303,6,"Total Transaksi : ".rp($grandtotal)."",0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Output('Laporan Pekerja Proyek'.date("F Y").'.pdf', 'I');
		
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		
		$data['title']="CIPS - Master Barang Keluar";
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['page_title']="CIPS - Master Barang Keluar";
		$data2['data'] = json_encode($this->getAllBarangKeluar($config['per_page'], $config['segmen'],false));
		
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('barangkeluar_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function detailBarangKeluar($IdBarangKeluar){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Barang Keluar";
		$data2['data3'] = json_encode($this->getBarang());
		$data2['data4'] = json_encode($this->getSatuan());
		$data2['data5'] = json_encode($this->getDataBarangKeluar($IdBarangKeluar));
		$data2['data6'] = json_encode($this->getSupplier());
		$data2['data'] = json_encode($this->getDataDetailBarang($config['per_page'], $config['segmen'],false,$IdBarangKeluar ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailbarangkeluar_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


