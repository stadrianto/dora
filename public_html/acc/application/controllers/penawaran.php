<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Penawaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getSupplierCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdSupplier,4) as IdSupplier FROM mssupplier ORDER BY IdSupplier DESC", true);
		$result["IdSupplier"] = $result["IdSupplier"]+1;
		if($result["IdSupplier"] < 10)
			$result["IdSupplier"] = "C000".$result["IdSupplier"];
		else if($result["IdSupplier"]< 100)
			$result["IdSupplier"] = "C00".$result["IdSupplier"];
		else if($result["IdSupplier"]< 1000)
			$result["IdSupplier"] = "C0".$result["IdSupplier"];
		else
			$result["IdSupplier"] = "C".$result["IdSupplier"];
		return $result["IdSupplier"];
	}
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
			
			);
		}else{
			$search = array(
					'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array();			
						
		$result = $this->all_model->get_data("IdSupplier as IdSupplier,Nama as Nama,Alamat as Alamat,Telepon as Telepon,Handphone as Handphone", "mssupplier a",$join, $where, $search, false, $perPage, $segmen, false,"IdSupplier","ASC");
		
		$result2 = $this->all_model->get_data("IdSupplier as IdSupplier,Nama as Nama,Alamat as Alamat,Telepon as Telepon,Handphone as Handphone", "mssupplier a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
			
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function addPenawaran(){
		
		$data = array(
		'IdCustomer' => $this->input->post('IdCustomer'),		
		'Project' => $this->input->post('Project'),	
		'Status' => 'Pending',
		'PIC' => $this->input->post('PIC'),
		'Keterangan' => $this->input->post('Keterangan'),
		'Nominal' => $this->input->post('Nominal'),
		'Tanggal' => $this->input->post('Tanggal'),
		'TanggalSelesai' => $this->input->post('TanggalSelesai'),
		'UpdateId' => $this->session->userdata("UserId"),	
			);
		$query = $this->all_model->insert_data("penawaran", $data );
		
		echo json_encode($query);
		exit();
	}	
	

	public function editPenawaran(){
		$data = array(
		'IdCustomer' => $this->input->post('IdCustomer'),		
		'Project' => $this->input->post('Project'),	
		'PIC' => $this->input->post('PIC'),
		'Keterangan' => $this->input->post('Keterangan'),
		'Nominal' => $this->input->post('Nominal'),
		'UpdateId' => $this->session->userdata("UserId"),	
			);
		$where = array(	
					'NoPenawaran'			=> $this->input->post('NoPenawaran'),
				
		);	
		$query = $this->all_model->update_data("penawaran", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function updateDataSupplier(){
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'Alamat' => $this->input->post('Alamat'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'Handphone' => $this->input->post('Handphone'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdSupplier'			=> $this->input->post('IdSupplier'),
				
		);	
		$query = $this->all_model->update_data("mssupplier", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	

	
	
	public function deletePenawaran()
	{
		$IdUser = $this->input->post('NoPenawaran');
		$where = array('NoPenawaran'=>$NoPenawaran);
		$query = $this->all_model->delete_data("penawaran", $where);
		echo json_encode($query);
		exit();
	}
	
	
	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Penawaran";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('penawaran_view',$data2);
		$this->load->view('home_footer');
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


