<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($request = true){
		$search = array();
		$where = array("Active"=>'Y');
		$join = array(
			);
		$result2 = $this->all_model->get_data("MenuId, MenuName", "msmenu a",$join, $where, $search, false,true);
		return $result2;	
		exit();
	}
	public function getNewId($id){
		$result = $this->all_model->query_data("SELECT RIGHT(SalesId,3) as IdSales FROM trsales WHERE SalesId like '".$id."%' ORDER BY SalesId DESC", true);
		$result["IdSales"] = $result["IdSales"]+1;
		if($result["IdSales"] < 10)
			$result["IdSales"] = $id."00".$result["IdSales"];
		else if($result["IdSales"]< 100)
			$result["IdSales"] = $id."0".$result["IdSales"];
		else
			$result["IdSales"] = $id.$result["IdSales"];
		return $result["IdSales"];
	}
	public function ajax_finder(){
		$msg = array();
		if ( $_POST ){
			$key = $this->input->post("key");
			if ( empty($key) ){
				$msg['type'] = 'failed';
				$msg['msg'] = "Key Empty.";
			}
			else{
				$join = array(
					array('table'=>'mscategory b','field' => 'a.Category = b.CategoryId','method'=>'Left')
					);
				$data = $this->all_model->view("msmenu a", "a.MenuId as MenuId, MenuName, Price, Category, CategoryName", array("MenuId"=>$key,'Active'=>'Y'), $join);
				if ( $data == '0' ){
					$msg['type'] = "failed";
					$msg['msg'] = "Data tidak ditemukan.";
				}
				else{
					//foreach($data as $a) xss_filter($a);
					$msg['type'] = "done";
					$msg['msg'] = $data;
				}
			}
		}
		else{
			$msg['type'] = 'failed';
			$msg['msg'] = "Parameter tidak ditemukan.";
		}
		//die (var_dump($msg));
		echo json_encode($msg);
	}
	public function cetak()
	{

		if ($_POST['action'] == 'print')
		{
		$printer = "\\\\localhost\\zebra"; 
		if($ph = printer_open($printer)) 
		{ 
		   $data = " PRINT THIS ";  
		   // Cut Paper
		   $data .= "\x00\x1Bi\x00";
		   printer_write($ph, $data); 
		   printer_close($ph); 
		} 
		else die('writing failed');
		}

	}
	public function insert(){
		$detail = json_decode($_POST["detail"]);
		$myarray = $detail->arry;
		$itemneeded =  array();
		$flagstock = 0;
		for($i=0;$i<count($myarray);$i++)
		{
			$search = array();
			$where = array("MenuId"=>$myarray[$i]->id);
			$join = array(	);
			$result2 = $this->all_model->get_data("ItemId, Amount", "msrecipe a",$join, $where, $search, false,false);
			for($j=0;$j<count($result2);$j++)
			{
				if(count($itemneeded)==0)
				{
					array_push($itemneeded,[$result2[$j]['ItemId'],$result2[$j]['Amount']*$myarray[$i]->qty]);
				}
				else
				{
					$flag=0;
					for($k=0;$k<count($itemneeded);$k++)
					{
						if($itemneeded[$k][0]==$result2[$j]['ItemId'])
						{
							$itemneeded[$k][1]+=($result2[$j]['Amount']*$myarray[$i]->qty);
							$flag=1;
							break;
						}
					}
					if($flag==0)
					{
						array_push($itemneeded,[$result2[$j]['ItemId'],$result2[$j]['Amount']*$myarray[$i]->qty]);
					}
					
				}
			}
		}
		//var_dump($itemneeded);
		$textin='';
		for($i=0;$i<count($itemneeded);$i++)
		{
			if($textin=='')
			{
				$textin=$itemneeded[$i][0];
			}
			else
			{
				$textin.=','.$itemneeded[$i][0];
			}
		}
		
		$stock = $this->all_model->query_data("SELECT a.itemid, b.itemname, sum(qty) as qty FROM msitemdetail a join msitem b on a.itemid=b.itemid WHERE qty > 0 and a.itemid in (".$textin.") GROUP BY a.ItemId, itemname ORDER BY a.ItemId ASC");
		//var_dump($stock);
		for($i=0;$i<count($itemneeded);$i++)
		{
			for($j=0;$j<count($stock);$j++)
			{
				if($itemneeded[$i][0]==$stock[$j]["itemid"])
				{
					if($itemneeded[$i][1]>=$stock[$j]["qty"])
					{
						$flagstock=1;
						$message = array ($stock[$j]["itemname"]." is not enough");
					}
					break;
				}
			}
			if($flagstock==1)
			{
				break;
			}
		}
		//die("asd".$flagstock);
		if($flagstock==0)
		{
			$id=$this->getNewId($this->input->post('SalesId'));
			$data = array(
				'SalesId' => $id,
				'EmployeeId' => $this->session->userdata("Username"),
				'CustomerId' => $this->input->post('CustomerId'),
				'SalesDate' => date("Y/m/d")
			);
			$query = $this->db->insert("trsales", $data );

			
			//var_dump($myarray);
			for ($i=0;$i<count($myarray);$i++)
			{
				$batch[] = array(
					'SalesId' => $id,
					'MenuId' => $myarray[$i]->id,
					'Qty' => $myarray[$i]->qty,
					'Price' => $myarray[$i]->price
					);
				
			}
			$this->db->insert_batch('trsalesdetail', $batch);

			for($i=0;$i<count($itemneeded);$i++)
			{
				
				while($itemneeded[$i][1]>0)
				{
					$temp1=$this->all_model->query_data("SELECT itemid, purchaseid, qty, price FROM msitemdetail  WHERE qty > 0 and itemid=".$itemneeded[$i][0]." ORDER BY purchaseid ASC LIMIT 1",true);

					if($itemneeded[$i][1]>$temp1["qty"])
					{
						$itemneeded[$i][1]-=$temp1["qty"];
						$this->all_model->query_exec("UPDATE msitemdetail SET qty=0 where itemid=".$itemneeded[$i][0]." and purchaseid like '".$temp1["purchaseid"]."'");

						$datacogs = array(
							'salesid' => $id,
							'purchaseid' => $temp1["purchaseid"],
							'itemid' => $temp1["itemid"],
							'price' => $temp1["price"],
							'qty' => $temp1["qty"]
						);
						$querycogs = $this->db->insert("trsalescogs", $datacogs );

					}
					else
					{
						
						$this->all_model->query_exec("UPDATE msitemdetail SET qty=qty-".$itemneeded[$i][1]." where itemid=".$itemneeded[$i][0]." and purchaseid like '".$temp1["purchaseid"]."'");
						
						$datacogs = array(
							'salesid' => $id,
							'purchaseid' => $temp1["purchaseid"],
							'itemid' => $temp1["itemid"],
							'price' => $temp1["price"],
							'qty' => $itemneeded[$i][1]
						);
						$querycogs = $this->db->insert("trsalescogs", $datacogs );
						$itemneeded[$i][1]=0;
					}
					
				}
			}
			//die (var_dump($data));
			//var_dump($query);
			
			echo json_encode($query);
		}
		else
		{
			echo json_encode($message);
		}
		exit();
		
		
	}	
	public function getMenu()
	{	
		
		$search = array();
		$join = array(
			array('table'=>'mscategory b','field' => 'a.Category = b.CategoryId','method'=>'Left')

			);
		$where = array("a.Active"=>"Y");		
		$result = $this->all_model->get_data("MenuId, MenuName, CategoryName, Price", "msmenu a", $join, $where, $search, false, false, 'CategoryName', 'ASC');
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function getRecipe()
	{	
		$key = $this->input->post("key");

		$result = $this->all_model->query_data("select ItemId as ItemId, Amount as Amount from msrecipe where menuid=".$key, false);
		if(!$result){
			$result= "0";
		}
		echo json_encode($result);
		exit();
	}
	public function getItem()
	{	
		
		$search = array();
		$join = array();
		$where = array();		
		$result = $this->all_model->get_data("ItemId, ItemName", "msitem a", $join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function edit(){
		$data = array(
			'MenuName' => $this->input->post('Name'), //$this->input->post('Id')
			'Category' => $this->input->post('Category'),
			'Price' => $this->input->post('Price')
			
			);
		$where = array(	
			'MenuId' => $this->input->post('Id'),
		);
		$query = $this->all_model->update_data("msmenu", $data ,$where);

		$where = array('MenuId'=>$this->input->post('Id'));
		$query = $this->all_model->delete_data("msrecipe", $where);

		$recipe = json_decode($_POST["recipe"]);
		$myarray = $recipe->arry;
		for ($i=0;$i<count($myarray);$i++)
		{
			$batch[] = array(
				'MenuId' => $this->input->post('Id'),
				'ItemId' => $myarray[$i][0],
				'Amount' => $myarray[$i][1]
				);
			
		}
		$this->db->insert_batch('msrecipe', $batch);
		
		echo json_encode($query);
		exit();
	}
	
	public function delete(){
		$Id = $this->input->post('Id');
		$data = array(
			'Active' => 'N', //$this->input->post('Id')
			);
		$where = array('MenuId'=>$Id);
		$query = $this->all_model->update_data("msmenu", $data ,$where);
		echo json_encode($query);
		exit();
	}
	
	
	public function getAccess()
	{	
		$page=$this->all_model->query_data("select pageid from mspage where pagename = '".$this->router->fetch_class()."'",true);
		//die($page["pageid"]);
		$result = $this->all_model->query_data("select flag from trprivillege where roleid=".$this->session->userdata('Role')." and PageName=".$page["pageid"]." and flag!=0",true);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function index(){
		if($this->session->userdata("Username")=="")
		redirect(base_url(). "login","refresh");
		/*if($this->session->userdata('Role')!="1")
		{
			$access=$this->getAccess();
			if(!is_array($access))
			{
				redirect(base_url(). "home","refresh");
			}
		}*/
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Sales";
		$data['page']="sales_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['menu'] = json_encode($this->getMenu());
		$data['data'] = $this->getData(false);
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}