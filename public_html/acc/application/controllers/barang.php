<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class barang extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	
	public function getNewIdBarang(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdBarang,3) as IdBarang FROM Barang ORDER BY IdBarang DESC", true);
		$result["IdBarang"] = $result["IdBarang"]+1;
		if($result["IdBarang"] < 10)
			$result["IdBarang"] = "B00".$result["IdBarang"];
		else if($result["IdBarang"]< 100)
			$result["IdBarang"] = "B0".$result["IdBarang"];
		else
			$result["IdBarang"] = "C".$result["IdBarang"];
		return $result["IdBarang"];
	}

	public function getDataNew($request = true){
		$search = array(
				
			);
		$where = array(
			
		);
		$join = array(
			array('table'=>'kategori b','field' => 'a.idkategori = b.idkategori','method'=>'Left')
		);
		$result2 = $this->all_model->get_data("IdBarang as IdBarang, Jenis as Jenis,Nama as Nama,Quantity as Quantity, Harga as Harga", "barang a",$join, $where, $search, false);

		return $result2;
		echo json_encode($result2);		
		exit();
	}
	public function getKategoriNew($request = true){
		$search = array(
				
			);
		$where = array(
			
		);
		$join = array(
			
		);
		$result2 = $this->all_model->get_data("IdKategori as IdKategori, Jenis as Jenis", "kategori a",$join, $where, $search, false);

		return $result2;
		echo json_encode($result2);		
		exit();
	}
	
	public function getDataKategori(){
		$join = array();
		$where = array(
			'a.ActiveYN'	=>"Y"
		);	
		$result = $this->all_model->get_data("IdKategori as IdKategori,Jenis as Jenis", "kategori a",$join, $where, array(),false);
		if(!$result){
			$result= "No Data";
		}
		echo json_encode($result);
		exit();
	}
	
	public function getDataKategoriBesar(){
		$join = array();
		$where = array(
			'a.ActiveYN'	=>"Y"
		);	
		$result = $this->all_model->get_data("IdKategoriBesar as IdKategoriBesar,Keterangan as Keterangan", "kategoriBesar a",$join, $where, array(),false);
		if(!$result){
			$result= "No Data";
		}
		echo json_encode($result);
		exit();
	}
	
	public function generateBarang(){ 
		$data["status"] = "sukses";
		$data["msg"] = $this->getData(15,0,false);
		echo json_encode($data);
		exit();
	}
	
	public function getData($perPage=15, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdKategoriBesar' => $this->input->post('menu'),
			);
		}else{
			$search = array(
					'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
				array('table'=>'kategori b','field' => 'a.IdKategori = b.IdKategori','method'=>'Left'),
				array('table'=>'satuan c','field' => 'a.IdSatuan = c.IdSatuan','method'=>'Left'),		
				array('table'=>'kategoriBesar	d','field' => 'a.IdKategoriBesar = d.IdKategoriBesar','method'=>'Left'),			
			);			
						
		$result = $this->all_model->get_data("IdBarang as IdBarang,d.Keterangan as Keterangan,b.Jenis as Jenis, d.IdKategoriBesar as IdKategoriBesar, b.IdKategori as IdKategori, c.Satuan as Satuan,Nama as Nama,Quantity as Quantity, Jumlah as Jumlah,(Jumlah/Quantity) as Rata, a.ActiveYN as ActiveYN", "Barang a",$join, $where, $search, false, $perPage, $segmen, false,"IdBarang","ASC");
		
		$result2 = $this->all_model->get_data("IdBarang as IdBarang,d.Keterangan as Keterangan,b.Jenis as Jenis, d.IdKategoriBesar as IdKategoriBesar, b.IdKategori as IdKategori, c.Satuan as Satuan,Nama as Nama,Quantity as Quantity, Jumlah as Jumlah,(Jumlah/Quantity) as Rata, a.ActiveYN as ActiveYN", "Barang a",$join, $where, $search, false );
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					//$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					//$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function insertBarang(){
		
		$data = array(
		'IdBarang'  => $this->getNewIdBarang(),
		'IdKategori' => $this->input->post('Kategori'),	
		'Nama' => $this->input->post('Nama'),
		'Quantity' => $this->input->post('Qty'),	
		'Harga' => $this->input->post('Harga'),			
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("barang", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function editBarang(){
		$data = array(
			'IdKategori' => $this->input->post('Kategori'),	
			'IdSatuan' 	=> $this->input->post('IdSatuan'),
			'Nama' => $this->input->post('Nama'),	
			'Quantity' => $this->input->post('Qty'),	
			'Harga' => $this->input->post('Harga'),			
			'UpdateId' => $this->session->userdata("UserId"),
			'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdBarang'			=> $this->input->post('IdBarang'),
				
		);	
		$query = $this->all_model->update_data("Barang", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function deleteBarang(){
		$IdBarang = $this->input->post('IdBarang');
		$where = array('IdBarang'=>$IdBarang);
		$query = $this->all_model->delete_data("barang", $where);
		echo json_encode($query);
		exit();
	}
	
	public function insertKategori(){
		
		$data = array(
		'Jenis' => $this->input->post('Nama'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
		);
		$query = $this->all_model->insert_data("kategori", $data );
		
		echo json_encode($query);
		exit();
	}
	
	public function editKategori(){
		$data = array(
			'Jenis' => $this->input->post('Nama'),		
			'UpdateId' => $this->session->userdata("UserId"),
			'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdKategori'=> $this->input->post('IdKategori'),
				
		);	
		$query = $this->all_model->update_data("kategori", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function deleteKategori(){
		$IdKategori = $this->input->post('IdKategori');
		$where = array('IdKategori'=>$IdKategori);
		$query = $this->all_model->delete_data("kategori", $where);
		echo json_encode($query);
		exit();
	}
	
	public function editStat(){
		$data = array(
			'ActiveYN'  => 'Y',
			);
		$where = array(	
					'IdBarang'		=> $this->input->post('IdBarang'),
				
		);	
		$query = $this->all_model->update_data("Barang", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function getKategori()
	{	
		
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y', );		
		$result = $this->all_model->get_data("IdKategori as IdKategori,Jenis as Jenis", "kategori a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getKategoriRet()
	{	
		$IdKategoriBesar = $this->input->post('IdKategoriBesar');
		$result = $this->all_model->query_data("Select * FROM kategori WHERE IdKategoriBesar like '".$IdKategoriBesar."'",false);
		if(!$result){
			$result= "0";
		}
		echo json_encode($result);
		exit();
	}
	
	public function getKategoriBesar()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdKategoriBesar as IdKategoriBesar,Keterangan as Keterangan", "kategoribesar a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getSatuan()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdSatuan as IdSatuan,Satuan as Satuan, Keterangan as Keterangan", "satuan a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getDataBarang($IdBarang)
	{
		$result = $this->all_model->query_data("SELECT * FROM Barang where IdBarang='".$IdBarang."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "login","refresh");
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Master Barang";
		$data['page']="barang_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['data'] = json_encode($this->getDataNew(false));
		$data['kategori'] = json_encode($this->getKategori());
		//$data2['data4'] = json_encode($this->getSatuan());
		//$data2['data3'] = json_encode($this->getKategoriBesar());
		//$data['include']=$this->load->view('script','',true);
		$this->load->view('main',$data);
	}
	
	public function addBarang(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Tambah Barang";
		$data2['data6'] = json_encode($this->getKategori());
		$data2['data4'] = json_encode($this->getSatuan());
		$data2['data5'] = json_encode($this->getNewIdBarang());
		$data2['data3'] = json_encode($this->getKategoriBesar());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('addbarang_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


