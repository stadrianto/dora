<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pitching extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		$this->load->model('all_model2');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	
	public function antiinjection($data)
	{
		$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter_sql;
	}
	
	public function getBenefit($notransaksi)
	{
		$search = array(
		
			);
		$join = array(
			array('table'=>'m_creativeitem b','field' => 'a.TIBD_CreativeItem = b.MCI_KodeItem','method'=>'Left'),
		);
		$where = array(
				'a.TIBD_NoTransaksi' => $notransaksi,
		);
			
		$result = $this->all_model->get_data("TIBD_NoTransaksi as NoTransaksi,TIBD_CreativeItem as KodeItem,TIBD_Qty as Qty,MCI_NamaItem as NamaItem,MRC_Harga as Harga","t_iklanmkt_benefit_detail a",$join, $where, $search);
		foreach($result as $key => $value){
			$result[$key]["NamaItem"] = str_replace("\"", "@@@", $result[$key]["NamaItem"]);
			$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
		}
		if($result)
		return $result;
	}
	
	public function getProduk($notransaksi)
	{
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.TIPD_NoTransaksi' => $notransaksi,
		);
			
		$result = $this->all_model->get_data("TIPD_KodeProduk as KodeProduk,Itm_NamaProduk as NamaProduk,Itm_DivNama as NamaDivisi,TIPD_Budget as Budget","t_iklanmkt_produk_detail a",$join, $where, $search);
	
		foreach($result as $key => $value){
			$result[$key]['Budget'] = $this->all_model->rp($result[$key]['Budget']);
		}
		if($result)
		return $result;
	}
		
	public function getDetailIklan($notransaksi)
	{
			 
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.TIH_AktifYN'	=>"Y",
				'a.TIH_NoTransaksi'	=> $notransaksi,
		);
			
			
		$result = $this->all_model->get_data("TIH_NoTransaksi as NoTransaksi,MHA_NamaAcara as ProgramAcara,MJP_NamaPlacement as Placement,TIH_BudgetTotal as BudgetTotal,TIH_StartTayang as StartTayang,TIH_EndTayang as EndTayang,TIH_CloseTime as CloseTime,TIH_Episode as Episode", "t_iklanmkt_header a",$join, $where, $search, false);
		
		if($result)
		{
			$result2 = $this->getProduk($result[0]["NoTransaksi"]);
			$result3 = $this->getBenefit($result[0]["NoTransaksi"]);
			$result[0]['BudgetTotal'] = $this->all_model->rp($result[0]['BudgetTotal']);
			$dt = strtotime($result[0]['StartTayang']);
			$result[0]['StartTayang'] = date("d-M-Y",$dt);
			$dt2 = strtotime($result[0]['EndTayang']);
			$result[0]['EndTayang'] = date("d-M-Y",$dt2);
			
			$dt3 = strtotime($result[0]['CloseTime']);
			$result[0]['CloseTime'] = date("d-M-Y",$dt3);
			if(date("Y-m-d H:i:s",time()) > date("Y-m-d H:i:s",$dt3) ) $result[0]['Status'] = "Close Pitching";
			else $result[0]['Status'] = "Open Pitching";
			
			$result[0]['Produk'] = $result2;
			$result[0]['Benefit'] = $result3;
		}
		return $result;
	
	}
	
	public function checkTransaksi($notransaksi)
	{
		$result = $this->all_model->query_data("SELECT * FROM  t_iklanmkt_header WHERE TIH_NoTransaksi = '$notransaksi'", true);	
		if($result) return $result["TIH_AcaraID"];
		else return 0;
	}
	
	public function checkAgenPitching($notransaksi)
	{
		$userid = $this->session->userdata("UserId");
		$idacara = $this->checkTransaksi($notransaksi);
		if($idacara != 0)
		{
			$result = $this->all_model->query_data("SELECT * FROM  t_pitching_header WHERE TPH_KodeAgency = '$userid' and TPH_NoTransaksi='".$notransaksi."'", true);	
			if($result){
								$newdata = array(
									'NoPitching'		=> $result["TPH_NoPitching"],
									'KodeAcara' => $idacara,
							   );
							 
						$this->session->set_userdata($newdata);
				return true;
			}
			else
			{
				$data = array(
					'TPH_NoTransaksi' => $notransaksi,
					'TPH_KodeAgency'	=> $userid,
					'TPH_AktifYN' 	=> 'Y',
					'TPH_UpdateID' 	=> $userid,
					'TPH_UpdateTime' => date("Y-m-d H:i:s"),	
					'TPH_Flag' => "1",
				);
				if($notransaksi != 0)
				$query = $this->all_model->insert_data("t_pitching_header", $data );
				
				if($query){
				
					$result = $this->all_model->query_data("SELECT TPH_NoPitching FROM  t_pitching_header WHERE TPH_KodeAgency = '$userid' and TPH_NoTransaksi='".$notransaksi."'", true);	
					
								$newdata = array(
									'NoPitching'		=> $result["TPH_NoPitching"],
									'KodeAcara' => $idacara,
							   );
							 
						$this->session->set_userdata($newdata);

					return true;
				}
			}
		}
		else
		{
			redirect(base_url(). "home","refresh");
		}
	}
	
	
	public function checkItem(){
		$iduser = $this->session->userdata("UserId");
		$nopitching = $this->session->userdata("NoPitching");
		$noitem = $this->input->post('noitem');
		$result = $this->all_model->query_data("SELECT TPCD_CreativeItem FROM t_pitching_creative_detail WHERE TPCD_KodeAgency = '".$iduser."' and TPCD_CreativeItem='".$noitem."' and TPCD_NoPitching='".$nopitching."'", true);
		
		if($result)
		{
			echo "true";
		}
		else echo "false";

		exit();
	}
	
	
	public function deleteCreative()
	{
		$kodeitem = $this->input->post('noitem');
		$where = array('TPCD_CreativeItem'=>$kodeitem,'TPCD_KodeAgency'=>$this->session->userdata("UserId"));
		$query = $this->all_model->delete_data("t_pitching_creative_detail", $where);
		if($query){
			$data2["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data2["msg"] = $calculate;	
		}
		else 
		{
			$data2["status"] = "gagal";
		}
			
		echo json_encode($data2);
		exit();
	
	}
	
	public function insertCreative()
	{	
		
		$bonus = $this->input->post('bonus');
		$data = array();
		if($bonus == "no")
		{		
			$data = array(
				'TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'TPCD_CreativeItem'	=> $this->input->post('noitem'),
				'TPCD_KodeAgency' 	=> $this->session->userdata("UserId"),
				'TPCD_Qty' 	=> $this->input->post('spot'),
				'TPCD_Harga' => $this->input->post('harga'),	
				'TPCD_BonusYN' => "N",
				'TPCD_JmlEpisode' => $this->input->post('episode'),
				'TPCD_UpdateID' => $this->session->userdata("UserId"),
				'TPCD_UpdateTime' => date("Y-m-d H:i:s"),
			);
		}
		else if($bonus == "bonus")
		{
			$data = array(
				'TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'TPCD_CreativeItem'	=> $this->input->post('noitem'),
				'TPCD_KodeAgency' 	=> $this->session->userdata("UserId"),
				'TPCD_Qty' 	=> $this->input->post('spot'),
				'TPCD_Harga' => $this->input->post('harga'),	
				'TPCD_BonusYN' => "Y",
				'TPCD_JmlEpisode' => $this->input->post('jmlepisode'),
				'TPCD_KodeAcara' => $this->input->post('noacara'),
				'TPCD_UpdateID' => $this->session->userdata("UserId"),
				'TPCD_UpdateTime' => date("Y-m-d H:i:s"),
			);		
		}
		else
		{
			$data = array(
				'TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'TPCD_CreativeItem'	=> $this->input->post('noitem'),
				'TPCD_KodeAgency' 	=> $this->session->userdata("UserId"),
				'TPCD_Qty' 	=> $this->input->post('spot'),
				'TPCD_Harga' => $this->input->post('harga'),	
				'TPCD_BonusYN' => "N",
				'TPCD_JmlEpisode' => $this->input->post('jmlepisode'),
				'TPCD_KodeAcara' => $this->input->post('noacara'),
				'TPCD_UpdateID' => $this->session->userdata("UserId"),
				'TPCD_UpdateTime' => date("Y-m-d H:i:s"),
			);	
		}
		$query = $this->all_model->insert_data("t_pitching_creative_detail", $data );
		if($query){
			$data2["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data2["msg"] = $calculate;	
		}
		else 
		{
			$data2["status"] = "gagal";
		}
			
		echo json_encode($data2);
		exit();
	
	}
	
	public function getCalculate()
	{
		$total = 0;
		$floater = 0;
		$package = 0;
		$join = array(
			
		);
		$where = array(
				'a.TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'a.TPCD_KodeAgency' => $this->session->userdata("UserId"),
		);	
		$search = array(
	
		);
		$result = $this->all_model->get_data("TPCD_NoPitching as NoPitching,TPCD_CreativeItem as KodeItem,TPCD_Qty as Qty,TPCD_Harga as Harga,TPCD_BonusYN as Bonus,TPCD_JmlEpisode as JmlEpisode","t_pitching_creative_detail a",$join, $where, $search);
		if($result)
		{
			foreach($result as $key => $value){
				$result[$key]['TotalSpot'] = $result[$key]['Qty'];
				if($result[$key]['Bonus'] == "N")
				$total += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				else
				$floater += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
		
			}
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
			);
			return $newdata;
		}
		else
		{
			$newdata = array(
				'TotalBenefit'		=> 0,
				'TotalFloater'		=> 0,
				'TotalPackage'		=> 0,
			);
			return $newdata;
		}
	}
	
	public function getPitching($notransaksi)
	{
		$total = 0;
		$floater = 0;
		$package = 0;
		$search = array(
		
			);
		$join = array(
			array('table'=>'m_creativeitem b','field' => 'a.TPCD_CreativeItem = b.MCI_KodeItem','method'=>'Left'),array('table'=>'mh_acara c','field' => 'a.TPCD_KodeAcara=c.MHA_KodeAcara','method'=>'Left')
		);
		$where = array(
				'a.TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'a.TPCD_KodeAgency' => $this->session->userdata("UserId"),
		);
			
		$result = $this->all_model->get_data("TPCD_NoPitching as NoPitching,TPCD_CreativeItem as KodeItem,TPCD_Qty as Qty,MCI_NamaItem as NamaItem,TPCD_Harga as Harga,TPCD_BonusYN as Bonus,TPCD_KodeAcara as KodeAcara,TPCD_JmlEpisode as JmlEpisode,MHA_NamaAcara as NamaAcara","t_pitching_creative_detail a",$join, $where, $search);

		$array_items = array('TotalFloater'=>'', 'TotalBenefit' => '','TotalPackage' => '');
		$this->session->unset_userdata($array_items);
		
		
		if($result)
		{
			foreach($result as $key => $value){
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				$result[$key]['TotalSpot'] = $result[$key]['Qty'];
				if($result[$key]['Bonus'] == "N")
				$total += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				else
				$floater += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				
				$result[$key]['Value'] = $this->all_model->rp(($result[$key]['Harga']*$result[$key]['TotalSpot']));
				$result[$key]['Harga'] = $this->all_model->rp($result[$key]['Harga']);
				if($result[$key]["KodeAcara"] == 0) $result[$key]["KodeAcara"] = "";
				
			}
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
			);
	 
			$this->session->set_userdata($newdata);
			
			return $result;
		}
		else
		{
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
			);
	 
			$this->session->set_userdata($newdata);
			
			return "No Data";
		}
	
	}
	
	public function getItemPitching()
	{
		$notransaksi = $this->session->userdata("NoTransaksi");
		if($this->checkAgenPitching($notransaksi))
		{
			$result = $this->getPitching($notransaksi);
			if($result != "No Data")
			echo json_encode($result);
			else echo json_encode(array());
		}
	}
	
	public function checkUserPitching($nopitching)
	{
		$userid = $this->session->userdata("UserId");
		$result = $this->all_model->query_data("SELECT * FROM  t_pitching_header WHERE TPH_KodeAgency = '$userid' and TPH_NoPitching='".$nopitching."'", true);	
		if($result){
			return false;
		}
		else return true;
	}
	
	public function batalpitching($nopitching)
	{
		if($this->session->userdata("UserId")=="" || $this->checkUserPitching($nopitching))
		redirect(base_url(). "home","refresh");
		$where = array('TPH_NoPitching'=>$nopitching);
		$where2 = array('TPCD_NoPitching'=>$nopitching);
		
		$query = $this->all_model->delete_data("t_pitching_header", $where);
		if($query){
			$query2 = $this->all_model->delete_data("t_pitching_creative_detail", $where2);
			if($query2)
			redirect(base_url(). "viewpitching","refresh");
			else
			redirect(base_url(). "home","refresh");
		}
		else 
		{
			redirect(base_url(). "home","refresh");
		}
		
	}
	
	public function pitchingiklan($notransaksi)
	{
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="Pitching";
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['page_title']="Media Planning Pharos";
		$data2['data'] = json_encode($this->getDetailIklan($notransaksi));
		$data2['data3'] = json_encode($this->getStation());
		$data2['data4'] = json_encode($this->getGenre());
		$newdata = array(
			'NoTransaksi'		=> $notransaksi,
	   );
	 
		$this->session->set_userdata($newdata);
		
		if($this->checkAgenPitching($notransaksi))
		{
			$data2['data2'] = json_encode($this->getPitching($notransaksi));
		}
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('pitching_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function getStation()
	{
		
		$search = array();
		$join = array();
		$where = array('a.MS_ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("MS_KodeStation as KodeStation,MS_NamaStation as NamaStation", "m_station a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getGenre()
	{	
		$search = array();
		$join = array();
		$where = array('a.Gen_ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("Gen_ID as Gen_ID,Gen_Name as Genre", "m_genre a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function index($notransaksi){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="Pitching";
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['page_title']="Media Planning Pharos";
		$data2['data'] = json_encode($this->getDetailIklan($notransaksi));
		//$data2['data2'] = json_encode($this->getTempatTest());
		$data2['data3'] = json_encode($this->getStation());
		$data2['data4'] = json_encode($this->getGenre());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('pitching_view',$data2);
		$this->load->view('home_footer');
	}
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


