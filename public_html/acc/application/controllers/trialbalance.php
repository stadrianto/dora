<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class trialbalance extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataJurnal($noakun,$tanggal2)
	{
		$result = $this->all_model->query_data("SELECT Debit as Debit,Kredit as Kredit FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where noakun='".$noakun."' and tanggaltransaksi='".$tanggal2."'" , false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataTrial()
	{
		$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NamaAkun as NamaAkun,SaldoAwal as SaldoAwal,IdNeraca as IdNeraca FROM tabelakun a join kelompokakun b on a.idkelompokakun=b.idkelompokakun", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	
	public function printTrialBalance()
	{
		//$noproject = $this->session->userdata('NoProject');
		//$dataproject = $this->getDataTrial();
		$tanggal2=$this->input->post('dob1');
		$qtanggal2=substr($tanggal2,6,4).'-'.substr($tanggal2,0,2).'-'.substr($tanggal2,3,2);
		$result = $this->getDataTrial();
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		//$this->fpdf->Image('images/logo.png',10,0,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Trial Balance',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','BU',12);
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
		$this->fpdf->Ln(15);

		$y_axis_initial = 40;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(25,6,'NoAkun',1,0,'C',1);
		$this->fpdf->Cell(60,6,'NamaAkun',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Debit',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Kredit',1,0,'C',1);
		//$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$totala=0;
		$totalb=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		//$grandtotal = 0;
		if($result != "0")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(25,6,'NoAkun',1,0,'C',1);
		$this->fpdf->Cell(60,6,'NamaAkun',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Debit',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Kredit',1,0,'C',1);

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		//$grandtotal+=$row['Total'];
		$saldo=floatval($result[$key]["SaldoAwal"]);
		$saldo2=floatval($result[$key]["SaldoAwal"]);	
		$result2 = $this->getDataJurnal($result[$key]["NoAkun"]."",$qtanggal2);
		//$result3 = $this->getDataJurnal($result[$key]["NoAkun"]."");
		if($result[$key]["IdNeraca"]=="1")
		{
			//echo "<script type='text/javascript'>alert('jalan');</script>";		
			$saldo2=0;
			$saldo=0;
			if($result2!="0")
			{
				for($z=0;$z<count($result2);$z++){
					$saldo+=$result2[$z]["Debit"];
					$saldo-=$result2[$z]["Kredit"];
					$totala+=$result2[$z]["Debit"];
					$totala-=$result2[$z]["Kredit"];
				}
			}
		}	
		else if($result[$key]["IdNeraca"]=="2")
		{
			$saldo2=0;
			$saldo=0;
			if($result2!="0")
			{
				for($z=0;$z<count($result2);$z++){
					$saldo2-=$result2[$z]["Debit"];
					$saldo2+=$result2[$z]["Kredit"];
					$totalb-=$result2[$z]["Debit"];
					$totalb+=$result2[$z]["Kredit"];					
				}
			}
		}
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		//$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["NoAkun"],1,0,'C',0);
		$this->fpdf->Cell(60,6,$result[$key]["NamaAkun"],1,0,'C',0);
		$this->fpdf->Cell(25,6,$this->all_model->rp($saldo),1,0,'R',0);
		//$this->fpdf->Cell(35,6,$this->all_model->rp($result[$key]["Insentif"]),1,0,'C',0);
		$this->fpdf->Cell(25,6,$this->all_model->rp($saldo2),1,0,'R',0);
		$this->fpdf->Ln();

		}
		}
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(25,6,'',1,0,'C',0);
		$this->fpdf->Cell(60,6,'Total',1,0,'C',0);
		//$this->fpdf->Cell(25,6,$totala,1,0,'R',0);
		$this->fpdf->Cell(25,6,$this->all_model->rp($totala),1,0,'R',0);
		$this->fpdf->Cell(25,6,$this->all_model->rp($totalb),1,0,'R',0);
		//buat footer
		$now = date("d F Y");
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		//$this->fpdf->Cell(250,6,"Total Transaksi : ",0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Output('Laporan Pekerja Proyek'.date("F Y").'.pdf', 'I');
		
	}
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Trial Balance";
		//$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('trialbalance_view');
		$this->load->view('home_footer');
	}	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

