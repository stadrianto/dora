<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Satuan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	

	public function editSatuan(){
		$idSatuan = $this->input->post('idSatuan');
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'Satuan' => $this->input->post('Satuan'),
				'Keterangan'=> $this->input->post('Keterangan'),
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array("IdSatuan" => $idSatuan);
		$query = $this->all_model->update_data("satuan", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	

	public function addSatuan(){
		
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'Satuan' => $this->input->post('Satuan'),
				'Keterangan'=> $this->input->post('Keterangan'),
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("satuan", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function deleteSatuan()
	{
		$idSatuan = $this->input->post('idSatuan');
		$where = array('IdSatuan'=>$idSatuan);
		$query = $this->all_model->delete_data("satuan", $where);
		echo json_encode($query);
		exit();
	}
	
	public function generateSatuan()
	{ 
		$data["status"] = "sukses";
		$data["msg"] = $this->getAllSatuan(100,0,false);
		echo json_encode($data);
		exit();
	}

	public function getAllSatuan($perPage=100, $segmen=0, $request = true){
		$current = $this->input->post('page');
		
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
			);
		$join = array(
		);
		$where = array(
		);

		$result = $this->all_model->get_data("IdSatuan as IdSatuan,Satuan as Satuan, Keterangan as Keterangan, UpdateID as UpdateID,ActiveYN as ActiveYN,UpdateTime as UpdateTime", "satuan a",$join, $where, $search, false, $perPage, $segmen, false,"IdSatuan","ASC");
		$result2 = $this->all_model->get_data("IdSatuan as IdSatuan,Satuan as Satuan, Keterangan as Keterangan, UpdateID as UpdateID,ActiveYN as ActiveYN,UpdateTime as UpdateTime", "satuan a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllSatuan($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				
				foreach($result as $key => $value){	
				}
				
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllSatuan($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				}
			}
			echo json_encode($result);
			exit();
		}
	}

	
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		
		$data['title']="CIPS - Master Satuan";
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['page_title']="CIPS - Master Satuan";
		$data2['data'] = json_encode($this->getAllSatuan($config['per_page'], $config['segmen'],false));
		//$data['data'] = json_encode($this->getNews());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('satuan_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


