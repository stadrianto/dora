<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SalesHistory extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($request = true){
		$search = array();
		$where = array();
		$group = array("SalesId","SalesDate","EmployeeId");
		$join = array(
			array('table'=>'trsalesdetail b','field' => 'a.SalesId = b.SalesId','method'=>'Left')
			);
		$result2 = $this->all_model->get_data("a.SalesId, SalesDate, EmployeeId, Sum(Qty * Price) as Total", "trsales a",$join, $where, $search, false,true,'','DESC',$group);
		return $result2;	
		exit();
	}

	public function getNewId($id){
		$result = $this->all_model->query_data("SELECT RIGHT(PurchaseId,3) as PurchaseId FROM trpurchase WHERE PurchaseId like '".$id."%' ORDER BY PurchaseId DESC", true);
		$result["PurchaseId"] = $result["PurchaseId"]+1;
		if($result["PurchaseId"] < 10)
			$result["PurchaseId"] = $id."00".$result["PurchaseId"];
		else if($result["PurchaseId"]< 100)
			$result["PurchaseId"] = $id."0".$result["PurchaseId"];
		else
			$result["PurchaseId"] = $id.$result["PurchaseId"];
		return $result["PurchaseId"];
	}

	public function ajax_finder(){
		$msg = array();
		if ( $_POST ){
			$key = $this->input->post("key");
			if ( empty($key) ){
				$msg['type'] = 'failed';
				$msg['msg'] = "Key Empty.";
			}
			else{
				$join = array(
			
				);
				$data = $this->all_model->view("trsales a", "a.SalesId, SalesDate, EmployeeId", array("SalesId"=>$key), $join);
				if ( $data == '0' ){
					$msg['type'] = "failed";
					$msg['msg'] = "Data tidak ditemukan.";
				}
				else{
					//foreach($data as $a) xss_filter($a);
					$msg['type'] = "done";
					$msg['msg'] = $data;
				}
			}
		}
		else{
			$msg['type'] = 'failed';
			$msg['msg'] = "Parameter tidak ditemukan.";
		}
		//die (var_dump($msg));
		echo json_encode($msg);
	}

	public function insert(){
		//die($this->input->post('Recipe'));
		//$menu = json_decode($_POST["menu"]);
		
		//die($this->input->post('Name'));
		$id=$this->getNewId($this->input->post('Id'));
		$data = array(
			'PurchaseId' => $id,
			'PurchaseDate' => $this->input->post('PurchaseDate'),
			'EmployeeId' => $this->session->userdata("Username")
		);
		$query = $this->db->insert("trpurchase", $data );

		$item = json_decode($_POST["itemlist"]);
		$myarray = $item->arry;
		for ($i=0;$i<count($myarray);$i++)
		{
			$batch[] = array(
				'PurchaseId' => $id,
				'ItemId' => $myarray[$i][0],
				'Qty' => $myarray[$i][1],
				'Price' => $myarray[$i][2],
				);
			
		}
		$this->db->insert_batch('trpurchasedetail', $batch);
		//die (var_dump($data));
		
		
		echo json_encode($query);
		exit();
		
		
	}	
	
	public function getPurchaseDetail()
	{	
		$key = $this->input->post("key");

		$result = $this->all_model->query_data("select ItemId as ItemId, Qty as Qty, Price from trpurchasedetail where purchaseid = '".$key."'", false);
		if(!$result){
			$result= "0";
		}
		echo json_encode($result);
		exit();
	}
	public function getItem()
	{	
		
		$search = array();
		$join = array();
		$where = array();		
		$result = $this->all_model->get_data("ItemId, ItemName", "msitem a", $join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function edit(){
		$data = array(
			'PurchaseDate' => $this->input->post('PurchaseDate')
			
			);
		$where = array(	
			'PurchaseId' => $this->input->post('Id'),
		);
		$query = $this->all_model->update_data("trpurchase", $data ,$where);

		$where = array('PurchaseId'=>$this->input->post('Id'));
		$query = $this->all_model->delete_data("trpurchasedetail", $where);

		$itemlist = json_decode($_POST["itemlist"]);
		$myarray = $itemlist->arry;
		for ($i=0;$i<count($myarray);$i++)
		{
			$batch[] = array(
				'PurchaseId' => $this->input->post('Id'),
				'ItemId' => $myarray[$i][0],
				'Qty' => $myarray[$i][1],
				'Price' => $myarray[$i][2],
				);
			
		}
		$this->db->insert_batch('trpurchasedetail', $batch);
		
		echo json_encode($query);
		exit();
	}
	
	public function delete(){
		$id = $this->input->post('Id');
		$where = array('SalesId'=>$this->input->post('Id'));
		$query = $this->all_model->delete_data("trsales", $where);
		$query = $this->all_model->delete_data("trsalesdetail", $where);
		echo json_encode($query);
		exit();
	}
	
	
	public function getAccess()
	{	
		$page=$this->all_model->query_data("select pageid from mspage where pagename = '".$this->router->fetch_class()."'",true);
		//die($page["pageid"]);
		$result = $this->all_model->query_data("select flag from trprivillege where roleid=".$this->session->userdata('Role')." and PageName=".$page["pageid"]." and flag!=0",true);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function index(){
		if($this->session->userdata("Username")=="")
		redirect(base_url(). "login","refresh");
		/*if($this->session->userdata('Role')!="1")
		{
			$access=$this->getAccess();
			if(!is_array($access))
			{
				redirect(base_url(). "home","refresh");
			}
		}*/
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Sales History";
		$data['page']="saleshistory_view";
		$data['nama']=$this->session->userdata('Name');
		$data['itemlist'] = json_encode($this->getItem());
		$data['data'] = $this->getData(false);
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}