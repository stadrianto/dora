<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo SITE_TITLE?> | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?php echo base_url()?>media/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<!--link rel="stylesheet" href="<?php echo base_url()?>media/fontawesome4.5.0/css/font-awesome.min.css"-->
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url()?>media/ionicons2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url()?>media/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>media/site/css/site.css">
	
	<noscript>
		<style>
			.wrapper { display:none; }
			.warning { height:100vh; background:black; color:white; margin:0; padding:10px 50px 20px 50px; }
		</style>
		<div class="warning">
			<h1>WARNING, JavaScript is disabled in your browser !</h1>
			<p>We will not be able to serve you properly with Javascript disabled in your browser.</p>
			<p>Please enabled JavaScript support in your browser and refresh this page before you continue.</p>
			<p>Here are the instructions <a href="http://www.enable-javascript.com/" target="_blank">how to enable JavaScript in your web browser</a>.</p>
			<p>If you are not sure how to do this, please contact your system administrator.</p>
			<p>- <?php echo SITE_TITLE?> -</p>
		</div>
	</noscript>
	
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url()?>html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="<?php echo base_url()?>respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">

	<section class="login">
		<div class="login-box">
			<div class="login-logo vertical flip-container" ontouchstart="this.classList.toggle('hover');">
				<div class="logo-flip1 flipper">
					<div class="logo-front front">
						<img src="<?php echo base_url()?>media/site/img/logo2.jpg" style="width:100%;">
					</div>
					<div class="logo-back back">
						<img src="<?php echo base_url()?>media/site/img/logo-black2.jpg" style="width:100%;">
					</div>
				</div>
			</div>
			<!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg" style="margin-bottom:10px;">Sign in to start your session</p>
				<form id="zlog" method="post">
					<div class="form-group has-feedback">
						<input type="text" name="username" class="form-control" placeholder="Username" required>
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" placeholder="Password" required>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-12">
						  <button type="submit" class="btn btn-primary btn-block btn-flat btn-go">Sign In</button>
						</div>
						<!-- /.col -->
					</div>
				</form>
			</div>
			<!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->
	</section>
	<?php
		$from = '2016'; 
		$now = date('Y');
		$year = $from.(($from != $now) ? '-'.$now : '');
	?>
	

<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url()?>media/js/jQuery-2.1.4.min.js"></script>
<script src="<?php echo base_url()?>media/js/jquery.ajax.form.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url()?>media/bootstrap/js/bootstrap.min.js"></script>

<script>
	$(function(){
		
		$("#zlog").ajaxForm({
			url : "<?php echo base_url()?>login/cekLogin",
			type : "post",
			dataType : "json",
			beforeSubmit : function(){
				$(".login-box-msg").html ( "<br />Checkin username and password..." ).addClass('bg-danger').removeClass('bg-red');
				$(".btn-go").prop('disabled', true);
			},
			success : function(data){
				if ( data.type == "failed" ){					
					$(".login-box-msg").html( data.msg ).addClass('bg-red');											
					setTimeout(function(){
						$(".login-box-msg").html ( "<br />Sign in to start your session").removeClass('bg-red bg-danger');
						$(".btn-go").prop('disabled', false);
					}, 2000);
				}
				else if ( data.type == "done" ){
					$(".login-box-msg").html( data.msg ).removeClass('bg-danger bg-red').addClass('bg-success');
					setTimeout(function(){ window.location.replace("<?php echo base_url()?>"); }, 2000);
				}
			}
		});
	});
</script>
</body>
</html>
