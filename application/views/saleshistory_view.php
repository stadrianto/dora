		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Sales History<small> Sales Data</small></h1>                    
                </div>
            </div>	 
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Sales List </h3>
                        </div>
                        <div class="panel-body">
                            <table id="example1" class="table table-bordered table-striped">
								<thead>
								  <tr>
									<th>Sales Id</th>
									<th>Sales Date</th>
									<th>Employee</th>
									<th>Total</th>
									<th>Action</th>
								  </tr>
								</thead>
								<tbody>
									<?php
										//die (var_dump($data));
										if ( isset($data) and $data != '0' ){
											$no = 1;
											foreach($data as $row){
									?>
									<tr>
										<td><?php echo $row->SalesId?></td>
										<td><?php echo $row->SalesDate?></td>
										<td><?php echo $row->EmployeeId?></td>					
										<td><?php echo $row->Total?></td>										
										<td>
											<div class="btn-group" style="width:100px">
											  <button type="button" class="btn btn-primary btn-rem" data-key="<?php echo $row->SalesId?>">Action</button>
											  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											  </button>
											  <ul class="dropdown-menu" role="menu" style="margin-left:-70px;position:relative">
											
												<li><a href="javascript:void(0);" class="btn-rem" data-key="<?php echo $row->SalesId?>"><i class="fa fa-times"></i> Remove</a></li>
											  </ul>
											</div>
										</td>
									</tr>
									<?php $no++;}}?>
								</tbody>
							</table>		
                        </div>
                    </div>
                </div>
            </div>         		 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> New Purchase </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div class="form-group">
								<label>Purchase Id</label>
								<input readonly id="txtid" name="txtid" class="form-control">
							</div>
							<div class="form-group">
								<label>Purchase Date</label>
								<div class="input-group"  data-provide="datepicker">
								    <input id="txttgl" name="txttgl" type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>				
							</div>	
							<div>
								<table id="itemArea">
																	
								</table>
								<button id="btnRow" type="button" class="btn btn-default">Add Item</button>
							</div>
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>	
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes03" id="tes03" class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Sales </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-12" style="float:none;">
							<h4>Are you sure want to delete this data?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Sales Id</th>
										<th>Sales Date</th>
										<th>Employee</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div id="deleteid"></div></td>
										<td><div id="deletedate"></div></td>
										<td><div id="deleteemployee"></div></td>
									</tr>							
								</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="button" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	var flag=0;
	var iCnt = 0;
	$(document).ready(function () {
		$("#example1").DataTable({
			scrollX : true,
			scrollCollapse : true
		});
		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd', autoclose: true });
		var itemlist =  JSON.parse('<?php echo $itemlist; ?>');
		iCnt = 0;
		flag = 0;
		console.log(itemlist);
		$('#btnRow').click(function() {
            if (iCnt <= 10) {
                // ADD TEXTBOX.
				var test='<tr><td><select class="form-control" name="ddlItem'+iCnt+'" id="ddlItem'+iCnt+'">';
				for(i=0;i<itemlist.length;i++){
					test+='<option value="'+itemlist[i].ItemId+'">'+itemlist[i].ItemName+'</option>'}				
				test+='</select></td><td><input class="form-control" onkeyup="this.value=numberWithCommas(this.value)" type=text name="txtqty" id=txtqty' + iCnt + ' ' +'/></td><td><input class="form-control" onkeyup="this.value=numberWithCommas(this.value)" type=text name="txtprice" id="txtprice' + iCnt + '"/></td></tr>';
				
				$('#itemArea').append(test);
				
				iCnt = iCnt + 1;
				flag = flag + 1;
            }
            else {  
                $(container).append('<label>Reached the limit</label>'); 
                $('#btnRow').attr('class', 'bt-disable'); 
                $('#btnRow').attr('disabled', 'disabled');
            }
        });

		

		$("#example1").on("click", ".btn-edit", function(){
			var data = {"key" : $(this).attr("data-key")};
			$.ajax({
				url : "<?php echo base_url()?>/SalesHistory/ajax_finder",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){					
					if ( data.type === "done" ){
						setUpdate(data.msg[0].PurchaseId, data.msg[0].PurchaseDate);
						getDetailRecipe(data.msg[0].PurchaseId);						
					}
					else{
						alert(data.msg);
					}
				},
			   	error: function(ts) { alert(ts.responseText) }
			});
		});
		$("#example1").on("click", ".btn-rem", function(){
			var data = {"key" : $(this).attr("data-key")};
			$.ajax({
				url : "<?php echo base_url()?>/SalesHistory/ajax_finder",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){					
					if ( data.type === "done" ){
						setDelete(data.msg[0].SalesId, data.msg[0].SalesDate, data.msg[0].EmployeeId);													
					}
					else{
						alert(data.msg);
					}
				},
			   	error: function(ts) { alert(ts.responseText) }
			});
		});

		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
		});
		
		$('#tes03').click(function(e) {
			$('#dialogDelete').modal('hide');
		});

		
	});
	function getDetailRecipe(key)
	{
		$('#itemArea').html("");
		var data2 = {"key" : key};
			$.ajax({
				url : "<?php echo base_url()?>/SalesHistory/getPurchaseDetail",
				type : "post",
				dataType : "json",
				data : data2,
				success : function(data){			
					//var hasil = JSON.parse(msg);
					console.log(data[0].ItemId);
					for(z=0;z<data.length;z++)
					{
						var itemlist =  JSON.parse('<?php echo $itemlist; ?>');
						var test='<tr><td><select class="form-control" name="ddlItem'+z+'" id="ddlItem'+z+'">';
						for(i=0;i<itemlist.length;i++){
							test+='<option value="'+itemlist[i].ItemId+'">'+itemlist[i].ItemName+'</option>'}				
						test+='</select></td><td><input class="form-control" onkeyup="this.value=numberWithCommas(this.value)" type=text name="txtqty" id=txtqty' + z + ' ' +'/></td><td><input class="form-control" onkeyup="this.value=numberWithCommas(this.value)" type=text name="txtprice" id=txtprice' + z + ' ' +'/></td></tr>';
						
						$('#itemArea').append(test);
						
						
						$("#ddlItem"+z).val(data[z].ItemId);
						$("#txtqty"+z).val(data[z].Qty);
						$("#txtprice"+z).val(data[z].Price);
					}
					iCnt = data.length;
					flag = data.length;
				},
			   	error: function(ts) { alert(ts.responseText) }
			});

	}
	
	
	function updateData(){
		if($('#txttgl').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Purchase Date must be chosen</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flag==0){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>At least 1 item must be chosen</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}

		var tanggal= new Date($('#txttgl').val());
		var itemlist=[];
		for (var i = 0; i < flag; i++) {
			itemlist.push([$('#ddlItem'+i).val(),$('#txtqty'+i).val().replace(/,/g,""),$('#txtprice'+i).val().replace(/,/g,"")]);	
			console.log("a"+i);		
		}
		var params = { arry : itemlist };
		var jsonString = JSON.stringify(params);
		console.log(jsonString);
	
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>Purchase/edit",
			type : "post",
			data : {Id : $('#txtid').val(), PurchaseDate: $('#txttgl').val(), itemlist:jsonString},	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			},
			error: function(ts) { alert(ts.responseText) }	
			});
			
	}
	function deleteData(id){		
		var data="Id="+id;
		//alert(data);
		$.ajax({				
		url : "<?php echo base_url()?>SalesHistory/delete",
		type : "post",
		data : data,	
		dataType : "json",				
		success : function(msg){
			var hasil = JSON.parse(msg);
			if ( hasil == true || hasil == "true"){					
				
				location.reload();
			}
			else {	
				
				setTimeout(function(){
					alert(hasil);
				}, 2000);
			}
		}
		});
	}

	function numberWithCommas(x) {
		var parts = x.toString().replace(/,/g,"")
	    parts = parts.toString().split(".");
	    //alert(parts[0])
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#txtid').val('');
		$('#txttgl').val('');
		$('#dialog').modal('show'); 
		$('#itemArea').html("");
		$('#btnSave').attr("onclick","addNew()");
	}
	function setUpdate(Id, PurchaseDate){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#txtid').val(Id);
		$('.datepicker').datepicker('update', PurchaseDate);
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(Id, PurchaseDate, EmployeeId){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(Id);
		$('#deletedate').html(PurchaseDate);
		$('#deleteemployee').html(EmployeeId);
		$('#deleteBtn').attr("onclick","deleteData('"+Id+"')");
	}
	/* function setUpdate(idcust,namacust,alamatcust,telpcust){
		$('#dialog').modal('show');
		$('#custid').val(idcust);
		$('#custname').val(namacust);
		$('#custaddress').val(alamatcust);
		$('#custphone').val(telpcust);
		$('#btnSave').attr("onclick","updateData()");			
	} */
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
             
            
        });
		
		
    </script>
</body>
</html>