		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
		<style>
			#grid th{
			text-align:center;				
			}
		</style>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Sales <small>Penjualan</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New Sales</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Sales List </h3>
                        </div>
                        <div class="panel-body">
                            <div id="grid"></div>				
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Laporan Penjualan</h3>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<form method='post' action='<?php echo base_url(); ?>penjualan/printPenjualan' id='fm1' name='fm1'>
								Dari Tanggal:<input type='date' id='dob1' name='dob1' class="form-control"/>
								Sampai Tanggal:<input type='date' id='dob2' name='dob2' class="form-control"/>
								<input class='btn btn-primary' type='submit' value=' Buat Laporan Penjualan ' style='margin-right:15px;margin-top:15px;' /><br/>
								</form>
							</div>				
						</div>
					</div>
				</div>
			</div>			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Sales </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">							
							<div class="form-group">
								<label>Id Penjualan</label>					
								<input readonly size="9" id="IdPenjualan" name="IdPenjualan" value="" class="form-control">		
							</div>							
							<div class="form-group">
								<label>Tanggal Penjualan</label>
								<input type="date" id="TanggalTransaksi" name="TanggalTransaksi" class="form-control">				
							</div>
							<div class="form-group">
								<label>Status</label>
								<select class="form-control" id="StatusPenjualan" name="StatusPenjualan">
								<option value="Lunas">Lunas</option>
								<option value="Belum Lunas">Belum Lunas</option>
								</select>								
							</div>
							<div class="form-group">
								<label>Customer</label>
								<div id="custaera"></div>		
							</div>							
							<div class="form-group">
								<label>Keterangan</label>
								<textarea id="Keterangan" name="Keterangan" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label>Ongkir</label>
								<input id="Ongkir" name="Ongkir" class="form-control"/>
							</div>
							<div class="form-group">
								<table id="coba1">
									<tr>
										<td align="center">Barang</td><td align="center">Qty</td><td align="center">Harga</td>
									</tr>
									<tr>
										<td><div id="akunarea"></td>
										<td><input class="form-control" type="text" name="Qty0" id="Qty0"/></td>
										<td><input class="form-control" type="text" name="Harga0" id="Harga0"/></td>
									</tr>									
								</table>
							</div>
							<div><button id="btnRow" type="button" class="btn btn-default">AddRow</button></div>				
							</div>							
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div name="tes02" id="tes02" class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Detail Penjualan </h3>
					</div>					
					<div class="panel-body">
						<form>
							<table class="table table-bordered table-striped">
							<tbody>
								<tr>
									<td>IdPenjualan:</td>
									<td><input class="form-control" type="text" id="delId" readonly="readonly" name="delId"/></td>
									
								</tr>
								<tr>
									<td>TanggalPenjualan:</td>
									<td><input class="form-control" readonly="readonly" type="text" id="delTanggal" name="delTanggal"/></td>			
								</tr>
								<tr>
									<td>Status:</td>
									<td><input class="form-control" readonly="readonly" type="text" id="delStatus" name="delStatus"/></td>			
								</tr>								
								<tr>
									<td>Customer:</td>
									<td><input class="form-control" readonly="readonly" type="text" id="delSupplier" name="delSupplier"/></td>			
								</tr>
								<tr>
									<td>Keterangan:</td>
									<td><textarea class="form-control" readonly="readonly" id="delKeterangan" name="delKeterangan"></textarea></td>			
								</tr>
								<tr>
									<td>Ongkir:</td>
									<td><input class="form-control" readonly="readonly" id="delOngkir" name="delOngkir"/></td>			
								</tr>
								<tr>
									<td id="detailTrans" colspan=2></td>		
								</tr>						
							</tbody>							
							</table>	<div align="center"><input type="button" id="deleteBtn" class="btn btn-default" value="Delete"/></div>						
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	var flag=0;

	
	$(document).ready(function () {
		var iCnt = 1;
		flag=1;		
		//var hasil3 = JSON.parse('<?php echo $barang; ?>');
		var hasil3 =  <?=$customer?>;
		var text = '';
		
		if(hasil3 == "No Data"){
			$('#custaera').html("No Active Departemen");
		}else{
			text+= '<select class="form-control" onchange="" name="NamaCustomer" id="NamaCustomer">';
			for(i=0;i<hasil3.length;i++){
				
				text+= '<option value="'+hasil3[i].IdCustomer+'">'+hasil3[i].IdCustomer+'-'+hasil3[i].Nama+'</option>'	
			}
			text+= '</select>';
			$('#custaera').html(text);
		}
		var hasil3 =  <?=$barang?>;
		var text = '';
		
		if(hasil3 == "No Data"){
			$('#akunarea').html("No Active Departemen");
		}else{
			text+= '<select class="form-control" onchange="" name="NamaBarang0" id="NamaBarang0">';
			for(i=0;i<hasil3.length;i++){
				
				text+= '<option value="'+hasil3[i].IdBarang+'">'+hasil3[i].IdBarang+'-'+hasil3[i].Nama+'</option>'	
			}
			text+= '</select>';
			$('#akunarea').html(text);
		
		$('#btnRow').click(function() {
            if (iCnt <= 10) {
                
				var test='<tr><td><select class="form-control" name="NamaBarang'+iCnt+'" id="NamaBarang'+iCnt+'">';
				for(i=0;i<hasil3.length;i++){
					test+='<option value="'+hasil3[i].IdBarang+'">'+hasil3[i].IdBarang+'-'+hasil3[i].Nama+'</option>'}				
				test+='</select></td><td><input class="form-control" type=text name="Qty" id=Qty' + iCnt + ' ' +'/></td><td><input class="form-control" type=text name="Harga" id=Harga' + iCnt + ' ' +'/></td></tr>';
				
				$('#coba1').append(test);
				
				iCnt = iCnt + 1;
				flag=flag+1;
            }
            else {  
                $(container).append('<label>Reached the limit</label>'); 
                $('#btnRow').attr('class', 'bt-disable'); 
                $('#btnRow').attr('disabled', 'disabled');
            }
        });
		
		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
			for(x=iCnt;x>0;x--)
			{
				$('#NamaBarang'+x).remove()
				$('#Qty'+x).remove()
				$('#Harga'+x).remove()
			}
			iCnt = 1;
		});
		$('#tes02').click(function(e) {
			$('#dialogDelete').modal('hide');
		});
		
		$('#dialog').on('hidden.bs.modal', function () {			
			for(x=iCnt;x>0;x--)
			{
				$('#NamaBarang'+x).remove()
				$('#Qty'+x).remove()
				$('#Harga'+x).remove()
			}
			iCnt = 1;
			flag=1;
		})
		
		}
	});
	
	function addNew(){
		var flagdebit=0;
		var flagkredit=0;
		var sumkredit=0;
		var sumdebit=0;
		var flagakun=0;
		for(i=0;i<flag;i++)
		{ 
			//alert(i);
			for(j=flag;j>i;j--)
			{
				if($('#NamaBarang'+i).val()==$('#NamaBarang'+j).val())
				{
					flagakun=1;
					break;
				}
			}			
			flagdebit=0;
			if($('#Qty'+i).val()==""||$('#Harga'+i).val()=="")
			{
				flagdebit=2;
				break;
			}
			if(isNaN($('#Qty'+i).val())==true||isNaN($('#Harga'+i).val())==true)
			{
				flagdebit=1;
				break;
			}
			else{
				
				sumdebit+=parseInt($('#Qty'+i).val())*parseInt($('#Harga'+i).val());
				
			}

		}
		if($('#IdPenjualan').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>IdPenjualan Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#TanggalTransaksi').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Tanggal Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if(flagdebit==2){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Qty dan Harga harus disi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagdebit==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Debit dan kredit harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagakun==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Barang yang dibeli tidak boleh sama</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		var tanggaltransaksi=$('#TanggalTransaksi').val();
		var des=$('#Keterangan').val();
		if(des==""||des==null)
			des="-";

		var data = "IdPenjualan="+$('#IdPenjualan').val()+"&TanggalTransaksi="+tanggaltransaksi+"&Keterangan="+des+"&IdCustomer="+$('#NamaCustomer').val()+"&TotalPenjualan="+sumdebit+"&Status="+$('#StatusPenjualan').val()+"&Ongkir="+$('#Ongkir').val();
		//alert(data);
		
		$.ajax({				
			url : "<?php echo base_url()?>penjualan/insertPenjualan",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					
					
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
		});
		
		var data2="";
		for(k=0;k<flag;k++)
		{
		//alert(flag);
			data2 = "IdPenjualan="+$('#IdPenjualan').val()+"&IdBarang="+$('#NamaBarang'+k).val()+"&Qty="+$('#Qty'+k).val()+"&Harga="+$('#Harga'+k).val();
			//alert(data2);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>penjualan/insertDetailPenjualan",
				data: data2,
				success: function(msg){
					var hasil = JSON.parse(msg);
					if(hasil == true || hasil == "true"){
						alert("sukses");
						setTimeout(function(){
						
						location.reload();
						}, 1500);
					}else{
						alert(hasil.msg);
					}
				}
			});
		} 
	}
	
	
	function updateData(){
		var flagdebit=0;
		var flagkredit=0;
		var sumkredit=0;
		var sumdebit=0;
		var flagakun=0;
		for(i=0;i<flag;i++)
		{ 
			//alert(i);
			for(j=flag;j>i;j--)
			{
				if($('#NamaBarang'+i).val()==$('#NamaBarang'+j).val())
				{
					flagakun=1;
					break;
				}
			}			
			flagdebit=0;
			if($('#Qty'+i).val()==""||$('#Harga'+i).val()=="")
			{
				flagdebit=2;
				break;
			}
			if(isNaN($('#Qty'+i).val())==true||isNaN($('#Harga'+i).val())==true)
			{
				flagdebit=1;
				break;
			}
			else{
				
				sumdebit+=parseInt($('#Qty'+i).val())*parseInt($('#Harga'+i).val());
				
			}

		}
		if($('#IdPenjualan').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>IdPenjualan harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#TanggalTransaksi').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Tanggal Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		else if(flagdebit==2){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Qty dan Harga harus disi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagdebit==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Debit dan kredit harus angka</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if(flagakun==1){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Barang yang dijual tidak boleh sama</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}		
		var tanggaltransaksi=$('#TanggalTransaksi').val();
		var des=$('#Keterangan').val();
		if(des==""||des==null)
			des="-";
		/* if(tanggaltransaksi != "" || tanggaltransaksi != null)
		tanggaltransaksi= tanggaltransaksi.substring(7,4) +"-"+tanggaltransaksi.substring(0,2) +"-"+tanggaltransaksi.substring(3,5); */	
		var data = "IdPenjualan="+$('#IdPenjualan').val()+"&TanggalTransaksi="+tanggaltransaksi+"&Keterangan="+des+"&IdCustomer="+$('#NamaCustomer').val()+"&TotalPenjualan="+sumdebit+"&Status="+$('#StatusPenjualan').val()+"&Ongkir="+$('#Ongkir').val();
		
			$.ajax({				
			url : "<?php echo base_url()?>penjualan/editPenjualan",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					alert("sukses");
					
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
		
			$.ajax({				
			url : "<?php echo base_url()?>penjualan/deleteDetailPenjualan",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					alert("sukses");
					
				}
				else {
					
					setTimeout(function(){
						alert("gagal delete Detail");
					}, 2000);
				}
			}	
			});
		var data2="";
		//alert(flag);
		for(k=0;k<flag;k++)
		{
		
			data2 = "IdPenjualan="+$('#IdPenjualan').val()+"&IdBarang="+$('#NamaBarang'+k).val()+"&Qty="+$('#Qty'+k).val()+"&Harga="+$('#Harga'+k).val();
			//alert(data2);
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>penjualan/insertDetailPenjualan",
				data: data2,
				success: function(msg){
					var hasil = JSON.parse(msg);
					if(hasil == true || hasil == "true"){
						setTimeout(function(){
			
						location.reload();
						}, 1500);
					}else{
						alert(hasil.msg);
					}
				}
			});
		} 	
	}
	
	
	
	function deleteData(id){		
			var data= "IdPenjualan="+id;
			//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>penjualan/deletePenjualan",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
			
			$.ajax({				
			url : "<?php echo base_url()?>penjualan/deleteDetailPenjualan",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					setTimeout(function(){
			
						location.reload();
						}, 1500);
				}
				else {
					
					
				}
			}	
			});
	}
	
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		//$("#NamaSupplier").val('');
		$("#Keterangan").val('');
		$("#TanggalTransaksi").val('');
		$("#NamaBarang0").val('');
		$("#Ongkir").val('');
		$("#Qty0").val('');
		$("#Harga0").val('');
		$('#dialog').modal('show'); 
		$('#btnSave').html("Save");
		$('#btnSave').attr("onclick","addNew()");
		flag=1;
		iCnt=1;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>penjualan/getPenjualanCode",
			success: function(msg){			
				var hasil = JSON.parse(msg);
				var text='';
				//alert(hasil[0].NoJurnal);
				$("#IdPenjualan").val(hasil.IdPenjualan);			
			}
		});
	}

	function setUpdate(IdPenjualan){
		$('#dialog').modal('show');  
		//$('#IdPembelian').val(IdPembelian);			
		var data = "IdPenjualan="+IdPenjualan;
		//alert(data);
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>penjualan/getPenjualan",
			data: data,
			success: function(msg){			
				var hasil = JSON.parse(msg);
				var text='';
				//alert(hasil[0].NoJurnal);
				$("#IdPenjualan").val(hasil[0].IdPenjualan);
				$("#NamaCustomer").val(hasil[0].IdCustomer);
				$("#Keterangan").val(hasil[0].Keterangan);
				$("#Ongkir").val(hasil[0].Ongkir);
				$("#StatusPenjualan").val(hasil[0].Status);
				$("#TanggalTransaksi").val(hasil[0].TanggalPenjualan);
				$("#NamaBarang0").val(hasil[0].IdBarang);
				$("#Qty0").val(hasil[0].Jumlah);
				$("#Harga0").val(hasil[0].Harga);
				for(z=1;z<hasil.length;z++)
				{
					var hasil3 =  <?=$barang?>;
					var test='<tr><td><select class="form-control" name="NamaBarang'+z+'" id="NamaBarang'+z+'">';
					for(i=0;i<hasil3.length;i++){
						test+='<option value="'+hasil3[i].IdBarang+'">'+hasil3[i].IdBarang+'-'+hasil3[i].Nama+'</option>'}				
					test+='</select></td><td><input class="form-control" type=text name="Qty" id=Qty' + z + ' ' +'/></td><td><input class="form-control" type=text name="Harga" id=Harga' + z + ' ' +'/></td></tr>';
					
					$('#coba1').append(test);
					$("#NamaBarang"+z).val(hasil[z].IdBarang);
					$("#Qty"+z).val(hasil[z].Jumlah);
					$("#Harga"+z).val(hasil[z].Harga);
				}
				iCnt = hasil.length;
				flag = hasil.length;				
			}
		});
		
		$('#btnSave').attr("onclick","updateData()");
		//$('#btnSave').html("Delete");			
	}
	function setDelete(IdPenjualan){
		$('#dialogDelete').modal('show');  	
		var data = "IdPenjualan="+IdPenjualan;
		$('#deleteBtn').attr("onclick","deleteData('"+IdPenjualan+"')");	
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>penjualan/getPenjualan",
			data: data,
			success: function(msg){			
				var hasil = JSON.parse(msg);
				var text='';
	
				$("#delId").val(hasil[0].IdPenjualan);
				$("#delSupplier").val(hasil[0].IdCustomer);
				$("#delStatus").val(hasil[0].Status);
				$("#delOngkir").val(hasil[0].Ongkir);
				$("#delKeterangan").val(hasil[0].Keterangan);
				$("#delTanggal").val(hasil[0].TanggalPenjualan);				
				text+= '<table class="table table-bordered table-striped">';
				text+= '<tr><td>Id Barang</td><td>Nama Barang</td><td>Jumlah</td><td>Harga</td></tr>';
				for(h=0;h<hasil.length;h++)
				{
					text+= '<tr><td>'+hasil[h].IdBarang+'</td><td>'+hasil[h].NamaBarang+'</td><td>'+hasil[h].Jumlah+'</td><td>'+hasil[h].Harga+'</td></tr>';
				}
				text+='</table>';
				text+='<div>Total Penjualan='+hasil[0].TotalPenjualan+'</div>';				
				$('#detailTrans').html(text);			
			}
		});
	}
		
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>;
				

            if(hasil.length>0)
			{
				$("#grid").shieldGrid({
					dataSource: {
						data: hasil
					},
					sorting: {
						multiple: true
					},
					rowHover: false,
					paging: true,
					selection: 
					{
						type: "cell",
						multiple: false,
						toggle: false
					},
					events: {
						selectionChanged: onSelectedChanged,
						doubleClick: onDouble
					},				
					columns: [
					{ field: "IdPenjualan", title: "IdPenjualan",attributes: {style: "text-align: center;"}},
					{ field: "TanggalPenjualan", title: "TanggalPenjualan",attributes: {style: "text-align: center;"}},
					{ field: "TotalPenjualan", title: "TotalPenjualan",attributes: {style: "text-align: right;"} },
					{ field: "Nama", title: "Nama" , attributes: {style: "text-align: center;"}},
					{ field: "Status", title: "Status", attributes: {style: "text-align: center;"}},
					{
						width: "13%",
						title: " ",
						buttons: [						
							{
								cls: "button button-primary",
								caption: " Detail",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).IdPenjualan;			
									setUpdate(id); 
								}
							},
							{
								cls: "button button-primary",
								caption: " Delete",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).IdPenjualan;			
									setDelete(id); 
								}
							}
						]
					}
					]
				});       
			}			
        });
		
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
