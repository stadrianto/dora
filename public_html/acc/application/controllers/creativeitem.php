<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Creativeitem extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;

			$search = array(
					'a.MCI_NamaItem' => $this->input->post('search')
				);
			$where = array(
			);
		$join = array();
			
		$result = $this->all_model->get_data("MCI_KodeItem as KodeItem,MCI_NamaItem as NamaItem,MRC_Harga as Rate,MCI_UserID as UpdateID", "m_creativeitem a",$join, $where, $search, false, $perPage, $segmen, false,"MCI_KodeItem","ASC");
		$result2 = $this->all_model->get_data("MCI_KodeItem as KodeItem,MCI_NamaItem as NamaItem,MRC_Harga as Rate,MCI_UserID as UpdateID", "m_creativeitem a", $join, $where, $search, false);
	

		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					$result[$key]["Harga"] = $result[$key]["Rate"];
					$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					$result[$key]["Harga"] = $result[$key]["Rate"];
					$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
	
				}
			}
			echo json_encode($result);
			exit();
		}
	}
		
	public function getLastCode()
	{
		$result = $this->all_model->query_data("SELECT MCI_KodeItem FROM m_creativeitem order by MCI_KodeItem desc", true);
		
		if(!$result){
			$result= "0";
		}	
		
		return $result["MCI_KodeItem"];
	
	}
		
	public function addItem(){

		$namaitem = $this->input->post('namaitem');
		$harga = $this->input->post('harga');
		$code = (int)$this->getLastCode();
		$code++;
		$newcode = sprintf("%05s", $code);
		
		$data = array(
				'MCI_KodeItem' => $newcode,
				'MCI_NamaItem' => $namaitem,
				'MRC_Harga' => $harga,
				'MCI_SpokePersonYN' => 'Y',
				'MCI_ActiveYN' => 'Y',
				'MCI_KodeMedia' => '00',
				'MCI_LastUpdate' => date("Y-m-d H:i:s"),
				'MCI_UserID' => $this->session->userdata("UserId"),
			
			);
		$query = $this->all_model->insert_data("m_creativeitem", $data );
		echo json_encode($query);
		exit();
	}	
	
	public function checkItem(){
		$kodeitem = $this->input->post('kodeitem');
		$result = $this->all_model->query_data("SELECT * from  m_ratecard where MRC_KodeAcara='".$this->input->post('noacara')."' and MRC_KodeItem ='$kodeitem'", true);
		
		if($result)
		{
			echo "true";
		}
		else echo "false";

		exit();
	}
	
	public function addItem2(){

		$namaitem = $this->input->post('namaitem');
		$kodeitem = $this->input->post('kodeitem');
		$harga = $this->input->post('harga'); 
		$data = array(
				'MRC_KodeAcara' => $this->input->post('noacara'),
				'MRC_KodeItem' => $kodeitem,
				'MRC_Harga' => $harga,
				'MRC_ActiveYN' => 'Y',
				'MRC_Flag' => '2',
				'MRC_LastUpdate' => date("Y-m-d H:i:s"),
				'MRC_UserID' => $this->session->userdata("UserId"),
			);
		$query = $this->all_model->insert_data("m_ratecard", $data );
		echo json_encode($query);
		exit();
	}	
	
	public function editItem(){
		$kodeitem = $this->input->post('kodeitem');
		$namaitem = $this->input->post('namaitem');
		$harga = $this->input->post('harga');
		
		$data = array(
				'MCI_NamaItem' => $namaitem,
				'MRC_Harga' => $harga,
				'MCI_SpokePersonYN' => 'Y',
				'MCI_ActiveYN' => 'Y',
				'MCI_KodeMedia' => '00',
				'MCI_LastUpdate' => date("Y-m-d H:i:s"),
				'MCI_UserID' => $this->session->userdata("UserId"),
			
			);
		$where = array(
				'MCI_KodeItem' => $kodeitem,
				);
		$query = $this->all_model->update_data("m_creativeitem", $data ,$where);
		echo json_encode($query);
		exit();
	}	
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="Creative Item";
		$data['page_title']="Media Planning Pharos";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('creativeitem_view',$data2);
		$this->load->view('home_footer');
	}
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


