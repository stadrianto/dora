<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pembelian extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getPembelianCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdPembelian,4) as IdPembelian FROM pembelian ORDER BY IdPembelian DESC", true);
		$result["IdPembelian"] = $result["IdPembelian"]+1;
		if($result["IdPembelian"] < 10)
			$result["IdPembelian"] = "PB0000".$result["IdPembelian"];
		else if($result["IdPembelian"]< 100)
			$result["IdPembelian"] = "PB000".$result["IdPembelian"];
		else if($result["IdPembelian"]< 1000)
			$result["IdPembelian"] = "PB00".$result["IdPembelian"];
		else if($result["IdPembelian"]< 10000)
			$result["IdPembelian"] = "PB0".$result["IdPembelian"];			
		else
			$result["IdPembelian"] = "PB".$result["IdPembelian"];
		//return $result["IdPembelian"];
		echo json_encode($result);
		exit();
	}
	
	public function getPembelianCode2(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdPembelian,4) as IdPembelian FROM pembelian ORDER BY IdPembelian DESC", true);
		$result["IdPembelian"] = $result["IdPembelian"]+1;
		if($result["IdPembelian"] < 10)
			$result["IdPembelian"] = "PB0000".$result["IdPembelian"];
		else if($result["IdPembelian"]< 100)
			$result["IdPembelian"] = "PB000".$result["IdPembelian"];
		else if($result["IdPembelian"]< 1000)
			$result["IdPembelian"] = "PB00".$result["IdPembelian"];
		else if($result["IdPembelian"]< 10000)
			$result["IdPembelian"] = "PB0".$result["IdPembelian"];			
		else
			$result["IdPembelian"] = "PB".$result["IdPembelian"];
		return $result["IdPembelian"];
		
	}
	public function getPembelian()
	{	
		$nojurnal=$this->input->post('IdPembelian');

		$result = $this->all_model->query_data("select a.IdPembelian as IdPembelian, a.IdSupplier, TanggalPembelian, a.Keterangan, TotalPembelian, Status, b.IdBarang as IdBarang, d.Nama as NamaSupplier ,c.Nama as NamaBarang, Jumlah, b.Harga as Harga, Ongkir from Pembelian a join detailPembelian b on a.IdPembelian=b.IdPembelian join barang c on b.IdBarang=c.IdBarang join mssupplier d on d.IdSupplier=a.IdSupplier where a.IdPembelian='".$nojurnal."' order by IdPembelian desc", false);
		if(!$result){
			$result= "0";
		}
		echo json_encode($result);
		exit();
	}
	public function getNamaBarang(){	
		$result = $this->all_model->query_data("select IdBarang, Nama from barang a where a.activeyn='N' order by idbarang asc", false);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getNamaBarang2($request = true){	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdBarang as IdBarang,Nama as Nama", "barang a",$join, $where, $search,false);
		if(!$result){
			$result= "0";
		}	
		return $result;
		echo json_encode($result);		
		exit();
	}
	public function getNamaSupplier($request = true){	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdSupplier as IdSupplier,Nama as Nama", "mssupplier a",$join, $where, $search,false);
		if(!$result){
			$result= "0";
		}	
		return $result;
		echo json_encode($result);		
		exit();
	}
	
	public function getDataNew($request = true){
		$search = array(
				
			);
		$where = array(
			
		);
		$join = array(
			array('table'=>'mssupplier b','field' => 'a.IdSupplier = b.IdSupplier','method'=>'Left')
		);
		$result2 = $this->all_model->get_data("IdPembelian as IdPembelian,TanggalPembelian as TanggalPembelian,TotalPembelian as TotalPembelian, Nama as Nama, a.Status as Status","pembelian a",$join, $where, $search, false);
		
		return $result2;
		echo json_encode($result2);		
		exit();
	}	
	
	public function getData($perPage=10, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.IdPembelian' => $this->input->post('search')
			);
			$where = array(
			
			);
		}else{
			$search = array(
				'a.IdPembelian' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
			array('table'=>'mssupplier b','field' => 'a.IdSupplier = b.IdSupplier','method'=>'Left'),
		);			
						
		$result = $this->all_model->get_data("IdPembelian as IdPembelian,a.IdSupplier as IdSupplier,TanggalPembelian as TanggalPembelian,Keterangan as Keterangan,TotalPembelian as TotalPembelian,Nama,Alamat,Telepon,Handphone,a.Status as Status", "pembelian a",$join, $where, $search, false, $perPage, $segmen, false,"IdPembelian","DESC");
		
		$result2 = $this->all_model->get_data("IdPembelian as IdPembelian,a.IdSupplier as IdSupplier,TanggalPembelian as TanggalPembelian,Keterangan as Keterangan,TotalPembelian as TotalPembelian,Nama,Alamat,Telepon,Handphone,a.Status as Status", "pembelian a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['TotalPembelian'] = $this->all_model->rp($result[$key]['TotalPembelian']);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['TotalPembelian'] = $this->all_model->rp($result[$key]['TotalPembelian']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function checkBarang(){

		$IdPembelian = $this->input->post('IdPembelian');
		$IdBarang = $this->input->post('IdBarang');
		$result = $this->all_model->query_data("SELECT IdBarang FROM detailpembelian WHERE IdPembelian='".$IdPembelian."' and IdBarang='".$IdBarang."'", true);
		
		if($result)
		{
			echo json_encode("true");
		}
		else echo json_encode("false");

		exit();
	}
	
	public function getDataDetailBarangMasuk($IdBarangMasuk){

		$result = $this->all_model->query_data("SELECT IdDetailBarangMasuk FROM detailbarangmasuk WHERE IdBarangMasuk='".$IdBarangMasuk."' order by IdDetailBarangMasuk desc", true);
		return $result["IdDetailBarangMasuk"];
		
	}
	
	public function getDataDetailPembelian($perPage=100, $segmen=0,  $request = true,$IdPembelian){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				//'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdPembelian' => $IdPembelian
			);
		}else{
			$search = array(
				//'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdPembelian' => $IdPembelian
			);
		}
		$join = array(
			array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),	
			array('table'=>'satuan c','field' => 'b.IdSatuan = c.IdSatuan','method'=>'Left'),		
		);			
						
		$result = $this->all_model->get_data("IdDetailPembelian as IdDetailPembelian,IdPembelian as IdPembelian,a.IdBarang as IdBarang,Nama as Nama,a.Jumlah as Jumlah,a.Harga as Harga,Satuan as Satuan", "detailpembelian a",$join, $where, $search, false, $perPage, $segmen, false,"IdDetailPembelian","ASC");
		
		$result2 = $this->all_model->get_data("IdDetailPembelian as IdDetailPembelian,IdPembelian as IdPembelian,Nama as Nama,a.Jumlah as Jumlah,a.Harga as Harga,Satuan as Satuan", "detailpembelian a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailPembelian(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Harga1'] = $this->all_model->rp($result[$key]['Harga']);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailPembelian(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Harga1'] = $this->all_model->rp($result[$key]['Harga']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}

	public function getDataPembelian($idpembelian)
	{
		$totalpembelian = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.IdPembelian' => $idpembelian
		);
		
		$join = array(
			array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),		
		);	
	
		$result = $this->all_model->get_data("IdDetailPembelian as IdDetailPembelian,IdPembelian as IdPembelian,a.Jumlah as Jumlah,a.IdBarang as IdBarang,a.Harga as Harga,b.Nama as Nama", "detailpembelian a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				//$totalpembelian += ($result[$key]['Harga']*$result[$key]['Jumlah']);
			}
			return $result;
		}
		else
			return "No Data";
		
		
	
	}
	
	public function getDataTransaksi($Periode,$Periode2)
	{
		$result = $this->all_model->query_data("SELECT a.IdPembelian as IdPembelian, a.TanggalPembelian as TanggalPembelian, Nama, a.Keterangan as Keterangan, TotalPembelian FROM pembelian a join mssupplier b on a.idsupplier=b.idsupplier WHERE a.TanggalPembelian BETWEEN '".$Periode."' AND '".$Periode2."'", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function printPembelian()
	{
		//$noproject = $this->session->userdata('NoProject');
		//$dataproject = $this->getDataTrial();
		$Periode = $this->input->post('dob1');
		$Periode2 = $this->input->post('dob2');
		//$Periode=substr($Periode,6,10)."-".substr($Periode,0,2)."-".substr($Periode,3,2);
		//$Periode2= substr($Periode2,6,10)."-".substr($Periode2,0,2)."-".substr($Periode2,3,2);
		$result = $this->getDataTransaksi($Periode,$Periode2);
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		//$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		//$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(20);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Laporan Pembelian',0,0,'C');
		$this->fpdf->SetFont('Arial','BU',12);
		$this->fpdf->Ln(5);
		$this->fpdf->Cell(190,6,$Periode.' Sampai '.$Periode2,0,0,'C');
		
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
		$this->fpdf->Ln(15);

		$y_axis_initial = 50;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(8);
		//Header tabel halaman 1
		$this->fpdf->CELL(20,6,'IdPembelian',1,0,'C',1);
		$this->fpdf->CELL(50,6,'Nama Supplier',1,0,'C',1);
		$this->fpdf->CELL(35,6,'Tanggal Pembelian',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Keterangan',1,0,'C',1);
		$this->fpdf->Cell(30,6,'Total Pembelian',1,0,'C',1);
		//$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);

		$this->fpdf->Ln();
		$max=20;//max baris perhalaman
		$i=0;
		$no=0;
		$totala=0;
		$totalb=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$y=60;
		$x=0;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "0")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
			$this->fpdf->AddPage();
			$this->fpdf->SetY(8);
			$this->fpdf->SetX(10);
			$this->fpdf->CELL(20,6,'IdPembelian',1,0,'C',1);
			$this->fpdf->CELL(50,6,'Nama Supplier',1,0,'C',1);
			$this->fpdf->CELL(35,6,'Tanggal Pembelian',1,0,'C',1);
			$this->fpdf->Cell(60,6,'Keterangan',1,0,'C',1);
			$this->fpdf->Cell(30,6,'Total Pembelian',1,0,'C',1);

			$this->fpdf->SetY(8);
			$this->fpdf->SetX(60);
			$y_axis = $y_axis + $row_height;
			$i=0;
			$this->fpdf->Ln();
			
		}

		$grandtotal+=$result[$key]["TotalPembelian"];		
		
		$i++;
		$no++;
		$x=8;
		$this->fpdf->SetY($y);
		$this->fpdf->SetX($x);
		//$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->MultiCell(20,6,$result[$key]["IdPembelian"],0,'C',0);
		$x=$x+20;
		$this->fpdf->SetY($y);
		$this->fpdf->SetX($x);
		$this->fpdf->MultiCell(50,6,$result[$key]["Nama"],0,'C',0);
		$this->fpdf->SetY($y);
		$x=$x+50;
		$this->fpdf->SetX($x);
		$this->fpdf->MultiCell(35,6,$result[$key]["TanggalPembelian"],0,'C',0);
		$this->fpdf->SetY($y);
		$x=$x+35;
		$this->fpdf->SetX($x);
		$this->fpdf->MultiCell(60,6,$result[$key]["Keterangan"],0,'C',0);
		$this->fpdf->SetY($y);
		$x=$x+60;
		$this->fpdf->SetX($x);
		$this->fpdf->MultiCell(30,6,$this->all_model->rp($result[$key]["TotalPembelian"]),0,'R',0);
		$y=$y+12;
		//$this->fpdf->Cell(25,6,$this->all_model->rp($saldo),1,0,'R',0);
		//$this->fpdf->Cell(35,6,$this->all_model->rp($result[$key]["Insentif"]),1,0,'C',0);
		

		$this->fpdf->Ln();

		}
		}
		$this->fpdf->SetX(30);

		//buat footer
		$now = date("d F Y");
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(275,6,"Total Pembelian : ".$this->all_model->rp($grandtotal),0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Output('Laporan Pembelian'.date("F Y").'.pdf', 'I');
		
	}
	
	public function calculatePembelian($idpembelian){
	
		$totalpembelian = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.IdPembelian' => $idpembelian
		);
		
		$join = array(
				
		);	
	
		$result = $this->all_model->get_data("IdDetailPembelian as IdDetailPembelian,IdPembelian as IdPembelian,a.Jumlah as Jumlah,a.Harga as Harga", "detailpembelian a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				$totalpembelian += ($result[$key]['Harga']*$result[$key]['Jumlah']);
			}
		}
		
		$data = array(	
		'TotalPembelian' => $totalpembelian,	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			
		$where = array(	
					'IdPembelian'=> $idpembelian,
				
		);	
		$query = $this->all_model->update_data("pembelian", $data ,$where);
		
		
	}
	
	public function insertPembelian(){
		
		$data = array(
		'IdPembelian'  => $this->input->post('IdPembelian'),
		'IdSupplier' => $this->input->post('IdSupplier'),		
		'TotalPembelian' => $this->input->post('TotalPembelian'),
		'Ongkir' => $this->input->post('Ongkir'),		
		'Keterangan' 	=> $this->input->post('Keterangan'),
		'TanggalPembelian' => $this->input->post('TanggalTransaksi'),
		'Status' => $this->input->post('Status'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("pembelian", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function insertDetailPembelian(){
		
		$data = array(
		'IdPembelian'  => $this->input->post('IdPembelian'),
		'IdBarang' => $this->input->post('IdBarang'),		
		'Jumlah' => $this->input->post('Qty'),	
		'Harga' => $this->input->post('Harga'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("detailpembelian", $data );
		//$this->calculatePembelian($this->input->post('IdPembelian'));
		
		echo json_encode($query);
		exit();
	}	
	
	public function editPembelian(){
		$data = array(
		'IdSupplier' => $this->input->post('IdSupplier'),		
		'TotalPembelian' => $this->input->post('TotalPembelian'),
		'Ongkir' => $this->input->post('Ongkir'),
		'Status' => $this->input->post('Status'),
		'Keterangan' 	=> $this->input->post('Keterangan'),
		'TanggalPembelian' => $this->input->post('TanggalTransaksi'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdPembelian'			=> $this->input->post('IdPembelian'),
				
		);	
		$query = $this->all_model->update_data("pembelian", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function editDetailPembelian(){
		$data = array(
		'IdPembelian'  => $this->input->post('IdPembelian'),
		'IdBarang' => $this->input->post('IdBarang'),		
		'Jumlah' => $this->input->post('Jumlah'),
		'Harga' => $this->input->post('Harga'),	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdDetailPembelian'=> $this->input->post('IdDetailPembelian'),
				
		);	
		$query = $this->all_model->update_data("detailpembelian", $data ,$where);
		$this->calculatePembelian($this->input->post('IdPembelian'));
		echo json_encode($query);
		exit();
	}

	public function masukBarang(){
		$data = array(
		'Status' => '1',		
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdPembelian'=> $this->input->post('IdPembelian'),
				
		);	
		$query = $this->all_model->update_data("pembelian", $data ,$where);
		
		
		$data4 = array(
				'Keterangan' => 'Pembelian '.$this->input->post('IdPembelian'),
				'UpdateID' => $this->session->userdata("UserId"),
				'TanggalMasuk' => date("Y-m-d H:i:s"),
				'Tag' => 'Pembelian',
				'Reference' => $this->input->post('IdPembelian')
			);
		$this->all_model->insert_data("barangmasuk", $data4);	
		
		$result2 = $this->all_model->query_data("SELECT IdBarangMasuk FROM barangmasuk where Reference='".$this->input->post('IdPembelian')."'", true);
		$idbarangmasuk = $result2["IdBarangMasuk"];
		
		$search = array(
			
		);
		$where1 = array(
			'a.IdPembelian' => $this->input->post('IdPembelian')
		);
		
		$join = array(
			array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),		
		);	
	
		$result = $this->all_model->get_data("IdDetailPembelian as IdDetailPembelian,IdPembelian as IdPembelian,a.Jumlah as Jumlah,a.IdBarang as IdBarang,a.Harga as Harga,b.Nama as Nama,b.Quantity as Quantity,b.Jumlah as JumlahBarang", "detailpembelian a",$join, $where1, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
			
					$quantitybarang = $result[$key]['Quantity'] + $result[$key]['Jumlah'];
					$totalbarang = $result[$key]['Harga'] + $result[$key]['JumlahBarang'];
					
					$data3 = array(	
					'Quantity' => $quantitybarang,
					'Jumlah' => $totalbarang,	
					'UpdateId' => $this->session->userdata("UserId"),
					'UpdateTime' => date("Y-m-d H:i:s")
						);
					$where3 = array(	
								'IdBarang'=> $result[$key]['IdBarang'],
							
					);	
					$this->all_model->update_data("barang", $data3 ,$where3);
					
					$data5 = array(
							'IdBarangMasuk'  => $idbarangmasuk,
							'IdBarang'  => $result[$key]['IdBarang'],
							'Nama' => $result[$key]['Nama'],		
							'Quantity' => $result[$key]['Jumlah'],	
							'Jumlah' => $result[$key]['Harga'],
							'Keterangan' => 'Barang Masuk Pembelian '.$this->input->post('IdPembelian'),
							'Status' => 'N',
						);
					$this->all_model->insert_data("detailbarangmasuk", $data5);
					
					
					$IdDetailBarangMasuk = $this->getDataDetailBarangMasuk($idbarangmasuk);
					
		
					$data6 = array(
							'IdDetailBarangMasuk'  => $IdDetailBarangMasuk,
							'IdBarang'  => $result[$key]['IdBarang'],	
							'SaldoQty' => $quantitybarang,	
							'SaldoJumlah' => $totalbarang,
							'Tag' => 'Pembelian',
							'Tanggal' => date("Y-m-d H:i:s"),
						);
					$this->all_model->insert_data("historybar", $data6);					
					
					

			}
			
		}
		
	
		echo json_encode($query);
		exit();
	}	
	
	public function deletePembelian()
	{
		$IdPembelian = $this->input->post('IdPembelian');
		$where = array('IdPembelian'=>$IdPembelian);
		$query = $this->all_model->delete_data("pembelian", $where);
		
		echo json_encode($query);
		exit();
	}
	
	public function deleteDetailPembelian()
	{
		$IdPembelian = $this->input->post('IdPembelian');
		$where = array('IdPembelian'=>$IdPembelian);
		$query = $this->all_model->delete_data("detailpembelian", $where);

		echo json_encode($query);
		exit();
	}
		
	public function getDetailPembelian($IdPembelian)
	{
		$result = $this->all_model->query_data("SELECT * FROM pembelian a join mssupplier b on a.IdSupplier=b.IdSupplier where a.IdPembelian='".$IdPembelian."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function index(){
	 if($this->session->userdata("UserId")=="")
		redirect(base_url(). "login","refresh");
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['title']="Pembelian";
		$data['page']="pembelian_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['data'] = json_encode($this->getDataNew(false));
		$data['barang'] = json_encode($this->getNamaBarang2(false));
		$data['supplier'] = json_encode($this->getNamaSupplier(false));
		$this->load->view('main',$data);
	}
	
	public function detailPembelian($IdPembelian){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Pembelian";
		$data2['data'] = json_encode($this->getDataDetailPembelian($config['per_page'], $config['segmen'],false,$IdPembelian ));
		$data2['data2'] = json_encode($this->getDetailPembelian($IdPembelian));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailpembelian_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


