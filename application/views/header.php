<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/local.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/bootstrap/css/datepicker.css" />
	<script src="<?php echo base_url(); ?>media/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
	<script src="<?php echo base_url(); ?>media/js/jQuery-2.1.4.min.js"></script>
	<script src="<?php echo base_url(); ?>media/js/jquery.backstretch.min.js"></script>
    <!--script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery-1.10.2.min.js"></script-->
    <script type="text/javascript" src="<?php echo base_url(); ?>media/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>media/bootstrap/js/bootstrap-datepicker.js"></script>
    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <!--link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/dark-bootstrap/all.min.css" />

    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script-->
<!-- DataTables -->
    <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script-->
    <script type="text/javascript" src="<?php echo base_url(); ?>media/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>media/datatables/dataTables.bootstrap.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>media/datatables/dataTables.bootstrap.css">
</head>

<body>
    <div id="wrapper">