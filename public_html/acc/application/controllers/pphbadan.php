<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pphbadan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function getDataPrinter($id)
	{
		$result = $this->all_model->query_data("SELECT Nama as Nama from msuser where iduser=".$id, false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataJurnal($noakun,$tanggal1,$tanggal2)
	{
		$result = $this->all_model->query_data("SELECT Debit as Debit,Kredit as Kredit FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where status='Approved' and noakun='".$noakun."' and tanggaltransaksi between '".$tanggal1."' and '".$tanggal2."'", false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataTrial()
	{
		$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NamaAkun as NamaAkun,SaldoAwal as SaldoAwal,IdNeraca as IdNeraca FROM tabelakun a join kelompokakun b on a.idkelompokakun=b.idkelompokakun where left(a.noakun,1)>3 order by a.noakun asc", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	
	public function printPphbadan()
	{
		$tanggal1=$this->input->post('dob');
		$tanggal2=$this->input->post('dob1');
		$qtanggal1=substr($tanggal1,6,4).'-'.substr($tanggal1,0,2).'-'.substr($tanggal1,3,2);
		$qtanggal2=substr($tanggal2,6,4).'-'.substr($tanggal2,0,2).'-'.substr($tanggal2,3,2);		
		$result = $this->getDataTrial();		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/logo.png',35,10,30,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('Arial','B',15);
		$this->fpdf->Cell(190,6,'Citra Inti Prima Sejati',0,0,'C');
		$this->fpdf->Ln(5);
		$this->fpdf->Cell(190,6,'PPH Badan',0,0,'C');
		$this->fpdf->SetFont('Arial','BU',12);
		$this->fpdf->Ln(5);
		$this->fpdf->Cell(190,6,$tanggal1.' - '.$tanggal2,0,0,'C');	
		$this->fpdf->Ln(15);
		$y_axis_initial = 50;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(50);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no4=0;
		$no5=0;
		$no6=0;
		$no7=0;
		$no8=0;
		$hpp=0;
		$totala=0;
		$totalb=0;
		$totalc=0;
		$totald=0;
		$totale=0;
		$totalf=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		//$grandtotal = 0;
		if($result != "0")
		{
			foreach($result as $key => $value)
			{
				$i++;				
				$saldo=floatval($result[$key]["SaldoAwal"]);
				$result2 = $this->getDataJurnal($result[$key]["NoAkun"]."",$qtanggal1."",$qtanggal2."");
				if($result[$key]["IdNeraca"]=="1")
				{	
					$saldo=0;
					if($result2!="0")
					{
						for($z=0;$z<count($result2);$z++){
							$saldo+=$result2[$z]["Debit"];
							$saldo-=$result2[$z]["Kredit"];
						}
					}
				}	
				else if($result[$key]["IdNeraca"]=="2")
				{
					$saldo=0;
					if($result2!="0")
					{
						for($z=0;$z<count($result2);$z++){
							$saldo-=$result2[$z]["Debit"];
							$saldo+=$result2[$z]["Kredit"];				
						}
					}
				}
				$i++;
				if(substr($result[$key]["NoAkun"],0,1)=="4")
				{
					$no4++;
					$totala+=$saldo;
				}
				else if(substr($result[$key]["NoAkun"],0,1)=="5")
				{				
					$no5++;
					$hpp+=$saldo;
				}
				else if(substr($result[$key]["NoAkun"],0,1)=="6")
				{
					$no6++;
					$totalb+=$saldo;
				}		
				else if(substr($result[$key]["NoAkun"],0,1)=="7")
				{
					$no7++;
					$totald+=$saldo;
				}
				else if(substr($result[$key]["NoAkun"],0,1)=="8")
				{
					$no8++;
					$totale+=$saldo;
				}				
			}
				//LabaKotor totala
				$this->fpdf->SetFont('Arial','B',10);		
				$this->fpdf->SetY(50+($no4*5)+($no5*5));
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,'Penghasilan Kotor',0,0,'L',0);
				$this->fpdf->SetX(120);
				//$this->fpdf->Cell(60,6,$this->all_model->rp($totala),0,0,'R',0);
				$this->fpdf->Cell(60,6,$this->all_model->rp(floatval($this->input->post('bruto'))),0,0,'R',0);
				$this->fpdf->Ln();
				//laba usaha totalc
				$totalc=$totala-$hpp-$totalb;
				/*
				if($totala>$totalb)				
					$this->fpdf->Cell(60,6,'Laba Usaha',0,0,'L',0);				
				else
					$this->fpdf->Cell(60,6,'Rugi Usaha',0,0,'L',0);
				*/
				//total penghasilan diluar usaha totald
				//total beban diluar usaha totale
				//Laba Bersih Sbelum Pajak totalf
				$totalf=($totalc+$totald)-$totale;
				$this->fpdf->SetY(55+($no4*5)+($no5*5));
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,'Laba Bersih Sebelum Pajak',0,0,'L',0);
				$this->fpdf->SetX(120);				
				//$this->fpdf->Cell(60,6,$this->all_model->rp($totalf),0,0,'R',0);
				$this->fpdf->Cell(60,6,$this->all_model->rp(floatval($this->input->post('laba'))),0,0,'R',0);
				//Pajak
				$pajak=0;
				$rumus="";
				if($totala<=4800000000)
				{
					$pajak=($totalf*0.25)*0.5;
					$rumus="Pajak yang harus dibayar dalam setahun=(".$totalf." x 25%) x 50%";
				}
				else if($totala>4800000000 && $totala<=50000000000)
				{
					$pajak=((((4800000000/$totala)* $totalf)*0.25) *0.5) + (($totalf-((4800000000/$totala)* $totalf))*0.25);
					$rumus="Pajak yang harus dibayar dalam setahun=(".(4800000000/$totala)* $totalf." x 25%) x 50% + (".($totalf-((4800000000/$totala)* $totalf)). " x 25%)";
				}
				else
				{
					$pajak=($totalf*0.25);
					$rumus="Pajak yang harus dibayar dalam setahun=(".$totalf." x 25%)";
				}
				$this->fpdf->SetY(60+($no4*5)+($no5*5));
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,$rumus,0,0,'L',0);
				$this->fpdf->Ln();
				$this->fpdf->SetX(120);				
				//$this->fpdf->Cell(60,6,$this->all_model->rp($pajak),0,0,'R',0);
				$this->fpdf->Cell(60,6,$this->all_model->rp(floatval($this->input->post('pajak'))),0,0,'R',0);
				$this->fpdf->SetY(75+($no4*5)+($no5*5));
				$this->fpdf->SetX(20);
				$this->fpdf->Cell(60,6,"Angsuran Pajak setiap bulan",0,0,'L',0);
				$this->fpdf->SetX(120);				
				//$this->fpdf->Cell(60,6,$this->all_model->rp($pajak/12),0,0,'R',0);
				$this->fpdf->Cell(60,6,$this->all_model->rp(floatval($this->input->post('angsuran'))),0,0,'R',0);
		}
		//buat footer
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$printer=$this->getDataPrinter($this->session->userdata("UserId"));
		$this->fpdf->SetX(0);			
		$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'C');
		$this->fpdf->SetX(40);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Output('Laporan PPh Badan'.date("F Y").'.pdf', 'I');
		
	}
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Pajak";
		//$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('pphbadan_view');
		$this->load->view('home_footer');
	}	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

