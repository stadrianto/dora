<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class laporantagihan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function getPrintTagihan($Periode,$Periode2)
	{

		$result = $this->all_model->query_data("SELECT b.Project,b.NoOrder,c.Nama,a.Nominal,b.TanggalOrder,a.TotalPembayaran from pembayaran a join trorder b on a.NoOrder = b.NoOrder join mscustomer c on b.IdCustomer = c.IdCustomer WHERE b.TanggalOrder >= '".$Periode."' AND b.TanggalOrder <= '".$Periode2."'", false);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	

	
	public function printLaporanTagihan(){
	
		$Periode = $this->input->post('dob2');
		$Periode2 = $this->input->post('dob');
		
		$Periode=substr($Periode,6,10)."-".substr($Periode,0,2)."-".substr($Periode,3,2);
		$Periode2= substr($Periode2,6,10)."-".substr($Periode2,0,2)."-".substr($Periode2,3,2);
		
			$result = $this->getPrintTagihan($Periode,$Periode2);
			
			$this->load->library('fpdf17/fpdf');
			
			//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
			$this->fpdf->FPDF('P','mm','A4');
			$this->fpdf->Open();
			$this->fpdf->SetAutoPageBreak(false);
			$this->fpdf->AddPage('L');
			$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
			$this->fpdf->Image('images/cips_nama.png',130,20,50,0,'','http://www.cips.or.id/'); 
			$this->fpdf->Ln(30);
			$this->fpdf->SetX(142);
			$this->fpdf->SetFont('Arial','BU',15);
			
			$this->fpdf->Cell(30,6,'Laporan Tagihan',0,0,'C');
			$this->fpdf->Ln();
			$this->fpdf->SetX(135);
			$this->fpdf->Cell(40,6,'Untuk Periode '.$Periode.' Hingga '.$Periode2.'',0,0,'C');
			$this->fpdf->Ln(10);
			$this->fpdf->SetFont('Arial','BU',12);
			//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
			$this->fpdf->Ln(10);

			$y_axis_initial = 60;
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->SetY($y_axis_initial);
			$this->fpdf->SetX(25);
			//Header tabel halaman 1
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(25,6,'No Project',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Customer',1,0,'C',1);
			$this->fpdf->Cell(50,6,'Nama Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Tanggal Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Nilai Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Sisa Pembayaran',1,0,'C',1);

			$this->fpdf->Ln();
			$max=15;//max baris perhalaman
			$i=0;
			$no=0;
			$total=0;
			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			$grandtotal = 0;
			if($result != "No Data")
			{
			foreach($result as $key => $value){
			//$total += $row['Total'];

			if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
			$this->fpdf->AddPage();
			$this->fpdf->SetY(10);
			$this->fpdf->SetX(25);
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(25,6,'No Order',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Customer',1,0,'C',1);
			$this->fpdf->Cell(50,6,'Nama Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Tanggal Order',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Nilai Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Sisa Pembayaran',1,0,'C',1);

			$this->fpdf->SetY(10);
			$this->fpdf->SetX(55);
			$y_axis = $y_axis + $row_height;
			$i=0;
			$this->fpdf->Ln();

			}

		
		$i++;
		$no++;
		$this->fpdf->SetX(25);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["NoOrder"],1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["Nama"],1,0,'C',0);
		$this->fpdf->Cell(50,6,$result[$key]["Project"],1,0,'C',0);
		$this->fpdf->Cell(45,6,date("d-m-Y",strtotime($result[$key]["TanggalOrder"])),1,0,'C',0);
		$this->fpdf->Cell(45,6,$this->all_model->rp($result[$key]["Nominal"]),1,0,'C',0);
		$sisa = $result[$key]["Nominal"] - $result[$key]["TotalPembayaran"];
		if($sisa <= 0) $sisa = 0;
		$this->fpdf->Cell(45,6,$this->all_model->rp($sisa),1,0,'C',0);	
			$this->fpdf->Ln();
		$grandtotal+=$sisa;	

			}
			}
			
				//buat footer
				
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(10);

		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(258,6,"Total Piutang Customer  : ".$this->all_model->rp($grandtotal)."",0,0,'R');
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(200);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(400,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		
		$this->fpdf->Output('Laporan Proyek'.date("F Y").'.pdf', 'I');
		
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		
		$data['title']="CIPS - Laporan Project";
		$data['page_title']="CIPS - Laporan Project";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('laporantagihan_view','');
		$this->load->view('home_footer');
	}
		
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

