<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jabatan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	

	public function editJabatan(){
		$idjabatan = $this->input->post('idjabatan');
		$nama = $this->input->post('nama');
		$pekerjaan = $this->input->post('pekerjaan');
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'Nama' => $nama,
				'Pekerjaan' => $pekerjaan,
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array("IdJabatan" => $idjabatan);
		$query = $this->all_model->update_data("msjabatan", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	

	public function addJabatan(){

		$nama = $this->input->post('nama');
		$pekerjaan = $this->input->post('pekerjaan');
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'Nama' => $nama,
				'Pekerjaan' => $pekerjaan,
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("msjabatan", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function deleteJabatan()
	{
		$idjabatan = $this->input->post('idjabatan');
		$where = array('IdJabatan'=>$idjabatan);
		$query = $this->all_model->delete_data("msjabatan", $where);
		echo json_encode($query);
		exit();
	}
	
	public function generateJabatan()
	{ 
		$data["status"] = "sukses";
		$data["msg"] = $this->getAllJabatan(100,0,false);
		echo json_encode($data);
		exit();
	}

	public function getAllJabatan($perPage=100, $segmen=0, $request = true){
		$current = $this->input->post('page');
		
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
			);
		$join = array(
		);
		$where = array(
		);

		$result = $this->all_model->get_data("IdJabatan as IdJabatan ,Nama as Nama ,Pekerjaan as Pekerjaan, UpdateID as UpdateID,ActiveYN as ActiveYN,UpdateTime as UpdateTime", "msjabatan a",$join, $where, $search, false, $perPage, $segmen, false,"IdJabatan","ASC");
		$result2 = $this->all_model->get_data("IdJabatan as IdJabatan ,Nama as Nama ,Pekerjaan as Pekerjaan, UpdateID as UpdateID,ActiveYN as ActiveYN,UpdateTime as UpdateTime", "msjabatan a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllJabatan($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				
				foreach($result as $key => $value){	
				}
				
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllJabatan($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				}
			}
			echo json_encode($result);
			exit();
		}
	}

	
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="CIPS - Master Jabatan";
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['page_title']="CIPS - Master Jabatan";
		$data2['data'] = json_encode($this->getAllJabatan($config['per_page'], $config['segmen'],false));
		//$data['data'] = json_encode($this->getNews());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('jabatan_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


