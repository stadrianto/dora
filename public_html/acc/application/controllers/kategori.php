<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kategori extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	

	public function editKategori(){
		$IdKategori = $this->input->post('IdKategori');
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'IdKategoriBesar' => $this->input->post('IdKategoriBesar'),
				'Jenis' => $this->input->post('Jenis'),
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array("IdKategori" => $IdKategori);
		$query = $this->all_model->update_data("kategori", $data , $where);
		
		echo json_encode($query);
		exit();
	}
	

	public function addKategori(){
		
		$activeyn = $this->input->post('activeyn');
		
		$data = array(
				'IdKategoriBesar' => $this->input->post('IdKategoriBesar'),
				'Jenis' => $this->input->post('Jenis'),
				'ActiveYN' => $activeyn,
				'UpdateID' => $this->session->userdata("UserId"),
				'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("kategori", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function deleteKategori()
	{
		$IdKategori = $this->input->post('IdKategori');
		$where = array('IdKategori'=>$IdKategori);
		$query = $this->all_model->delete_data("kategori", $where);
		echo json_encode($query);
		exit();
	}
	
	public function generateKategori()
	{ 
		$data["status"] = "sukses";
		$data["msg"] = $this->getAllKategori(100,0,false);
		echo json_encode($data);
		exit();
	}

	public function getAllKategori($perPage=100, $segmen=0, $request = true){
		$current = $this->input->post('page');
		
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
			);
		$join = array(
			array('table'=>'kategoribesar b','field' => 'a.IdKategoriBesar = b.IdKategoriBesar','method'=>'Left'),
		);
		$where = array(
		);

		$result = $this->all_model->get_data("IdKategori as IdKategori, a.IdKategoriBesar as IdKategoriBesar,Jenis as Jenis, a.UpdateID as UpdateID,a.ActiveYN as ActiveYN,a.UpdateTime as UpdateTime,b.Keterangan as Keterangan", "kategori a",$join, $where, $search, false, $perPage, $segmen, false,"IdKategori","ASC");
		$result2 = $this->all_model->get_data("IdKategori as IdKategori, a.IdKategoriBesar as IdKategoriBesar,Jenis as Jenis, a.UpdateID as UpdateID,a.ActiveYN as ActiveYN,a.UpdateTime as UpdateTime,b.Keterangan as Keterangan", "kategori a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllKategori($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				
				foreach($result as $key => $value){	
				}
				
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getAllKategori($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				}
			}
			echo json_encode($result);
			exit();
		}
	}

	public function getKategoriBesar()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdKategoriBesar as IdKategoriBesar,Keterangan as Keterangan", "kategoribesar a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		
		$data['title']="CIPS - Master Kategori";
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['page_title']="CIPS - Master Kategori";
		$data2['data'] = json_encode($this->getAllKategori($config['per_page'], $config['segmen'],false));
		$data2['data2'] = json_encode($this->getKategoriBesar());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('kategori_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


