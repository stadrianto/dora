<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Viewpitching extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		$this->load->model('all_model2');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	
	public function antiinjection($data)
	{
		$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter_sql;
	}
	
		public function getBenefit($notransaksi)
	{
		$search = array(
		
			);
		$join = array(
			array('table'=>'m_creativeitem b','field' => 'a.TIBD_CreativeItem = b.MCI_KodeItem','method'=>'Left'),
		);
		$where = array(
				'a.TIBD_NoTransaksi' => $notransaksi,
		);
			
		$result = $this->all_model->get_data("TIBD_NoTransaksi as NoTransaksi,TIBD_CreativeItem as KodeItem,TIBD_Qty as Qty,MCI_NamaItem as NamaItem,MRC_Harga as Harga","t_iklanmkt_benefit_detail a",$join, $where, $search);
		foreach($result as $key => $value){
			$result[$key]["NamaItem"] = str_replace("\"", "@@@", $result[$key]["NamaItem"]);
			$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);

		
		}
		if($result)
		return $result;
	}
	
	public function getProduk($notransaksi)
	{
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.TIPD_NoTransaksi' => $notransaksi,
		);
			
		$result = $this->all_model->get_data("TIPD_KodeProduk as KodeProduk,Itm_NamaProduk as NamaProduk,Itm_DivNama as NamaDivisi,TIPD_Budget as Budget","t_iklanmkt_produk_detail a",$join, $where, $search);
	
		foreach($result as $key => $value){
			$result[$key]['Budget'] = $this->all_model->rp($result[$key]['Budget']);
		}
		if($result)
		return $result;
	}
		
	public function getDetailIklan($notransaksi)
	{
			 
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.TIH_AktifYN'	=>"Y",
				'a.TIH_NoTransaksi'	=> $notransaksi,
		);
			
			
		$result = $this->all_model->get_data("TIH_NoTransaksi as NoTransaksi,MHA_NamaAcara as ProgramAcara,MJP_NamaPlacement as Placement,TIH_BudgetTotal as BudgetTotal,TIH_StartTayang as StartTayang,TIH_EndTayang as EndTayang,TIH_Episode as Episode", "t_iklanmkt_header a",$join, $where, $search, false);
		
		if($result)
		{
			$result2 = $this->getProduk($result[0]["NoTransaksi"]);
			$result3 = $this->getBenefit($result[0]["NoTransaksi"]);
			$result[0]['BudgetTotal'] = $this->all_model->rp($result[0]['BudgetTotal']);
			$dt = strtotime($result[0]['StartTayang']);
			$result[0]['StartTayang'] = date("d-M-Y",$dt);
			$dt2 = strtotime($result[0]['EndTayang']);
			$result[0]['EndTayang'] = date("d-M-Y",$dt2);
			
			$result[0]['Produk'] = $result2;
			$result[0]['Benefit'] = $result3;
		}
		return $result;
	
	}
	
	
	public function checkTransaksi($notransaksi)
	{
		$result = $this->all_model->query_data("SELECT * FROM  t_iklanmkt_header WHERE TIH_NoTransaksi = '$notransaksi'", true);	
		if($result) return true;
		else return false;
	}
	
	public function checkAgenPitching($notransaksi)
	{
		$userid = $this->session->userdata("UserId");
		if($this->checkTransaksi($notransaksi))
		{
			$result = $this->all_model->query_data("SELECT * FROM  t_pitching_header WHERE TPH_KodeAgency = '$userid' and TPH_NoTransaksi='".$notransaksi."'", true);	
			if($result){
								$newdata = array(
									'NoPitching'		=> $result["TPH_NoPitching"],
							   );
							 
						$this->session->set_userdata($newdata);
				return true;
			}
			else
			{
				$data = array(
					'TPH_NoTransaksi' => $notransaksi,
					'TPH_KodeAgency'	=> $userid,
					'TPH_AktifYN' 	=> 'Y',
					'TPH_UpdateID' 	=> $userid,
					'TPH_UpdateTime' => date("Y-m-d H:i:s"),	
					'TPH_Flag' => "1",
				);
				if($notransaksi != 0)
				$query = $this->all_model->insert_data("t_pitching_header", $data );
				
				if($query){
				
					$result = $this->all_model->query_data("SELECT TPH_NoPitching FROM  t_pitching_header WHERE TPH_KodeAgency = '$userid' and TPH_NoTransaksi='".$notransaksi."'", true);	
					
						$newdata = array(
									'NoPitching'		=> $result["TPH_NoPitching"],
							   );
							 
						$this->session->set_userdata($newdata);

					return true;
				}
			
			}
		}
		else
		{
			redirect(base_url(). "home","refresh");
		}
	}	
	
	public function checkItem(){
		$iduser = $this->session->userdata("UserId");
		$noitem = $this->input->post('noitem');
		$result = $this->all_model->query_data("SELECT TPCD_CreativeItem FROM t_pitching_creative_detail WHERE TPCD_KodeAgency = '".$iduser."' and TPCD_CreativeItem='".$noitem."'", true);
		
		if($result)
		{
			echo "true";
		}
		else echo "false";

		exit();
	}
	
	
	public function deleteCreative()
	{
		$kodeitem = $this->input->post('noitem');
		$where = array('TPCD_CreativeItem'=>$kodeitem,'TPCD_KodeAgency'=>$this->session->userdata("UserId"));
		$query = $this->all_model->delete_data("t_pitching_creative_detail", $where);
		if($query){
			$data2["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data2["msg"] = $calculate;	
		}
		else 
		{
			$data2["status"] = "gagal";
		}
			
		echo json_encode($data2);
		exit();
	
	}
	
	public function insertCreative()
	{	
		
		$bonus = $this->input->post('bonus');
		$data = array();
		if($bonus == "no")
		{		
			$data = array(
				'TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'TPCD_CreativeItem'	=> $this->input->post('noitem'),
				'TPCD_KodeAgency' 	=> $this->session->userdata("UserId"),
				'TPCD_Qty' 	=> $this->input->post('spot'),
				'TPCD_Harga' => $this->input->post('harga'),	
				'TPCD_BonusYN' => "N",
				'TPCD_JmlEpisode' => $this->input->post('episode'),
				'TPCD_UpdateID' => $this->session->userdata("UserId"),
				'TPCD_UpdateTime' => date("Y-m-d H:i:s"),
			);
		}
		else if($bonus == "bonus")
		{
			$data = array(
				'TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'TPCD_CreativeItem'	=> $this->input->post('noitem'),
				'TPCD_KodeAgency' 	=> $this->session->userdata("UserId"),
				'TPCD_Qty' 	=> $this->input->post('spot'),
				'TPCD_Harga' => $this->input->post('harga'),	
				'TPCD_BonusYN' => "Y",
				'TPCD_JmlEpisode' => $this->input->post('jmlepisode'),
				'TPCD_KodeAcara' => $this->input->post('noacara'),
				'TPCD_UpdateID' => $this->session->userdata("UserId"),
				'TPCD_UpdateTime' => date("Y-m-d H:i:s"),
			);		
		}
		else
		{
			$data = array(
				'TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'TPCD_CreativeItem'	=> $this->input->post('noitem'),
				'TPCD_KodeAgency' 	=> $this->session->userdata("UserId"),
				'TPCD_Qty' 	=> $this->input->post('spot'),
				'TPCD_Harga' => $this->input->post('harga'),	
				'TPCD_BonusYN' => "N",
				'TPCD_JmlEpisode' => $this->input->post('jmlepisode'),
				'TPCD_KodeAcara' => $this->input->post('noacara'),
				'TPCD_UpdateID' => $this->session->userdata("UserId"),
				'TPCD_UpdateTime' => date("Y-m-d H:i:s"),
			);	
		}
		$query = $this->all_model->insert_data("t_pitching_creative_detail", $data );
		if($query){
			$data2["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data2["msg"] = $calculate;	
		}
		else 
		{
			$data2["status"] = "gagal";
		}
			
		echo json_encode($data2);
		exit();
	
	}
	
	public function getCalculate()
	{
		$total = 0;
		$floater = 0;
		$package = 0;
		$join = array(
			
		);
		$where = array(
				'a.TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'a.TPCD_KodeAgency' => $this->session->userdata("UserId"),
		);	
		$search = array(
	
		);
		$result = $this->all_model->get_data("TPCD_NoPitching as NoPitching,TPCD_CreativeItem as KodeItem,TPCD_Qty as Qty,TPCD_Harga as Harga,TPCD_BonusYN as Bonus,TPCD_JmlEpisode as JmlEpisode","t_pitching_creative_detail a",$join, $where, $search);
		if($result)
		{
			foreach($result as $key => $value){
				$result[$key]['TotalSpot'] = $result[$key]['Qty']*$result[$key]['JmlEpisode'];
				if($result[$key]['Bonus'] == "N")
				$total += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				else
				$floater += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
		
			}
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
			);
			return $newdata;
		}
		else
		{
			$newdata = array(
				'TotalBenefit'		=> 0,
				'TotalFloater'		=> 0,
				'TotalPackage'		=> 0,
			);
			return $newdata;
		}
	}
	
	public function getPitching($notransaksi)
	{
		$total = 0;
		$floater = 0;
		$package = 0;
		$search = array(
		
			);
		$join = array(
			array('table'=>'m_creativeitem b','field' => 'a.TPCD_CreativeItem = b.MCI_KodeItem','method'=>'Left'),array('table'=>'mh_acara c','field' => 'a.TPCD_KodeAcara=c.MHA_KodeAcara','method'=>'Left')
		);
		$where = array(
				'a.TPCD_NoPitching' => $this->session->userdata("NoPitching"),
				'a.TPCD_KodeAgency' => $this->session->userdata("UserId"),
		);
			
		$result = $this->all_model->get_data("TPCD_NoPitching as NoPitching,TPCD_CreativeItem as KodeItem,TPCD_Qty as Qty,MCI_NamaItem as NamaItem,TPCD_Harga as Harga,TPCD_BonusYN as Bonus,TPCD_KodeAcara as KodeAcara,TPCD_JmlEpisode as JmlEpisode,MHA_NamaAcara as NamaAcara","t_pitching_creative_detail a",$join, $where, $search);

		$array_items = array('TotalFloater'=>'', 'TotalBenefit' => '','TotalPackage' => '');
		$this->session->unset_userdata($array_items);
		
		
		if($result)
		{
			foreach($result as $key => $value){
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				$result[$key]['TotalSpot'] = $result[$key]['Qty']*$result[$key]['JmlEpisode'];
				if($result[$key]['Bonus'] == "N")
				$total += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				else
				$floater += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				
				$result[$key]['Value'] = $this->all_model->rp(($result[$key]['Harga']*$result[$key]['TotalSpot']));
				$result[$key]['Harga'] = $this->all_model->rp($result[$key]['Harga']);
				if($result[$key]["KodeAcara"] == 0) $result[$key]["KodeAcara"] = "";
				
			}
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
			);
	 
			$this->session->set_userdata($newdata);
			
			return $result;
		}
		else
		{
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
			);
	 
			$this->session->set_userdata($newdata);
			
			return "No Data";
		}
	
	}
	
	public function getItemPitching()
	{
		$notransaksi = $this->session->userdata("NoTransaksi");
		if($this->checkAgenPitching($notransaksi))
		{
			$result = $this->getPitching($notransaksi);
			if($result != "No Data")
			echo json_encode($result);
			else echo json_encode(array());
		}
	}
	

	
	public function pitchingiklan($notransaksi)
	{
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="Pitching";
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['page_title']="Media Planning Pharos";
		$data2['data'] = json_encode($this->getDetailIklan($notransaksi));
		$newdata = array(
			'NoTransaksi'		=> $notransaksi,
	   );
	 
		$this->session->set_userdata($newdata);
		
		if($this->checkAgenPitching($notransaksi))
		{
			$data2['data2'] = json_encode($this->getPitching($notransaksi));
		}
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('pitching_view',$data2);
		$this->load->view('home_footer');
	}

	public function getCalculatePitching($nopitching)
	{
		$total = 0;
		$floater = 0;
		$package = 0;
		$count = 0;
		$join = array(
			
		);
		$where = array(
				'a.TPCD_NoPitching' => $nopitching,
				'a.TPCD_KodeAgency' => $this->session->userdata("UserId"),
		);	
		$search = array(
	
		);
		$result = $this->all_model->get_data("TPCD_NoPitching as NoPitching,TPCD_CreativeItem as KodeItem,TPCD_Qty as Qty,TPCD_Harga as Harga,TPCD_BonusYN as Bonus,TPCD_JmlEpisode as JmlEpisode","t_pitching_creative_detail a",$join, $where, $search);
		if($result)
		{
			foreach($result as $key => $value){
				$count++;
				$result[$key]['TotalSpot'] = $result[$key]['Qty'];
				if($result[$key]['Bonus'] == "N")
				$total += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
				else
				$floater += ($result[$key]['Harga']*$result[$key]['TotalSpot']);
		
			}
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
				'TotalItemPitching' => $count
			);
			return $newdata;
		}
		else
		{
			$package = $total+$floater;
			$total = $this->all_model->rp($total);
			$floater = $this->all_model->rp($floater);
			$package = $this->all_model->rp($package);
			$newdata = array(
				'TotalBenefit'		=> $total,
				'TotalFloater'		=> $floater,
				'TotalPackage'		=> $package,
				'TotalItemPitching' => $count
			);
			return $newdata;
		}
	}
	
	
	public function getDataPitching($perPage=5, $segmen=0, $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
		'TPH_NoPitching' => $this->input->post('search')
			);
		$join = array(
			array('table'=>'t_iklanmkt_header b','field' => 'a.TPH_NoTransaksi = b.TIH_NoTransaksi ','method'=>'Left')
		);
		$where = array(
				'a.TPH_AktifYN'	=>"Y",
				'a.TPH_UpdateID' => $this->session->userdata("UserId"),
		);

		$result = $this->all_model->get_data("TPH_NoPitching as NoPitching,TIH_NoTransaksi as NoTransaksi,MHA_NamaAcara as ProgramAcara,MJP_NamaPlacement as Placement,TIH_BudgetTotal as BudgetTotal,TIH_StartTayang as StartTayang,TIH_EndTayang as EndTayang,TIH_Episode as Episode", "  t_pitching_header a",$join, $where, $search, false, $perPage, $segmen, false,"TPH_NoPitching","DESC");
		$result2 = $this->all_model->get_data("TPH_NoPitching as NoPitching,TIH_NoTransaksi as NoTransaksi,MHA_NamaAcara as ProgramAcara,MJP_NamaPlacement as Placement,TIH_BudgetTotal as BudgetTotal,TIH_StartTayang as StartTayang,TIH_EndTayang as EndTayang,TIH_Episode as Episode", "t_pitching_header a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataPitching($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				
				$result[$key]['BudgetTotal'] = $this->all_model->rp($result[$key]['BudgetTotal']);
				$dt = strtotime($result[$key]['StartTayang']);
				$result[$key]['StartTayang'] = date("d-M-Y",$dt);
				$dt2 = strtotime($result[$key]['EndTayang']);
				$result[$key]['EndTayang'] = date("d-M-Y",$dt2);
				$total = $this->getCalculatePitching($result[$key]['NoPitching']);
				$result[$key]['TotalBenefit'] = $total['TotalBenefit'];
				$result[$key]['TotalFloater'] = $total['TotalFloater'];
				$result[$key]['TotalPackage'] = $total['TotalPackage'];
				$result[$key]['TotalItemPitching'] = $total['TotalItemPitching'];
				}		
		
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataPitching($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					$result[$key]['BudgetTotal'] = $this->all_model->rp($result[$key]['BudgetTotal']);
					$dt = strtotime($result[$key]['StartTayang']);
					$result[$key]['StartTayang'] = date("d-M-Y",$dt);
					$dt2 = strtotime($result[$key]['EndTayang']);
					$result[$key]['EndTayang'] = date("d-M-Y",$dt2);
					$total = $this->getCalculatePitching($result[$key]['NoPitching']);
					$result[$key]['TotalBenefit'] = $total['TotalBenefit'];
					$result[$key]['TotalFloater'] = $total['TotalFloater'];
					$result[$key]['TotalPackage'] = $total['TotalPackage'];
					$result[$key]['TotalItemPitching'] = $total['TotalItemPitching'];
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="Pitching";
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['page_title']="Media Planning Pharos";
		//$data2['data'] = json_encode($this->getDetailIklan($notransaksi));
		//$data2['data2'] = json_encode($this->getTempatTest());
		$data2['data'] = json_encode($this->getDataPitching($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('viewpitching_view',$data2);
		$this->load->view('home_footer');
	}
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


