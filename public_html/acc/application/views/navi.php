<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin Panel</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul id="active" class="nav navbar-nav side-nav">
                    <li class="selected"><a href="<?php echo base_url()?>home"><i class="fa fa-bullseye"></i> Home</a></li>
                    <li><a href="<?php echo base_url()?>customer"><i class="fa fa-tasks"></i> Master Customer</a></li>
                    <li><a href="<?php echo base_url()?>supplier"><i class="fa fa-globe"></i> Master Supplier</a></li>
                    <li><a href="<?php echo base_url()?>barang"><i class="fa fa-list-ol"></i> Master Barang</a></li>
                    <li><a href="<?php echo base_url()?>akun"><i class="fa fa-font"></i> Chart of Account</a></li>
                    <li><a href="<?php echo base_url()?>jurnal"><i class="fa fa-font"></i> Jurnal</a></li>
                    <li><a href="<?php echo base_url()?>pembelian"><i class="fa fa-list-ol"></i> Pembelian</a></li>
                    <li><a href="<?php echo base_url()?>penjualan"><i class="fa fa-font"></i> Penjualan</a></li>
					<li><a href="<?php echo base_url()?>expense"><i class="fa fa-font"></i> Master Expense</a></li>
                    <li><a href="<?php echo base_url()?>pengeluaran"><i class="fa fa-list-ul"></i> Expenditure</a></li>
                    <li><a href="<?php echo base_url()?>profitnloss"><i class="fa fa-table"></i> Profit and Loss</a></li>
					<li><a href="<?php echo base_url()?>neraca"><i class="fa fa-list-ol"></i> Balance Sheet</a></li>
                </ul>
				<ul class="nav navbar-nav navbar-right navbar-user">                    
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?=$nama?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url()?>login/doLogout"><i class="fa fa-power-off"></i> Log Out</a></li>

                        </ul>
                    </li>                    
                </ul>
            </div>
        </nav>