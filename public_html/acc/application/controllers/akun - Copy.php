<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class akun extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function getKelompokGL()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdKelompokGL as IdKelompokGL,NamaGL as NamaGL,IdKelompokAkun as IdKelompokAkun", "kelompokGL a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getAkunCode(){
		$result = $this->all_model->query_data("SELECT NoAkun FROM tabelakun ORDER BY NoAkun DESC", true);
		$result["NoAkun"] = $result["NoAkun"]+1;
		if($result["NoAkun"] < 10)
			$result["NoAkun"] = "00".$result["NoAkun"];
		else if($result["NoAkun"]< 100)
			$result["NoAkun"] = "0".$result["NoAkun"];
		else
			$result["NoAkun"] = "".$result["NoAkun"];
		return $result["NoAkun"];
	}
	
	public function getData($perPage=15, $segmen=0,  $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;

			$search = array(
					'a.NoAkun' => $this->input->post('search')
				);
			$where = array(
			);
		$join = array(
			array('table'=>'kelompokGL b','field' => 'a.IdKelompokGL = b.IdKelompokGL','method'=>'Left'),
			array('table'=>'kelompokakun c','field' => 'a.IdKelompokAkun = c.IdKelompokAkun','method'=>'Left'),
		);
			
		$result = $this->all_model->get_data("NoAkun as NoAkun, a.IdKelompokGL as IdKelompokGL, NamaKelompokAkun as NamaKelompokAkun, NamaAkun as NamaAkun, SaldoAwal as SaldoAwal, b.NamaGL as kelompokGL","tabelakun a",$join, $where, $search, false, $perPage, $segmen, false,"NoAkun","ASC");
		$result2 = $this->all_model->get_data("NoAkun as NoAkun, a.IdKelompokGL as IdKelompokGL, NamaKelompokAkun as NamaKelompokAkun, NamaAkun as NamaAkun, SaldoAwal as SaldoAwal, b.NamaGL as kelompokGL","tabelakun a", $join, $where, $search, false);
	

		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
	
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertAkun(){
		
		$data = array(
		'NoAkun'  => $this->input->post('NoAkun'),
		'NamaAkun' => $this->input->post('NamaAkun'),
		'SaldoAwal' => $this->input->post('SaldoAwal'),
		'IdKelompokAkun' => $this->input->post('IdKelompokAkun'),
		'IdKelompokGL' => $this->input->post('IdKelompokGL'),
		'ActiveYN' => 'Y',
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("tabelakun", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function editAkun(){
		$data = array(
		'NamaAkun' => $this->input->post('NamaAkun'),
		'SaldoAwal' => $this->input->post('SaldoAwal'),
		'IdKelompokGL' => $this->input->post('IdKelompokGL'),
		'ActiveYN' => 'Y',
		//'UpdateId' => $this->session->userdata("UserId"),
		//'UpdateTime' => date("Y-m-d H:i:s")			
			);
		$where = array(	
					'NoAkun'			=> $this->input->post('NoAkun'),
				
		);	
		$query = $this->all_model->update_data("tabelakun", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function cekJurnal($NoAkun)
	{
		$search = array();
		$join = array();
		$where = array('a.NoAkun'=>$NoAkun);		
		$result = $this->all_model->get_data("NoJurnal as NoJurnal", "detailjurnal a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function deleteAkun()
	{
		
		$NoAkun = $this->input->post('NoAkun');
		$cek = json_encode($this->cekJurnal($NoAkun));
		$data = array('ActiveYN' => 'N');
		$where = array('NoAkun'=>$NoAkun);
		$query = $this->all_model->update_data("tabelakun", $data ,$where);
		//$query = $this->all_model->delete_data("tabelakun", $where);
		echo json_encode($query);	
		exit();
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['data3'] = json_encode($this->getkelompokGL());
		$data['page_title']="CIPS - Chart of Account";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('akun_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function addAkun(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['data3'] = json_encode($this->getkelompokGL());
		$data['page_title']="CIPS - Tambah Akun";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('addakun_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

