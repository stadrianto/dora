<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class laporanbar extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function getHeaderBarang()
	{	
		$result = $this->all_model->query_data("(Select a.IdBarang,a.Nama FROM barang a INNER JOIN detailbarangmasuk b on a.IdBarang = b.IdBarang)
												UNION
												(Select a.IdBarang,a.Nama FROM barang a INNER JOIN detailbarangkeluar b on a.IdBarang = b.IdBarang) ORDER BY IdBarang",false);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getNamaBarang($Head)
	{	
		$result = $this->all_model->query_data("(Select a.IdBarang,a.Nama FROM barang a INNER JOIN detailbarangmasuk b on a.IdBarang = b.IdBarang WHERE a.IdBarang Like '".$Head."')
												UNION
												(Select a.IdBarang,a.Nama FROM barang a INNER JOIN detailbarangkeluar b on a.IdBarang = b.IdBarang WHERE a.IdBarang Like '".$Head."') ORDER BY IdBarang",false);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	
	public function getPrintBarangMasuk($Periode,$Periode2,$Heads)
	{
		$result = $this->all_model->query_data("(SELECT b.TanggalMasuk as Tanggal,a.IdBarang,Nama,Quantity,Jumlah,b.Keterangan, c.SaldoQty, c.SaldoJumlah, (c.SaldoJumlah/c.SaldoQty) as Rata, c.Tag as Tag  FROM detailbarangmasuk a JOIN barangmasuk b ON a.IdBarangMasuk = b.IdBarangMasuk JOIN historybar c ON a.IdDetailBarangMasuk = c.IdDetailBarangMasuk WHERE a.Status = 'N' AND a.IdBarang Like '".$Heads."' AND Tanggal >= '".$Periode."' AND Tanggal <= '".$Periode2."')
												UNION
												(SELECT b.TanggalKeluar as Tanggal,a.IdBarang,Nama,Quantity,Jumlah,b.Keterangan, c.SaldoQty, c.SaldoJumlah,(c.SaldoJumlah/c.SaldoQty) as Rata, c.Tag as Tag  FROM detailbarangkeluar a JOIN barangkeluar b ON a.IdBarangKeluar = b.IdBarangKeluar JOIN historybar c ON a.IdDetailBarangKeluar = c.IdDetailBarangKeluar WHERE a.Status = 'N' AND a.IdBarang Like '".$Heads."' AND Tanggal >= '".$Periode."' AND Tanggal <= '".$Periode2."')  ORDER BY IdBarang,Tanggal", false);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getPrintSimple()
	{
		$result = $this->all_model->query_data("SELECT IdBarang, Nama, Quantity, Jumlah, (Jumlah/Quantity) as Rata FROM barang ORDER BY IdBarang",false);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getDataPrinter($id)
	{
		$result = $this->all_model->query_data("SELECT Nama as Nama from msuser where iduser=".$id, false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	
	public function printLaporanBarang(){
	
		$Head=$this->input->post('NamaBarang');
		if($Head == "")
		{
			$Head = "%";
		}
		$Periode = $this->input->post('dob2');
		$Periode2 = $this->input->post('dob');
		$jenis = $this->input->post('jenis');
		$Periode=substr($Periode,6,10)."-".substr($Periode,0,2)."-".substr($Periode,3,2);
		$Periode2= substr($Periode2,6,10)."-".substr($Periode2,0,2)."-".substr($Periode2,3,2);
		$result2 = $this->getNamaBarang($Head);
		$this->load->library('fpdf17/fpdf');
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		
		if($jenis == "History")
		{
			for($x=0; $x<Count($result2);$x++ )
			{
				$this->fpdf->AddPage('L');
				$result = $this->getPrintBarangMasuk($Periode,$Periode2,$result2[$x]["IdBarang"]);
				$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Image('images/cips_nama.png',130,20,50,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Ln(20);
				$this->fpdf->SetX(142);
				$this->fpdf->SetFont('Arial','BU',15);
				$this->fpdf->Cell(30,6,'Laporan Barang',0,0,'C');
				$this->fpdf->Ln();
				$this->fpdf->SetX(135);
				$this->fpdf->Cell(40,6,'Untuk Periode '.$Periode.' Hingga '.$Periode2.'',0,0,'C');
				$this->fpdf->Ln(10);
				
				$this->fpdf->SetFont('Arial','B',12);
				$this->fpdf->SetY(50);
				$this->fpdf->SetX(10);
				$this->fpdf->Cell(30,6,$result2[$x]["IdBarang"],0,0,'L',0);
				$this->fpdf->Cell(40,6,$result2[$x]["Nama"],0,0,'L',0);
				
				$this->fpdf->SetY(50);
				$this->fpdf->SetX(230);
				$this->fpdf->Cell(40,6,'Saldo Awal :',0,0,'C',0);
				
				$this->fpdf->SetY(175);
				$this->fpdf->SetX(230);
				$this->fpdf->Cell(40,6,'Saldo Akhir :',0,0,'C',0);
				
				//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
				$this->fpdf->Ln(10);

				$y_axis_initial = 60;
				$this->fpdf->SetFont('Arial','',10);
				$this->fpdf->setFillColor(222,222,222);
				$this->fpdf->SetY($y_axis_initial);
				$this->fpdf->SetX(10);
				//Header tabel halaman 1
				$this->fpdf->CELL(10,6,'No',1,0,'C',1);
				$this->fpdf->Cell(40,6,'Tanggal',1,0,'C',1);
				$this->fpdf->Cell(70,6,'Keterangan',1,0,'C',1);
				$this->fpdf->Cell(25,6,'Quantity',1,0,'C',1);
				$this->fpdf->Cell(25,6,'Jumlah',1,0,'C',1);
				$this->fpdf->Cell(25,6,'SaldoQty',1,0,'C',1);
				$this->fpdf->Cell(35,6,'SaldoJumlah',1,0,'C',1);
				$this->fpdf->Cell(30,6,'Harga Rata-Rata',1,0,'C',1);
				$this->fpdf->Cell(20,6,'Tag',1,0,'C',1);

				$this->fpdf->Ln();
				$max=15;//max baris perhalaman
				$i=0;
				$no=0;
				$total=0;
				$hit=0;
				$row_height = 6;//tinggi tiap2 cell/baris
				$y_axis = $y_axis_initial + $row_height;
				$date = date("Y-m-d");
				//$grandtotal = 0;
				if($result != "No Data")
				{
				foreach($result as $key => $value){
					
				if($key == 0)
				{
					$hit = intval($result[0]["SaldoJumlah"])-intval($result[0]["Jumlah"]);
				}
				
				//$total += $row['Total'];

				if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
				$this->fpdf->AddPage('L');
				$this->fpdf->SetY(10);
				$this->fpdf->SetX(10);
				$this->fpdf->CELL(10,6,'No',1,0,'C',1);
				$this->fpdf->Cell(40,6,'Tanggal',1,0,'C',1);
				$this->fpdf->Cell(70,6,'Keterangan',1,0,'C',1);
				$this->fpdf->Cell(25,6,'Quantity',1,0,'C',1);
				$this->fpdf->Cell(25,6,'Jumlah',1,0,'C',1);
				$this->fpdf->Cell(25,6,'SaldoQty',1,0,'C',1);
				$this->fpdf->Cell(35,6,'SaldoJumlah',1,0,'C',1);
				$this->fpdf->Cell(30,6,'Harga Rata-Rata',1,0,'C',1);
				$this->fpdf->Cell(20,6,'Tag',1,0,'C',1);

				$this->fpdf->SetY(10);
				$this->fpdf->SetX(55);
				$y_axis = $y_axis + $row_height;
				$i=0;
				$this->fpdf->Ln();

				}
				
				
				//$grandtotal+=$row['Total'];
				$i++;
				$no++;
				
				$this->fpdf->SetX(10);
				$this->fpdf->Cell(10,6,$no,1,0,'C',0);
				$this->fpdf->Cell(40,6,$result[$key]["Tanggal"],1,0,'C',0);
				$this->fpdf->Cell(70,6,$result[$key]["Keterangan"],1,0,'C',0);
				$this->fpdf->Cell(25,6,$result[$key]["Quantity"],1,0,'C',0);
				$this->fpdf->Cell(25,6,$result[$key]["Jumlah"],1,0,'C',0);
				$this->fpdf->Cell(25,6,$result[$key]["SaldoQty"],1,0,'C',0);
				$this->fpdf->Cell(35,6,$result[$key]["SaldoJumlah"],1,0,'C',0);
				$this->fpdf->Cell(30,6,intval($result[$key]["Rata"]),1,0,'C',0);
				$this->fpdf->Cell(20,6,$result[$key]["Tag"],1,0,'C',0);

				$this->fpdf->Ln();

				}

					$this->fpdf->SetFont('Arial','B',12);
					$this->fpdf->SetY(50);
					$this->fpdf->SetX(270);
					$this->fpdf->Cell(20,6,$hit,0,0,'L',0);
					
					$this->fpdf->SetY(175);
					$this->fpdf->SetX(270);
					$this->fpdf->Cell(20,6,$result[$key]["SaldoJumlah"],0,0,'L',0);
					
				}
				
				
			}
			
				//buat footer
				$now = date("d F Y H:i:s");
				
				$this->fpdf->Ln();
				$this->fpdf->SetFont('Arial','B',10);
				$printer=$this->getDataPrinter($this->session->userdata("UserId"));
				$this->fpdf->SetY(190);
				$this->fpdf->SetX(10);			
				$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'L');
				$this->fpdf->SetX(100);		
				$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
				$this->fpdf->Ln();
				$this->fpdf->Output('Laporan Laba Rugi'.date("F Y").'.pdf', 'I');
		
		}
		else if($jenis == "Simple")
		{
			$result = $this->getPrintSimple();
			
			$this->load->library('fpdf17/fpdf');
			
			//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
			$this->fpdf->FPDF('P','mm','A4');
			$this->fpdf->Open();
			$this->fpdf->SetAutoPageBreak(false);
			$this->fpdf->AddPage('');
			$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
			$this->fpdf->Image('images/cips_nama.png',90,20,50,0,'','http://www.cips.or.id/'); 
			$this->fpdf->Ln(20);
			$this->fpdf->SetX(95);
			$this->fpdf->SetFont('Arial','BU',15);
			$this->fpdf->Cell(40,6,'Laporan Barang',0,0,'C');
			$this->fpdf->Ln();
			$this->fpdf->SetX(95);
			$this->fpdf->Cell(40,6,'Untuk Periode '.$Periode.' Hingga '.$Periode2.'',0,0,'C');
			$this->fpdf->Ln(10);
			$this->fpdf->SetFont('Arial','BU',12);
			//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
			$this->fpdf->Ln(10);

			$y_axis_initial = 60;
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->SetY($y_axis_initial);
			$this->fpdf->SetX(10);
			//Header tabel halaman 1
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(25,6,'IdBarang',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Nama',1,0,'C',1);
			$this->fpdf->Cell(20,6,'SaldoQty',1,0,'C',1);
			$this->fpdf->Cell(30,6,'SaldoJumlah',1,0,'C',1);
			$this->fpdf->Cell(40,6,'Harga Rata-Rata',1,0,'C',1);
			$this->fpdf->Cell(20,6,'Opname',1,0,'C',1);
			$this->fpdf->Cell(20,6,'Sisa',1,0,'C',1);

			$this->fpdf->Ln();
			$max=25;//max baris perhalaman
			$i=0;
			$no=0;
			$total=0;
			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			//$grandtotal = 0;
			if($result != "No Data")
			{
			foreach($result as $key => $value){
			$i++;
			//$total += $row['Total'];

			if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
			$this->fpdf->AddPage();
			$this->fpdf->SetY(10);
			$this->fpdf->SetX(10);
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(25,6,'IdBarang',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Nama',1,0,'C',1);
			$this->fpdf->Cell(20,6,'SaldoQty',1,0,'C',1);
			$this->fpdf->Cell(30,6,'SaldoJumlah',1,0,'C',1);
			$this->fpdf->Cell(40,6,'Harga Rata-Rata',1,0,'C',1);
			$this->fpdf->Cell(20,6,'Opname',1,0,'C',1);
			$this->fpdf->Cell(20,6,'Sisa',1,0,'C',1);

			$this->fpdf->SetY(10);
			$this->fpdf->SetX(55);
			$y_axis = $y_axis + $row_height;
			$i=0;
			$this->fpdf->Ln();

			}

			//$grandtotal+=$row['Total'];
			$i++;
			$no++;
			$this->fpdf->SetX(10);
			$this->fpdf->Cell(10,6,$no,1,0,'C',0);
			$this->fpdf->Cell(25,6,$result[$key]["IdBarang"],1,0,'C',0);
			$this->fpdf->Cell(25,6,$result[$key]["Nama"],1,0,'C',0);
			$this->fpdf->Cell(20,6,$result[$key]["Quantity"],1,0,'C',0);
			$this->fpdf->Cell(30,6,$result[$key]["Jumlah"],1,0,'C',0);
			$this->fpdf->Cell(40,6,intval($result[$key]["Rata"]),1,0,'C',0);
			$this->fpdf->Cell(20,6,'',1,0,'C',0);
			$this->fpdf->Cell(20,6,'',1,0,'C',0);
			
			$this->fpdf->Ln();

			}
			}
			
			//buat footer
			$now = date("d F Y H:i:s");
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont('Arial','B',10);
			$printer=$this->getDataPrinter($this->session->userdata("UserId"));
			$this->fpdf->SetY(270);
			$this->fpdf->SetX(20);			
			$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'L');
			$this->fpdf->SetX(30);		
			$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
			$this->fpdf->Ln();
			$this->fpdf->Output('Laporan Laba Rugi'.date("F Y").'.pdf', 'I');
			
		}

	
		
		
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		
		$data['title']="CIPS - Laporan Barang";
		$data['data4'] = json_encode($this->getHeaderBarang());
		$data['page_title']="CIPS - Laporan Barang";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('laporanbar_view','');
		$this->load->view('home_footer');
	}
		
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

