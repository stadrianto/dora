<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($request = true){
		$search = array();
		$where = array();
		$join = array(
			array('table'=>'msRole b','field' => 'a.RoleID = b.RoleID','method'=>'Left'),
			array('table'=>'msbranch c','field' => 'a.BranchId = c.BranchId','method'=>'Left')
			);
		$result2 = $this->all_model->get_data("Username, EmployeeId, EmployeeName, RoleName, BranchName", "msemployee a",$join, $where, $search, false,true);
		return $result2;	
		exit();
	}

	public function ajax_finder(){
			$msg = array();
			if ( $_POST ){
				$key = $this->input->post("key");
				if ( empty($key) ){
					$msg['type'] = 'failed';
					$msg['msg'] = "Key Empty.";
				}
				else{
					$join = array(
						array('table'=>'msRole b','field' => 'a.RoleId = b.roleid','method'=>'Left'),
						array('table'=>'msbranch c','field' => 'a.BranchId = c.BranchId','method'=>'Left')
					);
					$data = $this->all_model->view("msemployee a", "EmployeeId, EmployeeName, PhoneNumber, Address, Username, a.RoleId, a.BranchId, EntryDate, RoleName, BranchName", array("EmployeeId"=>$key), $join);
					if ( $data == '0' ){
						$msg['type'] = "failed";
						$msg['msg'] = "Data tidak ditemukan.";
					}
					else{
						//foreach($data as $a) xss_filter($a);
						$msg['type'] = "done";
						$msg['msg'] = $data;
					}
				}
			}
			else{
				$msg['type'] = 'failed';
				$msg['msg'] = "Parameter tidak ditemukan.";
			}
			//die (var_dump($msg));
			echo json_encode($msg);
		}


	public function getNewId($id){
		$result = $this->all_model->query_data("SELECT RIGHT(EmployeeId,3) as IdKaryawan FROM msemployee WHERE EmployeeId like '".$id."%' ORDER BY EmployeeId DESC", true);
		$result["IdKaryawan"] = $result["IdKaryawan"]+1;
		if($result["IdKaryawan"] < 10)
			$result["IdKaryawan"] = $id."00".$result["IdKaryawan"];
		else if($result["IdKaryawan"]< 100)
			$result["IdKaryawan"] = $id."0".$result["IdKaryawan"];
		else
			$result["IdKaryawan"] = $id.$result["IdKaryawan"];
		return $result["IdKaryawan"];
	}

	public function insert(){
		//die ("1");
		$check=$this->all_model->query_data("SELECT * FROM MSUser WHERE username = '".$this->input->post('Username')."'", true);
		if($check==null)
		{
			$data = array(
			'Username' => $this->input->post('Username'), //$this->input->post('Id')
			'PhoneNumber' => $this->input->post('Phone'),
			'EmployeeName' => $this->input->post('Name'),
			'Address' => $this->input->post('Address'),
			'RoleId' => $this->input->post('Role'),	
			'EntryDate' => $this->input->post('Date'),	
			'EmployeeId' => $this->getNewId($this->input->post('EmployeeId')),
			'BranchId' => $this->input->post('Branch'),
			'Password' => md5("1234")
			);

			//die (var_dump($data));
			$query = $this->all_model->insert_data("msemployee", $data );
			
			echo json_encode($query);
			exit();
		}
		else
		{
			$msg['type'] = "failed";
			$msg['error'] = "Duplicate UserId";
			echo json_encode($msg);
			exit();
		}
		
	}	
	
	public function edit(){
		$data = array(
			'Username' => $this->input->post('Username'), //$this->input->post('Id')
			'PhoneNumber' => $this->input->post('Phone'),
			'EmployeeName' => $this->input->post('Name'),
			'Address' => $this->input->post('Address'),
			'RoleId' => $this->input->post('Role'),	
			'EntryDate' => $this->input->post('Date'),
			'BranchId' => $this->input->post('Branch'),
			);
		$where = array(	
			'EmployeeId' => $this->input->post('EmployeeId'),
		);
		$query = $this->all_model->update_data("msemployee", $data ,$where);
		
		echo json_encode($query);
		exit();
	}
	
	public function delete(){
		$Id = $this->input->post('Id');
		$where = array('EmployeeId'=>$Id);
		$query = $this->all_model->delete_data("msemployee", $where);
		echo json_encode($query);
		exit();
	}
	
	public function getRole()
	{	
		
		$search = array();
		$join = array();
		$where = array();		
		$result = $this->all_model->get_data("RoleId, RoleName", "msRole a", $join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function getBranch()
	{	
		
		$search = array();
		$join = array();
		$where = array();		
		$result = $this->all_model->get_data("BranchId, BranchName", "msbranch a", $join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function getAccess()
	{	
		$page=$this->all_model->query_data("select pageid from mspage where pagename = '".$this->router->fetch_class()."'",true);
		//die($page["pageid"]);
		$result = $this->all_model->query_data("select flag from trprivillege where roleid=".$this->session->userdata('Role')." and PageName=".$page["pageid"]." and flag!=0",true);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function index(){
		if($this->session->userdata("Username")=="")
		redirect(base_url(). "login","refresh");
		/*if($this->session->userdata('Role')!="1")
		{
			$access=$this->getAccess();
			if(!is_array($access))
			{
				redirect(base_url(). "home","refresh");
			}
		}*/
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Master User";
		$data['page']="user_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['data'] = $this->getData(false);
		$data['kategori'] = json_encode($this->getRole());
		$data['branch'] = json_encode($this->getBranch());
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}