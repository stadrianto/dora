<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($request = true){
		$search = array();
		$where = array("Active"=>'Y');
		$join = array(
			);
		$result2 = $this->all_model->get_data("MenuId, MenuName", "msmenu a",$join, $where, $search, false,true);
		return $result2;	
		exit();
	}

	public function ajax_finder(){
		$msg = array();
		if ( $_POST ){
			$key = $this->input->post("key");
			if ( empty($key) ){
				$msg['type'] = 'failed';
				$msg['msg'] = "Key Empty.";
			}
			else{
				$join = array(
					array('table'=>'mscategory b','field' => 'a.Category = b.CategoryId','method'=>'Left')
					);
				$data = $this->all_model->view("msmenu a", "a.MenuId as MenuId, MenuName, Price, Category, CategoryName", array("MenuId"=>$key,'Active'=>'Y'), $join);
				if ( $data == '0' ){
					$msg['type'] = "failed";
					$msg['msg'] = "Data tidak ditemukan.";
				}
				else{
					//foreach($data as $a) xss_filter($a);
					$msg['type'] = "done";
					$msg['msg'] = $data;
				}
			}
		}
		else{
			$msg['type'] = 'failed';
			$msg['msg'] = "Parameter tidak ditemukan.";
		}
		//die (var_dump($msg));
		echo json_encode($msg);
	}

	public function insert(){
		//die($this->input->post('Recipe'));
		//$menu = json_decode($_POST["menu"]);
		
		//die($this->input->post('Name'));
		$data = array(
			'MenuName' => $this->input->post('Name'),
			'Category' => $this->input->post('Category'),
			'Price' => $this->input->post('Price')
		);
		$query = $this->db->insert("msmenu", $data );

		$insert_id = $this->db->insert_id();

		$recipe = json_decode($_POST["recipe"]);
		$myarray = $recipe->arry;
		for ($i=0;$i<count($myarray);$i++)
		{
			$batch[] = array(
				'MenuId' => $insert_id,
				'ItemId' => $myarray[$i][0],
				'Amount' => $myarray[$i][1]
				);
			
		}
		$this->db->insert_batch('msrecipe', $batch);
		//die (var_dump($data));
		
		
		echo json_encode($query);
		exit();
		
		
	}	
	public function getCategory()
	{	
		
		$search = array();
		$join = array();
		$where = array();		
		$result = $this->all_model->get_data("CategoryId, CategoryName", "mscategory a", $join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function getRecipe()
	{	
		$key = $this->input->post("key");

		$result = $this->all_model->query_data("select ItemId as ItemId, Amount as Amount from msrecipe where menuid=".$key, false);
		if(!$result){
			$result= "0";
		}
		echo json_encode($result);
		exit();
	}
	public function getItem()
	{	
		
		$search = array();
		$join = array();
		$where = array();		
		$result = $this->all_model->get_data("ItemId, ItemName", "msitem a", $join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	public function edit(){
		$data = array(
			'MenuName' => $this->input->post('Name'), //$this->input->post('Id')
			'Category' => $this->input->post('Category'),
			'Price' => $this->input->post('Price')
			
			);
		$where = array(	
			'MenuId' => $this->input->post('Id'),
		);
		$query = $this->all_model->update_data("msmenu", $data ,$where);

		$where = array('MenuId'=>$this->input->post('Id'));
		$query = $this->all_model->delete_data("msrecipe", $where);

		$recipe = json_decode($_POST["recipe"]);
		$myarray = $recipe->arry;
		for ($i=0;$i<count($myarray);$i++)
		{
			$batch[] = array(
				'MenuId' => $this->input->post('Id'),
				'ItemId' => $myarray[$i][0],
				'Amount' => $myarray[$i][1]
				);
			
		}
		$this->db->insert_batch('msrecipe', $batch);
		
		echo json_encode($query);
		exit();
	}
	
	public function delete(){
		$Id = $this->input->post('Id');
		$data = array(
			'Active' => 'N', //$this->input->post('Id')
			);
		$where = array('MenuId'=>$Id);
		$query = $this->all_model->update_data("msmenu", $data ,$where);
		echo json_encode($query);
		exit();
	}
	
	
	public function getAccess()
	{	
		$page=$this->all_model->query_data("select pageid from mspage where pagename = '".$this->router->fetch_class()."'",true);
		//die($page["pageid"]);
		$result = $this->all_model->query_data("select flag from trprivillege where roleid=".$this->session->userdata('Role')." and PageName=".$page["pageid"]." and flag!=0",true);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}

	public function index(){
		if($this->session->userdata("Username")=="")
		redirect(base_url(). "login","refresh");
		/*if($this->session->userdata('Role')!="1")
		{
			$access=$this->getAccess();
			if(!is_array($access))
			{
				redirect(base_url(). "home","refresh");
			}
		}*/
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="Master Menu";
		$data['page']="menu_view";
		$data['nama']=$this->session->userdata('Name');
		$data['kategori'] = json_encode($this->getCategory());
		$data['itemlist'] = json_encode($this->getItem());
		$data['data'] = $this->getData(false);
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}