<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class rekon extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function getKelompokAkun()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdKelompokAkun as IdKelompokAkun,NamaKelompokAkun as NamaKelompokAkun", "kelompokakun a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getAkunCode(){
		$result = $this->all_model->query_data("SELECT NoAkun FROM tabelakun ORDER BY NoAkun DESC", true);
		$result["NoAkun"] = $result["NoAkun"]+1;
		if($result["NoAkun"] < 10)
			$result["NoAkun"] = "00".$result["NoAkun"];
		else if($result["NoAkun"]< 100)
			$result["NoAkun"] = "0".$result["NoAkun"];
		else
			$result["NoAkun"] = "".$result["NoAkun"];
		return $result["NoAkun"];
	}
	
	public function getData2($NoAkun){
		$search = array();
		$join = array(
			array('table'=>'kelompokakun b','field' => 'a.idkelompokakun = b.idkelompokakun','method'=>'Left'),		
		);
		$where = array(
		
			'a.ActiveYN' => 'Y',
			'a.NoAkun' => $NoAkun
		);		
		$result = $this->all_model->get_data("NoAkun as NoAkun,NamaAkun as NamaAkun, SaldoAwal as SaldoAwal, IdNeraca as IdNeraca", "tabelakun a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$current = $this->input->post('page');
		$noakun=$this->input->post('AkunBank');
		$tanggal1=$this->input->post('dob');		
		$qtanggal1=substr($tanggal1,6,4).'-'.substr($tanggal1,0,2).'-'.substr($tanggal1,3,2);		
		$tanggal2=$this->input->post('dob1');		
		$qtanggal2=substr($tanggal2,6,4).'-'.substr($tanggal2,0,2).'-'.substr($tanggal2,3,2);
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;

			$search = array(
					//'a.NoAkun' => $this->input->post('search')
				);
			$where = array(
				'a.IdJenisJurnal'=> '1',
				'b.NoAkun' => $noakun,
				'a.TanggalTransaksi >='=> $qtanggal1,
				'a.TanggalTransaksi <='=> $qtanggal2,
			);
		$join = array(
			array('table'=>'detailjurnal b','field' => 'a.nojurnal = b.nojurnal','method'=>'Left'),
		);
			
		$result = $this->all_model->get_data("a.NoJurnal as NoJurnal, Deskripsi as Deskripsi,TanggalTransaksi as TanggalTransaksi, Debit as Debit, Kredit as Kredit","jurnal a",$join, $where, $search, false, $perPage, $segmen, false,"TanggalTransaksi","ASC");
		$result2 = $this->all_model->get_data("a.NoJurnal as NoJurnal, Deskripsi as Deskripsi, TanggalTransaksi as TanggalTransaksi, Debit as Debit, Kredit as Kredit","jurnal a", $join, $where, $search, false);
	

		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData($i)@@@>$i</a>&nbsp;&nbsp;";
					}
				
				foreach($result as $key => $val){
					//$result[$key]["NamaItem"] = str_replace('"', "@@@", $result[$key]["NamaItem"]);
					//$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);
					//$result[$key]["Harga"] = $result[$key]["Rate"];
					//$result[$key]["Rate"] = $this->all_model->rp($result[$key]["Rate"]);
	
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function index(){
		$NoAkun=$this->input->post('AkunBank');
		
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Buku Besar";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false,$NoAkun));
		$data['data4'] = json_encode($this->getData2($NoAkun));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('rekon_view',$data2);
		$this->load->view('home_footer');
	}
	

	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

