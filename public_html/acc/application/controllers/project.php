<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Project extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getOrderCode($idcustomer){
		$result = $this->all_model->query_data("SELECT RIGHT(NoOrder,3) as NoOrder FROM trorder where IdCustomer='".$idcustomer."' ORDER BY NoOrder DESC", true);
		$result["NoOrder"] = $result["NoOrder"]+1;
		if($result["NoOrder"] < 10)
			$result["NoOrder"] = "00".$result["NoOrder"];
		else if($result["NoOrder"]< 100)
			$result["NoOrder"] = "0".$result["NoOrder"];
		else
			$result["NoOrder"] = "".$result["NoOrder"];
		return $idcustomer."-".$result["NoOrder"];
	}
	
	public function getProjectCode(){
		$noorder = $this->session->userdata('NoOrder');
		$result = $this->all_model->query_data("SELECT RIGHT(NoProject,2) as NoProject FROM project where NoOrder='".$noorder."' ORDER BY NoProject DESC", true);
		$result["NoProject"] = $result["NoProject"]+1;
		if($result["NoProject"] < 10)
			$result["NoProject"] = "0".$result["NoProject"];
		else
			$result["NoProject"] = "".$result["NoProject"];
		return $noorder."-".$result["NoProject"];
	}
	
	public function deleteProject()
	{
		$NoProject = $this->input->post('NoProject');
		$where = array('NoProject'=>$NoProject);
		$query = $this->all_model->delete_data("project", $where);
		echo json_encode($query);
		exit();
	}
	
	public function getData($perPage=10, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.NamaProject' => $this->input->post('search')
			);
			$where = array(
				
			);
		}else{
			$search = array(
				'a.NamaProject' => $this->input->post('search')
				);
			$where = array(
				
			);
		}
		$join = array(
			array('table'=>'trorder b','field' => 'a.NoOrder = b.NoOrder','method'=>'Left'),
			array('table'=>'mskaryawan c','field' => 'a.PIC = c.NIP','method'=>'Left'),
		);			
						
		$result = $this->all_model->get_data("NoProject as NoProject,a.NoOrder as NoOrder,NamaProject as NamaProject,TanggalProject as TanggalProject,BiayaProject as BiayaProject,c.Nama as PIC,Status as Status", "project a",$join, $where, $search, false, $perPage, $segmen, false,"NoProject,IdCustomer,TanggalProject","DESC");
		
		$result2 = $this->all_model->get_data("NoProject as NoProject,a.NoOrder as NoOrder,NamaProject as NamaProject,TanggalProject as TanggalProject,BiayaProject as BiayaProject,c.Nama as PIC,Status as Status", "project a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	

	
	public function addNewProject(){
		
		$data = array(
		'NoProject'  => $this->getProjectCode(),
		'NoOrder' => $this->input->post('NoOrder'),		
		'NamaProject' => $this->input->post('NamaProject'),	
		'BiayaProject' => $this->input->post('BiayaProject'),
		'PIC' => $this->input->post('PIC'),
		'TanggalProject' => date("Y-m-d"),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("project", $data );
		
		if($query){
			$data3["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data3["msg"] = $calculate;	
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
		
	public function getCalculate()
	{
		$total = 0;

		$join = array(
			
		);
		$where = array(
				'a.NoOrder' => $this->session->userdata("NoOrder"),
		);	
		$search = array(
	
		);
		$result = $this->all_model->get_data("BiayaProject as Biaya","project a",$join, $where, $search);
		if($result)
		{
			foreach($result as $key => $value){
				
				$total += $result[$key]['Biaya'];
	
			}
	
			$total = $this->all_model->rp($total);
			
			$newdata = array(
				'TotalBiayaProject'		=> $total,
			);
			return $newdata;
		}
		else
		{
			$newdata = array(
				'TotalBiayaProject'		=> 0,
			);
			return $newdata;
		}
	}
	
	
	public function deleteItemProject()
	{
		$NoProject = $this->input->post('NoProject');
		$where = array('NoProject'=>$NoProject);
		$query = $this->all_model->delete_data("project", $where);
		
		if($query){
			$data3["status"] = "sukses";
			$calculate = $this->getCalculate();
			$data3["msg"] = $calculate;	
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}
	
	public function getDataProject($noproject)
	{
		$result = $this->all_model->query_data("SELECT a.NoProject as NoProject,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}

	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Project";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('project_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function checkNIP(){

		$noproject = $this->session->userdata("NoProject");
		$nip = $this->input->post('NIP');
		$result = $this->all_model->query_data("SELECT NIP FROM projectworkers WHERE NoProject='".$noproject."' and NIP='".$nip."'", true);
		
		if($result)
		{
			echo json_encode("true");
		}
		else echo json_encode("false");

		exit();
	}
	
	
	
	public function addNewWorkerProject(){
		
		$data = array(
		'NoProject'  => $this->session->userdata('NoProject'),	
		'NIP' => $this->input->post('NIP'),	
		'Insentif' => $this->input->post('Insentif'),
		'Senin' => $this->input->post('Senin'),
		'Selasa' => $this->input->post('Selasa'),
		'Rabu' => $this->input->post('Rabu'),
		'Kamis' => $this->input->post('Kamis'),
		'Jumat' => $this->input->post('Jumat'),
		'Sabtu' => $this->input->post('Sabtu'),
		'Minggu' => $this->input->post('Minggu'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("projectworkers", $data );
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
	
	public function updateWorkerProject(){
		
		$data = array(
			
		'NIP' => $this->input->post('NIP'),	
		'Insentif' => $this->input->post('Insentif'),
		'Senin' => $this->input->post('Senin'),
		'Selasa' => $this->input->post('Selasa'),
		'Rabu' => $this->input->post('Rabu'),
		'Kamis' => $this->input->post('Kamis'),
		'Jumat' => $this->input->post('Jumat'),
		'Sabtu' => $this->input->post('Sabtu'),
		'Minggu' => $this->input->post('Minggu'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			
		$Id = $this->input->post('Id');	
		$where = array('Id'=>$Id);
		$query = $this->all_model->update_data("projectworkers", $data,$where);	
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
	
	
	public function deleteWorkerProject()
	{
		$Id = $this->input->post('Id');
		$where = array('Id'=>$Id);
		$query = $this->all_model->delete_data("projectworkers", $where);
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}	
		echo json_encode($data3);
		exit();
	}
	
	public function getWorkerProject()
	{
		$noproject = $this->session->userdata("NoProject");
		$result = $this->getWorker($noproject);
		if($result != "No Data")
		echo json_encode($result);
		else echo json_encode(array());
		
	}
	
	public function getWorker($noproject)
	{
		$search = array(
		
			);
		$join = array(
				array('table'=>'mskaryawan b','field' => 'a.NIP = b.NIP','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("NoProject as NoProject,a.NIP as NIP,b.Nama as Nama,b.Telepon as Telepon,Insentif as Insentif,Id as Id,Senin as Senin,Selasa as Selasa,Rabu as Rabu,Kamis as Kamis,Jumat as Jumat,Sabtu as Sabtu,Minggu as Minggu","projectworkers a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){/*
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				
				
				$total+= $result[$key]['BiayaProject'];
				$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
	*/
			}	
			return $result;
		}
		else
		{
			return "No Data";
		}
	
	}

	public function getNeed($noproject)
	{
		$search = array(
		
			);
		$join = array(
				array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("NoProject as NoProject,a.IdBarang as IdBarang,b.Quantity as Quantity,b.Jumlah as Harga,a.Jumlah as Jumlah,b.Nama as Nama","projectneeds a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){/*
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				
				
				$total+= $result[$key]['BiayaProject'];
				$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
	*/
				$result[$key]['Harga'] = $result[$key]['Harga'] / $result[$key]['Quantity'];
			}	
			return $result;
		}
		else
		{
			return "No Data";
		}
	
	}	
	
	public function printWorker()
	{
		$noproject = $this->session->userdata('NoProject');
		$dataproject = $this->getDataProject($noproject);
		$result = $this->getWorker($noproject);
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Surat Perintah Kerja Proyek',0,0,'C');
		$this->fpdf->Ln(25);
		$this->fpdf->SetFont('Arial','',12);
		$this->fpdf->SetX(30);
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'');	
		$this->fpdf->Ln(8);
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(190,6,'No SPK : SPK-'.date("m-Y").'-'.$noproject.'',0,0,'');		
		$this->fpdf->Ln(8);
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(190,6,'Nama Project : '.$dataproject['NamaProject'].'',0,0,'');		
		$this->fpdf->Ln(15);

		$y_axis_initial = 95;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'NIP',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Pekerja',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Telepon',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		//$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(30);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'NIP',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Pekerja',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Telepon',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		//$grandtotal+=$row['Total'];
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["NIP"],1,0,'C',0);
		$this->fpdf->Cell(60,6,$result[$key]["Nama"],1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["Telepon"],1,0,'C',0);
		$this->fpdf->Cell(35,6,$this->all_model->rp($result[$key]["Insentif"]),1,0,'C',0);

		$this->fpdf->Ln();

		}
		}

		//buat footer
		$now = date("d F Y");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(303,6,"Mengetahui",0,0,'C');
		$this->fpdf->Ln(25);
		$this->fpdf->Cell(303,6,"(                                                  )",0,0,'C');
		$this->fpdf->Ln();
		
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		
		
		$this->fpdf->Output('Surat Perintah Pekerja Proyek'.date("F Y").'.pdf', 'I');
		
	
		
				
		
	}
	
	
	
	
	public function detailProject($noproject){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
	
		$dataproject = $this->getDataProject($noproject);
		
	
		$newdata = array(
			'NoProject'		=> $noproject,
		);
		$this->session->set_userdata($newdata);
		
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Project";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data2['data2'] = json_encode($dataproject);
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailproject_view',$data2);
		$this->load->view('home_footer');
	}
	
		
	public function checkItem(){

		$noproject = $this->session->userdata("NoProject");
		$IdBarang = $this->input->post('IdBarang');
		$result = $this->all_model->query_data("SELECT IdBarang FROM projectneeds WHERE NoProject='".$noproject."' and IdBarang='".$IdBarang."'", true);
		
		if($result)
		{
			echo json_encode("true");
		}
		else echo json_encode("false");

		exit();
	}
	
	public function checkStock(){

		$noproject = $this->session->userdata("NoProject");
		$IdBarang = $this->input->post('IdBarang');
		$jumlah = $this->input->post('Jumlah');
		
		$result = $this->all_model->query_data("SELECT * FROM barang WHERE IdBarang='".$IdBarang."'", true);
		
		if($result)
		{
			if($result['Jumlah'] < $jumlah)
			echo json_encode("true");
			else echo json_encode("false");
		}
		else echo json_encode("true");

		exit();
	}

	public function getDataDetailBarangKeluar($IdBarangKeluar){

		$result = $this->all_model->query_data("SELECT IdDetailBarangKeluar FROM detailbarangkeluar WHERE IdBarangKeluar='".$IdBarangKeluar."' order by IdDetailBarangKeluar desc", true);
		return $result["IdDetailBarangKeluar"];
		
	}
	
	public function addNewProjectNeeds(){
		
		$IdBarang = $this->input->post('IdBarang');
		$Jumlah = $this->input->post('Jumlah');
		$data = array(
		'NoProject'  => $this->session->userdata('NoProject'),	
		'IdBarang' => $IdBarang,	
		'Jumlah' => $Jumlah,	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("projectneeds", $data );
		
		$barang = $this->all_model->query_data("SELECT * FROM barang WHERE IdBarang='".$IdBarang."'", true);
		$harga = $barang['Jumlah'] / $barang['Quantity'];
		$sisastok = $barang['Quantity'] - $Jumlah;
		$totalbarang =  $sisastok*$harga;
		
		$data2 = array(	
		'Jumlah' => $totalbarang,
		'Quantity' => $sisastok,	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			

		
		$result5 = $this->all_model->query_data("SELECT IdBarangKeluar FROM barangkeluar where Reference='".$this->session->userdata('NoProject')."'", true);
		$idbarangkeluar = 0;
		if($result5)
		{
			$idbarangkeluar = $result5["IdBarangKeluar"];
		}
		else
		{
		
		$data4 = array(
				'Keterangan' => 'Pengeluaran Project '.$this->session->userdata('NoProject'),
				'UpdateID' => $this->session->userdata("UserId"),
				'TanggalKeluar' => date("Y-m-d H:i:s"),
				'Tag' => 'Project',
				'Reference' => $this->session->userdata('NoProject')
			);
		$this->all_model->insert_data("barangkeluar", $data4);	
		
		$result6 = $this->all_model->query_data("SELECT IdBarangKeluar FROM barangkeluar where Reference='".$this->session->userdata('NoProject')."'", true);
		$idbarangkeluar = $result6["IdBarangKeluar"];
		
		}
		
		$data5 = array(
				'IdBarangKeluar'  => $idbarangkeluar,
				'IdBarang'  => $IdBarang,
				'Nama' => $barang['Nama'],		
				'Quantity' => (0-$Jumlah),	
				'Jumlah' => (0-($Jumlah*$harga)),
				'Keterangan' => 'Barang Keluar Project '.$this->session->userdata('NoProject'),
				'Status' => 'N',
			);
		$this->all_model->insert_data("detailbarangkeluar", $data5);
		
		
		$IdDetailBarangKeluar = $this->getDataDetailBarangKeluar($idbarangkeluar);
		
		$data6 = array(
				'IdDetailBarangKeluar'  => $IdDetailBarangKeluar,
				'IdBarang'  => $IdBarang,	
				'SaldoQty' => $sisastok,	
				'SaldoJumlah' => $totalbarang,
				'Tag' => 'Project',
				'Tanggal' => date("Y-m-d H:i:s"),
			);
		$this->all_model->insert_data("historybar", $data6);	

		
		//Update Master barang
		$where2 = array('IdBarang'=>$IdBarang);
		$query1 = $this->all_model->update_data("barang", $data2,$where2);	
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
	
	public function updateProjectNeeds(){
		
		$data = array(		
		'IdBarang' => $this->input->post('IdBarang'),	
		'Jumlah' => $this->input->post('Jumlah'),	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			
		$Id = $this->input->post('Id');	
		$where = array('Id'=>$Id);
		$query = $this->all_model->update_data("projectneeds", $data,$where);	
		
		
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
	
	
	public function deleteProjectNeeds()
	{
		$Id = $this->input->post('Id');
		$IdBarang = $this->input->post('IdBarang');
		$Jumlah = $this->input->post('Jumlah');
		
		$where = array('Id'=>$Id);
		$query = $this->all_model->delete_data("projectneeds", $where);
		
		$barang = $this->all_model->query_data("SELECT * FROM barang WHERE IdBarang='".$IdBarang."'", true);
		$harga = $barang['Jumlah'] / $barang['Quantity'];
		$sisastok = $barang['Quantity'] + $Jumlah;
		$totalbarang =  $sisastok*$harga;
		
		$data2 = array(	
		'Jumlah' => $totalbarang,
		'Quantity' => $sisastok,	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			
		$where2 = array('IdBarang'=>$IdBarang);
		$query1 = $this->all_model->update_data("barang", $data2,$where2);	
		
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}	
		echo json_encode($data3);
		exit();
	}
	
	public function getProjectNeeds()
	{
		$noproject = $this->session->userdata("NoProject");
		$result = $this->getNeeds($noproject);
		if($result != "No Data")
		echo json_encode($result);
		else echo json_encode(array());
		
	}
	
	public function getNeeds($noproject)
	{
		$search = array(
		
			);
		$join = array(
				array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),
				array('table'=>'kategoribesar c','field' => 'b.IdKategoriBesar = c.IdKategoriBesar','method'=>'Left'),
				array('table'=>'kategori d','field' => 'b.IdKategori = d.IdKategori','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("Id as Id,NoProject,a.IdBarang as IdBarang,b.Nama as Nama,b.Jumlah as Harga,a.Jumlah as Jumlah,b.Quantity as Quantity,Keterangan,Jenis","projectneeds a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){
			$result[$key]['Harga'] = $result[$key]['Harga'] / $result[$key]['Quantity'];
			$result[$key]['TotalHarga'] = $this->all_model->rp($result[$key]['Jumlah']*$result[$key]['Harga']);
			$result[$key]['Harga'] = $this->all_model->rp($result[$key]['Harga']);
			
			/*
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				
				
				$total+= $result[$key]['BiayaProject'];
				$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
	*/
			}	
			return $result;
		}
		else
		{
			return "No Data";
		}
	
	}
	
	
	public function printNeeds()
	{
		$noproject = $this->session->userdata('NoProject');
		$dataproject = $this->getDataProject($noproject);
		$result = $this->getNeed($noproject);
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Laporan Barang Proyek',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','',12);
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'');	
		$this->fpdf->Ln(8);
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(190,6,'Nama Project : '.$dataproject['NamaProject'].'',0,0,'');		
		$this->fpdf->Ln(15);

		$y_axis_initial = 75;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Id Barang',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Barang',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Harga',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		//$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(30);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Id Barang',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Barang',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Harga',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		//$grandtotal+=$row['Total'];
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["IdBarang"],1,0,'C',0);
		$this->fpdf->Cell(60,6,$result[$key]["Nama"],1,0,'C',0);
		$this->fpdf->Cell(25,6,$this->all_model->rp($result[$key]["Harga"]),1,0,'C',0);
		$this->fpdf->Cell(35,6,$result[$key]["Jumlah"],1,0,'C',0);

		$this->fpdf->Ln();

		}
		}

		//buat footer
		$now = date("d F Y");
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		//$this->fpdf->Cell(303,6,"Total Transaksi : ".rp($grandtotal)."",0,0,'C');
		$this->fpdf->Ln();
		
		
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		
		$this->fpdf->Output('Laporan Barang Proyek'.date("F Y").'.pdf', 'I');
		
	}
	
	public function printAll($no)
	{
		$noproject = $no;
		$dataproject = $this->getDataProject($noproject);
		$result = $this->getNeed($noproject);
		$result2 = $this->getKbProyek($noproject);
		$this->load->library('fpdf17/fpdf');
		$totalbarang = 0;
		$totalkebutuhan = 0;
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Summary Proyek',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','',12);

		$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'');	
		$this->fpdf->Ln(8);
	
		$this->fpdf->Cell(190,6,'Nama Project : '.$dataproject['NamaProject'].'',0,0,'');		
		$this->fpdf->Ln(15);
		$this->fpdf->Cell(190,6,'Detail Barang Proyek',0,0,'');		
		$this->fpdf->Ln(15);
		$y_axis_initial = 85;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Id Barang',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Barang',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Harga',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		
		$totalbarang+=$result[$key]["Harga"];
		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(30);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Id Barang',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Barang',1,0,'C',1);
		$this->fpdf->Cell(25,6,'Harga',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		$grandtotal+=$result[$key]["Harga"];
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["IdBarang"],1,0,'C',0);
		$this->fpdf->Cell(60,6,$result[$key]["Nama"],1,0,'C',0);
		$this->fpdf->Cell(25,6,$this->all_model->rp($result[$key]["Harga"]),1,0,'C',0);
		$this->fpdf->Cell(35,6,$result[$key]["Jumlah"],1,0,'C',0);

		$this->fpdf->Ln();

		}
		}
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(280,6,"Total Barang : ".$this->all_model->rp($totalbarang)."",0,0,'C');
		$this->fpdf->SetFont('Arial','',12);
		$this->fpdf->Ln(10);	
		$this->fpdf->Cell(190,6,'Detail Kebutuhan Proyek',0,0,'');		
		$this->fpdf->Ln(8);
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(55,6,'Id Kebutuhan Proyek',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Kebutuhan',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");

		if($result2 != "No Data")
		{
		foreach($result2 as $key => $value){
		$i++;
		

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(30);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(55,6,'Id Kebutuhan Proyek',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Kebutuhan',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		$totalkebutuhan+=$result2[$key]['Jumlah'];
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(55,6,$result2[$key]["IdKebutuhanProyek"],1,0,'C',0);
		$this->fpdf->Cell(60,6,$result2[$key]["Nama"],1,0,'C',0);
		$this->fpdf->Cell(35,6,$this->all_model->rp($result2[$key]["Jumlah"]),1,0,'C',0);
		$grandtotal+=$result2[$key]["Jumlah"];
		$this->fpdf->Ln();

		}
		}
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(303,6,"Total Kebutuhan Proyek : ".$this->all_model->rp($totalkebutuhan)."",0,0,'C');
			
		

		//buat footer

		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(288,6,"Total Transaksi : ".$this->all_model->rp($grandtotal)."",0,0,'C');
		$this->fpdf->Ln();
		
		
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		
		$this->fpdf->Output('Laporan Barang Proyek'.date("F Y").'.pdf', 'I');
		
	}	
	
	public function detailItemProject($noproject){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
	
		$dataproject = $this->getDataProject($noproject);

		$newdata = array(
			'NoProject'		=> $noproject,
		);
		$this->session->set_userdata($newdata);
		
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Item Project";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data2['data2'] = json_encode($dataproject);
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('itemproject_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function checkKebutuhanProyek(){

		$noproject = $this->session->userdata("NoProject");
		$IdKebutuhanProyek = $this->input->post('IdKebutuhanProyek');
		$result = $this->all_model->query_data("SELECT IdKebutuhanProyek FROM kebutuhanproyek WHERE NoProject='".$noproject."' and IdKebutuhanProyek='".$IdKebutuhanProyek."'", true);
		
		if($result)
		{
			echo json_encode("true");
		}
		else echo json_encode("false");

		exit();
	}
	
	public function addNewKebutuhanProyek(){
		
		$IdKebutuhanProyek = $this->input->post('IdKebutuhanProyek');
		$Jumlah = $this->input->post('Jumlah');
		$data = array(
		'NoProject'  => $this->session->userdata('NoProject'),	
		'IdKebutuhanProyek' => $IdKebutuhanProyek,	
		'Jumlah' => $Jumlah,	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("kebutuhanproyek", $data );
		
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
	
	public function updateKebutuhanProyek(){
		
		$data = array(		
		'IdKebutuhanProyek' => $this->input->post('IdKebutuhanProyek'),	
		'Jumlah' => $this->input->post('Jumlah'),	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			
		$Id = $this->input->post('Id');	
		$where = array('Id'=>$Id);
		$query = $this->all_model->update_data("kebutuhanproyek", $data,$where);	
		
		
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}
			
		echo json_encode($data3);
		exit();
	}	
	
	
	public function deleteKebutuhanProyek()
	{
		$Id = $this->input->post('Id');
		$IdKebutuhanProyek = $this->input->post('IdKebutuhanProyek');
		$Jumlah = $this->input->post('Jumlah');
		
		$where = array('Id'=>$Id);
		$query = $this->all_model->delete_data("kebutuhanproyek", $where);
		
		
		if($query){
			$data3["status"] = "sukses";
		}
		else 
		{
			$data3["status"] = "gagal";
		}	
		echo json_encode($data3);
		exit();
	}
	
	public function getKebutuhanProyek()
	{
		$noproject = $this->session->userdata("NoProject");
		$result = $this->getDataKebutuhanProyek($noproject);
		if($result != "No Data")
		echo json_encode($result);
		else echo json_encode(array());
		
	}
	
	public function getDataKebutuhanProyek($noproject)
	{
		$search = array(
		
			);
		$join = array(
				array('table'=>'mskebutuhanproyek b','field' => 'a.IdKebutuhanProyek = b.IdKebutuhanProyek','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("Id as Id,NoProject,a.IdKebutuhanProyek as IdKebutuhanProyek,b.Nama as Nama,a.Jumlah as Jumlah","kebutuhanproyek a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){

			$result[$key]['Harga'] = $this->all_model->rp($result[$key]['Jumlah']);

			}	
			return $result;
		}
		else
		{
			return "No Data";
		}
	
	}
	
	public function getKbProyek($noproject)
	{
		$search = array(
		
			);
		$join = array(
				array('table'=>'mskebutuhanproyek b','field' => 'a.IdKebutuhanProyek = b.IdKebutuhanProyek','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("NoProject as NoProject,a.IdKebutuhanProyek as IdKebutuhanProyek,a.Jumlah as Jumlah,b.Nama as Nama","kebutuhanproyek a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){
	
			}	
			return $result;
		}
		else
		{
			return "No Data";
		}
	
	}
	
	
	public function printKebutuhanProyek()
	{
		$noproject = $this->session->userdata('NoProject');
		$dataproject = $this->getDataProject($noproject);
		$result = $this->getKbProyek($noproject);
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(30);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Laporan Kebutuhan Barang Proyek',0,0,'C');
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','',12);
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'');	
		$this->fpdf->Ln(8);
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(190,6,'Nama Project : '.$dataproject['NamaProject'].'',0,0,'');		
		$this->fpdf->Ln(15);

		$y_axis_initial = 75;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(55,6,'Id Kebutuhan Proyek',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Kebutuhan',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "No Data")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
		$this->fpdf->AddPage();
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(30);
		$this->fpdf->CELL(10,6,'No',1,0,'C',1);
		$this->fpdf->Cell(55,6,'Id Kebutuhan Proyek',1,0,'C',1);
		$this->fpdf->Cell(60,6,'Nama Kebutuhan',1,0,'C',1);
		$this->fpdf->Cell(35,6,'Jumlah',1,0,'C',1);

		$this->fpdf->SetY(10);
		$this->fpdf->SetX(55);
		$y_axis = $y_axis + $row_height;
		$i=0;
		$this->fpdf->Ln();

		}

		$grandtotal+=$result[$key]['Jumlah'];
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(55,6,$result[$key]["IdKebutuhanProyek"],1,0,'C',0);
		$this->fpdf->Cell(60,6,$result[$key]["Nama"],1,0,'C',0);
		$this->fpdf->Cell(35,6,$this->all_model->rp($result[$key]["Jumlah"]),1,0,'C',0);

		$this->fpdf->Ln();

		}
		}

		//buat footer
		$now = date("d F Y");
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(303,6,"Total Kebutuhan : ".$this->all_model->rp($grandtotal)."",0,0,'C');
		$this->fpdf->Ln();
		
		
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->SetX(0);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		
		$this->fpdf->Output('Laporan Barang Proyek'.date("F Y").'.pdf', 'I');
		
	}
	
	public function detailKebutuhanProyek($noproject){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
	
		$dataproject = $this->getDataProject($noproject);

		$newdata = array(
			'NoProject'		=> $noproject,
		);
		$this->session->set_userdata($newdata);
		
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Kebutuhan Proyek";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data2['data2'] = json_encode($dataproject);
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailkebutuhanproyek_view',$data2);
		$this->load->view('home_footer');
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


