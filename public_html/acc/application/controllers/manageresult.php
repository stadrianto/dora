<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ManageResult extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		$this->load->model('all_model2');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	
	public function antiinjection($data)
	{
		$filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter_sql;
	}
	
	public function generateEmployee()
	{
		$nip=$this->antiinjection($this->input->post('nip'));
		
		$result = $this->all_model->query_data("SELECT * FROM msemployee WHERE Nip='$nip'", true);	
		if($result){
			$newdata = array(
						'UserId'		=> $result["UserID"],
						'NIP'		=> $result["NIP"],
						'Name'		=> $result["Name"],
						'Password'		=> $result["Password"],
						'Jabatan'		=> $result["Jabatan"],
						'Departemen'		=> $result["Departemen"],
						'Perusahaan'		=> $result["Perusahaan"]
				   );
				 
			$result2 = $this->all_model->query_data("SELECT scheduleid,result,statuslulus,mkeyh_nama FROM schedule a join  to_mh_keyactivities b on a.MKeyH_ID = b.MKeyH_ID WHERE Nip='$nip' and activeyn='Y'", false);		 
			if($result2) $data["schedule"] = $result2;
			else $data["schedule"] = "No Data";
			$data["status"] = "sukses";
			$data["msg"] = $newdata;
		}
		else{
			$data["status"] = "gagal";
			$data["msg"] = "NIP Tidak ditemukan";
		}	
		echo json_encode($data);
		exit();
	}
	
	public function generateActivity()
	{
	
		$idkeyact=$this->input->post('idkeyact');
		$result = $this->all_model->query_data("SELECT * FROM to_mh_keyactivities WHERE mkeyh_id='$idkeyact' and mkeyh_activeyn='Y'", true);
		
		if($result){
			$newdata = array(
						'LamaTest'		=> $result["MKeyH_LamaTest"],
						'IdKeyAct'		=> $result["MKeyH_ID"]
				   );
				 
			$data["status"] = "sukses";
			$data["msg"] = $newdata;
		}
		else{
			$data["status"] = "gagal";
			$data["msg"] = "Koneksi Gagal";
		}	
		echo json_encode($data);
		exit();
	}
	
	public function generateSchedule()
	{
	
		$idschedule=$this->input->post('idschedule');
		$result = $this->all_model->query_data("SELECT * FROM schedule WHERE scheduleid='$idschedule' and activeyn='Y'", true);
		
		if($result){
			$newdata = array(
						'StatusLulus'		=> $result["StatusLulus"],
						'ScheduleId'		=> $result["ScheduleID"],
						'Result'		=> $result["Result"],
						'Waktu'		=> date("d-M-Y , H:i",strtotime($result["Waktu"]))." WIB",
				   );
				 
			$data["status"] = "sukses";
			$data["msg"] = $newdata;
		}
		else{
			$data["status"] = "gagal";
			$data["msg"] = "Koneksi Gagal";
		}	
		echo json_encode($data);
		exit();
	}
	
	
	public function checkBankAnswer($IdSoal,$NoUrut,$Jawaban)
	{
		$result = $this->all_model->query_data("SELECT mdsoal_urutjawaban FROM to_md_banksoal WHERE MDSoal_ID = '".$IdSoal ."' and MDSoal_NoUrut='".$NoUrut."'", true);
		if($result)
		{
			if($Jawaban == $result['mdsoal_urutjawaban']) return true;
			else return false;
		}
		else return false;
	}
	
	public function checkAnswer($IdAnswer,$IdSoal,$nilaipersoal)
	{
		$nilai = 0;
		
		$search = array();
		$join = array();
		$where = array('mhanswer_id' => $IdAnswer);		
		$result = $this->all_model->get_data("MDSoal_NoUrut as NoUrut,MDAnswer_NoUrutJawaban as Jawaban", "to_md_answer a",$join, $where, $search);
		
		if(!$result){
			$nilai= "0";
		}	
		else
		{
			foreach($result as $key => $value){
			
				if($this->checkBankAnswer($IdSoal,$result[$key]['NoUrut'],$result[$key]['Jawaban']))
				{
					$nilai+=$nilaipersoal;
				}
			}
		}
		return $nilai;
	
	}
	
	public function updateResult()
	{
		$idschedule = $this->input->post('idschedule');
		$result = $this->input->post('result');
		$statuslulus = $this->input->post('statuslulus');

		$data = array(
				'Result' 	 => $result,
				'StatusLulus' => $statuslulus,
				'UpdateTime'	 => date("Y-m-d H:i:s"),
				'UpdateID' => $this->session->userdata("UserId"),
			);
		$where = array(	
				'ScheduleID'			=> $idschedule,	
				);
		$query = $this->all_model->update_data("schedule", $data , $where);
		echo json_encode($query);
		
		
		exit();
	}
	
	public function getResult()
	{
	
		$idschedule=$this->input->post('idschedule');
		$result = $this->all_model->query_data("SELECT MHAnswer_ID,MHSoal_ID,MKeyH_NilaiPerSoal FROM schedule a join to_mh_keyactivities b on a.MKeyH_ID = b.MKeyH_ID WHERE scheduleid='$idschedule' and activeyn='Y'", true);
		
		if($result['MHAnswer_ID'] != 0){
			$data["status"] = "sukses";
			$data["msg"] = $this->checkAnswer($result['MHAnswer_ID'],$result['MHSoal_ID'],$result['MKeyH_NilaiPerSoal']);
		}
		else{
			$data["status"] = "gagal";
			$data["msg"] = "Karyawan belum mengerjakan soal";
		}	
		echo json_encode($data);
		exit();
	
	}
	
	public function index(){
		if($this->session->userdata("UserId")!="1" || $this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="Schedule";
		$data['page_title']="Website Training Pharos";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('manageresult_view');
		$this->load->view('home_footer');
	}
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


