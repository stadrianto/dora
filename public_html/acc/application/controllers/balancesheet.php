<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class balancesheet extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataJurnal($noakun,$tanggal1,$tanggal2)
	{
		$result = $this->all_model->query_data("SELECT Debit as Debit,Kredit as Kredit FROM jurnal a join detailjurnal b on a.nojurnal = b.nojurnal where status='Approved' and noakun='".$noakun."' and tanggaltransaksi <= '".$tanggal1."'", false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataPrinter($id)
	{
		$result = $this->all_model->query_data("SELECT Nama as Nama from msuser where iduser=".$id, false);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function getDataTrial()
	{
		$result = $this->all_model->query_data("SELECT c.IdKelompokGl as IdKelompokGl,c.NamaGL as NamaAkun,Sum(SaldoAwal) as SaldoAwal,IdNeraca as IdNeraca, NoAkun as NoAkun 
												FROM tabelakun a 
												join kelompokakun b on a.idkelompokakun=b.idkelompokakun 
												left join kelompokgl c on a.idkelompokgl = c.idkelompokgl 
												where a.idkelompokakun<=3 
												group by IdKelompokGl,NamaGL,IdNeraca
												order by c.Idkelompokgl asc", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	
	public function printBalanceSheet()
	{
		//$noproject = $this->session->userdata('NoProject');
		//$dataproject = $this->getDataTrial();
		$format = $this->input->post('format');
		$untuk = $this->input->post('untuk');
		$tanggal1=$this->input->post('dob2');
		$tanggal2=$this->input->post('dob');
		$qtanggal1=substr($tanggal1,6,4).'-'.substr($tanggal1,0,2).'-'.substr($tanggal1,3,2);
		$qtanggal2=substr($tanggal2,6,4).'-'.substr($tanggal2,0,2).'-'.substr($tanggal2,3,2);		
		$result = $this->getDataTrial();
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		
		if($untuk == "CIPS")
		{
			if($format == "Biasa")
			{
				$this->fpdf->AddPage();
				$this->fpdf->Image('images/logo.png',35,10,30,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Ln(10);
				$this->fpdf->SetFont('Arial','B',15);
				$this->fpdf->Cell(190,6,'Citra Inti Prima Sejati',0,0,'C');
				$this->fpdf->Ln(5);
				$this->fpdf->Cell(190,6,'Balance Sheet',0,0,'C');
				$this->fpdf->SetFont('Arial','BU',12);
				$this->fpdf->Ln(5);
				$this->fpdf->Cell(190,6,'Per Periode '.$tanggal1,0,0,'C');
				//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
				//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
				$this->fpdf->Ln(15);
			}
			else if($format == "Bersebelahan")
			{
				$this->fpdf->AddPage('L');
				$this->fpdf->Image('images/logo.png',35,10,30,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Ln(10);
				$this->fpdf->SetFont('Arial','B',15);
				$this->fpdf->Cell(280,6,'Citra Inti Prima Sejati',0,0,'C');
				$this->fpdf->Ln(5);
				$this->fpdf->Cell(280,6,'Balance Sheet',0,0,'C');
				$this->fpdf->SetFont('Arial','BU',12);
				$this->fpdf->Ln(5);
				$this->fpdf->Cell(280,6,'Per Periode '.$tanggal1,0,0,'C');
				//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
				//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
				$this->fpdf->Ln(15);
			}
			
		}
		else if($untuk == "CPA")
		{
			if($format == "Biasa")
			{
				$this->fpdf->AddPage();
				$this->fpdf->Image('images/logo.png',35,10,30,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Ln(10);
				$this->fpdf->SetFont('Arial','BU',12);
				$this->fpdf->SetY(30);
				$this->fpdf->SetX(10);
				$this->fpdf->Cell(190,6,'Balance Sheet',0,0,'C');
				$this->fpdf->SetFont('Arial','BU',12);
				$this->fpdf->Ln(5);
				$this->fpdf->Cell(190,6,'Per Periode '.$tanggal1,0,0,'C');
				//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
				//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
				$this->fpdf->Ln(15);
			}
			else if($format == "Bersebelahan")
			{
				$this->fpdf->AddPage('L');
				$this->fpdf->Image('images/logo.png',35,10,30,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Image('images/cips_nama.png',125,20,50,0,'','http://www.cips.or.id/'); 
				$this->fpdf->Ln(10);
				$this->fpdf->SetFont('Arial','BU',12);
				$this->fpdf->SetY(30);
				$this->fpdf->SetX(10);
				$this->fpdf->Cell(280,6,'Balance Sheet',0,0,'C');
				$this->fpdf->SetFont('Arial','BU',12);
				$this->fpdf->Ln(5);
				$this->fpdf->Cell(280,6,'Per Periode '.$tanggal1,0,0,'C');
				//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
				//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
				$this->fpdf->Ln(15);
			}

		}
		
		if($format == "Biasa")
		{
			$y_axis_initial = 45;
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->SetY($y_axis_initial);
			$this->fpdf->SetX(30);
			//Header tabel halaman 1
			/*
			$this->fpdf->CELL(25,6,'NoAkun',1,0,'C',1);
			$this->fpdf->Cell(60,6,'NamaAkun',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Debit',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Kredit',1,0,'C',1);
			*/
			//$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);

			$this->fpdf->SetX(30);
			$this->fpdf->CELL(90,6,'NamaAkun',1,0,'C',1);
			$this->fpdf->CELL(60,6,'Saldo',1,0,'C',1);

			$max=25;//max baris perhalaman
			$i=0;
			$no1=0;
			$no2=0;
			$no3=0;
			$totala=0;
			$totalb=0;
			$totalc=0;
			$totald=0;

			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			//$grandtotal = 0;
			if($result != "0")
			{
				foreach($result as $key => $value)
				{
					$i++;
					if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
					$this->fpdf->AddPage();
					$this->fpdf->Ln();
					$this->fpdf->SetX(30);
					$this->fpdf->CELL(90,6,'NamaAkun',1,0,'C',1);
					$this->fpdf->CELL(60,6,'Saldo',1,0,'C',1);

					$this->fpdf->SetY(10);
					$this->fpdf->SetX(35);
					$y_axis = $y_axis + $row_height;
					$i=0;
					$this->fpdf->Ln();

					}
					$saldo=floatval($result[$key]["SaldoAwal"]);
					$result2 = $this->getDataJurnal($result[$key]["NoAkun"]."",$qtanggal1."",$qtanggal2."");
					if($result[$key]["IdNeraca"]=="1")
					{	
						$saldo=0;
						if($result2!="0")
						{
							for($z=0;$z<count($result2);$z++){
								$saldo+=$result2[$z]["Debit"];
								$saldo-=$result2[$z]["Kredit"];
							}
						}
					}	
					else if($result[$key]["IdNeraca"]=="2")
					{
						$saldo=0;
						if($result2!="0")
						{
							for($z=0;$z<count($result2);$z++){
								$saldo-=$result2[$z]["Debit"];
								$saldo+=$result2[$z]["Kredit"];
								//$totalb-=$result2[$z]["Debit"];
								//$totalb+=$result2[$z]["Kredit"];					
							}
						}
					}
					$i++;
					//$no++;
					if(substr($result[$key]["NoAkun"],0,1)=="1")
					{
						$this->fpdf->SetY(65+($no1*5));
						$this->fpdf->SetX(35);
						$this->fpdf->Cell(10,6,$result[$key]["IdKelompokGl"].' -',0,0,'L',0);
						$this->fpdf->Cell(85,6,$result[$key]["NamaAkun"],0,0,'L',0);
						$this->fpdf->Cell(50,6,$this->all_model->rp($saldo),0,0,'L',0);
						//$this->fpdf->Cell(25,6,45+($no4*5),0,0,'R',0);
						//$this->fpdf->Ln(0);
						$no1++;
						$totala+=$saldo;
					}
					else if(substr($result[$key]["NoAkun"],0,1)=="2")
					{		
						$this->fpdf->SetY(80+($no1*5)+($no2*5));
						$this->fpdf->SetX(35);
						$this->fpdf->Cell(10,6,$result[$key]["IdKelompokGl"].' -',0,0,'L',0);
						$this->fpdf->Cell(85,6,$result[$key]["NamaAkun"],0,0,'L',0);
						$this->fpdf->Cell(50,6,$this->all_model->rp($saldo),0,0,'L',0);
						//$this->fpdf->Cell(25,6,45+($no4*5)+($no5*5),0,0,'R',0);
						//$this->fpdf->Ln(0);			
						$no2++;
						$totalc+=$saldo;
					}
					else if(substr($result[$key]["NoAkun"],0,1)=="3")
					{
						//$this->fpdf->SetY(60);
						$this->fpdf->SetY(95+($no1*5)+($no2*5)+($no3*5));
						$this->fpdf->SetX(35);
						$this->fpdf->Cell(10,6,$result[$key]["IdKelompokGl"].' -',0,0,'L',0);
						$this->fpdf->Cell(85,6,$result[$key]["NamaAkun"],0,0,'L',0);
						$this->fpdf->Cell(50,6,$this->all_model->rp($saldo),0,0,'L',0);
						//$this->fpdf->Cell(25,6,65+($no4*5)+($no6*5),0,0,'R',0);
						$no3++;
						$totalb+=$saldo;
						//$this->fpdf->SetY(70);
					}		
									
				}
				
					//header Harta
					$this->fpdf->SetFont('Arial','B',10);
					$this->fpdf->SetY(60);
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(150,6,'Harta',0,0,'L',0);
					$this->fpdf->SetY(61);
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(150,6,'___________________________',0,0,'L',0);
					
					//header utang				
					$this->fpdf->SetY(70+($no1*5)+($no2*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(150,6,'Utang',0,0,'L',0);
					$this->fpdf->SetY(71+($no1*5)+($no2*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(150,6,'___________________________',0,0,'L',0);
					
					//header Modal
					$this->fpdf->SetY(85+($no1*5)+($no2*5)+($no3*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(150,6,'Modal',0,0,'L',0);
					$this->fpdf->SetY(86+($no1*5)+($no2*5)+($no3*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(150,6,'___________________________',0,0,'L',0);
					
					//LabaKotor totala
					$this->fpdf->SetFont('Arial','B',10);
					$this->fpdf->SetY(65+($no1*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(100,6,'Total Harta',0,0,'L',0);
					$this->fpdf->Cell(50,6,$this->all_model->rp($totala),0,0,'R',0);
					$this->fpdf->SetY(61+($no1*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(150,6,'___________________________',0,0,'R',0);
					
					//total modal totalb
					$this->fpdf->SetY(95+($no1*5)+($no2*5)+($no3*5));
					$this->fpdf->SetX(30);	
					$this->fpdf->Cell(80,6,'Total Modal',0,0,'L',0);			
					$this->fpdf->Cell(50,6,$this->all_model->rp($totalb),0,0,'R',0);
					$this->fpdf->SetY(91+($no1*5)+($no2*5)+($no3*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(130,6,'________________',0,0,'R',0);
					
					//total utang totalc
					$this->fpdf->SetY(80+($no1*5)+($no2*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(80,6,'Total Utang',0,0,'L',0);		
					$this->fpdf->Cell(50,6,$this->all_model->rp($totalc),0,0,'R',0);
					$this->fpdf->SetY(76+($no1*5)+($no2*5));
					$this->fpdf->SetX(30);
					$this->fpdf->Cell(130,6,'________________',0,0,'R',0);
					
			}
			
			$this->fpdf->Ln();
			$this->fpdf->SetY(100+($no1*5)+($no2*5)+($no3*5));
			$this->fpdf->SetX(30);
			$this->fpdf->Cell(100,6,'Total Utang dan Modal',0,0,'L',0);
			$totald = $totalb+$totalc;
			$this->fpdf->Cell(50,6,$this->all_model->rp($totald),0,0,'R',0);
			$this->fpdf->SetY(96+($no1*5)+($no2*5)+($no3*5));
			$this->fpdf->SetX(30);
			$this->fpdf->Cell(150,6,'___________________________',0,0,'R',0);
			
			if($totala != $totald)
			{
				$this->fpdf->SetFont('Arial','',12);
				$this->fpdf->setTextColor(222,50,50);
				$this->fpdf->SetY(5);
				$this->fpdf->SetX(170);
				$this->fpdf->Cell(30,6,'Total Harta dibanding Utang dan Modal tidak Seimbang',0,0,'R',0);
			}
			
			$now = date("d F Y H:i:s");
		
		
			$this->fpdf->Ln();
			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->setTextColor(0,0,0);
			$printer=$this->getDataPrinter($this->session->userdata("UserId"));
			$this->fpdf->SetY(270);
			$this->fpdf->SetX(20);			
			$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'L');
			$this->fpdf->SetX(30);		
			$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
			$this->fpdf->Ln();
			$this->fpdf->Output('Laporan Laba Rugi'.date("F Y").'.pdf', 'I');
	
		}
		else if($format == "Bersebelahan")
		{
			$y_axis_initial = 45;
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->SetY($y_axis_initial);
			$this->fpdf->SetX(20);
			//Header tabel halaman 1
			/*
			$this->fpdf->CELL(25,6,'NoAkun',1,0,'C',1);
			$this->fpdf->Cell(60,6,'NamaAkun',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Debit',1,0,'C',1);
			$this->fpdf->Cell(25,6,'Kredit',1,0,'C',1);
			*/
			//$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);
			
			$this->fpdf->CELL(130,6,'Aktiva',1,0,'C',1);
			$this->fpdf->Cell(130,6,'Pasiva',1,0,'C',1);
			$this->fpdf->Ln();
			$this->fpdf->SetX(20);
			$this->fpdf->CELL(90,6,'NamaAkun',1,0,'C',1);
			$this->fpdf->CELL(40,6,'Saldo',1,0,'C',1);
			$this->fpdf->CELL(90,6,'NamaAkun',1,0,'C',1);
			$this->fpdf->CELL(40,6,'Saldo',1,0,'C',1);

			$this->fpdf->Ln();
			$max=15;//max baris perhalaman
			$i=0;
			$no1=0;
			$no2=0;
			$no3=0;
			$totala=0;
			$totalb=0;
			$totalc=0;
			$totald=0;

			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			//$grandtotal = 0;
			if($result != "0")
			{
				foreach($result as $key => $value)
				{
					$i++;
					if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
					$this->fpdf->AddPage('L');
					$this->fpdf->SetY(10);
					$this->fpdf->SetX(20);
					$this->fpdf->CELL(130,6,'Aktiva',1,0,'C',1);
					$this->fpdf->Cell(130,6,'Pasiva',1,0,'C',1);
					$this->fpdf->Ln();
					$this->fpdf->SetX(20);
					$this->fpdf->CELL(90,6,'NamaAkun',1,0,'C',1);
					$this->fpdf->CELL(40,6,'Saldo',1,0,'C',1);
					$this->fpdf->CELL(90,6,'NamaAkun',1,0,'C',1);
					$this->fpdf->CELL(40,6,'Saldo',1,0,'C',1);

					$this->fpdf->SetY(10);
					$this->fpdf->SetX(25);
					$y_axis = $y_axis + $row_height;
					$i=0;
					$this->fpdf->Ln();

					}
					$saldo=floatval($result[$key]["SaldoAwal"]);
					$result2 = $this->getDataJurnal($result[$key]["NoAkun"]."",$qtanggal1."",$qtanggal2."");
					if($result[$key]["IdNeraca"]=="1")
					{	
						$saldo=0;
						if($result2!="0")
						{
							for($z=0;$z<count($result2);$z++){
								$saldo+=$result2[$z]["Debit"];
								$saldo-=$result2[$z]["Kredit"];
							}
						}
					}	
					else if($result[$key]["IdNeraca"]=="2")
					{
						$saldo=0;
						if($result2!="0")
						{
							for($z=0;$z<count($result2);$z++){
								$saldo-=$result2[$z]["Debit"];
								$saldo+=$result2[$z]["Kredit"];
								//$totalb-=$result2[$z]["Debit"];
								//$totalb+=$result2[$z]["Kredit"];					
							}
						}
					}
					$i++;
					//$no++;
					if(substr($result[$key]["NoAkun"],0,1)=="1")
					{
						$this->fpdf->SetY(65+($no1*5));
						$this->fpdf->SetX(25);
						$this->fpdf->Cell(10,6,$result[$key]["IdKelompokGl"].' -',0,0,'L',0);
						$this->fpdf->Cell(75,6,$result[$key]["NamaAkun"],0,0,'L',0);
						$this->fpdf->Cell(30,6,$this->all_model->rp($saldo),0,0,'R',0);
						//$this->fpdf->Cell(25,6,45+($no4*5),0,0,'R',0);
						//$this->fpdf->Ln(0);
						$no1++;
						$totala+=$saldo;
					}
					else if(substr($result[$key]["NoAkun"],0,1)=="2")
					{		
						$this->fpdf->SetY(65+($no2*5));
						$this->fpdf->SetX(155);
						$this->fpdf->Cell(10,6,$result[$key]["IdKelompokGl"].' -',0,0,'L',0);
						$this->fpdf->Cell(75,6,$result[$key]["NamaAkun"],0,0,'L',0);
						$this->fpdf->Cell(30,6,$this->all_model->rp($saldo),0,0,'R',0);
						//$this->fpdf->Cell(25,6,45+($no4*5)+($no5*5),0,0,'R',0);
						//$this->fpdf->Ln(0);			
						$no2++;
						$totalc+=$saldo;
					}
					else if(substr($result[$key]["NoAkun"],0,1)=="3")
					{
						//$this->fpdf->SetY(60);
						$this->fpdf->SetY(80+($no2*5)+($no3*5));
						$this->fpdf->SetX(155);
						$this->fpdf->Cell(10,6,$result[$key]["IdKelompokGl"].' -',0,0,'L',0);
						$this->fpdf->Cell(75,6,$result[$key]["NamaAkun"],0,0,'L',0);
						$this->fpdf->Cell(30,6,$this->all_model->rp($saldo),0,0,'R',0);
						//$this->fpdf->Cell(25,6,65+($no4*5)+($no6*5),0,0,'R',0);
						$no3++;
						$totalb+=$saldo;
						//$this->fpdf->SetY(70);
					}		
									
				}
				
					//header Harta
					$this->fpdf->SetFont('Arial','B',10);
					$this->fpdf->SetY(60);
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(85,6,'Harta',0,0,'L',0);
					$this->fpdf->SetY(61);
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(85,6,'_______________________',0,0,'L',0);
					
					//headerutang		
					$this->fpdf->SetFont('Arial','B',10);					
					$this->fpdf->SetY(60);
					$this->fpdf->SetX(150);
					$this->fpdf->Cell(85,6,'Utang',0,0,'L',0);
					$this->fpdf->SetY(61);
					$this->fpdf->SetX(150);
					$this->fpdf->Cell(85,6,'_______________________',0,0,'L',0);
					
					//header Modal
					$this->fpdf->SetFont('Arial','B',10);
					$this->fpdf->SetY(70+($no2*5)+($no3*5));
					$this->fpdf->SetX(150);
					$this->fpdf->Cell(85,6,'Modal',0,0,'L',0);
					$this->fpdf->SetY(71+($no2*5)+($no3*5));
					$this->fpdf->SetX(150);
					$this->fpdf->Cell(85,6,'_______________________',0,0,'L',0);
					
					//LabaKotor totala
					$this->fpdf->SetFont('Arial','B',10);
					$this->fpdf->SetY(65+($no1*5));
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(90,6,'Total Harta',0,0,'L',0);
					$this->fpdf->Cell(40,6,$this->all_model->rp($totala),0,0,'R',0);
					$this->fpdf->SetY(61+($no1*5));
					$this->fpdf->SetX(20);
					$this->fpdf->Cell(130,6,'_______________________',0,0,'R',0);
					
					//total modal totalb
					$this->fpdf->SetY(80+($no2*5)+($no3*5));
					$this->fpdf->SetX(150);	
					$this->fpdf->Cell(90,6,'Total Modal',0,0,'L',0);			
					$this->fpdf->Cell(40,6,$this->all_model->rp($totalb),0,0,'R',0);
					$this->fpdf->SetY(76+($no2*5)+($no3*5));
					$this->fpdf->SetX(150);
					$this->fpdf->Cell(130,6,'_______________________',0,0,'R',0);
					
					//total utang totalc
					$this->fpdf->SetY(65+($no2*5));
					$this->fpdf->SetX(150);
					$this->fpdf->Cell(90,6,'Total Utang',0,0,'L',0);		
					$this->fpdf->Cell(40,6,$this->all_model->rp($totalc),0,0,'R',0);
					$this->fpdf->SetY(61+($no2*5));
					$this->fpdf->SetX(150);
					$this->fpdf->Cell(130,6,'_______________________',0,0,'R',0);
					
			}
			
			$this->fpdf->Ln();
			$this->fpdf->SetY(90+($no2*5)+($no3*5));
			$this->fpdf->SetX(20);
			$this->fpdf->Cell(90,6,'Grand Total',0,0,'L',0);
			//$this->fpdf->Cell(25,6,$totala,1,0,'R',0);
			$this->fpdf->Cell(40,6,$this->all_model->rp($totala),0,0,'R',0);
			$this->fpdf->Cell(90,6,'Grand Total',0,0,'L',0);
			$totald = $totalb+$totalc;
			$this->fpdf->Cell(40,6,$this->all_model->rp($totald),0,0,'R',0);
			
			if($totala!=$totald)
			{

				$this->fpdf->SetFont('Arial','',12);
				$this->fpdf->setTextColor(222,50,50);
				$this->fpdf->SetY(5);
				$this->fpdf->SetX(250);
				$this->fpdf->Cell(30,6,'Total Harta dibanding Utang dan Modal tidak Seimbang',0,0,'R',0);
					
			}
			$now = date("d F Y H:i:s");
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->setTextColor(0,0,0);
			$printer=$this->getDataPrinter($this->session->userdata("UserId"));
			$this->fpdf->SetY(190);
			$this->fpdf->SetX(20);			
			$this->fpdf->Cell(100,6,"Processor: ".$printer[0]["Nama"],0,0,'L');
			$this->fpdf->SetX(130);		
			$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
			$this->fpdf->Ln();
			$this->fpdf->Output('Laporan Laba Rugi'.date("F Y").'.pdf', 'I');
				
		}
		//$this->fpdf->SetX(30);
		//$this->fpdf->Cell(25,6,'',1,0,'C',0);
		//$this->fpdf->Cell(60,6,'Total',1,0,'C',0);
		//$this->fpdf->Cell(25,6,$totala,1,0,'R',0);
		//$this->fpdf->Cell(25,6,$this->all_model->rp($totala),1,0,'R',0);
		//$this->fpdf->Cell(25,6,$this->all_model->rp($totalb),1,0,'R',0);
		//buat footer
		
		
		
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Balance Sheet";
		//$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('balancesheet_view');
		$this->load->view('home_footer');
	}	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

