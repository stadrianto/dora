<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	
	
	public function getRole()
	{
		$join = array();
		$where = array(
			'a.ActiveYN'	=>"Y"
		);	
		$result = $this->all_model->get_data("IdRole as IdRole,NamaRole as NamaRole", "msrole a",$join, $where, array(),false);
		if(!$result){
			$result= "No Data";
		}
		echo json_encode($result);
		exit();
	}
	
	
	public function getData($perPage=5, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdRole' => $this->input->post('menu'),
			);
		}else{
			$search = array(
					'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
				array('table'=>'msrole b','field' => 'a.IdRole = b.IdRole','method'=>'Left'),
							);			
						
		$result = $this->all_model->get_data("IdUser as IdUser,Nama as Nama,NIP as NIP,Username as Username,Password as Password,Email as Email,a.IdRole as IdRole,NamaRole as NamaRole", "msuser a",$join, $where, $search, false, $perPage, $segmen, false,"IdUser","ASC");
		
		$result2 = $this->all_model->get_data("IdUser as IdUser,Nama as Nama,NIP as NIP,Username as Username,Password as Password,Email as Email,a.IdRole as IdRole,NamaRole as NamaRole", "msuser a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					//$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					//$result[$key]['BaseRate'] = $this->all_model->rp($result[$key]["BaseRate30s"]);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertUser(){
		
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'NIP' => $this->input->post('NIP'),	
		'Username' 	=> $this->input->post('Username'),
		'Password' => md5($this->input->post('Password')),
		'Email' => $this->input->post('Email'),
		'IdRole' => $this->input->post('IdRole'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("msuser", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function cekUsername(){
	
		$Username = $this->input->post('Username');

		$result = $this->all_model->query_data("SELECT * FROM msuser WHERE Username='".$Username ."'", true);
		if(!$result)
			echo "false";
		else{
			echo "true";
		}
		exit();
	}
	
	public function deleteUser()
	{
		$IdUser = $this->input->post('IdUser');
		$where = array('IdUser'=>$IdUser);
		$query = $this->all_model->delete_data("msuser", $where);
		echo json_encode($query);
		exit();
	}
	
	public function editUser(){
	
	if($this->input->post('Password') == "" || $this->input->post('Password') == null)
	{
		 $data = array(
		'Nama' => $this->input->post('Nama'),		
		'NIP' => $this->input->post('NIP'),	
		'Email' => $this->input->post('Email'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);	
	}
	else
	{
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'NIP' => $this->input->post('NIP'),	
		'Password' => md5($this->input->post('Password')),
		'Email' => $this->input->post('Email'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
	}
	
		$where = array(	
					'IdUser'=> $this->input->post('IdUser'),
				
		);	
		$query = $this->all_model->update_data("msuser", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	
	public function getDataRole()
	{	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdRole as IdRole,NamaRole as NamaRole", "msrole a",$join, $where, $search);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	

	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Master User";
		$data2['data'] = json_encode($this->getData($config['per_page'], $config['segmen'],false ));
		$data2['data3'] = json_encode($this->getDataRole());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('user_view',$data2);
		$this->load->view('home_footer');
	}
	
	
	
	public function addUser(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Tambah User";
		$data2['data3'] = json_encode($this->getDataRole());
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('addUser_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


