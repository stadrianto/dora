<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Supplier extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getSupplierCode(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdSupplier,4) as IdSupplier FROM mssupplier ORDER BY IdSupplier DESC", true);
		$result["IdSupplier"] = $result["IdSupplier"]+1;
		if($result["IdSupplier"] < 10)
			$result["IdSupplier"] = "S000".$result["IdSupplier"];
		else if($result["IdSupplier"]< 100)
			$result["IdSupplier"] = "S00".$result["IdSupplier"];
		else if($result["IdSupplier"]< 1000)
			$result["IdSupplier"] = "S0".$result["IdSupplier"];
		else
			$result["IdSupplier"] = "S".$result["IdSupplier"];
		return $result["IdSupplier"];
	}
	public function getDataNew($request = true){
		$search = array(
				
			);
		$where = array(
		
		);
		$join = array();
		$result2 = $this->all_model->get_data("IdSupplier as IdSupplier,Nama as Nama,Alamat as Alamat,Telepon as Telepon, Keterangan as Keterangan", "mssupplier a",$join, $where, $search, false);
		/* foreach($result2 as $key => $val){
				
		} */
		return $result2;
		echo json_encode($result2);		
		exit();
	}
	public function getData($perPage=15, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.Keterangan' => $this->input->post('menu'),
			);
		}else{
			$search = array(
					'a.Nama' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array();			
						
		$result = $this->all_model->get_data("IdSupplier as IdSupplier,Nama as Nama,Keterangan as Keterangan,Fax as Fax,ContactPerson as ContactPerson,JenisUsaha as JenisUsaha,Alamat as Alamat,Telepon as Telepon,Handphone as Handphone", "mssupplier a",$join, $where, $search, false, $perPage, $segmen, false,"IdSupplier","ASC");
		
		$result2 = $this->all_model->get_data("IdSupplier as IdSupplier,Nama as Nama,Alamat as Alamat,Fax as Fax,ContactPerson as ContactPerson,JenisUsaha as JenisUsaha,Telepon as Telepon,Handphone as Handphone,Keterangan as Keterangan", "mssupplier a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
				
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
			
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function insertSupplier(){
		
		$data = array(
		'IdSupplier'  => $this->getSupplierCode(),
		'Nama' => $this->input->post('Nama'),		
		'Alamat' => $this->input->post('Alamat'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'ActiveYN' => 'Y',
		'Keterangan' => $this->input->post('Keterangan'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("mssupplier", $data );
		
		echo json_encode($query);
		exit();
	}	
	

	public function editSupplier(){
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'Alamat' => $this->input->post('Alamat'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'Handphone' => $this->input->post('Handphone'),
		'Keterangan' => $this->input->post('Keterangan'),
		'JenisUsaha' => $this->input->post('JenisUsaha'),
		'ContactPerson' => $this->input->post('ContactPerson'),
		'Fax' => $this->input->post('Fax'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdSupplier'			=> $this->input->post('IdSupplier'),
				
		);	
		$query = $this->all_model->update_data("mssupplier", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function updateDataSupplier(){
		$data = array(
		'Nama' => $this->input->post('Nama'),		
		'Alamat' => $this->input->post('Alamat'),	
		'Telepon' 	=> $this->input->post('Telepon'),
		'Keterangan' => $this->input->post('Keterangan'),
		'ActiveYN' => 'Y',
		'UpdateId' => $this->session->userdata("UserId"),		
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdSupplier'			=> $this->input->post('IdSupp'),
				
		);	
		$query = $this->all_model->update_data("mssupplier", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	

	
	
	public function deleteSupplier()
	{
		$IdSupplier = $this->input->post('IdSupp');
		$where = array('IdSupplier'=>$IdSupplier);
		$query = $this->all_model->delete_data("mssupplier", $where);
		echo json_encode($query);
		exit();
	}
	
		
	public function getDetailSupplier($idSupplier)
	{
		$result = $this->all_model->query_data("SELECT * FROM mssupplier where IdSupplier='".$idSupplier."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function index(){
	/* if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh"); */
		$config['per_page'] = 15; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page']="supplier_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['data'] = json_encode($this->getDataNew(false));
		//$data['include']=$this->load->view('script','',true);
		$this->load->view('main',$data);
	}
	
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


