<?php
class All_model3 extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$otherdb = $this->load->database('dbwow', TRUE); 
	}
	

	function get_data($field, $table, $join = array(), $where= array(), $like = array(), $single = false,$perPage='', $uri='', $object = false, $index_field='', $order_by='DESC',$group_by = NULL)
	{
		$otherdb = $this->load->database('dbwow', TRUE); 
		$otherdb->select($field);
		if(count($like) > 0)
			$otherdb->or_like($like);
		$otherdb->from($table);
		if(count($join) > 0){
			foreach($join as $key => $value){
				$otherdb->join($join[$key]['table'], $join[$key]['field'],$join[$key]['method']);
			}
		}
		if(count($where) > 0)
			$otherdb->where($where); 
		if($group_by != NULL)
			$otherdb->group_by($group_by);
		if($index_field !='')
			$otherdb->order_by($index_field, $order_by); 
		if($perPage!='' )
			$query =  $otherdb->get('', $perPage, $uri);
		else
			$query = $otherdb->get('');
		if($query->num_rows()>0){
			if($single){
				if($object)
					return $query->row();
				else
					return $query->row_array();
			}else{
				if($object)
					return $query->result();
				else
					return $query->result_array();
			}
		}else
			return false;
	}
	function query_data($query_code, $single = false)
	{
		$otherdb = $this->load->database('dbwow', TRUE); 
		$query = $otherdb->query($query_code);
		if($query->num_rows()>0){
			if ($single) return $query->row_array();
			else return $query->result_array();
		}else
			return false;
	}
	function delete_data($table, $where)//where is an array
	{
		$otherdb = $this->load->database('dbwow', TRUE); 
		return $otherdb->delete($table, $where);	
	}
	
	function sp_delete_data($namesp, $where)//where is an array
	{
		$otherdb = $this->load->database('dbwow', TRUE); 
		$sql_sp = "CALL ".$namesp." (";
		for($x = 0;$x<count($where);$x++)
		{
		    $key=key($where);
			$val=$where[$key];
			if($x != count($where)-1)
			{
				$sql_sp .= "'$val',";
			}
			else $sql_sp .= "'$val')";
			
			next($where);
		}
					
		$query=$otherdb->query($sql_sp);
		return $query;
		
	}
	
	function sp_insert_data($namesp, $data)//data is an array
	{
		$otherdb = $this->load->database('dbwow', TRUE); 
		$sql_sp = "CALL ".$namesp." (";
		for($x = 0;$x<count($data);$x++)
		{
		    $key=key($data);
			$val=$data[$key];
			if($x != count($data)-1)
			{
				$sql_sp .= "'$val',";
			}
			else $sql_sp .= "'$val')";
			
			next($data);
		}
					
		$query=$otherdb->query($sql_sp);
		return $query;
		
	}
	
	function sp_exec_data($namesp, $data)//data is an array
	{
		$otherdb = $this->load->database('dbwow', TRUE); 
		$sql_sp = "CALL ".$namesp." (";
		for($x = 0;$x<count($data);$x++)
		{
		    $key=key($data);
			$val=$data[$key];
			if($x != count($data)-1)
			{
				$sql_sp .= "'$val',";
			}
			else $sql_sp .= "'$val')";
			
			next($data);
		}
					
		$query=$otherdb->query($sql_sp);
		return $query;
		
	}
	
	function sp_query_data($namesp,$single = false)
	{
		$otherdb = $this->load->database('dbwow', TRUE);	
		$sql_sp="CALL ".$namesp."()";
		$query=$otherdb->query($sql_sp);
		if($query->num_rows()>0){
			if ($single) return $query->row_array();
			else return $query->result_array();
		}else
			return false;
	}
	
	function sp_getdata($namesp)
	{
		$otherdb = $this->load->database('dbwow', TRUE);	
		$sql_sp="CALL ".$namesp."()";
		$query=$otherdb->query($sql_sp);
		return $query->result();
	}
	
	function sp_getdata2($namesp, $data)//data is an array
	{
		$otherdb = $this->load->database('dbwow', TRUE); 
		$sql_sp = "CALL ".$namesp." (";
		for($x = 0;$x<count($data);$x++)
		{
		    $key=key($data);
			$val=$data[$key];
			if($x != count($data)-1)
			{
				$sql_sp .= "'$val',";
			}
			else $sql_sp .= "'$val')";
			
			next($data);
		}
					
		$query=$otherdb->query($sql_sp);
		return $query->result();
		
	}
	
	function insert_data($table, $data){
		$otherdb = $this->load->database('dbwow', TRUE); 
		return $otherdb->insert($table,$data);
	}
	function update_data($table, $data, $where){
		$otherdb = $this->load->database('dbwow', TRUE); 
		$otherdb->where($where); 
		return $otherdb->update($table,$data);  
	}

}
?>