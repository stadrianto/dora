<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class laporanhpp extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function getPrintProject($Periode,$Periode2)
	{
		$result = $this->all_model->query_data("SELECT * from project a join trorder b on a.NoOrder = b.NoOrder WHERE TanggalProject >= '".$Periode."' AND TanggalProject <= '".$Periode2."' AND b.Status='Finish Project'", false);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
public function getNeeds($noproject)
	{
		$totalhpp = 0;
		$search = array(
		
			);
		$join = array(
				array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),
		);
		$where = array(
				'a.NoProject' => $noproject
		);
			
		$result = $this->all_model->get_data("Id as Id,NoProject,a.IdBarang as IdBarang,b.Nama as Nama,b.Jumlah as Harga,a.Jumlah as Jumlah,b.Quantity as Quantity","projectneeds a",$join, $where, $search);

	
		
		if($result)
		{
			foreach($result as $key => $value){
			$result[$key]['Harga'] = $result[$key]['Harga'] / $result[$key]['Quantity'];
			$totalhpp+=$result[$key]['Jumlah']*$result[$key]['Harga'];
		
			/*
				$result[$key]["NamaItem"] = str_replace("\"", "\"", $result[$key]["NamaItem"]);
				$result[$key]["NamaItem"] = str_replace("'", "\'", $result[$key]["NamaItem"]);
				
				
				$total+= $result[$key]['BiayaProject'];
				$result[$key]['BiayaProject'] = $this->all_model->rp($result[$key]['BiayaProject']);
	*/
			}	
			return $totalhpp;
		}
		else
		{
			return "No Data";
		}
	
	}
	
	public function printLaporanProject(){
	
		$Periode = $this->input->post('dob2');
		$Periode2 = $this->input->post('dob');
		
		$Periode=substr($Periode,6,10)."-".substr($Periode,0,2)."-".substr($Periode,3,2);
		$Periode2= substr($Periode2,6,10)."-".substr($Periode2,0,2)."-".substr($Periode2,3,2);
		
			$result = $this->getPrintProject($Periode,$Periode2);
			
			$this->load->library('fpdf17/fpdf');
			
			//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
			$this->fpdf->FPDF('P','mm','A4');
			$this->fpdf->Open();
			$this->fpdf->SetAutoPageBreak(false);
			$this->fpdf->AddPage('L');
			$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
			$this->fpdf->Image('images/cips_nama.png',130,20,50,0,'','http://www.cips.or.id/'); 
			$this->fpdf->Ln(30);
			$this->fpdf->SetX(142);
			$this->fpdf->SetFont('Arial','BU',15);
			
			$this->fpdf->Cell(30,6,'Laporan Project',0,0,'C');
			$this->fpdf->Ln();
			$this->fpdf->SetX(135);
			$this->fpdf->Cell(40,6,'Untuk Periode '.$Periode.' Hingga '.$Periode2.'',0,0,'C');
			$this->fpdf->Ln(10);
			$this->fpdf->SetFont('Arial','BU',12);
			//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
			$this->fpdf->Ln(10);

			$y_axis_initial = 60;
			$this->fpdf->SetFont('Arial','',10);
			$this->fpdf->setFillColor(222,222,222);
			$this->fpdf->SetY($y_axis_initial);
			$this->fpdf->SetX(40);
			//Header tabel halaman 1
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(25,6,'No Project',1,0,'C',1);
			$this->fpdf->Cell(50,6,'Nama Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Tanggal Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Nilai Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'NIlai HPP',1,0,'C',1);	
			$this->fpdf->Ln();
			$max=15;//max baris perhalaman
			$i=0;
			$no=0;
			$total=0;
			$row_height = 6;//tinggi tiap2 cell/baris
			$y_axis = $y_axis_initial + $row_height;
			$date = date("Y-m-d");
			$grandtotal = 0;
			if($result != "No Data")
			{
			foreach($result as $key => $value){
			//$total += $row['Total'];

			if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
			$this->fpdf->AddPage();
			$this->fpdf->SetY(10);
			$this->fpdf->SetX(40);
			$this->fpdf->CELL(10,6,'No',1,0,'C',1);
			$this->fpdf->Cell(25,6,'No Project',1,0,'C',1);
			$this->fpdf->Cell(50,6,'Nama Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Tanggal Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Nilai Project',1,0,'C',1);
			$this->fpdf->Cell(45,6,'Nilai HPP',1,0,'C',1);	
			$this->fpdf->SetY(10);
			$this->fpdf->SetX(55);
			$y_axis = $y_axis + $row_height;
			$i=0;
			$this->fpdf->Ln();

			}
		$temp = 0;	
		$temp = $this->getNeeds($result[$key]["NoProject"]);
		$grandtotal+=($temp);
		$i++;
		$no++;
		$this->fpdf->SetX(40);
		$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["NoProject"],1,0,'C',0);
		$this->fpdf->Cell(50,6,$result[$key]["NamaProject"],1,0,'C',0);	
		$this->fpdf->Cell(45,6,date("d-m-Y",strtotime($result[$key]["TanggalProject"])),1,0,'C',0);
		$this->fpdf->Cell(45,6,$this->all_model->rp($result[$key]["BiayaProject"]),1,0,'C',0);
		$this->fpdf->Cell(45,6,$this->all_model->rp($temp),1,0,'C',0);	
			$this->fpdf->Ln();

			}
			}
			
				//buat footer
				
		$now = date("d F Y H:i:s");
		$this->fpdf->Ln(10);

		$this->fpdf->SetFont('Arial','B',12);
		$this->fpdf->Cell(248,6,"Total HPP Project           : ".$this->all_model->rp($grandtotal)."",0,0,'R');
		$this->fpdf->SetX(40);		
		$this->fpdf->SetY(280);	
		$this->fpdf->Cell(90,6,"Printed By: ".$this->session->userdata("Nama"),0,0,'');
		$this->fpdf->SetX(50);			
		$this->fpdf->Cell(250,6,"Print Date: ".$now,0,0,'C');
		$this->fpdf->Ln();
		
		$this->fpdf->Output('Laporan Proyek'.date("F Y").'.pdf', 'I');
		
	}
	
	public function index(){
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		
		$data['title']="CIPS - Laporan Project";
		$data['page_title']="CIPS - Laporan Project";
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('laporanhpp_view','');
		$this->load->view('home_footer');
	}
		
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}

