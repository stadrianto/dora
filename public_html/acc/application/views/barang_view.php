		<div id="page-wrapper">
		<script type="text/javascript">
			/* $(document).ready(function(){
			$(".homenav").html( data.msg ).addClass('selected')			
			});		 */	
		</script>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Master Barang <small>Data Barang</small></h1>                    
                </div>
            </div>	 
            <div class="row">
                <div class="col-lg-12">
                    <p>
					<button onclick="setAddNew()" class="btn btn-primary" type="button">Add New Barang</button>
					<button onclick="showJenis()" class="btn btn-primary" type="button">Show Kategori</button>
					</p>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Barang List </h3>
                        </div>
                        <div class="panel-body">
                            <div id="grid"></div>				
                        </div>
                    </div>
                </div>
            </div>			 			
        </div>
    </div>
<div id="dialog" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes01" id="tes01" class="panel-title"><i class="fa fa-bar-chart-o"></i> Barang </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace" class="">
							
						</div>
						<form method="post" id="fm2" name="fm2">
							<div class="form-group">
								<label>ID Barang</label>
								<input id="barangid" name="barangid" disabled="true" value="" class="form-control">						
							</div>							
							<div class="form-group">
								<label>Nama Barang</label>
								<input id="barangname" name="barangname" class="form-control">				
							</div>
							<div class="form-group">
								<label>Kategori</label>
								<div id="akunarea"></div>			
							</div>
							<div class="form-group">
								<label>Quantity</label>
								<input id="qty" name="qty" class="form-control" >
							</div>
							<div class="form-group">
								<label>Harga Jual</label>
								<input id="harga" name="harga" class="form-control">	
							</div>
							<div align="center" class="form-group">
								<button id="btnSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>
							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div style="z-index:2000" id="dialogAddJenis" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes02" id="tes02" class="panel-title"><i class="fa fa-bar-chart-o"></i> Jenis Barang </h3>
					</div>
					<div id="test1" class="panel-body">
						<div id="errorplace2" class="">
							
						</div>
						<form method="post">
							<div class="form-group">
								<label>ID Kategori</label>
								<input id="kategoriid2" name="kategoriid2" disabled="true" value="" class="form-control">						
							</div>							
							<div class="form-group">
								<label>Nama Kategori</label>
								<input id="kategoriname2" name="kategoriname2" class="form-control">				
							</div>							
							<div align="center" class="form-group">
								<button id="katSave" type="button" class="btn btn-default">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>					
							</div>							
						</form>			
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="dialogDelete" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes03" id="tes03" class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Customer </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-6" style="float:none;">
							<h4>Yakin Hapus Data Ini?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Id Barang</th>
									<th>Nama Barang</th>
									<th>jenis Barang</th>
									<th>Quantity Barang</th>
									<th>Harga Barang</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="deleteid"></div></td>
									<td><div id="deletenama"></div></td>
									<td><div id="deletejenis"></div></td>
									<td><div id="deletequantity"></div></td>
									<td><div id="deleteharga"></div></td>
								</tr>							
							</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn" type="submit" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="z-index:2000" id="dialogDeleteJenis" class="modal">
	<div id="page-wrapper">		
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes04" id="tes04" class="panel-title"><i class="fa fa-bar-chart-o"></i> Delete Kategori </h3>
					</div>					
					<div class="panel-body">
						<div align="left" class="col-lg-6" style="float:none;">
							<h4>Yakin Hapus Data Ini?</h4>
						</div>
						<form>
							<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Id Kategori</th>
									<th>Nama Kategori</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div id="deleteid2"></div></td>
									<td><div id="deletenama2"></div></td>
								</tr>							
							</tbody>							
							</table>
							<div align="center">
								<button id="deleteBtn2" type="submit" class="btn btn-default">Delete</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="z-index:1999" id="dialogJenis" class="modal">
	<div id="page-wrapper">
		<div align="center" class="row">
			<div align="left" class="col-lg-6" style="float:none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 name="tes05" id="tes05" class="panel-title"><i class="fa fa-bar-chart-o"></i> Jenis Barang</h3>
					</div>					
					<div class="panel-body">
						<div>
							<p>
								<button onclick="setAddNewJenis()" class="btn btn-primary" type="button">Add New Kategori</button>
							</p>
						</div>					
						<div id="grid2"></div>					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>media/js/jquery.ajax.form.js"></script>
    <!-- /#wrapper -->
	<script>
	$(document).ready(function () {
		var hasil3 = JSON.parse('<?php echo $kategori; ?>');
		var text = '';
		if(hasil3 == "No Data"){
			$('#akunarea').html("No Active Departemen");
		}else{
			text+= '<select class="form-control" onchange="" name="kategori" id="kategori">';
			text+='<option value="0">Select</option>';
			for(i=0;i<hasil3.length;i++){
				
				text+= '<option value="'+hasil3[i].IdKategori+'">'+hasil3[i].Jenis+'</option>'	
			}
			text+= '</select>';
			$('#akunarea').html(text);
		
		}
		/* $("#test1").on("click", "#btnSave", function(){			
		}); */
		$('#tes01').click(function(e) {
			$('#dialog').modal('hide');
		});
		$('#tes02').click(function(e) {
			$('#dialogAddJenis').modal('hide');
		});
		$('#tes03').click(function(e) {
			$('#dialogDelete').modal('hide');
		});
		$('#tes04').click(function(e) {
			$('#dialogDeleteJenis').modal('hide');
		});
		$('#tes05').click(function(e) {
			$('#dialogJenis').modal('hide');
		});
	});
	function showJenis(){
		$('#dialogJenis').modal('show');
	}
	function addNewJenis(){
		if($('#kategoriname2').val()== "" ){
				$("#errorplace2").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Kategori Harus diisi</p>");
				$("#errorplace2").addClass("alert alert-dismissable alert-warning");
				return;
		}
		
	
		var data= "Nama="+$('#kategoriname2').val();
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>barang/insertKategori",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
			});			
	}
	function addNew(){
		if($('#barangname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Barang Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#kategori').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Kategori Barang harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#qty').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Quantity Barang harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#harga').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Harga Barang harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
	
		var data= "Nama="+$('#barangname').val()+"&Kategori="+$('#kategori').val()+"&Qty="+$('#qty').val()+"&Harga="+$('#harga').val();
		//alert(data);
			$.ajax({				
			url : "<?php echo base_url()?>barang/insertBarang",
			type : "post",
			data : data,	
			dataType : "json",
			beforeSubmit : function(){
				setTimeout(function(){
						alert("wahaha before");
					}, 2000);
			},	
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){		
					
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("wahaha2");
					}, 2000);
				}
			}	
			});			
	}
	
	function updateDataKategori(){
		if($('#kategoriname2').val()== "" ){
				$("#errorplace2").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Kategori Harus diisi</p>");
				$("#errorplace2").addClass("alert alert-dismissable alert-warning");
				return;
		}
		var data= "IdKategori="+$('#kategoriid2').val()+"&Nama="+$('#kategoriname2').val();
			$.ajax({				
			url : "<?php echo base_url()?>barang/editKategori",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
			
	}
	
	function updateData(){
		if($('#barangname').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Nama Barang Harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#kategori').val()== "0" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Kategori Barang harus dipilih</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#qty').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Quantity Barang harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		else if($('#harga').val()== "" ){
				$("#errorplace").html("<button type=button' class='close' data-dismiss='alert'>&times;</button><h4>Warning!</h4>                <p>Harga Barang harus diisi</p>");
				$("#errorplace").addClass("alert alert-dismissable alert-warning");
				return;
		}
		var data= "IdBarang="+$('#barangid').val()+"&Nama="+$('#barangname').val()+"&Kategori="+$('#kategori').val()+"&Qty="+$('#qty').val()+"&Harga="+$('#harga').val();
			$.ajax({				
			url : "<?php echo base_url()?>barang/editBarang",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
			
	}
	function deleteData(id){		
			var data= "IdBarang="+id;
			$.ajax({				
			url : "<?php echo base_url()?>barang/deleteBarang",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	function deleteDataKategori(id){		
			var data= "IdKategori="+id;
			$.ajax({				
			url : "<?php echo base_url()?>barang/deleteKategori",
			type : "post",
			data : data,	
			dataType : "json",				
			success : function(msg){
				var hasil = JSON.parse(msg);
				if ( hasil == true || hasil == "true"){						
					location.reload();
				}
				else {
					
					setTimeout(function(){
						alert("gagal");
					}, 2000);
				}
			}	
			});
	}
	function setAddNew(){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#barangname').val('');
		$('#kategori').val('0');
		$('#qty').val('');
		$('#harga').val('');
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","addNew()");			
	}
	function setAddNewJenis(){
		$("#errorplace2").html("");
		$("#errorplace2").removeClass("alert alert-dismissable alert-warning");
		$('#kategoriid2').val('');
		$('#kategoriname2').val('');
		$('#dialogAddJenis').modal('show');
		$('#katSave').attr("onclick","addNewJenis()");			
	}
	function setUpdateKategori(id,nama){
		$("#errorplace2").html("");
		$("#errorplace2").removeClass("alert alert-dismissable alert-warning");
		$('#kategoriid2').val(id);
		$('#kategoriname2').val(nama);
		$('#dialogAddJenis').modal('show');
		$('#katSave').attr("onclick","updateDataKategori()");		
	}
	function setDeleteKategori(id,nama){
		$('#dialogDeleteJenis').modal('show');
		$('#deleteid2').html(id);
		$('#deletenama2').html(nama);
		$('#deleteBtn2').attr("onclick","deleteDataKategori('"+id+"')");			
	}
	/* function setUpdate(idcust,namacust,alamatcust,telpcust){
		$('#dialog').modal('show');
		$('#custid').val(idcust);
		$('#custname').val(namacust);
		$('#custaddress').val(alamatcust);
		$('#custphone').val(telpcust);
		$('#btnSave').attr("onclick","updateData()");			
	} */
	function setUpdate(id,nama,jenis,quantity,harga){
		$("#errorplace").html("");
		$("#errorplace").removeClass("alert alert-dismissable alert-warning");
		$('#barangid').val(id);
		$('#barangname').val(nama);
		$('#kategori').val("0");
		$('#qty').val(quantity);
		$('#harga').val(harga);
		$('#dialog').modal('show'); 
		$('#btnSave').attr("onclick","updateData()");			
	}
	function setDelete(id,nama,jenis,quantity,harga){
		$('#dialogDelete').modal('show');
		$('#deleteid').html(id);
		$('#deletenama').html(nama);
		$('#deletejenis').html(jenis);
		$('#deletequantity').html(quantity);
		$('#deleteharga').html(harga);
		$('#deleteBtn').attr("onclick","deleteData('"+id+"')");			
	}
	
	/* $(document).ready(function () { 
		var hasil = JSON.parse('<?php echo $data; ?>');	
	}); */
	</script>
    <script type="text/javascript">	
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
				hasil = <?=$data?>,
				kategori = <?=$kategori?>;
			if(kategori.length>0)
			{
				$("#grid2").shieldGrid({
					dataSource: {
						data: kategori
					},
					sorting: {
						multiple: true
					},
					rowHover: false,
					paging: {
						pageSize:5					
					},
					selection: 
					{
						type: "cell",
						multiple: false,
						toggle: false
					},
					events: {
						selectionChanged: onSelectedChanged,
						doubleClick: onDouble
					},				
					columns: [
					{ field: "IdKategori", width: "170px", title: "IdKategori" },
					{ field: "Jenis", title: "Jenis"},
					{
						width: "25%",
						title: " ",
						buttons: [						
							{
								cls: "button button-primary",
								caption: " Edit",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid2").swidget().dataItem(rowIndex).IdKategori;
									var nama=$("#grid2").swidget().dataItem(rowIndex).Jenis;
									setUpdateKategori(id,nama);
								}
							},
							{
								cls: "mybuttonCssClass",
								caption: " Delete",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid2").swidget().dataItem(rowIndex).IdKategori;
									var nama=$("#grid2").swidget().dataItem(rowIndex).Jenis;
									setDeleteKategori(id,nama);
								}
							}
						]
					}
					]
				});            
			}
            if(hasil.length>0)
			{
				$("#grid").shieldGrid({
					dataSource: {
						data: hasil
					},
					sorting: {
						multiple: true
					},
					rowHover: false,
					paging: true,
					selection: 
					{
						type: "cell",
						multiple: false,
						toggle: false
					},
					events: {
						selectionChanged: onSelectedChanged,
						doubleClick: onDouble
					},				
					columns: [
					{ field: "IdBarang", width: "170px", title: "IdBarang" },
					{ field: "Nama", title: "Nama"},
					{ field: "Jenis", title: "JenisBarang" },             
					{ field: "Quantity", width: "50px", title: "Qty" },
					{ field: "Harga", title: "Harga Jual" },
					{
						width: "12%",
						title: " ",
						buttons: [						
							{
								cls: "button button-primary",
								caption: " Edit",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).IdBarang;
									var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
									var jenis=$("#grid").swidget().dataItem(rowIndex).Jenis;
									var quantity=$("#grid").swidget().dataItem(rowIndex).Quantity;
									var harga=$("#grid").swidget().dataItem(rowIndex).Harga;
									setUpdate(id,nama,jenis,quantity,harga);
								}
							},
							{
								cls: "mybuttonCssClass",
								caption: " Delete",
								commandName: "details", // build in - edit, delete
								click: function(rowIndex) {
									//alert(rowIndex);
									var id=$("#grid").swidget().dataItem(rowIndex).IdBarang;
									var nama=$("#grid").swidget().dataItem(rowIndex).Nama;
									var jenis=$("#grid").swidget().dataItem(rowIndex).Jenis;
									var quantity=$("#grid").swidget().dataItem(rowIndex).Quantity;
									var harga=$("#grid").swidget().dataItem(rowIndex).Harga;
									setDelete(id,nama,jenis,quantity,harga);
								}
							}
						]
					}
					]
				});
			}
        });
		
		function onSelectedChanged(e) {
			//alert("ganti");
			/* var selected = e.target.contentTable.find(".sui-selected");
			if (selected.length > 0) {
				$("#grid").swidget().select("td:eq(0)");
                alert(selected.get(0).innerHTML);
            } */
		}
		
		function onDouble(e) {
			//alert("ganti1");
		}
    </script>
</body>
</html>
