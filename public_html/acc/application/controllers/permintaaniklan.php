<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PermintaanIklan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('uri','session'));
		$this->load->model('all_model');
		$this->load->model('all_model2');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getBenefit($notransaksi)
	{
		$search = array(
		
			);
		$join = array(
			array('table'=>'m_creativeitem b','field' => 'a.TIBD_CreativeItem = b.MCI_KodeItem','method'=>'Left'),
		);
		$where = array(
				'a.TIBD_NoTransaksi' => $notransaksi,
		);
			
		$result = $this->all_model->get_data("TIBD_NoTransaksi as NoTransaksi,TIBD_CreativeItem as KodeItem,TIBD_Qty as Qty,MCI_NamaItem as NamaItem,MRC_Harga as Harga","t_iklanmkt_benefit_detail a",$join, $where, $search);
		foreach($result as $key => $value){
			$result[$key]["NamaItem"] = str_replace("\"", "@@@", $result[$key]["NamaItem"]);
			$result[$key]["NamaItem"] = str_replace("'", "~~~", $result[$key]["NamaItem"]);

		
		}
		if($result)
		return $result;
	}
	
	public function getProduk($notransaksi)
	{
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.TIPD_NoTransaksi' => $notransaksi,
		);
			
		$result = $this->all_model->get_data("TIPD_KodeProduk as KodeProduk,Itm_NamaProduk as NamaProduk,Itm_DivNama as NamaDivisi,TIPD_Budget as Budget","t_iklanmkt_produk_detail a",$join, $where, $search);
	
		foreach($result as $key => $value){
			$result[$key]['Budget'] = $this->all_model->rp($result[$key]['Budget']);
		}
		if($result)
		return $result;
	}
		
	public function getDetailIklan($notransaksi)
	{
			 
		$search = array(
		
			);
		$join = array(

		);
		$where = array(
				'a.TIH_AktifYN'	=>"Y",
				'a.TIH_NoTransaksi'	=> $notransaksi,
		);
			
			
		$result = $this->all_model->get_data("TIH_NoTransaksi as NoTransaksi,MHA_NamaAcara as ProgramAcara,MJP_NamaPlacement as Placement,TIH_BudgetTotal as BudgetTotal,TIH_StartTayang as StartTayang,TIH_EndTayang as EndTayang,TIH_CloseTime as CloseTime,TIH_Episode as Episode", "t_iklanmkt_header a",$join, $where, $search, false);
		
		if($result)
		{
			$result2 = $this->getProduk($result[0]["NoTransaksi"]);
			$result3 = $this->getBenefit($result[0]["NoTransaksi"]);
			$result[0]['BudgetTotal'] = $this->all_model->rp($result[0]['BudgetTotal']);
			$dt = strtotime($result[0]['StartTayang']);
			$result[0]['StartTayang'] = date("d-M-Y",$dt);
			$dt2 = strtotime($result[0]['EndTayang']);
			$result[0]['EndTayang'] = date("d-M-Y",$dt2);
			
			$dt3 = strtotime($result[0]['CloseTime']);
			$result[0]['CloseTime'] = date("d-M-Y",$dt3);
			if(date("Y-m-d H:i:s",time()) > date("Y-m-d H:i:s",$dt3) ) $result[0]['Status'] = "Close Pitching";
			else $result[0]['Status'] = "Open Pitching";
			$result[0]['Produk'] = $result2;
			$result[0]['Benefit'] = $result3;
		}
		return $result;
	
	}
	
	public function detail($notransaksi)
	{
		if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$data['title']="Media Planning Pharos";
		$data['include']=$this->load->view('script','',true);
		$data['page_title']="Media Planning Pharos";
		
		$result = $this->getDetailIklan($notransaksi);

		$data2['data'] = json_encode($result);
		$this->load->view('home_header',$data);
		$this->load->view('detailpermintaan_view',$data2);
		$this->load->view('home_footer');
	}
	
	public function getPermintaanIklan($perPage=5, $segmen=0, $request = true){
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		$search = array(
		'TIH_NoTransaksi' => $this->input->post('search')
			);
		$join = array(

		);
		$where = array(
				'a.TIH_AktifYN'	=>"Y",
		);

		$result = $this->all_model->get_data("TIH_NoTransaksi as NoTransaksi,MHA_NamaAcara as ProgramAcara,MJP_NamaPlacement as Placement,TIH_BudgetTotal as BudgetTotal,TIH_StartTayang as StartTayang,TIH_EndTayang as EndTayang,TIH_Episode as Episode", "t_iklanmkt_header a",$join, $where, $search, false, $perPage, $segmen, false,"TIH_NoTransaksi","DESC");
		$result2 = $this->all_model->get_data("TIH_NoTransaksi as NoTransaksi,MHA_NamaAcara as ProgramAcara,MJP_NamaPlacement as Placement,TIH_BudgetTotal as BudgetTotal,TIH_StartTayang as StartTayang,TIH_EndTayang as EndTayang,TIH_Episode as Episode", "t_iklanmkt_header a",$join, $where, $search, false);
		
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getPermintaanIklan($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
				
				$result[$key]['BudgetTotal'] = $this->all_model->rp($result[$key]['BudgetTotal']);
				$dt = strtotime($result[$key]['StartTayang']);
				$result[$key]['StartTayang'] = date("d-M-Y",$dt);
				$dt2 = strtotime($result[$key]['EndTayang']);
				$result[$key]['EndTayang'] = date("d-M-Y",$dt2);
				}		
		
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				$result[0]["Search"] = "";
				$total = count($result2);
				$page = ceil($total/$perPage);
				if($this->input->post('search')!="")	
					$result[0]["Search"] = $this->input->post('search');
				
				for($i=1;$i<=$page;$i++){
					if($i==$current)
					$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
					else
					$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getPermintaanIklan($i)@@@>$i</a>&nbsp;&nbsp;";
				}
				foreach($result as $key => $value){
					$result[$key]['BudgetTotal'] = $this->all_model->rp($result[$key]['BudgetTotal']);
					$dt = strtotime($result[$key]['StartTayang']);
					$result[$key]['StartTayang'] = date("d-M-Y",$dt);
					$dt2 = strtotime($result[$key]['EndTayang']);
					$result[$key]['EndTayang'] = date("d-M-Y",$dt2);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	
	public function index(){
		if($this->session->userdata("UserId")=="")
			redirect(base_url(). "home","refresh");
		$config['per_page'] = 5; 
		$config['segmen'] = 0;
		$data['title']="Permintaan Iklan";
		$data['page_title']="Media Planning Pharos";
		$data2['data'] = json_encode($this->getPermintaanIklan($config['per_page'], $config['segmen'],false ));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('permintaaniklan_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


