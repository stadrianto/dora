<?php
	//ini buat dia beli barang
	{
		public function getTransaksiCode(){
		$result = $this->all_model->query_data("SELECT IdTransaksi as IdTransaksi FROM Transaksi ORDER BY IdTransaksi DESC", true);
		$result["IdTransaksi"] = $result["IdTransaksi"]+1;
		
		return $result["IdTransaksi"];
	}
		public function insertPembelian(){
			
			//masukin data ke tabel transaksi
			$kode=$this->getTransaksiCode(),
			$data = array(
			'NoTransaksi'  => $this->input->post('IdPembelian'),
			'IdTransaksi' => $kode,
			'IdJenisTransaksi' => '1'
			);
			$query = $this->all_model->insert_data("transaksi", $data );
			
			$data = array(
			'NoJurnal' => $this->input->post('NoJurnal'),
			'TanggalTransaksi' => $this->input->post('TanggalTransaksi'),
			'Deskripsi' => 'Pembelian dari '.$this->input->post('NamaSupplier'). ' NoFaktur: '.$this->input->post('NoFaktur'),
			'Status' => 'Pending',
			'IdTransaksi' => $kode,
			'IdJenisJurnal' => '1',
			'TanggalInput' => date("Y-m-d H:i:s"),
			'Validator' => '',
			'NIP' => $this->session->userdata("UserId"),
			'ActiveYN' => 'Y'
			);
			$query = $this->all_model->insert_data("jurnal", $data );
		}	
		
		public function kasKeluar($totaluang)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor kas dia',
			'Debit' => '0',
			'Kredit' => $this->input->post($totaluang.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
		public function barangMasuk($nominalbarang)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor inventory dia',
			'Kredit' => '0',
			'Debit' => $this->input->post($nominalbarang.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
		//ini klo beli kredit
		public function utangPerusahaan($sisabayar)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor utang dia',
			'Debit' => '0',
			'Kredit' => $this->input->post($sisabayar.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
	}
	
	
	
	
	
	
	//ini buat klo dia ada project
	{		
		public function insertProject(){
			
			//masukin data ke tabel transaksi
			$kode=$this->getTransaksiCode();
			$data = array(
			'NoTransaksi'  => $this->input->post('NoFaktur'),
			'IdTransaksi' => $kode,
			'IdJenisTransaksi' => '2'
			);
			$query = $this->all_model->insert_data("transaksi", $data );
			
			$data = array(
			'NoJurnal' => $this->input->post('NoJurnal'),
			'TanggalTransaksi' => $this->input->post('TanggalTransaksi'),
			'Deskripsi' => 'Penjualan ke '.$this->input->post('NamaCustomer'). ' NoFaktur: '.$this->input->post('NoFaktur'),
			'Status' => 'Pending',
			'IdJenisJurnal' => '1',
			'IdTransaksi' => $kode,
			'TanggalInput' => date("Y-m-d H:i:s"),
			'Validator' => '',
			'NIP' => $this->session->userdata("UserId"),
			'ActiveYN' => 'Y'
			);
			$query = $this->all_model->insert_data("jurnal", $data );
		}
		public function hargaPokokJual($nominalbarang)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor HPP dia',
			'Kredit' => '0',
			'Debit' => $this->input->post($nominalbarang.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
		public function barangKeluar($nominalbarang)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor inventory dia',
			'Debit' => '0',
			'Kredit' => $this->input->post($nominalbarang.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
		
		public function insertPendapatan($totaljual)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor pendapatan dia',
			'Kredit' => '0',
			'Debit' => $this->input->post($totaljual.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
		
		public function kasMasuk($totaluang)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor kas dia',
			'Kredit' => '0',
			'Debit' => $this->input->post($totaluang.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
		//ini klo jual kredit
		public function piutangCustomer($sisabayar)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor piutang dia',
			'Debit' => '0',
			'Kredit' => $this->input->post($sisabayar.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
	}
	
	//bayar pengeluaran
	{
		public function insertPengeluaran(){
			
			//masukin data ke tabel transaksi
			$kode=$this->getTransaksiCode();
			$data = array(
			'NoTransaksi'  => $this->input->post('IdBayar'),
			'IdTransaksi' => $kode,
			'IdJenisTransaksi' => '3'
			);
			$query = $this->all_model->insert_data("transaksi", $data );
			
			$data = array(
			'NoJurnal' => $this->input->post('NoJurnal'),
			'TanggalTransaksi' => $this->input->post('TanggalTransaksi'),
			'Deskripsi' => 'Bayar '.$this->input->post('NamaPengeluaran'). ' NoKwitansi: '.$this->input->post('NoKwitansi'),
			'IdTransaksi' => $kode,
			'Status' => 'Pending',
			'IdJenisJurnal' => '1',
			'TanggalInput' => date("Y-m-d H:i:s"),
			'Validator' => '',
			'NIP' => $this->session->userdata("UserId"),
			'ActiveYN' => 'Y'
			);
			$query = $this->all_model->insert_data("jurnal", $data );
		}
		
		public function pengeluaranDibayar($totalbayar)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor beban dia',
			'Debit' => '0',
			'Kredit' => $this->input->post($totalbayar.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
		public function kasKeluar($totaluang)
		{
			$data = array(
			'NoJurnal'  => $this->input->post('NoJurnal'),
			'NoAkun' => 'ini tergantung nomor kas dia',
			'Debit' => '0',
			'Kredit' => $this->input->post($totaluang.''),		
			);
			$query = $this->all_model->insert_data("detailjurnal", $data );
		}
	}
	
	
	
	
?>