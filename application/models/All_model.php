<?php
class All_model extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	function rp($angka){
	$angka = number_format($angka);
	$angka = str_replace(',', '.', $angka);
	$angka ="$angka";
	return "Rp.".$angka.",00";
	}

	function view($table, $select, $where=false, $join = array(), $order_by=false, $limit=false, $ex_select = true){
			$this->db->select($select, $ex_select);
			$this->db->from($table);
			if ( $where )
				$this->db->where($where);
			
			if ( $order_by )
				$this->db->order_by($order_by);
			
			if ( $join ){
				/*foreach($join as $key => $value){
					/*$exp = explode(',', $value);
					$this->db->join($key, $exp[0], $exp[1]);
					$this->db->join($join[$key]['table'], $join[$key]['field'],$join[$key]['method']);
				}*/
				foreach($join as $key => $value){
				$this->db->join($join[$key]['table'], $join[$key]['field'],$join[$key]['method']);
				}
			}
			
			if ( $limit ){
				$this->db->limit($limit);
			}
			
			$q = $this->db->get();
			if ( $q->num_rows() > 0 )
				return $q->result();
			else
				return '0';
		}
	
	function get_data($field, $table, $join = array(), $where= array(), $like = array(), $single = false, $object = false, $index_field='', $order_by='DESC', $group_by = NULL)
	{
		$this->db->select($field);
		if(count($like) > 0)
			$this->db->or_like($like);
		$this->db->from($table);
		if(count($join) > 0){
			foreach($join as $key => $value){
				$this->db->join($join[$key]['table'], $join[$key]['field'],$join[$key]['method']);
			}
		}

		if(count($where) > 0)
			$this->db->where($where); 

		if($group_by != NULL)
			$this->db->group_by($group_by);

		if($index_field !='')
			$this->db->order_by($index_field, $order_by); 

		
			$query = $this->db->get('');

		if($query->num_rows()>0){
			if($single){
				if($object)
					return $query->row();
				else
					return $query->row_array();
			}else{
				if($object)
					return $query->result();
				else
					return $query->result_array();
			}
		}else
			return false;
	}

	function query_data($query_code, $single = false)
	{
		$query = $this->db->query($query_code);
		if($query->num_rows()>0){
			if ($single) return $query->row_array();
			else return $query->result_array();
		}else
			return false;
	}
	function query_exec($query_code)
	{
		return $this->db->query($query_code);
	}
	function delete_data($table, $where)//where is an array
	{
		return $this->db->delete($table, $where);	
	}
	
	function insert_data($table, $data){
		return $this->db->insert($table,$data);
	}
	function update_data($table, $data, $where){
		$this->db->where($where); 
		return $this->db->update($table,$data);  
	}
	function update_data_in($table, $data, $where){
		$this->db->where_in($where); 
		return $this->db->update($table,$data);  
	}

}
?>