<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengeluaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session'));
		$this->load->model('all_model');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getPengeluaranCode(){
		$result = $this->all_model->query_data("SELECT IdPengeluaran FROM Pengeluaran ORDER BY IdPengeluaran DESC", true);
		
		return $result["IdPengeluaran"];
	}
	
	public function getPengeluaranCode2(){
		$result = $this->all_model->query_data("SELECT RIGHT(IdPengeluaran,4) as IdPengeluaran FROM Pengeluaran ORDER BY IdPengeluaran DESC", true);
		$result["IdPengeluaran"] = $result["IdPengeluaran"]+1;
		if($result["IdPengeluaran"] < 10)
			$result["IdPengeluaran"] = "PG0000".$result["IdPengeluaran"];
		else if($result["IdPengeluaran"]< 100)
			$result["IdPengeluaran"] = "PG000".$result["IdPengeluaran"];
		else if($result["IdPengeluaran"]< 1000)
			$result["IdPengeluaran"] = "PG00".$result["IdPengeluaran"];
		else if($result["IdPengeluaran"]< 10000)
			$result["IdPengeluaran"] = "PG0".$result["IdPengeluaran"];			
		else
			$result["IdPengeluaran"] = "PG".$result["IdPengeluaran"];
		return $result["IdPengeluaran"];
		
		//echo json_encode($result);
		//exit();
		
	}
	public function getPengeluaran()
	{	
		$nojurnal=$this->input->post('IdPengeluaran');

		$result = $this->all_model->query_data("select a.IdPengeluaran as IdPengeluaran, TanggalPengeluaran, a.Keterangan, TotalPengeluaran, b.IdExpense as IdExpense, c.Nama as NamaExpense, Nominal from Pengeluaran a join detailPengeluaran b on a.IdPengeluaran=b.IdPengeluaran join expense c on b.IdExpense=c.IdExpense where a.IdPengeluaran='".$nojurnal."' order by IdPengeluaran desc", false);
		if(!$result){
			$result= "0";
		}
		echo json_encode($result);
		exit();
	}
	public function getNamaExpense(){	
		$result = $this->all_model->query_data("select IdExpense, Nama from expense a where a.activeyn='Y' order by Idexpense asc", false);
		if(!$result){
			$result= "0";
		}	
		return $result;
	}
	
	public function getNamaExpense2($request = true){	
		$search = array();
		$join = array();
		$where = array('a.ActiveYN' => 'Y');		
		$result = $this->all_model->get_data("IdExpense as IdExpense,Nama as Nama", "expense a",$join, $where, $search,false);
		if(!$result){
			$result= "0";
		}	
		return $result;
		echo json_encode($result);		
		exit();
	}
	
	
	public function getDataNew($request = true){
		$search = array(
				
			);
		$where = array(
			
		);
		$join = array(

		);
		$result2 = $this->all_model->get_data("IdPengeluaran as IdPengeluaran,TanggalPengeluaran as TanggalPengeluaran,TotalPengeluaran as TotalPengeluaran, Keterangan as Keterangan","Pengeluaran a",$join, $where, $search, false);
		
		return $result2;
		echo json_encode($result2);		
		exit();
	}	
	
	public function getData($perPage=10, $segmen=0,  $request = true){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				'a.IdPengeluaran' => $this->input->post('search')
			);
			$where = array(
			
			);
		}else{
			$search = array(
				'a.IdPengeluaran' => $this->input->post('search')
				);
			$where = array(
			);
		}
		$join = array(
			array('table'=>'mssupplier b','field' => 'a.IdSupplier = b.IdSupplier','method'=>'Left'),
		);			
						
		$result = $this->all_model->get_data("IdPengeluaran as IdPengeluaran,a.IdSupplier as IdSupplier,TanggalPengeluaran as TanggalPengeluaran,Keterangan as Keterangan,TotalPengeluaran as TotalPengeluaran,Nama,Alamat,Telepon,Handphone,a.Status as Status", "Pengeluaran a",$join, $where, $search, false, $perPage, $segmen, false,"IdPengeluaran","DESC");
		
		$result2 = $this->all_model->get_data("IdPengeluaran as IdPengeluaran,a.IdSupplier as IdSupplier,TanggalPengeluaran as TanggalPengeluaran,Keterangan as Keterangan,TotalPengeluaran as TotalPengeluaran,Nama,Alamat,Telepon,Handphone,a.Status as Status", "Pengeluaran a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['TotalPengeluaran'] = $this->all_model->rp($result[$key]['TotalPengeluaran']);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getData(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['TotalPengeluaran'] = $this->all_model->rp($result[$key]['TotalPengeluaran']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}
	
	public function checkBarang(){

		$IdPengeluaran = $this->input->post('IdPengeluaran');
		$IdBarang = $this->input->post('IdBarang');
		$result = $this->all_model->query_data("SELECT IdBarang FROM detailPengeluaran WHERE IdPengeluaran='".$IdPengeluaran."' and IdBarang='".$IdBarang."'", true);
		
		if($result)
		{
			echo json_encode("true");
		}
		else echo json_encode("false");

		exit();
	}
	
	public function getDataDetailBarangMasuk($IdBarangMasuk){

		$result = $this->all_model->query_data("SELECT IdDetailBarangMasuk FROM detailbarangmasuk WHERE IdBarangMasuk='".$IdBarangMasuk."' order by IdDetailBarangMasuk desc", true);
		return $result["IdDetailBarangMasuk"];
		
	}
	public function getDataTransaksi($Periode,$Periode2)
	{
		$result = $this->all_model->query_data("SELECT a.IdPengeluaran as IdPengeluaran, a.TanggalPengeluaran as TanggalPengeluaran, a.Keterangan as Keterangan, TotalPengeluaran FROM pengeluaran a WHERE a.TanggalPengeluaran BETWEEN '".$Periode."' AND '".$Periode2."'", false);
		//$result = $this->all_model->query_data("SELECT a.NoAkun as NoAkun,NoOrder as NoOrder,TanggalProject as TanggalProject,NamaProject as NamaProject,BiayaProject as BiayaProject,PIC FROM project a where NoProject='".$noproject."' ", true);
		if(!$result){
			$result= "0";
		}	
		return $result;	
	}
	public function printPengeluaran()
	{
		//$noproject = $this->session->userdata('NoProject');
		//$dataproject = $this->getDataTrial();
		$Periode = $this->input->post('dob1');
		$Periode2 = $this->input->post('dob2');
		//$Periode=substr($Periode,6,10)."-".substr($Periode,0,2)."-".substr($Periode,3,2);
		//$Periode2= substr($Periode2,6,10)."-".substr($Periode2,0,2)."-".substr($Periode2,3,2);
		$result = $this->getDataTransaksi($Periode,$Periode2);
		
		$this->load->library('fpdf17/fpdf');
		
		//(potrait, satuan , ukuran kertas(lebar x tinggi)) 'P','mm',array(120,200)
		$this->fpdf->FPDF('P','mm','A4');
		$this->fpdf->Open();
		$this->fpdf->SetAutoPageBreak(false);
		$this->fpdf->AddPage();
		//$this->fpdf->Image('images/cips_logo.png',10,10,35,0,'','http://www.cips.or.id/'); 
		//$this->fpdf->Image('images/cips_nama.png',80,20,50,0,'','http://www.cips.or.id/'); 
		$this->fpdf->Ln(20);
		$this->fpdf->SetFont('Arial','BU',15);
		$this->fpdf->Cell(190,6,'Laporan Pengeluaran',0,0,'C');
		$this->fpdf->SetFont('Arial','BU',12);
		$this->fpdf->Ln(5);
		$this->fpdf->Cell(190,6,$Periode.' Sampai '.$Periode2,0,0,'C');
		
		//$this->fpdf->Cell(190,6,'Periode dari '.$tanggal1.'/'.date("M",strtotime($date1)).'/'.$tahun1.' sampai '.$tanggal2.'/'.date("M",strtotime($date2)).'/'.$tahun2.'',0,0,'C');
		//$this->fpdf->Cell(190,6,'No Project : '.$noproject.'',0,0,'C');		
		$this->fpdf->Ln(15);

		$y_axis_initial = 50;
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->setFillColor(222,222,222);
		$this->fpdf->SetY($y_axis_initial);
		$this->fpdf->SetX(30);
		//Header tabel halaman 1
		$this->fpdf->CELL(25,6,'IdPengeluaran',1,0,'C',1);
		$this->fpdf->CELL(40,6,'TanggalPengeluaran',1,0,'C',1);
		$this->fpdf->Cell(40,6,'Keterangan',1,0,'C',1);
		$this->fpdf->Cell(40,6,'TotalPengeluaran',1,0,'C',1);
		//$this->fpdf->Cell(35,6,'Insentif',1,0,'C',1);

		$this->fpdf->Ln();
		$max=25;//max baris perhalaman
		$i=0;
		$no=0;
		$totala=0;
		$totalb=0;
		$row_height = 6;//tinggi tiap2 cell/baris
		$y_axis = $y_axis_initial + $row_height;
		$date = date("Y-m-d");
		$grandtotal = 0;
		if($result != "0")
		{
		foreach($result as $key => $value){
		$i++;
		//$total += $row['Total'];

		if ($i == $max){               //jika $i=25 maka buat header baru seperti di atas
			$this->fpdf->AddPage();
			$this->fpdf->SetY(10);
			$this->fpdf->SetX(10);
			$this->fpdf->CELL(25,6,'IdPengeluaran',1,0,'C',1);
			$this->fpdf->CELL(40,6,'TanggalPengeluaran',1,0,'C',1);
			$this->fpdf->Cell(40,6,'Keterangan',1,0,'C',1);
			$this->fpdf->Cell(40,6,'TotalPengeluaran',1,0,'C',1);

			$this->fpdf->SetY(10);
			$this->fpdf->SetX(55);
			$y_axis = $y_axis + $row_height;
			$i=0;
			$this->fpdf->Ln();

		}

		$grandtotal+=$result[$key]["TotalPengeluaran"];		
		
		$i++;
		$no++;
		$this->fpdf->SetX(30);
		//$this->fpdf->Cell(10,6,$no,1,0,'C',0);
		$this->fpdf->Cell(25,6,$result[$key]["IdPengeluaran"],1,0,'C',0);
		$this->fpdf->Cell(40,6,$result[$key]["TanggalPengeluaran"],1,0,'C',0);
		$this->fpdf->Cell(40,6,$result[$key]["Keterangan"],1,0,'C',0);
		$this->fpdf->Cell(40,6,$this->all_model->rp($result[$key]["TotalPengeluaran"]),1,0,'R',0);
		//$this->fpdf->Cell(25,6,$this->all_model->rp($saldo),1,0,'R',0);
		//$this->fpdf->Cell(35,6,$this->all_model->rp($result[$key]["Insentif"]),1,0,'C',0);
		

		$this->fpdf->Ln();

		}
		}
		$this->fpdf->SetX(30);

		//buat footer
		$now = date("d F Y");
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(275,6,"Total Pengeluaran : ".$this->all_model->rp($grandtotal),0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Output('Laporan Pengeluaran'.date("F Y").'.pdf', 'I');
		
	}
	public function getDataDetailPengeluaran($perPage=100, $segmen=0,  $request = true,$IdPengeluaran){
		$menu = $this->input->post('menu');
		$current = $this->input->post('page');
		if($current=="")
			$current = 1;
		else
			 $segmen = ($current-1) * $perPage;
		if($menu!=""){
			$search = array(
				//'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdPengeluaran' => $IdPengeluaran
			);
		}else{
			$search = array(
				//'a.Nama' => $this->input->post('search')
			);
			$where = array(
				'a.IdPengeluaran' => $IdPengeluaran
			);
		}
		$join = array(
			array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),	
			array('table'=>'satuan c','field' => 'b.IdSatuan = c.IdSatuan','method'=>'Left'),		
		);			
						
		$result = $this->all_model->get_data("IdDetailPengeluaran as IdDetailPengeluaran,IdPengeluaran as IdPengeluaran,a.IdBarang as IdBarang,Nama as Nama,a.Jumlah as Jumlah,a.Harga as Harga,Satuan as Satuan", "detailPengeluaran a",$join, $where, $search, false, $perPage, $segmen, false,"IdDetailPengeluaran","ASC");
		
		$result2 = $this->all_model->get_data("IdDetailPengeluaran as IdDetailPengeluaran,IdPengeluaran as IdPengeluaran,Nama as Nama,a.Jumlah as Jumlah,a.Harga as Harga,Satuan as Satuan", "detailPengeluaran a",$join, $where, $search, false);
		
		if($menu!="")$result = $result2;
		//var_dump( $result);
		if($request == false){
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailPengeluaran(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Harga1'] = $this->all_model->rp($result[$key]['Harga']);
				}
			}
			return $result;
		}else{
			if(!$result){
				$result= "No Data";
			}else{
				$result[0]["Link"] = "";
				if($menu==""){
					$result[0]["Search"] = "";
					$total = count($result2);
					$page = ceil($total/$perPage);
					if($this->input->post('search')!="")	
						$result[0]["Search"] = $this->input->post('search');
					
					for($i=1;$i<=$page;$i++){
						if($i==$current)
						$result[0]["Link"] .= "<span style=@@@color:white@@@>$i</span>&nbsp;&nbsp;";
						else
						$result[0]["Link"] .= "<a style=@@@cursor:pointer@@@ onclick=@@@getDataDetailPengeluaran(~~~$menu~~~,$i)@@@>$i</a>&nbsp;&nbsp;";
					}
				}
				foreach($result as $key => $val){
					$result[$key]['Harga1'] = $this->all_model->rp($result[$key]['Harga']);
				}
			}
			echo json_encode($result);
			exit();
		}
	}

	public function getDataPengeluaran($idPengeluaran){
		$totalPengeluaran = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.IdPengeluaran' => $idPengeluaran
		);
		
		$join = array(
			array('table'=>'expense b','field' => 'a.IdExpense = b.IdExpense','method'=>'Left'),		
		);	
	
		$result = $this->all_model->get_data("IdDetailPengeluaran as IdDetailPengeluaran,IdPengeluaran as IdPengeluaran,a.Jumlah as Jumlah,a.IdBarang as IdBarang,a.Harga as Harga,b.Nama as Nama", "detailPengeluaran a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				//$totalPengeluaran += ($result[$key]['Harga']*$result[$key]['Jumlah']);
			}
			return $result;
		}
		else
			return "No Data";
		
		
	
	}
	
	
	public function calculatePengeluaran($idPengeluaran){
	
		$totalPengeluaran = 0;
		
		$search = array(
			//'a.Nama' => $this->input->post('search')
		);
		$where = array(
			'a.IdPengeluaran' => $idPengeluaran
		);
		
		$join = array(
				
		);	
	
		$result = $this->all_model->get_data("IdDetailPengeluaran as IdDetailPengeluaran,IdPengeluaran as IdPengeluaran,a.Jumlah as Jumlah,a.Harga as Harga", "detailPengeluaran a",$join, $where, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
				$totalPengeluaran += ($result[$key]['Harga']*$result[$key]['Jumlah']);
			}
		}
		
		$data = array(	
		'TotalPengeluaran' => $totalPengeluaran,	
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
			
		$where = array(	
					'IdPengeluaran'=> $idPengeluaran,
				
		);	
		$query = $this->all_model->update_data("Pengeluaran", $data ,$where);
		
		
	}
	
	public function insertPengeluaran(){
		
		$data = array(
		'IdPengeluaran'  => $this->getPengeluaranCode2(),				
		'TotalPengeluaran' => $this->input->post('TotalPengeluaran'),	
		'Keterangan' 	=> $this->input->post('Keterangan'),
		'TanggalPengeluaran' => $this->input->post('TanggalTransaksi'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("Pengeluaran", $data );
		
		echo json_encode($query);
		exit();
	}	
	
	public function insertDetailPengeluaran(){
		$id=$this->input->post('IdPengeluaran');
		if($id=="-")
		{
			$id=$this->getPengeluaranCode();
		}
		$data = array(
		'IdPengeluaran'  => $id,
		'IdExpense' => $this->input->post('IdExpense'),		
		'Nominal' => $this->input->post('Nominal'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$query = $this->all_model->insert_data("detailpengeluaran", $data );
		//$this->calculatePengeluaran($this->input->post('IdPengeluaran'));
		
		echo json_encode($query);
		exit();
	}	
	
	public function editPengeluaran(){
		$data = array(		
		'TotalPengeluaran' => $this->input->post('TotalPengeluaran'),
		'Keterangan' 	=> $this->input->post('Keterangan'),
		'TanggalPengeluaran' => $this->input->post('TanggalTransaksi'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdPengeluaran'			=> $this->input->post('IdPengeluaran'),
				
		);	
		$query = $this->all_model->update_data("Pengeluaran", $data ,$where);
		
		echo json_encode($query);
		exit();
	}	
	
	public function editDetailPengeluaran(){
		$data = array(
		'IdPengeluaran'  => $this->input->post('IdPengeluaran'),
		'IdExpense' => $this->input->post('IdExpense'),		
		'Nominal' => $this->input->post('Nominal'),
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdDetailPengeluaran'=> $this->input->post('IdDetailPengeluaran'),
				
		);	
		$query = $this->all_model->update_data("detailPengeluaran", $data ,$where);
		$this->calculatePengeluaran($this->input->post('IdPengeluaran'));
		echo json_encode($query);
		exit();
	}

	public function masukBarang(){
		$data = array(
		'Status' => '1',		
		'UpdateId' => $this->session->userdata("UserId"),
		'UpdateTime' => date("Y-m-d H:i:s")
			);
		$where = array(	
					'IdPengeluaran'=> $this->input->post('IdPengeluaran'),
				
		);	
		$query = $this->all_model->update_data("Pengeluaran", $data ,$where);
		
		
		$data4 = array(
				'Keterangan' => 'Pengeluaran '.$this->input->post('IdPengeluaran'),
				'UpdateID' => $this->session->userdata("UserId"),
				'TanggalMasuk' => date("Y-m-d H:i:s"),
				'Tag' => 'Pengeluaran',
				'Reference' => $this->input->post('IdPengeluaran')
			);
		$this->all_model->insert_data("barangmasuk", $data4);	
		
		$result2 = $this->all_model->query_data("SELECT IdBarangMasuk FROM barangmasuk where Reference='".$this->input->post('IdPengeluaran')."'", true);
		$idbarangmasuk = $result2["IdBarangMasuk"];
		
		$search = array(
			
		);
		$where1 = array(
			'a.IdPengeluaran' => $this->input->post('IdPengeluaran')
		);
		
		$join = array(
			array('table'=>'barang b','field' => 'a.IdBarang = b.IdBarang','method'=>'Left'),		
		);	
	
		$result = $this->all_model->get_data("IdDetailPengeluaran as IdDetailPengeluaran,IdPengeluaran as IdPengeluaran,a.Jumlah as Jumlah,a.IdBarang as IdBarang,a.Harga as Harga,b.Nama as Nama,b.Quantity as Quantity,b.Jumlah as JumlahBarang", "detailPengeluaran a",$join, $where1, $search, false);
		
		if($result)
		{
			foreach($result as $key => $val){
			
					$quantitybarang = $result[$key]['Quantity'] + $result[$key]['Jumlah'];
					$totalbarang = $result[$key]['Harga'] + $result[$key]['JumlahBarang'];
					
					$data3 = array(	
					'Quantity' => $quantitybarang,
					'Jumlah' => $totalbarang,	
					'UpdateId' => $this->session->userdata("UserId"),
					'UpdateTime' => date("Y-m-d H:i:s")
						);
					$where3 = array(	
								'IdBarang'=> $result[$key]['IdBarang'],
							
					);	
					$this->all_model->update_data("barang", $data3 ,$where3);
					
					$data5 = array(
							'IdBarangMasuk'  => $idbarangmasuk,
							'IdBarang'  => $result[$key]['IdBarang'],
							'Nama' => $result[$key]['Nama'],		
							'Quantity' => $result[$key]['Jumlah'],	
							'Jumlah' => $result[$key]['Harga'],
							'Keterangan' => 'Barang Masuk Pengeluaran '.$this->input->post('IdPengeluaran'),
							'Status' => 'N',
						);
					$this->all_model->insert_data("detailbarangmasuk", $data5);
					
					
					$IdDetailBarangMasuk = $this->getDataDetailBarangMasuk($idbarangmasuk);
					
		
					$data6 = array(
							'IdDetailBarangMasuk'  => $IdDetailBarangMasuk,
							'IdBarang'  => $result[$key]['IdBarang'],	
							'SaldoQty' => $quantitybarang,	
							'SaldoJumlah' => $totalbarang,
							'Tag' => 'Pengeluaran',
							'Tanggal' => date("Y-m-d H:i:s"),
						);
					$this->all_model->insert_data("historybar", $data6);					
					
					

			}
			
		}
		
	
		echo json_encode($query);
		exit();
	}	
	
	public function deletePengeluaran()
	{
		$IdPengeluaran = $this->input->post('IdPengeluaran');
		$where = array('IdPengeluaran'=>$IdPengeluaran);
		$query = $this->all_model->delete_data("Pengeluaran", $where);
		
		echo json_encode($query);
		exit();
	}
	
	public function deleteDetailPengeluaran()
	{
		$IdPengeluaran = $this->input->post('IdPengeluaran');
		$where = array('IdPengeluaran'=>$IdPengeluaran);
		$query = $this->all_model->delete_data("detailPengeluaran", $where);

		echo json_encode($query);
		exit();
	}
		
	public function getDetailPengeluaran($IdPengeluaran)
	{
		$result = $this->all_model->query_data("SELECT * FROM Pengeluaran a where a.IdPengeluaran='".$IdPengeluaran."'", true);
		
		if(!$result){
			$result= "0";
		}	
		return $result;
	
	}	
	
	public function index(){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "login","refresh");
		$config['per_page'] = 10; 
		$config['segmen'] = 0;
		$data['title']="Pengeluaran";
		$data['page']="pengeluaran_view";
		$data['nama']=$this->session->userdata('Nama');
		$data['data'] = json_encode($this->getDataNew(false));
		$data['expense'] = json_encode($this->getNamaExpense2(false));
		$this->load->view('main',$data);
	}
	
	public function detailPengeluaran($IdPengeluaran){
	if($this->session->userdata("UserId")=="")
		redirect(base_url(). "home","refresh");
		$config['per_page'] = 100; 
		$config['segmen'] = 0;
		$data['title']="CIPS";
		$data['page_title']="CIPS - Detail Pengeluaran";
		$data2['data'] = json_encode($this->getDataDetailPengeluaran($config['per_page'], $config['segmen'],false,$IdPengeluaran ));
		$data2['data2'] = json_encode($this->getDetailPengeluaran($IdPengeluaran));
		$data['include']=$this->load->view('script','',true);
		$this->load->view('home_header',$data);
		$this->load->view('detailPengeluaran_view',$data2);
		$this->load->view('home_footer');
	}
	
	private function no_cache(){
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
}


